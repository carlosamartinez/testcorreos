<?php

namespace App\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Environment;
use App\Entity\User;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Funciones de ayuda en la aplicacion
 * @author Pablo bravo [Waplicaciones SAS]
 * @author p.trivino@waplicaciones.co
 * @since 5.0
*/
class Mail {

    private $em;
    private $mailer;
    private $twig;
    private $params;
  /**
   * Inicializa objetos de la clase.
   * @param EntityManagerInterface $entityManager Conexión a Base de datos.
   * @author Julian Restrepo<j.restrepo@waplicaciones.co>
  */
  public function __construct(\Swift_Mailer $mailer,EntityManagerInterface $entityManager,Environment $twig, ParameterBagInterface $params) {

        $this->em = $entityManager;
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->params = $params;
  }

    /**
    * Enviá correo de confirmación de cuenta al usuario nuevo
    *
    * @param string $pass Contraseña del usuario
    * @param string $email Correo electrónico del usuario
    * @param string $usuario número de identificación del usuario
    * @param string $path Ruta del proyecto
    * 
    * @return bool
    */
    public function sendMail(string $pass, $email,string $usuario,string $nombre ){
        $transport = (new \Swift_SmtpTransport($this->params->get('conectorEmail'), $this->params->get('puertoEmail'), $this->params->get('seguridadSSLEmail')))
            ->setStreamOptions(array('ssl' => array(
                'verify_peer' => false,
                'allow_self_signed' => true
            ))
        );
        $mailer = new \Swift_Mailer($transport);
        $message = new \Swift_Message();
        $message->setSubject('📨COOPIDORGAS CORREOS: 🗝Datos de usuario');
        $message->setFrom(array($this->params->get('emailPredeterminado') => $this->params->get('emailNombre')));
        $imageLogo = $message->embed(\Swift_Image::fromPath('images/logo-correos.png'));
        $message->setTo($email)
            ->setBody($this->twig->render('emails/confirmAccount.html.twig',[ "nombre" => $nombre, "usuario" => $usuario , "pass" => $pass, "headerImg" => $imageLogo ]), 'text/html');
        
        $res = $mailer->send($message);
        if($res){
            return new Response(true, Response::HTTP_OK);
        }
        return false;
    }
    
    /**
    * Enviá un correo de confirmación de cambio de clave
    *
    * @param string $pass Contraseña del usuario
    * @param string $email Correo electrónico del usuario
    * @param string $nombre Nombre del usuarioq 1|
    * @param string $tokenLifetime tiempo de vida del token de cambio de clave
    * @param string $resetToken token de cambio de clave
    * @param string $path ruta del proyecto
    * 
    * @return bool
    */
    public function forgotPassword(string $email, string $nombre, $resetToken, $tokenLifetime, string $path ){
        $transport = (new \Swift_SmtpTransport($this->params->get('conectorEmail'), $this->params->get('puertoEmail'), $this->params->get('seguridadSSLEmail')))
            ->setStreamOptions(array('ssl' => array(
                'verify_peer' => false,
                'allow_self_signed' => true
            ))
        );
        $mailer = new \Swift_Mailer($transport);
        $message = new \Swift_Message();
        $message->setSubject('📨CORREOS MASIVOS:🗝Recuperación de contraseña');
        $message->setFrom(array($this->params->get('emailPredeterminado') => $this->params->get('emailNombre')));
        $imageLogo = $message->embed(\Swift_Image::fromPath('images/logo-correos.png'));
        $message->setTo($email)
            ->setBody($this->twig->render('reset_password/email.html.twig',[
                'nombre' => $nombre,
                'resetToken' => $resetToken,
                'headerImg' => $imageLogo,
                'tokenLifetime' => $tokenLifetime]), 'text/html');
        $res = $mailer->send($message);
        if($res){
            return new Response(true, Response::HTTP_OK);
        }
        return false;
    }
}