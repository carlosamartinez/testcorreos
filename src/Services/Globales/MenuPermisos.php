<?php

namespace App\Services\Globales;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Funciones de ayuda en la aplicacion
 * @author Julian Restrepo [Waplicaciones SAS]
 * @author j.restrepo@waplicaciones.co
 * @since 5.0
*/
class MenuPermisos {

  private $em;

  /**
   * Inicializa objetos de la clase.
   * @param EntityManagerInterface $entityManager Conexión a Base de datos.
   * @author Julian Restrepo<j.restrepo@waplicaciones.co>
  */
  public function __construct(EntityManagerInterface $entityManager) {
    $this->entityManager = $entityManager;
  }

  public function getMenu($sRole, $idUser){
    
    // Data Menu
    $qrMenu = $this->entityManager->createQuery("SELECT mn.id, mn.posicion, mn.label, mn.modulo, mn.vista, mn.pathName, mn.icono
      FROM App\Entity\Menu mn
      WHERE mn.rol = '{$sRole}' AND mn.estado = 1 ORDER BY mn.posicion ASC"
    );
    $aDataMenu = $qrMenu->getScalarResult();
    
    $aMenu = array();
    $sPathGenerate = '';
    if( count( $aDataMenu ) > 0 ){

      foreach( $aDataMenu as $aItem ){

        $idMenu = $aItem['id'];
        $sModulo = $aItem['modulo'];
        $sVista = ($aItem['vista'] == null) ? 0 : $aItem['vista'];

        if( !isset($aMenu[$sModulo][$idMenu]) ){
          $aPermisos = $this->getPermisos($idMenu, $idUser);
          $aMenu[$sModulo][$sVista] = array(
            'idMenu' => $idMenu,
            'posicion' => $aItem['posicion'],
            'label' => $aItem['label'],
            'pathName' => $aItem['pathName'],
            'icono' => $aItem['icono'],
            'permisos' => $aPermisos
          );
          if( $sPathGenerate == '' && isset($aPermisos['ver']) && $aPermisos['ver']['permiso'] == 1) $sPathGenerate = $aItem['pathName'];
        }
      }
      return array( 'pathDefault' => $sPathGenerate, 'menu' => $aMenu);
    }else{
      return null;
    }
  }

  public function getPermisos($idMenu, $idUser){

    // DQL
    $qrMenuPermisos = $this->entityManager->createQuery("SELECT acc.id, acc.accion, acc.label, acc.iconos, accp.permiso
      FROM App\Entity\Acciones acc
      LEFT JOIN App\Entity\MenuPermisos accp WITH accp.idUser = {$idUser} AND acc.id = accp.idAccion 
      WHERE acc.idMenu = $idMenu ORDER BY acc.posicion ASC"
    );
    $aDataPermisos = $qrMenuPermisos->getScalarResult();
    $aPermisos = array();
    if( count($aDataPermisos) > 0  ){
      foreach( $aDataPermisos as $aPermiso ){
        $sKey = $aPermiso['accion'];
        $aPermisos[$sKey] = array(
          'idAccion' => $aPermiso['id'],
          'label' => $aPermiso['label'],
          'iconos' => json_decode($aPermiso['iconos']),
          'permiso' => $aPermiso['permiso']
        );
      }
    }
    return $aPermisos;
  }

  // Valida si tiene acceso a la vista
  public function validarAccesoVista( $session, $sModulo, $sVista){

    if( empty($session) || empty($sModulo) || empty($sVista) ){
      //dump($session, '1' );exit();
      throw new \Exception('Los parámetros no pueden estar vacíos.!');
    }
    // Obtener Menu
    $aMenu = $session->get('menuPermisos');
    if( !isset($aMenu[$sModulo]) ){
      throw new \Exception('El módulo al que trata de acceder no está disponible.!');
      return "false";
    }
    if( !isset($aMenu[$sModulo][$sVista]) ){
      //dump($session, '3' );exit();
      throw new \Exception('La vista a la que trata de acceder no está disponible.!');
    }
    if( !isset($aMenu[$sModulo][$sVista]['permisos']['ver']['permiso']) || $aMenu[$sModulo][$sVista]['permisos']['ver']['permiso'] == null  ){
      //dump($session, '4' );exit();
      throw new \Exception('Usted no posee permisos para acceder. ');
    }
  }

  // Valida si tiene acceso a una accion 
  public function getAccesoVistaAccion( $session, $sModulo, $sVista, $sAccion){

    if( empty($session) || empty($sModulo) || empty($sVista) ) throw new \Exception('Los parámetros no pueden estar vacíos.!');
    // Obtener Menu
    $bPermiso = false;
    $aMenu = $session->get('menuPermisos');
    if( !isset($aMenu[$sModulo]) ) throw new \Exception('El módulo al que trata de acceder no está disponible.!');
    if( !isset($aMenu[$sModulo][$sVista]) ) throw new \Exception('La vista a la que trata de acceder no está disponible.!');

    if(
      ( isset($aMenu[$sModulo][$sVista]['permisos']['ver']) && $aMenu[$sModulo][$sVista]['permisos'][$sAccion]) &&
      ($aMenu[$sModulo][$sVista]['permisos']['ver']['permiso'] == 1 && $aMenu[$sModulo][$sVista]['permisos'][$sAccion]['permiso'] == 1)
    ) $bPermiso = true;
    return $bPermiso;
  }

  // Obtiene los permisos del modulo
  public function getPermisosModuloVista( $session, $sModulo, $sVista){

    if( empty($session) || empty($sModulo) || empty($sVista) ) throw new \Exception('Los parámetros no pueden estar vacíos.!');

    $aMenu = $session->get('menuPermisos');
    $aPermisos = null;
    if( !isset($aMenu[$sModulo]) || !isset($aMenu[$sModulo][$sVista])) $aPermisos = null;
    else $aPermisos = $aMenu[$sModulo][$sVista]['permisos'];
    return $aPermisos;
  }

  public function getGridButtons( $sMenuModulo , $session , $aPermisos ){

    if( empty($aPermisos) || empty($session) || is_null($session) )return array();
    $em = $this->entityManager;
    $icons = $em->createQuery("SELECT a.label , a.iconos , a.accion
    FROM App\Entity\MenuPermisos mp 
    JOIN App\Entity\User u WITH u.id = mp.idUser
    JOIN App\Entity\Acciones a WITH mp.idAccion = a.id
    JOIN App\Entity\Menu m WITH m.id = a.idMenu
    WHERE m.modulo = '{$sMenuModulo}' AND mp.idUser = " . $session->get("id"))->getResult();
    $aButtonsGrid = [];
    $modificar_permisos = false;
    foreach($icons as $key => $element){

        if($element["accion"] == "modificar_permisos")$modificar_permisos = true;

        if($element["accion"] == "ver" || $element["accion"] ==  "modificar_permisos")continue;
        $aButtonsGrid[$element["accion"]] =[
            'id' => $element["accion"],
            'text' => "",
            'title' => $element['label'],
            'icon' => $element["iconos"],
            'class' => array('hover-success')
        ];
    }
    return ["iconos" => $aButtonsGrid, "modificar_permisos" => $modificar_permisos];

    $aButtonsGrid = array(
      'crear' => array(
        'id' => 'nuevo-registro',
        'text' => '',
        'title' => 'Nuevo Registro',
        'icon' => array('fas fa-plus'),
        'class' => array('hover-success'),
      ),
      'editar' => array(
        'id' => 'editar-registro',
        'text' => '',
        'title' => 'Editar Registro',
        'icon' => array('fas fa-edit'),
        'class' => array('hover-warning'),
      ),
      'eliminar' => array(
        'id' => 'eliminar-registro',
        'text' => '',
        'title' => 'Eliminar Registro',
        'icon' => array('far fa-trash-alt'),
        'class' => array('hover-danger'),
      ),
      'descargar_csv' => array(
        'id' => 'descargar-csv',
        'text' => '',
        'title' => 'Descargar informe',
        'icon' => array('fas fa-download text-primary', 'fas fa-file-csv'),
        'class' => array('hover-success'),
      ),
      'cargar_imagenes' => array(
        'id' => 'cargar-imagenes',
        'text' => '',
        'title' => 'Cargar Imagenes',
        'icon' => array('fas fa-upload text-primary'),
        'class' => array('hover-success'),
      ),
      'cargar_masivo' => array(
        'id' => 'cargar-masivo',
        'text' => '',
        'title' => 'Carga Masiva',
        'icon' => array('fas fa-upload text-primary', 'fas fa-file-csv'),
        'class' => array('hover-success'),
      ),
      'agregar_destinatarios' => array(
        'id' => 'agregar-destinatarios',
        'text' => '',
        'title' => 'Agregar Destinatarios',
        'icon' => array('far fa-eye text-info text-primary', 'far fa-images text-success'),
        'class' => array('hover-success'),
      ),
      'cargar_plano' => array(
        'id' => 'cargar-plano',
        'text' => '',
        'title' => 'Cargar Plano',
        'icon' => array('fas fa-upload text-primary'),
        'class' => array('hover-success'),
      ),

    );
    

    $aGridButtons = array();
    foreach( $aPermisos as $key => $aPermiso ){
      if( isset($aButtonsGrid[$key]) && $aPermiso['permiso'] == 1 ){
        $aGridButtons[] = $aButtonsGrid[$key];
      }
    }
  }

}