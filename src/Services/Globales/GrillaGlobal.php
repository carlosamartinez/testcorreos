<?php

namespace App\Services\Globales;

use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Waplicaciones SAS (class GeneradorExcel)
 * 
 * Procesos adicionales que se ejecutan en la grilla
 * @author Anderson Barbosa [Waplicaciones SAS]
 * @author a.barbosa@waplicaciones.co
 *
 * @package 7000002259
 */
class GrillaGlobal {

    private $request;
    private $session;
    private $aCondiciones;

    public function __construct(RequestStack $requestStack) {
        $this->request = $requestStack->getCurrentRequest();
        $this->session = $this->request->getSession();

        // Variables Globales
        $this->aCondiciones = array(
            'eq' => " = ", // Igual
            'ne' => " <> ", // No igual a
            'lt' => " < ", // Menor que
            'le' => " <= ", // Menor o igual que
            'gt' => " > ", // Mayor que
            'ge' => " >= ", // Mayor o igual a
            'nu' => " IS NULL ", // Is null
            'nn' => " IS NOT NULL ", // Is not null
            'bw' => " LIKE ", // Empiece por
            'bn' => " NOT LIKE ", // No empiece por
            'ew' => " LIKE ", // Termina por
            'en' => " NOT LIKE ", // No termina por
            'cn' => " LIKE ", // Contiene
            'nc' => " NOT LIKE " // No Contiene
        );
    }

    /**
     * Realiza el proceso para generar las condiciones en las consultas de la grilla
     *
     * @param array $equivalenciasColumnas Columnas que se realizan filtro
     * @param string $sFiltrosIniciales Condiciones estáticas para la consulta
     * @param string $sIniOrder Columna(s) para ordenar primero
     * @param array $aNoColumnas Columnas que no aparezcan en la consulta
     * @param array $aHaving Campos que cuentan con algun procedimiento y es necesario implementarlo en el Having
     * @return array
     */
    public function realizarFiltro($equivalenciasColumnas, $sFiltrosIniciales = "", $sIniOrder = "", $aNoColumnas = array(), $aHaving = array()){
        /**
        * @autor Anderson Barbosa
        * @date 2018-06-06
        */

        // Variables Petición "GET"
        $aGet_TipoBusqueda = $this->request->query->get('filtroTipoBusqueda', "AND", true);
        $aGet_OrdenTipo = $this->request->query->get('sord', ["DESC"], true);
        $aGet_OrdenCampo = $this->request->query->get('sidx', ['id'], true);
        $aGet_Filtros = $this->request->query->get('filter', '', true);
        $aGet_OperadoresFiltros = $this->request->query->get('filterOperator', '', true);
        $aGet_ValorFiltros = $this->request->query->get('filterValue', '', true);
        $nGet_Rows = $this->request->get('rows');
        $nGet_Pagina = $this->request->get('page');

        // --------------------------- Lógica Tabla --------------------------- //
        // Paginación
        $nPaginacion = ($nGet_Pagina * $nGet_Rows) - $nGet_Rows;

        // Realizar Filtros
        $sSql_Hv_Filtrar = ""; // SQL Having
        $sSql_Wh_Filtrar = $sFiltrosIniciales; // SQL Where
        $arrayFiltrosValores = array(); // Parametros de la consulta
        if (is_array($aGet_Filtros) && count($aGet_Filtros) == count($aGet_ValorFiltros)) {
            $sFiltrosWhere = ""; // Filtros generado por la grilla
            $sFiltrosHaving = ""; // Filtros generado por la grilla
            for ($i = 0; $i < count($aGet_Filtros); ++$i) {
                // Variables para filtro
                $sFiltrarPor = $aGet_Filtros[$i]; // Grilla - Campo para filtrar
                $sOperador = $aGet_OperadoresFiltros[$i]; // Grilla - Código operador
                $valorFiltro = $aGet_ValorFiltros[$i]; // Grilla - Campo Valor 
                $sSqlCampo = (isset($equivalenciasColumnas[$sFiltrarPor])) ? $equivalenciasColumnas[$sFiltrarPor] : null; // Campo Sql para filtrar

                if(!empty($sSqlCampo)){

                    // Asignamos variable a implementar SQL
                    if(!in_array($sFiltrarPor, $aHaving)){
                        $sFiltro = &$sFiltrosWhere;
                    }else{
                        $sFiltro = &$sFiltrosHaving;
                    }

                    // Iniciamos condicionales
                    $sFiltro .= ( !empty($sFiltro) ) ? " " . $aGet_TipoBusqueda . " " : " "; // AND o OR

                    // Un campo puede contener múltiples valores, Ej. "0|null", casos en combos
                    $aValoresFiltros = explode("|", $valorFiltro);
                    $nCantidadFiltros = count($aValoresFiltros) - 1;
                    foreach($aValoresFiltros as $j => $sValorFiltro){
                        // Inidice
                        $nIndiceValor = $sFiltrarPor . "_" . $i . "_" . $j;

                        // Guardamos Resultados
                        switch($sOperador){
                            case 'bw': case 'bn':
                                $arrayFiltrosValores[$nIndiceValor] = $sValorFiltro."%";
                                break;
                            case 'ew': case 'en':
                                $arrayFiltrosValores[$nIndiceValor] = "%".$sValorFiltro;
                                break;
                            case 'cn': case 'nc':
                                $arrayFiltrosValores[$nIndiceValor] = "%".$sValorFiltro."%";
                                break;
                            case 'nu': case "nn":

                                break;
                            default:
                                $arrayFiltrosValores[$nIndiceValor] = $sValorFiltro;
                                break;
                        }


                        // Implementamos SQL
                        $sFiltro .= ( $j === 0 ) ? " ( " : " OR "; // Comienza Campo
                        $sFiltro .= " " . $sSqlCampo . " "; // Campo SQL
                        $sFiltro .= " " . $this->aCondiciones[$sOperador] . " "; // Operador SQL
                        $sFiltro .= ( isset($arrayFiltrosValores[$nIndiceValor]) ) ? " :{$nIndiceValor} " : ""; // VALOR A FILTRAR
                        $sFiltro .= ( $j === $nCantidadFiltros ) ? " ) " : ""; // Finaliza Campo
                    }
                }
            }
            // Unificamos Condiciones
            if($sFiltrosWhere != ""){
                $sSql_Wh_Filtrar .= ( (!empty($sSql_Wh_Filtrar)) ? " AND " : "" ) . " ({$sFiltrosWhere}) ";
            }
            $sSql_Hv_Filtrar .= $sFiltrosHaving;
        }

        // Validamos Condiciones
        $sSql_Wh_Filtrar = (!empty($sSql_Wh_Filtrar)) ? $sSql_Wh_Filtrar : " 1 = 1";

        // Obetener ordenamiento por columnas
        $orderByCols = $sIniOrder;
        foreach($aGet_OrdenCampo as $nIndice => $sGrillaCampo){
            if(isset($equivalenciasColumnas[$sGrillaCampo])){
                $orderByCols .= ((!empty($orderByCols)) ? ', ' : " " ) . $equivalenciasColumnas[$sGrillaCampo] . ' ' . $aGet_OrdenTipo[$nIndice];
            }
        }

        // Columnas por equivalenciasColumnas
        $sColumnas = "";
        foreach($equivalenciasColumnas as $sColumna => $sCampo){
            if(!in_array($sColumna, $aNoColumnas)){
                $sColumnas .= ( (!empty($sColumnas)) ? ", " : "" ) . "{$sCampo} AS {$sColumna}";
            }
        }

        return array(
            "paginacion" => $nPaginacion,
            "paginaActual" => $nGet_Pagina,
            "maximoFilas" => $nGet_Rows,
            "order" => $orderByCols,
            "columnas" => $sColumnas,
            "valoresWhere" => $arrayFiltrosValores,
            "where" => $sSql_Wh_Filtrar,
            "having" => $sSql_Hv_Filtrar
        );
    }
}