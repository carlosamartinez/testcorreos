<?php

namespace App\Services;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\RegistroActividadAdmin;

/**
 * Funciones de ayuda en la aplicacion
 * @author Julian Restrepo [Waplicaciones SAS]
 * @author j.restrepo@waplicaciones.co
 * @since 5.0
*/
class GruposGeneral {

  private $em;

  /**
   * Inicializa objetos de la clase.
   * @param EntityManagerInterface $entityManager Conexión a Base de datos.
   * @author Julian Restrepo<j.restrepo@waplicaciones.co>
  */
  public function __construct(EntityManagerInterface $entityManager) {
    $this->em = $entityManager;
  }

  /**
   * Realiza el proceso de insercion de destinatarios en la tabla envio_integrante
   *
   * @param int $idEnvio
   * @param int $idGrupo
   * @return string
  */
  public function setEnvioIntegrante($idEnvio, $idGrupo){
    
    $em = $this->em;
    $conexion = $em->getConnection();

    // ------ Se buscan los registros de la tabla envio_integrante ------//
    $sqlEnvioIntegrante = "SELECT ei.integrante_grupo_id FROM envio_integrante ei  WHERE ei.envio_id=".$idEnvio;
    $envioIntegrante = $conexion->query($sqlEnvioIntegrante);

    $arrayEnvioIntegrante = array();
    foreach ($envioIntegrante as $key => $value) {
      $arrayEnvioIntegrante[$value['integrante_grupo_id']]=$value['integrante_grupo_id'];
    }

    //---------------//


    //------Se buscan los integrantes del grupo seleccionado ---------//
    $sql="SELECT ig.id FROM integrante_grupo ig WHERE ig.grupo_id=".$idGrupo." AND ig.activo =1";
    $integrantesGrupo = $conexion->query($sql);

    $arrayIntegranteGrupo = array();
    foreach($integrantesGrupo as $integrante){
      $arrayIntegranteGrupo[$integrante['id']] = $integrante['id'];
    }
    //--------------------//


    $faltantes = array_diff($arrayIntegranteGrupo, $arrayEnvioIntegrante);



    $sqlInsert = "INSERT INTO envio_integrante (envio_id, integrante_grupo_id, enviado, reenviado, fecha_envio, fecha_reenvio, envio_error_id) VALUES ";
    $values ="";
    $cont = 0;

    foreach($faltantes as $integrante){

      if($values != ""){
        $values .= ", ";
      }

      $values .= "('".$idEnvio."','".$integrante."','0','0',NULL,NULL,NULL)";

      $cont++;

      if($cont >= 100){
        $conexion->query($sqlInsert.$values);
        $values="";
        $cont=0;

      }


    }

    if($values != ""){
      $conexion->query($sqlInsert.$values);
      $values="";
      $cont=0;
    }

  }


  /**
   * Realiza el proceso de eliminacion de registros de la tabla envio_integrante
   *
   * @param int $idEnvio
   * @param int $idGrupo
   * @return string
  */
  public function deleteEnvioIntegrante($idEnvio){
    
    $em = $this->em;
    $conexion = $em->getConnection();

    $sqlDelete ="DELETE FROM envio_integrante WHERE envio_id=".$idEnvio;

    $conexion->query($sqlDelete);


  }


}