<?php

namespace App\Services;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use App\Entity\LogAdministrador;
use App\Entity\User;


/**
 * Funciones de ayuda en la aplicacion
 * @author Julian Restrepo [Waplicaciones SAS]
 * @since 5.0
*/
class Logs {

	private $em;
	private $ip;
	private $session;

	/**
	* Inicializa objetos de la clase.
	* @param EntityManagerInterface $entityManager Conexión a Base de datos.
	* @author Julian Restrepo
	*/
	public function __construct(EntityManagerInterface $entityManager, RequestStack $requestStack) {
		$this->em = $entityManager;
		$this->ip = $requestStack->getCurrentRequest()->getClientIp();
		$this->session = $requestStack->getCurrentRequest()->getSession();
	}


	/**
	* Registra log de actividades para Admin.
	* @param EntityManagerInterface $entityManager Conexión a Base de datos.
	* @author Julian Restrepo
	*/
	public function setLogAdmin($actividad){

		$em = $this->em;
		$log = new LogAdministrador();
        
        $date = new \DateTime();
		$log->setFechaIngreso(new \DateTime());
		$log->setActividad($actividad);
		$log->setIp($this->ip);
		//$log->setAdministradorId($em->getReference('App\Entity\User', $this->session->get('id') ));
		$log->setAdministradorId($this->session->get('id'));


		$em->persist($log);
		$em->flush();

	}


}