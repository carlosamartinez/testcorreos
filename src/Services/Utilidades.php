<?php

namespace App\Services;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\RegistroActividadAdmin;

/**
 * Funciones de ayuda en la aplicacion
 * @author Julian Restrepo [Waplicaciones SAS]
 * @author j.restrepo@waplicaciones.co
 * @since 5.0
*/
class Utilidades {

  private $em;

  /**
   * Inicializa objetos de la clase.
   * @param EntityManagerInterface $entityManager Conexión a Base de datos.
   * @author Julian Restrepo<j.restrepo@waplicaciones.co>
  */
  public function __construct(EntityManagerInterface $entityManager) {
    $this->em = $entityManager;
  }

  /**
   * Realiza el proceso para generar las condiciones en las consultas de la grilla
   *
   * @param int $longitud de caracteres aleatorios
   * @return string
  */
  public function getTxtPassword($longitud = 14){
    $sBasePassRandon = '#-0123456789-*abcdefghijklmnopqrstuvwxyz*$ABCDEFGHIJKLMNOPQRSTUVWXYZ$#';
    $nBaseTxtRandon = strlen($sBasePassRandon);
    $sRandomString = '';
    for($i = 0; $i < $longitud; $i++) {
      $sRandomCharacter = $sBasePassRandon[mt_rand(0, $nBaseTxtRandon - 1)];
      $sRandomString .= $sRandomCharacter;
    }
    return $sRandomString;
  }


  public function setLogAdmin(){
    
    $em = $this->em;

    $log = new RegistroActividadAdmin();
    //$log = $em->getRepository(RegistroActividadAdmin::Class->findById(1);


    $log->setFecha(new \DateTime());

    $em->persist($log);
    $em->flush();


  }
}