<?php

namespace App\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Environment;
use App\Entity\User;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Debug\ExceptionHandler;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Funciones de ayuda en la aplicacion
 * @author Julian Restrepo [Waplicaciones SAS]
 * @author j.restrepo@waplicaciones.co
 * @since 5.0
*/
class EnvioMasivo {

    /*private $em;
    private $mailer;
    private $twig;*/
    private $params;
    private $router;
  /**
   * Inicializa objetos de la clase.
   * @param EntityManagerInterface $entityManager Conexión a Base de datos.
   * @author Julian Restrepo<j.restrepo@waplicaciones.co>
  */
  public function __construct(/*\Swift_Mailer $mailer,EntityManagerInterface $entityManager,Environment $twig,*/ ParameterBagInterface $params,RouterInterface $router) {

        /*$this->em = $entityManager;
        $this->mailer = $mailer;
        $this->twig = $twig;*/
        $this->params = $params;
        $this->router = $router;
  }

  public function provedoresBloqueadosSip ($emspConect){

    /************SE BUSCAN PROVEEDORES BLOQUEADOS SIPPROVEEDORES*************/
    $arrayProvBloqueados = array();
    $sqlProvBloqueados = "SELECT nit FROM proveedor WHERE estado =0 ";
    $proveedoresBloqueados = $emspConect->query($sqlProvBloqueados);
    foreach($proveedoresBloqueados as $provBloq){
      $arrayProvBloqueados[$provBloq['nit']] = $provBloq['nit'];
    }

    return $arrayProvBloqueados;

  }


  public function cargosSinSeguimiento ($emContacConect){

    /**********SE BUSCAN LOS CARGOS SIN SEGUIMIENTO ****************/
    $arrayCargoSinSeguimiento = array();
    $sqlCargoSinSeg = "SELECT id FROM cargos WHERE sin_seguimiento =1 ";

    $cargosBloqueados = $emContacConect->query($sqlCargoSinSeg);
    foreach($cargosBloqueados as $cargoBloq){
      $arrayCargoSinSeguimiento[$cargoBloq['id']] = $cargoBloq['id'];
    }

    return $arrayCargoSinSeguimiento;

  }

  public function getDroguerias($emConect){

    $asociados = array();

    $asociadosEntity = $emConect->query("SELECT id, codigo, drogueria, nit, asociado, email, email_asociado, zona, direcion, ciudad, ruta, depto,telefono, centro FROM cliente");

    foreach($asociadosEntity as $asociadoEntity){
      $asociados[$asociadoEntity['id']]['id']=$asociadoEntity['id'];
      $asociados[$asociadoEntity['id']]['codigo']=$asociadoEntity['codigo'];
      $asociados[$asociadoEntity['id']]['drogueria']=$asociadoEntity['drogueria'];
      $asociados[$asociadoEntity['id']]['nit']=$asociadoEntity['nit'];
      $asociados[$asociadoEntity['id']]['asociado']=$asociadoEntity['asociado'];
      $asociados[$asociadoEntity['id']]['email']=$asociadoEntity['email'];
      $asociados[$asociadoEntity['id']]['email_asociado']=$asociadoEntity['email_asociado'];
      $asociados[$asociadoEntity['id']]['emailAsociado']=$asociadoEntity['email_asociado'];
      $asociados[$asociadoEntity['id']]['zona']=$asociadoEntity['zona'];
      $asociados[$asociadoEntity['id']]['direcion']=$asociadoEntity['direcion'];
      $asociados[$asociadoEntity['id']]['direccion']=$asociadoEntity['direcion'];
      $asociados[$asociadoEntity['id']]['ciudad']=$asociadoEntity['ciudad'];
      $asociados[$asociadoEntity['id']]['ruta']=$asociadoEntity['ruta'];
      $asociados[$asociadoEntity['id']]['depto']=$asociadoEntity['depto'];
      $asociados[$asociadoEntity['id']]['departamento']=$asociadoEntity['depto'];
      $asociados[$asociadoEntity['id']]['telefono']=$asociadoEntity['telefono'];
      $asociados[$asociadoEntity['id']]['centro']=$asociadoEntity['centro'];

    }

    return $asociados;

  }


  public function getContactosProvDBContactos($emConectContactos){

    

    $contactosEntity = $emConectContactos->query("SELECT c.id, c.nombre_contacto, c.telefono AS telefono, c.movil AS celular, c.email AS email1, c.id_proveedor, c.id_cargo, p.codigo, p.nit, p.nombre AS empresa, ca.nombre AS cargo 
      FROM contactos c 
      JOIN proveedores p ON c.id_proveedor = p.id
      JOIN cargos ca ON c.id_cargo = ca.id
      ");
    $contactosProveedor = array();
    foreach($contactosEntity as $contactoEntity){
      $contactosProveedor[$contactoEntity['id']]['id']=$contactoEntity['id'];
      $contactosProveedor[$contactoEntity['id']]['nombre']=$contactoEntity['nombre_contacto'];
      $contactosProveedor[$contactoEntity['id']]['cargo']=$contactoEntity['cargo'];
      $contactosProveedor[$contactoEntity['id']]['telefono']=$contactoEntity['telefono'];
      $contactosProveedor[$contactoEntity['id']]['celular']=$contactoEntity['celular'];
      $contactosProveedor[$contactoEntity['id']]['email']=$contactoEntity['email1'];
      $contactosProveedor[$contactoEntity['id']]['email1']=$contactoEntity['email1'];
      $contactosProveedor[$contactoEntity['id']]['id_proveedor']=$contactoEntity['id_proveedor'];
      $contactosProveedor[$contactoEntity['id']]['Codigo']=$contactoEntity['codigo'];
      $contactosProveedor[$contactoEntity['id']]['codigo']=$contactoEntity['codigo'];
      $contactosProveedor[$contactoEntity['id']]['Nit']= (int)$contactoEntity['nit'];
      $contactosProveedor[$contactoEntity['id']]['nit']= (int)$contactoEntity['nit'];
      $contactosProveedor[$contactoEntity['id']]['empresa']=$contactoEntity['empresa'];
      $contactosProveedor[$contactoEntity['id']]['id_cargo']=$contactoEntity['id_cargo'];
      $contactosProveedor[$contactoEntity['id']]['tipo']='ContactoProveedor';
    }

    return $contactosProveedor;

  }


  public function getProveedoresDBContactos($emConectContactos){

    $proveedoresEntity = $emConectContactos->query("SELECT p.id, p.representante_legal AS nombre, p.nombre AS empresa, p.telefono_representante_legal AS telefono, p.telefono_representante_legal AS celular, p.email_representante_legal AS email, p.codigo, p.nit FROM proveedores p");
    $proveedorObj = array();
    foreach($proveedoresEntity as $proveedorEntity){
      $proveedorObj[$proveedorEntity['id']]['id']=$proveedorEntity['id'];
      $proveedorObj[$proveedorEntity['id']]['nombre']=$proveedorEntity['nombre'];
      $proveedorObj[$proveedorEntity['id']]['email']=$proveedorEntity['email'];
      $proveedorObj[$proveedorEntity['id']]['id_proveedor']=$proveedorEntity['id'];
      $proveedorObj[$proveedorEntity['id']]['Codigo']=$proveedorEntity['codigo'];
      $proveedorObj[$proveedorEntity['id']]['codigo']=$proveedorEntity['codigo'];
      $proveedorObj[$proveedorEntity['id']]['Nit']= (int)$proveedorEntity['nit'];
      $proveedorObj[$proveedorEntity['id']]['nit']= (int)$proveedorEntity['nit'];
      $proveedorObj[$proveedorEntity['id']]['empresa']=$proveedorEntity['empresa'];
      $proveedorObj[$proveedorEntity['id']]['tipo']='Proveedor';
    }

    return $proveedorObj;

  }



  public function combinarCorrespondencia($envio, $dest, $contenido, $emConect){

    
    $columnaCombinacion = $envio['columna_combinacion'];
    $condicion = $dest[$columnaCombinacion];
    $cabezeraSabana = $emConect->fetchAssoc("SELECT datos FROM datos_envio WHERE envio_id=".$envio['id']);
    $sabanaDatos = $emConect->query("SELECT id, datos FROM columnas_datos_envio WHERE envio_id=".$envio['id']." AND identificador ='".$condicion."'")->fetchAll();
    $control =0;
    if(strpos($contenido, '//TABLA//') !== FALSE){
      $columnas = unserialize($cabezeraSabana['datos']);
      $contColumnas = count($columnas) - 1;
      $posicion = 0;
      $informacionTabla = '<table class="tablaDatos"><tr>';
      foreach($columnas as $ThArray){
        $informacionTabla .= '<td>' . ucfirst(htmlentities($ThArray)) . '</td>';
      }
      $informacionTabla .= '</tr>';
      foreach ($sabanaDatos as $dato) {
        $adjuntosCsv[] = $dato['id'];
        $data = unserialize($dato['datos']);
        $contColumnas = count($data) - 1;
        $informacionTabla .= '<tr>';
        for ($i = 0; $i <= $contColumnas; $i++) {
          $informacionTabla .= '<td>' . $data[$i] . '</td>';
        }
        $informacionTabla .= '</tr>';
      }
      $informacionTabla .= '</table>';
      $control++;
      if($sabanaDatos){
        $contenido = str_replace("//TABLA//", $informacionTabla, $contenido);
      }else{
        $contenido = str_replace("//TABLA//", "", $contenido);
      }
    }
    $usuarioTipo=$dest['tipo'];
    if(strpos($contenido, '//ARCHIVO//') !== FALSE){
      if($sabanaDatos){   
        $url = $this->params->get('urlApp').$this->router->generate('admin_envio_destinatario_xls', ['envio_id' => $envio['id'],'tipo_usuario'=>$usuarioTipo,'id_usuario'=>$dest['id'],'llave'=>md5($envio['id'].$usuarioTipo.$dest['id'])]);
        $linkArchivo='<a style="color:#E01F69;font-size:24px;" href="'.$url.'" target="_blank">Clic aqu&iacute; para descargar Archivo</a>';
        $control++;
        $contenido = str_replace("//ARCHIVO//", $linkArchivo, $contenido);
      }else{
        $contenido = str_replace("//ARCHIVO//", ' ', $contenido);
      }
    }
    if(strpos($contenido, '//COMBINAR//') !== FALSE){
      foreach ($sabanaDatos as $dato) {
        $datosFila = unserialize($dato['datos']);
        $cantidadDatos = count($datosFila) - 1;
        for($i = 0; $i <= $cantidadDatos; $i++){
          $contenido = str_replace('//'.($i+1).'//', utf8_decode($datosFila[$i]), $contenido);
        }
      }
      $contenido = str_replace('//COMBINAR//', '', $contenido);
    }


    return $contenido;
  }


    
    

    

}