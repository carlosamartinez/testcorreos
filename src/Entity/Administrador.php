<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Administrador
 *
 * @ORM\Table(name="administrador")
 * @ORM\Entity
 */
class Administrador
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=45, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="usuario", type="string", length=45, nullable=false)
     */
    private $usuario;

    /**
     * @var string
     *
     * @ORM\Column(name="clave", type="string", length=100, nullable=false)
     */
    private $clave;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=45, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="clave_email", type="string", length=100, nullable=false)
     */
    private $claveEmail;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="seguimiento", type="boolean", nullable=true)
     */
    private $seguimiento;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="tipo_usuario", type="boolean", nullable=true)
     */
    private $tipoUsuario;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creado", type="datetime", nullable=false)
     */
    private $fechaCreado;

    /**
     * @var int
     *
     * @ORM\Column(name="creador_id", type="integer", nullable=false)
     */
    private $creadorId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_ultimo_ingreso", type="datetime", nullable=true)
     */
    private $fechaUltimoIngreso;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ultima_ip", type="string", length=30, nullable=true)
     */
    private $ultimaIp;

    /**
     * @var bool
     *
     * @ORM\Column(name="activo", type="boolean", nullable=false)
     */
    private $activo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getUsuario(): ?string
    {
        return $this->usuario;
    }

    public function setUsuario(string $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getClave(): ?string
    {
        return $this->clave;
    }

    public function setClave(string $clave): self
    {
        $this->clave = $clave;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getClaveEmail(): ?string
    {
        return $this->claveEmail;
    }

    public function setClaveEmail(string $claveEmail): self
    {
        $this->claveEmail = $claveEmail;

        return $this;
    }

    public function getSeguimiento(): ?bool
    {
        return $this->seguimiento;
    }

    public function setSeguimiento(?bool $seguimiento): self
    {
        $this->seguimiento = $seguimiento;

        return $this;
    }

    public function getTipoUsuario(): ?bool
    {
        return $this->tipoUsuario;
    }

    public function setTipoUsuario(?bool $tipoUsuario): self
    {
        $this->tipoUsuario = $tipoUsuario;

        return $this;
    }

    public function getFechaCreado(): ?\DateTimeInterface
    {
        return $this->fechaCreado;
    }

    public function setFechaCreado(\DateTimeInterface $fechaCreado): self
    {
        $this->fechaCreado = $fechaCreado;

        return $this;
    }

    public function getCreadorId(): ?int
    {
        return $this->creadorId;
    }

    public function setCreadorId(int $creadorId): self
    {
        $this->creadorId = $creadorId;

        return $this;
    }

    public function getFechaUltimoIngreso(): ?\DateTimeInterface
    {
        return $this->fechaUltimoIngreso;
    }

    public function setFechaUltimoIngreso(?\DateTimeInterface $fechaUltimoIngreso): self
    {
        $this->fechaUltimoIngreso = $fechaUltimoIngreso;

        return $this;
    }

    public function getUltimaIp(): ?string
    {
        return $this->ultimaIp;
    }

    public function setUltimaIp(?string $ultimaIp): self
    {
        $this->ultimaIp = $ultimaIp;

        return $this;
    }

    public function getActivo(): ?bool
    {
        return $this->activo;
    }

    public function setActivo(bool $activo): self
    {
        $this->activo = $activo;

        return $this;
    }


}
