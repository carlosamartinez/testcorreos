<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Menu
 *
 * @ORM\Table(name="menu")
 * @ORM\Entity
 */
class Menu
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="path_name", type="string", length=80, nullable=true)
     */
    private $pathName;

    /**
     * @var string
     *
     * @ORM\Column(name="rol", type="string", length=30, nullable=false)
     */
    private $rol;

    /**
     * @var string
     *
     * @ORM\Column(name="modulo", type="string", length=80, nullable=false)
     */
    private $modulo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vista", type="string", length=60, nullable=true)
     */
    private $vista;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=60, nullable=false)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="icono", type="string", length=60, nullable=false)
     */
    private $icono;

    /**
     * @var bool
     *
     * @ORM\Column(name="posicion", type="boolean", nullable=false)
     */
    private $posicion;

    /**
     * @var bool
     *
     * @ORM\Column(name="estado", type="boolean", nullable=false)
     */
    private $estado;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPathName(): ?string
    {
        return $this->pathName;
    }

    public function setPathName(?string $pathName): self
    {
        $this->pathName = $pathName;

        return $this;
    }

    public function getRol(): ?string
    {
        return $this->rol;
    }

    public function setRol(string $rol): self
    {
        $this->rol = $rol;

        return $this;
    }

    public function getModulo(): ?string
    {
        return $this->modulo;
    }

    public function setModulo(string $modulo): self
    {
        $this->modulo = $modulo;

        return $this;
    }

    public function getVista(): ?string
    {
        return $this->vista;
    }

    public function setVista(?string $vista): self
    {
        $this->vista = $vista;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getIcono(): ?string
    {
        return $this->icono;
    }

    public function setIcono(string $icono): self
    {
        $this->icono = $icono;

        return $this;
    }

    public function getPosicion(): ?bool
    {
        return $this->posicion;
    }

    public function setPosicion(bool $posicion): self
    {
        $this->posicion = $posicion;

        return $this;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }


}
