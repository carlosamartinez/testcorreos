<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EnvioLectura
 *
 * @ORM\Table(name="envio_lectura", indexes={@ORM\Index(name="envio_id", columns={"envio_id"}), @ORM\Index(name="envio_integrate_id", columns={"envio_integrate_id"})})
 * @ORM\Entity
 */
class EnvioLectura
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="asunto", type="string", length=200, nullable=false)
     */
    private $asunto;

    /**
     * @var string
     *
     * @ORM\Column(name="integrante", type="string", length=200, nullable=false)
     */
    private $integrante;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=false)
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=false)
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=30, nullable=false)
     */
    private $ip;

    /**
     * @var int|null
     *
     * @ORM\Column(name="envio_id", type="bigint", nullable=true)
     */
    private $envioId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="envio_integrate_id", type="integer", nullable=true)
     */
    private $envioIntegrateId;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getAsunto(): ?string
    {
        return $this->asunto;
    }

    public function setAsunto(string $asunto): self
    {
        $this->asunto = $asunto;

        return $this;
    }

    public function getIntegrante(): ?string
    {
        return $this->integrante;
    }

    public function setIntegrante(string $integrante): self
    {
        $this->integrante = $integrante;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getEnvioId(): ?string
    {
        return $this->envioId;
    }

    public function setEnvioId(?string $envioId): self
    {
        $this->envioId = $envioId;

        return $this;
    }

    public function getEnvioIntegrateId(): ?int
    {
        return $this->envioIntegrateId;
    }

    public function setEnvioIntegrateId(?int $envioIntegrateId): self
    {
        $this->envioIntegrateId = $envioIntegrateId;

        return $this;
    }


}
