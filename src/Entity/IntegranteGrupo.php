<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IntegranteGrupo
 *
 * @ORM\Table(name="integrante_grupo", indexes={@ORM\Index(name="fk_asociado_integranteGrupo_idx", columns={"asociado_id"}), @ORM\Index(name="fk_grupo_integranteGrupo_idx", columns={"grupo_id"}), @ORM\Index(name="fk_proveedor_integranteGrupo_idx", columns={"proveedor_id"}), @ORM\Index(name="fk_tipoIntegrante_integranteGrupo_idx", columns={"tipo_integrante_id"})})
 * @ORM\Entity
 */
class IntegranteGrupo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="asociado_id", type="bigint", nullable=true)
     */
    private $asociadoId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="proveedor_id", type="integer", nullable=true)
     */
    private $proveedorId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="contacto_proveedor_id", type="integer", nullable=true)
     */
    private $contactoProveedorId;

    /**
     * @var int
     *
     * @ORM\Column(name="activo", type="integer", nullable=false)
     */
    private $activo;

    /**
     * @var \Grupo
     *
     * @ORM\ManyToOne(targetEntity="Grupo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="grupo_id", referencedColumnName="id")
     * })
     */
    private $grupo;

    /**
     * @var \TipoIntegrante
     *
     * @ORM\ManyToOne(targetEntity="TipoIntegrante")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_integrante_id", referencedColumnName="id")
     * })
     */
    private $tipoIntegrante;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getAsociadoId(): ?string
    {
        return $this->asociadoId;
    }

    public function setAsociadoId(?string $asociadoId): self
    {
        $this->asociadoId = $asociadoId;

        return $this;
    }

    public function getProveedorId(): ?int
    {
        return $this->proveedorId;
    }

    public function setProveedorId(?int $proveedorId): self
    {
        $this->proveedorId = $proveedorId;

        return $this;
    }

    public function getContactoProveedorId(): ?int
    {
        return $this->contactoProveedorId;
    }

    public function setContactoProveedorId(?int $contactoProveedorId): self
    {
        $this->contactoProveedorId = $contactoProveedorId;

        return $this;
    }

    public function getActivo(): ?int
    {
        return $this->activo;
    }

    public function setActivo(int $activo): self
    {
        $this->activo = $activo;

        return $this;
    }

    public function getGrupo(): ?Grupo
    {
        return $this->grupo;
    }

    public function setGrupo(?Grupo $grupo): self
    {
        $this->grupo = $grupo;

        return $this;
    }

    public function getTipoIntegrante(): ?TipoIntegrante
    {
        return $this->tipoIntegrante;
    }

    public function setTipoIntegrante(?TipoIntegrante $tipoIntegrante): self
    {
        $this->tipoIntegrante = $tipoIntegrante;

        return $this;
    }


}
