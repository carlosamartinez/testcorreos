<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ColumnasDatosEnvio
 *
 * @ORM\Table(name="columnas_datos_envio", indexes={@ORM\Index(name="creador_id", columns={"creador_id"}), @ORM\Index(name="envio_id", columns={"envio_id"}), @ORM\Index(name="identificador", columns={"identificador"})})
 * @ORM\Entity
 */
class ColumnasDatosEnvio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creado", type="datetime", nullable=false)
     */
    private $fechaCreado;

    /**
     * @var int
     *
     * @ORM\Column(name="envio_id", type="integer", nullable=false)
     */
    private $envioId;

    /**
     * @var string
     *
     * @ORM\Column(name="datos", type="text", length=65535, nullable=false)
     */
    private $datos;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_modificado", type="datetime", nullable=false)
     */
    private $fechaModificado;

    /**
     * @var string|null
     *
     * @ORM\Column(name="identificador", type="string", length=60, nullable=true)
     */
    private $identificador;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="creador_id", referencedColumnName="id")
     * })
     */
    private $creador;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFechaCreado(): ?\DateTimeInterface
    {
        return $this->fechaCreado;
    }

    public function setFechaCreado(\DateTimeInterface $fechaCreado): self
    {
        $this->fechaCreado = $fechaCreado;

        return $this;
    }

    public function getEnvioId(): ?int
    {
        return $this->envioId;
    }

    public function setEnvioId(int $envioId): self
    {
        $this->envioId = $envioId;

        return $this;
    }

    public function getDatos(): ?string
    {
        return $this->datos;
    }

    public function setDatos(string $datos): self
    {
        $this->datos = $datos;

        return $this;
    }

    public function getFechaModificado(): ?\DateTimeInterface
    {
        return $this->fechaModificado;
    }

    public function setFechaModificado(\DateTimeInterface $fechaModificado): self
    {
        $this->fechaModificado = $fechaModificado;

        return $this;
    }

    public function getIdentificador(): ?string
    {
        return $this->identificador;
    }

    public function setIdentificador(?string $identificador): self
    {
        $this->identificador = $identificador;

        return $this;
    }

    public function getCreador(): ?User
    {
        return $this->creador;
    }

    public function setCreador(?User $creador): self
    {
        $this->creador = $creador;

        return $this;
    }


}
