<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Configuracion
 *
 * @ORM\Table(name="configuracion")
 * @ORM\Entity
 */
class Configuracion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="envio_inmediato", type="integer", nullable=false)
     */
    private $envioInmediato;

    /**
     * @var int
     *
     * @ORM\Column(name="envio_programado", type="integer", nullable=false)
     */
    private $envioProgramado;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEnvioInmediato(): ?int
    {
        return $this->envioInmediato;
    }

    public function setEnvioInmediato(int $envioInmediato): self
    {
        $this->envioInmediato = $envioInmediato;

        return $this;
    }

    public function getEnvioProgramado(): ?int
    {
        return $this->envioProgramado;
    }

    public function setEnvioProgramado(int $envioProgramado): self
    {
        $this->envioProgramado = $envioProgramado;

        return $this;
    }


}
