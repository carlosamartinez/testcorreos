<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Proveedores
 *
 * @ORM\Table(name="proveedores")
 * @ORM\Entity
 */
class Proveedores
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="nit", type="integer", nullable=false)
     */
    private $nit;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=256, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=32, nullable=false)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="representante_legal", type="string", length=255, nullable=false)
     */
    private $representanteLegal;

    /**
     * @var string
     *
     * @ORM\Column(name="email_representante_legal", type="string", length=255, nullable=false)
     */
    private $emailRepresentanteLegal;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono_representante_legal", type="string", length=255, nullable=false)
     */
    private $telefonoRepresentanteLegal;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_ultima_modificacion", type="datetime", nullable=true)
     */
    private $fechaUltimaModificacion;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNit(): ?int
    {
        return $this->nit;
    }

    public function setNit(int $nit): self
    {
        $this->nit = $nit;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getCodigo(): ?string
    {
        return $this->codigo;
    }

    public function setCodigo(string $codigo): self
    {
        $this->codigo = $codigo;

        return $this;
    }

    public function getRepresentanteLegal(): ?string
    {
        return $this->representanteLegal;
    }

    public function setRepresentanteLegal(string $representanteLegal): self
    {
        $this->representanteLegal = $representanteLegal;

        return $this;
    }

    public function getEmailRepresentanteLegal(): ?string
    {
        return $this->emailRepresentanteLegal;
    }

    public function setEmailRepresentanteLegal(string $emailRepresentanteLegal): self
    {
        $this->emailRepresentanteLegal = $emailRepresentanteLegal;

        return $this;
    }

    public function getTelefonoRepresentanteLegal(): ?string
    {
        return $this->telefonoRepresentanteLegal;
    }

    public function setTelefonoRepresentanteLegal(string $telefonoRepresentanteLegal): self
    {
        $this->telefonoRepresentanteLegal = $telefonoRepresentanteLegal;

        return $this;
    }

    public function getFechaUltimaModificacion(): ?\DateTimeInterface
    {
        return $this->fechaUltimaModificacion;
    }

    public function setFechaUltimaModificacion(\DateTimeInterface $fechaUltimaModificacion): self
    {
        $this->fechaUltimaModificacion = $fechaUltimaModificacion;

        return $this;
    }


}
