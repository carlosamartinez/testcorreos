<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Grupo
 *
 * @ORM\Table(name="grupo", indexes={@ORM\Index(name="fk_administrador_idx", columns={"administrador_id"})})
 * @ORM\Entity
 */
class Grupo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=60, nullable=false)
     */
    private $nombre;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creado", type="datetime", nullable=false)
     */
    private $fechaCreado;

    /**
     * @var string
     *
     * @ORM\Column(name="clasificacion", type="string", length=25, nullable=false)
     */
    private $clasificacion;

    /**
     * @var int|null
     *
     * @ORM\Column(name="integrantes", type="integer", nullable=true)
     */
    private $integrantes;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=250, nullable=false)
     */
    private $descripcion;

    /**
     * @var int|null
     *
     * @ORM\Column(name="agrupar_proveedor", type="integer", nullable=true)
     */
    private $agruparProveedor;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="administrador_id", referencedColumnName="id")
     * })
     */
    private $administrador;

    public function setId(int $id): ?int
    {
        return $this->id;
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getFechaCreado(): ?\DateTimeInterface
    {
        return $this->fechaCreado;
    }

    public function setFechaCreado(\DateTimeInterface $fechaCreado): self
    {
        $this->fechaCreado = $fechaCreado;

        return $this;
    }

    public function getClasificacion(): ?string
    {
        return $this->clasificacion;
    }

    public function setClasificacion(string $clasificacion): self
    {
        $this->clasificacion = $clasificacion;

        return $this;
    }

    public function getIntegrantes(): ?int
    {
        return $this->integrantes;
    }

    public function setIntegrantes(?int $integrantes): self
    {
        $this->integrantes = $integrantes;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getAgruparProveedor(): ?int
    {
        return $this->agruparProveedor;
    }

    public function setAgruparProveedor(?int $agruparProveedor): self
    {
        $this->agruparProveedor = $agruparProveedor;

        return $this;
    }

    public function getAdministrador(): ?User
    {
        return $this->administrador;
    }

    public function setAdministrador(?User $administrador): self
    {
        $this->administrador = $administrador;

        return $this;
    }


}
