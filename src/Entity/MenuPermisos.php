<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MenuPermisos
 *
 * @ORM\Table(name="menu_permisos", indexes={@ORM\Index(name="accion_acciones_permisos", columns={"id_accion"}), @ORM\Index(name="user_acciones_permisos", columns={"id_user"})})
 * @ORM\Entity(repositoryClass=MenuPermisosRepository::class)
 */
class MenuPermisos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="permiso", type="boolean", nullable=false)
     */
    private $permiso;

    /**
     * @var \Acciones
     *
     * @ORM\ManyToOne(targetEntity="Acciones")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_accion", referencedColumnName="id")
     * })
     */
    private $idAccion;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $idUser;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPermiso(): ?bool
    {
        return $this->permiso;
    }

    public function setPermiso(bool $permiso): self
    {
        $this->permiso = $permiso;

        return $this;
    }

    public function getIdAccion(): ?Acciones
    {
        return $this->idAccion;
    }

    public function setIdAccion(?Acciones $idAccion): self
    {
        $this->idAccion = $idAccion;

        return $this;
    }

    public function getIdUser(): ?User
    {
        return $this->idUser;
    }

    public function setIdUser(?User $idUser): self
    {
        $this->idUser = $idUser;

        return $this;
    }


}
