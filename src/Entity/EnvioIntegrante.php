<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EnvioIntegrante
 *
 * @ORM\Table(name="envio_integrante", indexes={@ORM\Index(name="fk_envio_envioIntegrante_idx", columns={"envio_id"}), @ORM\Index(name="fk_integranteGrupo_envioIntegrante_idx", columns={"integrante_grupo_id"})})
 * @ORM\Entity
 */
class EnvioIntegrante
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="integrante_grupo_id", type="bigint", nullable=false)
     */
    private $integranteGrupoId;

    /**
     * @var bool
     *
     * @ORM\Column(name="enviado", type="boolean", nullable=false)
     */
    private $enviado;

    /**
     * @var int|null
     *
     * @ORM\Column(name="reenviado", type="integer", nullable=true, options={"comment"="veces que ha sido reenviado el msj."})
     */
    private $reenviado = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_envio", type="datetime", nullable=false)
     */
    private $fechaEnvio;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_reenvio", type="datetime", nullable=true)
     */
    private $fechaReenvio;

    /**
     * @var int|null
     *
     * @ORM\Column(name="envio_error_id", type="bigint", nullable=true)
     */
    private $envioErrorId;

    /**
     * @var \Envio
     *
     * @ORM\ManyToOne(targetEntity="Envio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="envio_id", referencedColumnName="id")
     * })
     */
    private $envio;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIntegranteGrupoId(): ?string
    {
        return $this->integranteGrupoId;
    }

    public function setIntegranteGrupoId(string $integranteGrupoId): self
    {
        $this->integranteGrupoId = $integranteGrupoId;

        return $this;
    }

    public function getEnviado(): ?bool
    {
        return $this->enviado;
    }

    public function setEnviado(bool $enviado): self
    {
        $this->enviado = $enviado;

        return $this;
    }

    public function getReenviado(): ?int
    {
        return $this->reenviado;
    }

    public function setReenviado(?int $reenviado): self
    {
        $this->reenviado = $reenviado;

        return $this;
    }

    public function getFechaEnvio(): ?\DateTimeInterface
    {
        return $this->fechaEnvio;
    }

    public function setFechaEnvio(\DateTimeInterface $fechaEnvio): self
    {
        $this->fechaEnvio = $fechaEnvio;

        return $this;
    }

    public function getFechaReenvio(): ?\DateTimeInterface
    {
        return $this->fechaReenvio;
    }

    public function setFechaReenvio(?\DateTimeInterface $fechaReenvio): self
    {
        $this->fechaReenvio = $fechaReenvio;

        return $this;
    }

    public function getEnvioErrorId(): ?string
    {
        return $this->envioErrorId;
    }

    public function setEnvioErrorId(?string $envioErrorId): self
    {
        $this->envioErrorId = $envioErrorId;

        return $this;
    }

    public function getEnvio(): ?Envio
    {
        return $this->envio;
    }

    public function setEnvio(?Envio $envio): self
    {
        $this->envio = $envio;

        return $this;
    }


}
