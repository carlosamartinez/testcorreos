<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HistoricoPassword
 *
 * @ORM\Table(name="historico_password", indexes={@ORM\Index(name="id_user_idx", columns={"id_user"})})
 * @ORM\Entity
 */
class HistoricoPassword
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"comment"="Pk"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_cambio_clave", type="datetime", nullable=false, options={"comment"="Fecha ultimo cambio de clave exitosa"})
     */
    private $fechaCambioClave;

    /**
     * @var json|null
     *
     * @ORM\Column(name="claves", type="json", nullable=true)
     */
    private $claves;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $idUser;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFechaCambioClave(): ?\DateTimeInterface
    {
        return $this->fechaCambioClave;
    }

    public function setFechaCambioClave(\DateTimeInterface $fechaCambioClave): self
    {
        $this->fechaCambioClave = $fechaCambioClave;

        return $this;
    }

    public function getClaves(): ?array
    {
        return $this->claves;
    }

    public function setClaves(?array $claves): self
    {
        $this->claves = $claves;

        return $this;
    }

    public function getIdUser(): ?User
    {
        return $this->idUser;
    }

    public function setIdUser(?User $idUser): self
    {
        $this->idUser = $idUser;

        return $this;
    }


}
