<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;

/**
 * User
 *
 * @ORM\Table(name="user", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_8D93D6497BF39BE0", columns={"usuario"})})
 * @ORM\Entity(repositoryClass=UserRepository::class)
*/
class User implements UserInterface, \Serializable, EquatableInterface{

  /**
   * @var int
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
  */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="nombre", type="string", length=120, nullable=false)
  */
  private $nombre;


   /**
   * @var string
   *
   * @ORM\Column(name="usuario", type="string", length=25, nullable=false)
   */
    private $usuario;


  /**
   * @var string
   *
   * @ORM\Column(name="email", type="string", length=60, nullable=false)
  */
  private $email;


  /**
   * @var string
   *
   * @ORM\Column(name="clave_email", type="string", length=100, nullable=false)
  */
  private $claveEmail;



  /**
   * @var json
   *
   * @ORM\Column(name="roles", type="json", nullable=false)
  */
  private $roles = [];

  /**
   * @var string
   *
   * @ORM\Column(name="role", type="string", length=120, nullable=false)
  */
  private $role;

  /**
   * @var string The hashed password
   * @ORM\Column(name="password", type="string", length=255, nullable=false)
  */
  private $password;

  /**
   * @var bool|null
   *
   * @ORM\Column(name="seguimiento", type="boolean", nullable=true)
  */
  private $seguimiento;

  /**
   * @var int
   *
   * @ORM\Column(name="estado", type="integer", nullable=false, options={"comment"="0: Eliminado, 1:Activo, 2:Inactivo"})
  */
  private $estado;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="fecha_creado", type="datetime", nullable=false)
   */
  private $fechaCreado;
  
  /**
   * @var bool
   *
   * @ORM\Column(name="tipo_usuario", type="boolean", nullable=false)
   */
  private $tipoUsuario;
  public function getId(): ?int{
    return $this->id;
  }

  public function getNombre(): ?string{
    return $this->nombre;
  }

  public function setNombre(string $nombre): self{
    $this->nombre = $nombre;
    return $this;
  }
  public function getUsuario(): ?string
  {
      return $this->usuario;
  }

  public function setUsuario(string $usuario): self
  {
      $this->usuario = $usuario;

      return $this;
  }

  /**
   * A visual identifier that represents this user.
   *
   * @see UserInterface
  */
  public function getUsername(): string{
    return (string) $this->usuario;
  }

  public function getEmail(): ?string{
    return $this->email;
  }

  public function setEmail(string $email): self{
    $this->email = $email;
    return $this;
  }

  public function getClaveEmail(): ?string
  {
      return $this->claveEmail;
  }

  public function setClaveEmail(string $claveEmail): self
  {
      $this->claveEmail = $claveEmail;

      return $this;
  }


  /**
   * @see UserInterface
  */
  public function getRoles(): array{
    $roles = $this->roles;
    // guarantee every user at least has ROLE_USER
    $roles[] = 'ROLE_ADMIN';
    return array_unique($roles);
  }

  public function setRoles(array $roles): self{
    $this->roles = $roles;
    return $this;
  }

  public function getRole(): string{
    return $this->role;
  }

  public function setRole(string $role): self{
    $this->role = $role;
    return $this;
  }

  /**
   * @see UserInterface
  */
  public function getPassword(): ?string{
    return $this->password;
  }

  public function setPassword(string $password): self{  
    $this->password = $password;
    return $this;
  }


  public function getEstado(): ?int{
    return $this->estado;
  }

  public function setEstado(int $estado): self{
    $this->estado = $estado;
    return $this;
  }

  public function setFechaCreado(\DateTimeInterface $fechaCreado): self
  {
      $this->fechaCreado = $fechaCreado;

      return $this;
  }

  public function getFechaCreado(): ?\DateTimeInterface
  {
      return $this->fechaCreado;
  }

  public function getSeguimiento(): ?bool{
    return $this->seguimiento;
  }

  public function setSeguimiento(bool $seguimiento): self{
    $this->seguimiento = $seguimiento;
    return $this;
  }

  public function getTipoUsuario(): ?bool
  {
      return $this->tipoUsuario;
  }

  public function setTipoUsuario(bool $tipoUsuario): self
  {
      $this->tipoUsuario = $tipoUsuario;

      return $this;
  }
  
  /**
   * @see UserInterface
  */
  public function getSalt(){
    // not needed when using the "bcrypt" algorithm in security.yaml
  }

  /**
   * @see UserInterface
  */
  public function eraseCredentials(){
    // If you store any temporary, sensitive data on the user, clear it here
    // $this->plainPassword = null;
  }


  /** @see \Serializable::serialize() */
  public function serialize()
  {
      return serialize(array(
          $this->id,
          $this->nombre,
          $this->usuario,
          $this->email,
          $this->password
          // see section on salt below
          // $this->salt,
      ));
  }

  /** @see \Serializable::unserialize() */
  public function unserialize($serialized)
  {
      list(
          $this->id,
          $this->nombre,
          $this->usuario,
          $this->email,
          $this->password
          // see section on salt below
          // $this->salt
      ) = unserialize($serialized);
  }


  public function isEqualTo(UserInterface $user){

    if ($this->usuario !== $user->getUsuario()) {
        return false;
    }

    return true;
    
  }


}
