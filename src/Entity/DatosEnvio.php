<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DatosEnvio
 *
 * @ORM\Table(name="datos_envio", indexes={@ORM\Index(name="creador_id", columns={"creador_id"}), @ORM\Index(name="envio_id", columns={"envio_id"}), @ORM\Index(name="identificador", columns={"identificador"})})
 * @ORM\Entity
 */
class DatosEnvio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creado", type="datetime", nullable=false)
     */
    private $fechaCreado;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigo_destinatario", type="string", length=255, nullable=true)
     */
    private $codigoDestinatario;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tipo_usuario", type="integer", nullable=true)
     */
    private $tipoUsuario;

    /**
     * @var string|null
     *
     * @ORM\Column(name="datos", type="text", length=65535, nullable=true)
     */
    private $datos;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_modificado", type="datetime", nullable=false)
     */
    private $fechaModificado;

    /**
     * @var string|null
     *
     * @ORM\Column(name="identificador", type="string", length=30, nullable=true)
     */
    private $identificador;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="creador_id", referencedColumnName="id")
     * })
     */
    private $creador;

    /**
     * @var \Envio
     *
     * @ORM\ManyToOne(targetEntity="Envio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="envio_id", referencedColumnName="id")
     * })
     */
    private $envio;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFechaCreado(): ?\DateTimeInterface
    {
        return $this->fechaCreado;
    }

    public function setFechaCreado(\DateTimeInterface $fechaCreado): self
    {
        $this->fechaCreado = $fechaCreado;

        return $this;
    }

    public function getCodigoDestinatario(): ?string
    {
        return $this->codigoDestinatario;
    }

    public function setCodigoDestinatario(?string $codigoDestinatario): self
    {
        $this->codigoDestinatario = $codigoDestinatario;

        return $this;
    }

    public function getTipoUsuario(): ?int
    {
        return $this->tipoUsuario;
    }

    public function setTipoUsuario(?int $tipoUsuario): self
    {
        $this->tipoUsuario = $tipoUsuario;

        return $this;
    }

    public function getDatos(): ?string
    {
        return $this->datos;
    }

    public function setDatos(?string $datos): self
    {
        $this->datos = $datos;

        return $this;
    }

    public function getFechaModificado(): ?\DateTimeInterface
    {
        return $this->fechaModificado;
    }

    public function setFechaModificado(\DateTimeInterface $fechaModificado): self
    {
        $this->fechaModificado = $fechaModificado;

        return $this;
    }

    public function getIdentificador(): ?string
    {
        return $this->identificador;
    }

    public function setIdentificador(?string $identificador): self
    {
        $this->identificador = $identificador;

        return $this;
    }

    public function getCreador(): ?User
    {
        return $this->creador;
    }

    public function setCreador(?User $creador): self
    {
        $this->creador = $creador;

        return $this;
    }

    public function getEnvio(): ?Envio
    {
        return $this->envio;
    }

    public function setEnvio(?Envio $envio): self
    {
        $this->envio = $envio;

        return $this;
    }


}
