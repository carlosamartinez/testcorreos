<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cliente
 *
 * @ORM\Table(name="cliente", indexes={@ORM\Index(name="codigo_idx", columns={"codigo"})})
 * @ORM\Entity
 */
class Cliente
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="zona", type="string", length=100, nullable=false)
     */
    private $zona;

    /**
     * @var int|null
     *
     * @ORM\Column(name="codigo", type="integer", nullable=true)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="bloqueo_d", type="string", length=10, nullable=false)
     */
    private $bloqueoD;

    /**
     * @var string
     *
     * @ORM\Column(name="drogueria", type="string", length=100, nullable=false)
     */
    private $drogueria;

    /**
     * @var string
     *
     * @ORM\Column(name="nit", type="string", length=100, nullable=false)
     */
    private $nit;

    /**
     * @var string
     *
     * @ORM\Column(name="bloqueo_r", type="string", length=100, nullable=false)
     */
    private $bloqueoR;

    /**
     * @var string
     *
     * @ORM\Column(name="asociado", type="string", length=100, nullable=false)
     */
    private $asociado;

    /**
     * @var string
     *
     * @ORM\Column(name="nit_dv", type="string", length=100, nullable=false)
     */
    private $nitDv;

    /**
     * @var string
     *
     * @ORM\Column(name="direcion", type="string", length=100, nullable=false)
     */
    private $direcion;

    /**
     * @var string
     *
     * @ORM\Column(name="cod_mun", type="string", length=100, nullable=false)
     */
    private $codMun;

    /**
     * @var string
     *
     * @ORM\Column(name="ciudad", type="string", length=100, nullable=false)
     */
    private $ciudad;

    /**
     * @var string
     *
     * @ORM\Column(name="ruta", type="string", length=100, nullable=false)
     */
    private $ruta;

    /**
     * @var string
     *
     * @ORM\Column(name="un_geogra", type="string", length=100, nullable=false)
     */
    private $unGeogra;

    /**
     * @var string
     *
     * @ORM\Column(name="depto", type="string", length=100, nullable=false)
     */
    private $depto;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=100, nullable=false)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="centro", type="string", length=100, nullable=false)
     */
    private $centro;

    /**
     * @var string
     *
     * @ORM\Column(name="p_s", type="string", length=100, nullable=false)
     */
    private $pS;

    /**
     * @var string
     *
     * @ORM\Column(name="p_carga", type="string", length=100, nullable=false)
     */
    private $pCarga;

    /**
     * @var string
     *
     * @ORM\Column(name="diskette", type="string", length=100, nullable=false)
     */
    private $diskette;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="cliente_tiempo", type="datetime", nullable=true)
     */
    private $clienteTiempo;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="email_asociado", type="string", length=100, nullable=false)
     */
    private $emailAsociado;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo_cliente", type="string", length=1, nullable=false)
     */
    private $tipoCliente;

    /**
     * @var string
     *
     * @ORM\Column(name="cupo_asociado", type="string", length=100, nullable=false)
     */
    private $cupoAsociado;

    /**
     * @var int
     *
     * @ORM\Column(name="estado", type="integer", nullable=false, options={"comment"="0:ya no existen,1:para envio"})
     */
    private $estado = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="centro_costo", type="string", length=100, nullable=true)
     */
    private $centroCosto;

    /**
     * @var int|null
     *
     * @ORM\Column(name="retirado", type="integer", nullable=true)
     */
    private $retirado = '0';

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getZona(): ?string
    {
        return $this->zona;
    }

    public function setZona(string $zona): self
    {
        $this->zona = $zona;

        return $this;
    }

    public function getCodigo(): ?int
    {
        return $this->codigo;
    }

    public function setCodigo(?int $codigo): self
    {
        $this->codigo = $codigo;

        return $this;
    }

    public function getBloqueoD(): ?string
    {
        return $this->bloqueoD;
    }

    public function setBloqueoD(string $bloqueoD): self
    {
        $this->bloqueoD = $bloqueoD;

        return $this;
    }

    public function getDrogueria(): ?string
    {
        return $this->drogueria;
    }

    public function setDrogueria(string $drogueria): self
    {
        $this->drogueria = $drogueria;

        return $this;
    }

    public function getNit(): ?string
    {
        return $this->nit;
    }

    public function setNit(string $nit): self
    {
        $this->nit = $nit;

        return $this;
    }

    public function getBloqueoR(): ?string
    {
        return $this->bloqueoR;
    }

    public function setBloqueoR(string $bloqueoR): self
    {
        $this->bloqueoR = $bloqueoR;

        return $this;
    }

    public function getAsociado(): ?string
    {
        return $this->asociado;
    }

    public function setAsociado(string $asociado): self
    {
        $this->asociado = $asociado;

        return $this;
    }

    public function getNitDv(): ?string
    {
        return $this->nitDv;
    }

    public function setNitDv(string $nitDv): self
    {
        $this->nitDv = $nitDv;

        return $this;
    }

    public function getDirecion(): ?string
    {
        return $this->direcion;
    }

    public function setDirecion(string $direcion): self
    {
        $this->direcion = $direcion;

        return $this;
    }

    public function getCodMun(): ?string
    {
        return $this->codMun;
    }

    public function setCodMun(string $codMun): self
    {
        $this->codMun = $codMun;

        return $this;
    }

    public function getCiudad(): ?string
    {
        return $this->ciudad;
    }

    public function setCiudad(string $ciudad): self
    {
        $this->ciudad = $ciudad;

        return $this;
    }

    public function getRuta(): ?string
    {
        return $this->ruta;
    }

    public function setRuta(string $ruta): self
    {
        $this->ruta = $ruta;

        return $this;
    }

    public function getUnGeogra(): ?string
    {
        return $this->unGeogra;
    }

    public function setUnGeogra(string $unGeogra): self
    {
        $this->unGeogra = $unGeogra;

        return $this;
    }

    public function getDepto(): ?string
    {
        return $this->depto;
    }

    public function setDepto(string $depto): self
    {
        $this->depto = $depto;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getCentro(): ?string
    {
        return $this->centro;
    }

    public function setCentro(string $centro): self
    {
        $this->centro = $centro;

        return $this;
    }

    public function getPS(): ?string
    {
        return $this->pS;
    }

    public function setPS(string $pS): self
    {
        $this->pS = $pS;

        return $this;
    }

    public function getPCarga(): ?string
    {
        return $this->pCarga;
    }

    public function setPCarga(string $pCarga): self
    {
        $this->pCarga = $pCarga;

        return $this;
    }

    public function getDiskette(): ?string
    {
        return $this->diskette;
    }

    public function setDiskette(string $diskette): self
    {
        $this->diskette = $diskette;

        return $this;
    }

    public function getClienteTiempo(): ?\DateTimeInterface
    {
        return $this->clienteTiempo;
    }

    public function setClienteTiempo(?\DateTimeInterface $clienteTiempo): self
    {
        $this->clienteTiempo = $clienteTiempo;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getEmailAsociado(): ?string
    {
        return $this->emailAsociado;
    }

    public function setEmailAsociado(string $emailAsociado): self
    {
        $this->emailAsociado = $emailAsociado;

        return $this;
    }

    public function getTipoCliente(): ?string
    {
        return $this->tipoCliente;
    }

    public function setTipoCliente(string $tipoCliente): self
    {
        $this->tipoCliente = $tipoCliente;

        return $this;
    }

    public function getCupoAsociado(): ?string
    {
        return $this->cupoAsociado;
    }

    public function setCupoAsociado(string $cupoAsociado): self
    {
        $this->cupoAsociado = $cupoAsociado;

        return $this;
    }

    public function getEstado(): ?int
    {
        return $this->estado;
    }

    public function setEstado(int $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getCentroCosto(): ?string
    {
        return $this->centroCosto;
    }

    public function setCentroCosto(?string $centroCosto): self
    {
        $this->centroCosto = $centroCosto;

        return $this;
    }

    public function getRetirado(): ?int
    {
        return $this->retirado;
    }

    public function setRetirado(?int $retirado): self
    {
        $this->retirado = $retirado;

        return $this;
    }


}
