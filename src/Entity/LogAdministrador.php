<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LogAdministrador
 *
 * @ORM\Table(name="log_administrador", indexes={@ORM\Index(name="fk_admin_logAdmin_idx", columns={"administrador_id"}), @ORM\Index(name="fk_admin_logAdmin_idx1", columns={"administrador_id"})})
 * @ORM\Entity
 */
class LogAdministrador
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="actividad", type="string", length=255, nullable=false)
     */
    private $actividad;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=25, nullable=false)
     */
    private $ip;

    /**
     * @var int
     *
     * @ORM\Column(name="administrador_id", type="integer", nullable=false)
     */
    private $administradorId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_ingreso", type="datetime", nullable=false)
     */
    private $fechaIngreso;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getActividad(): ?string
    {
        return $this->actividad;
    }

    public function setActividad(string $actividad): self
    {
        $this->actividad = $actividad;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getAdministradorId(): ?int
    {
        return $this->administradorId;
    }

    public function setAdministradorId(int $administradorId): self
    {
        $this->administradorId = $administradorId;

        return $this;
    }

    public function getFechaIngreso(): ?\DateTimeInterface
    {
        return $this->fechaIngreso;
    }

    public function setFechaIngreso(\DateTimeInterface $fechaIngreso): self
    {
        $this->fechaIngreso = $fechaIngreso;

        return $this;
    }


}
