<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CopiaEnvio
 *
 * @ORM\Table(name="copia_envio", indexes={@ORM\Index(name="fk_asociado_integranteGrupo_idx", columns={"asociado_id"}), @ORM\Index(name="fk_grupo_integranteGrupo_idx", columns={"envio_id"}), @ORM\Index(name="fk_proveedor_integranteGrupo_idx", columns={"proveedor_id"})})
 * @ORM\Entity
 */
class CopiaEnvio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="asociado_id", type="bigint", nullable=true)
     */
    private $asociadoId;

    /**
     * @var bool
     *
     * @ORM\Column(name="activo", type="boolean", nullable=false)
     */
    private $activo;

    /**
     * @var \Envio
     *
     * @ORM\ManyToOne(targetEntity="Envio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="envio_id", referencedColumnName="id")
     * })
     */
    private $envio;

    /**
     * @var int|null
     *
     * @ORM\Column(name="proveedor_id", type="integer", nullable=true)
     */
    private $proveedorId;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getAsociadoId(): ?string
    {
        return $this->asociadoId;
    }

    public function setAsociadoId(?string $asociadoId): self
    {
        $this->asociadoId = $asociadoId;

        return $this;
    }

    public function getActivo(): ?bool
    {
        return $this->activo;
    }

    public function setActivo(bool $activo): self
    {
        $this->activo = $activo;

        return $this;
    }

    public function getEnvio(): ?Envio
    {
        return $this->envio;
    }

    public function setEnvio(?Envio $envio): self
    {
        $this->envio = $envio;

        return $this;
    }

    public function getProveedorId(): ?string
    {
        return $this->proveedorId;
    }

    public function setProveedorId(?string $proveedorId): self
    {
        $this->proveedorId = $proveedorId;

        return $this;
    }


}
