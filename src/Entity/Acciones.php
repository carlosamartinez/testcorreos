<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Acciones
 *
 * @ORM\Table(name="acciones", indexes={@ORM\Index(name="menu_acciones", columns={"id_menu"})})
 * @ORM\Entity
 */
class Acciones
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=60, nullable=false)
     */
    private $label;

    /**
     * @var json
     *
     * @ORM\Column(name="iconos", type="json", nullable=false)
     */
    private $iconos;

    /**
     * @var string
     *
     * @ORM\Column(name="accion", type="string", length=60, nullable=false)
     */
    private $accion;

    /**
     * @var bool
     *
     * @ORM\Column(name="posicion", type="boolean", nullable=false)
     */
    private $posicion;

    /**
     * @var \Menu
     *
     * @ORM\ManyToOne(targetEntity="Menu")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_menu", referencedColumnName="id")
     * })
     */
    private $idMenu;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getIconos(): ?array
    {
        return $this->iconos;
    }

    public function setIconos(array $iconos): self
    {
        $this->iconos = $iconos;

        return $this;
    }

    public function getAccion(): ?string
    {
        return $this->accion;
    }

    public function setAccion(string $accion): self
    {
        $this->accion = $accion;

        return $this;
    }

    public function getPosicion(): ?bool
    {
        return $this->posicion;
    }

    public function setPosicion(bool $posicion): self
    {
        $this->posicion = $posicion;

        return $this;
    }

    public function getIdMenu(): ?Menu
    {
        return $this->idMenu;
    }

    public function setIdMenu(?Menu $idMenu): self
    {
        $this->idMenu = $idMenu;

        return $this;
    }


}
