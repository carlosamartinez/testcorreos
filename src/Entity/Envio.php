<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Envio
 *
 * @ORM\Table(name="envio", indexes={@ORM\Index(name="fk_administrador_envio_idx", columns={"administrador_id"}), @ORM\Index(name="fk_administrador_envioCopia", columns={"copia_administrador_id"}), @ORM\Index(name="FK_grupo_envio", columns={"grupo_id"})})
 * @ORM\Entity
 */
class Envio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_envio", type="datetime", nullable=true)
     */
    private $fechaEnvio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="asunto", type="string", length=255, nullable=true)
     */
    private $asunto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="contenido", type="text", length=65535, nullable=true)
     */
    private $contenido;

    /**
     * @var int
     *
     * @ORM\Column(name="cantidad_enviados", type="integer", nullable=false)
     */
    private $cantidadEnviados;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creado", type="datetime", nullable=false)
     */
    private $fechaCreado;

    /**
     * @var int
     *
     * @ORM\Column(name="estado", type="integer", nullable=false)
     */
    private $estado;

    /**
     * @var string|null
     *
     * @ORM\Column(name="combinacion", type="string", length=55, nullable=true)
     */
    private $combinacion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="columna_combinacion", type="string", length=55, nullable=true, options={"default"=" "})
     */
    private $columnaCombinacion = ' ';

    /**
     * @var int|null
     *
     * @ORM\Column(name="copia_administrador_id", type="integer", nullable=true)
     */
    private $copiaAdministradorId;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="adjuntos", type="boolean", nullable=true)
     */
    private $adjuntos;

    /**
     * @var int|null
     *
     * @ORM\Column(name="programado", type="integer", nullable=true)
     */
    private $programado;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cantidad_destinatarios", type="integer", nullable=true)
     */
    private $cantidadDestinatarios;

    /**
     * @var int|null
     *
     * @ORM\Column(name="errores", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $errores;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="administrador_id", referencedColumnName="id")
     * })
     */
    private $administrador;

    /**
     * @var \Grupo
     *
     * @ORM\ManyToOne(targetEntity="Grupo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="grupo_id", referencedColumnName="id")
     * })
     */
    private $grupo;

    /**
     * @var int
     *
     * @ORM\Column(name="agrupar_correspondencia", type="integer", nullable=false)
     */
    private $agruparCorrespondencia;

    /**
     * @var int
     *
     * @ORM\Column(name="agrupar_proveedor", type="integer", nullable=false)
     */
    private $agruparProveedor;

    /**
     * @var int
     *
     * @ORM\Column(name="enviar_copia", type="integer", nullable=false)
     */
    private $enviarCopia;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getFechaEnvio(): ?\DateTimeInterface
    {
        return $this->fechaEnvio;
    }

    public function setFechaEnvio(?\DateTimeInterface $fechaEnvio): self
    {
        $this->fechaEnvio = $fechaEnvio;

        return $this;
    }

    public function getAsunto(): ?string
    {
        return $this->asunto;
    }

    public function setAsunto(?string $asunto): self
    {
        $this->asunto = $asunto;

        return $this;
    }

    public function getContenido(): ?string
    {
        return $this->contenido;
    }

    public function setContenido(?string $contenido): self
    {
        $this->contenido = $contenido;

        return $this;
    }

    public function getCantidadEnviados(): ?int
    {
        return $this->cantidadEnviados;
    }

    public function setCantidadEnviados(int $cantidadEnviados): self
    {
        $this->cantidadEnviados = $cantidadEnviados;

        return $this;
    }

    public function getFechaCreado(): ?\DateTimeInterface
    {
        return $this->fechaCreado;
    }

    public function setFechaCreado(\DateTimeInterface $fechaCreado): self
    {
        $this->fechaCreado = $fechaCreado;

        return $this;
    }

    public function getEstado(): ?int
    {
        return $this->estado;
    }

    public function setEstado(int $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getCombinacion(): ?string
    {
        return $this->combinacion;
    }

    public function setCombinacion(?string $combinacion): self
    {
        $this->combinacion = $combinacion;

        return $this;
    }

    public function getColumnaCombinacion(): ?string
    {
        return $this->columnaCombinacion;
    }

    public function setColumnaCombinacion(?string $columnaCombinacion): self
    {
        $this->columnaCombinacion = $columnaCombinacion;

        return $this;
    }

    public function getCopiaAdministradorId(): ?int
    {
        return $this->copiaAdministradorId;
    }

    public function setCopiaAdministradorId(?int $copiaAdministradorId): self
    {
        $this->copiaAdministradorId = $copiaAdministradorId;

        return $this;
    }

    public function getAdjuntos(): ?bool
    {
        return $this->adjuntos;
    }

    public function setAdjuntos(?bool $adjuntos): self
    {
        $this->adjuntos = $adjuntos;

        return $this;
    }

    public function getProgramado(): ?int
    {
        return $this->programado;
    }

    public function setProgramado(?int $programado): self
    {
        $this->programado = $programado;

        return $this;
    }

    public function getCantidadDestinatarios(): ?int
    {
        return $this->cantidadDestinatarios;
    }

    public function setCantidadDestinatarios(?int $cantidadDestinatarios): self
    {
        $this->cantidadDestinatarios = $cantidadDestinatarios;

        return $this;
    }

    public function getErrores(): ?int
    {
        return $this->errores;
    }

    public function setErrores(?int $errores): self
    {
        $this->errores = $errores;

        return $this;
    }

    public function getAdministrador(): ?User
    {
        return $this->administrador;
    }

    public function setAdministrador(?User $administrador): self
    {
        $this->administrador = $administrador;

        return $this;
    }

    public function getGrupo(): ?Grupo
    {
        return $this->grupo;
    }

    public function setGrupo(?Grupo $grupo): self
    {
        $this->grupo = $grupo;

        return $this;
    }

    public function getAgruparCorrespondencia(): ?int
    {
        return $this->agruparCorrespondencia;
    }

    public function setAgruparCorrespondencia(int $agruparCorrespondencia): self
    {
        $this->agruparCorrespondencia = $agruparCorrespondencia;

        return $this;
    }

    public function getAgruparProveedor(): ?int
    {
        return $this->agruparProveedor;
    }

    public function setAgruparProveedor(int $agruparProveedor): self
    {
        $this->agruparProveedor = $agruparProveedor;

        return $this;
    }

    public function getEnviarCopia(): ?int
    {
        return $this->enviarCopia;
    }

    public function setEnviarCopia(?int $enviarCopia): self
    {
        $this->enviarCopia = $enviarCopia;

        return $this;
    }


}
