<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EnvioError
 *
 * @ORM\Table(name="envio_error", indexes={@ORM\Index(name="envio_id", columns={"envio_id"}), @ORM\Index(name="envio_integrante_id", columns={"envio_integrante_id"})})
 * @ORM\Entity
 */
class EnvioError
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="envio_id", type="bigint", nullable=false, options={"comment"="ID del envio"})
     */
    private $envioId;

    /**
     * @var int
     *
     * @ORM\Column(name="envio_integrante_id", type="bigint", nullable=false, options={"comment"="ID del destinatario donde se presenta la excepcion"})
     */
    private $envioIntegranteId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=false, options={"comment"="Fecha y hora en que se produce la excepcion"})
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="error", type="text", length=255, nullable=false, options={"comment"="Error general que reporta el servidor de correos"})
     */
    private $error;

    /**
     * @var bool
     *
     * @ORM\Column(name="reportado", type="boolean", nullable=false, options={"comment"="0: sin reportar al remitente - 1: Reportado al remitente"})
     */
    private $reportado;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getEnvioId(): ?string
    {
        return $this->envioId;
    }

    public function setEnvioId(string $envioId): self
    {
        $this->envioId = $envioId;

        return $this;
    }

    public function getEnvioIntegranteId(): ?string
    {
        return $this->envioIntegranteId;
    }

    public function setEnvioIntegranteId(string $envioIntegranteId): self
    {
        $this->envioIntegranteId = $envioIntegranteId;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getError(): ?string
    {
        return $this->error;
    }

    public function setError(string $error): self
    {
        $this->error = $error;

        return $this;
    }

    public function getReportado(): ?bool
    {
        return $this->reportado;
    }

    public function setReportado(bool $reportado): self
    {
        $this->reportado = $reportado;

        return $this;
    }


}
