<?php 
namespace App\Command;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\DependencyInjection\ContainerInterface; 
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;


class AntiguosCommand extends Command{
  
  private $em;
  private $params;

  public function __construct(ParameterBagInterface $params, EntityManagerInterface $entityManager, ContainerInterface $container)
  {
      $this->em = $entityManager;
      $this->container = $container;
      $this->params = $params;

      parent::__construct();
  }
  protected function configure(){
    parent::configure();
    $this->setName('antiguos:eliminar')->setDescription('Elimina correos antiguos.');
  }


  protected function execute(InputInterface $input, OutputInterface $output) {
    set_time_limit(0);
    ini_set('memory_limit', '2048M');

    $output->writeln("<question>INICIO DE ELIMINACION DE ENVIOS ANTIGUOS:</question>");

    $em = $this->em;
    $emConect = $em->getConnection();
    

    $fecha = date('Y-m-d H:m:s');
    //descontar los dias
    $fechaBorrar = strtotime('-100 day', strtotime($fecha));
    $fechaBorrar = date ( 'Y-m-d H:i:s' , $fechaBorrar );
    
    $envios = $em->createQuery("SELECT e FROM App\Entity\Envio e WHERE e.fechaCreado < :fecha")->setParameter('fecha', $fechaBorrar)->execute();

    $output->writeln('<comment>Consulta  SELECT e FROM App\Entity\Envio e WHERE e.fechaCreado < '.$fechaBorrar.'</comment>');
    $cont=0;
    foreach($envios as $envio){


        $directorioRaiz1= $this->params->get('kernel.project_dir').'/public/adjuntos/'.$envio->getId();
        //$directorioRaiz1="./adjuntos/".$envio->getId();

        if(is_dir($directorioRaiz1)){

            //$directorioRaiz = opendir("./adjuntos/".$envio->getId().'/');
            $directorioRaiz = opendir( $this->params->get('kernel.project_dir').'/public/adjuntos/'.$envio->getId().'/');

            $raiz = $this->params->get('kernel.project_dir').'/public/adjuntos/';

            while($archivo = readdir($directorioRaiz)){

                if($archivo != '.' && $archivo != '..'){

                  if(is_file($raiz.$envio->getId().'/'.$archivo)){
                      unlink($raiz.$envio->getId().'/'.$archivo); 
                  }else{
                      if(is_dir($raiz.$envio->getId().'/'.$archivo)){
                          $otroAdm = opendir($raiz.$envio->getId().'/'.$archivo.'/');
                          
                          if(is_dir($raiz.$envio->getId().'/'.$archivo.'/csv')){
                              $otroAdm = opendir($raiz.$envio->getId().'/'.$archivo.'/csv/');
                              while($archivoCombinacion = readdir($otroAdm)){
                                  if(is_file($raiz.$envio->getId().'/'.$archivo.'/csv/'.$archivoCombinacion)){
                                      unlink($raiz.$envio->getId().'/'.$archivo.'/csv/'.$archivoCombinacion);
                                  }
                              }
                              rmdir($raiz.$envio->getId().'/'.$archivo.'/csv');
                          }
                          
                          if(is_dir($raiz.$envio->getId().'/'.$archivo.'/otros')){
                              $otroAdm = opendir($raiz.$envio->getId().'/'.$archivo.'/otros');
                              while($archivoAdjunto = readdir($otroAdm)){
                                  if(is_file($raiz.$envio->getId().'/'.$archivo.'/otros/'.$archivoAdjunto)){
                                      unlink($raiz.$envio->getId().'/'.$archivo.'/otros/'.$archivoAdjunto);
                                  }
                              }
                              rmdir($raiz.$envio->getId().'/'.$archivo.'/otros');
                              
                          }
                          rmdir($raiz.$envio->getId().'/'.$archivo);
                      }
                  }

                }

                
            }
            rmdir($raiz.$envio->getId());
        }
        $sqlEliminaLog1="DELETE FROM columnas_datos_envio WHERE envio_id = '".$envio->getId()."'";
        $Eliminar1=$emConect->prepare($sqlEliminaLog1);
        $Eliminar1->execute();

        $sqlEliminaLog2="DELETE FROM datos_envio WHERE envio_id = '".$envio->getId()."'";
        $Eliminar2=$emConect->prepare($sqlEliminaLog2);
        $Eliminar2->execute();

        $sqlEliminaLog3="DELETE FROM copia_envio WHERE envio_id = '".$envio->getId()."'";
        $Eliminar3=$emConect->prepare($sqlEliminaLog3);
        $Eliminar3->execute();

        $sqlEliminaLog4="DELETE FROM envio_lectura WHERE envio_id = '".$envio->getId()."'";
        $Eliminar4=$emConect->prepare($sqlEliminaLog4);
        $Eliminar4->execute();

        $sqlEliminaLog5="DELETE FROM envio_integrante WHERE envio_id = '".$envio->getId()."'";
        $Eliminar5=$emConect->prepare($sqlEliminaLog5);
        $Eliminar5->execute();

        $sqlEliminaLog6="DELETE FROM envio WHERE id = '".$envio->getId()."'";
        $Eliminar6=$emConect->prepare($sqlEliminaLog6);
        $Eliminar6->execute();


        $output->writeln('<comment>Elimine el envio '.$envio->getId().' y todas sus dependencias</comment>');
        $cont++;
    }
    $output->writeln('<info>Tarea Terminada eliminando '.$cont.' envios.</info>');



    return Command::SUCCESS;
  }

}