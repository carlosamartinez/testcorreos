<?php 
namespace App\Command;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\DependencyInjection\ContainerInterface; 
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Twig\Environment;

use App\Services\EnvioMasivo;

class envioMailsCommand extends Command{
  
  private $em;
  private $envioMasivo;
  private $router;
  private $params;
  private $twig;
  public function __construct(Environment $twig, ParameterBagInterface $params, EntityManagerInterface $entityManager, ContainerInterface $container, RouterInterface $router, EnvioMasivo $envioMasivo)
  {
      $this->em = $entityManager;
      $this->container = $container;
      $this->router = $router;
      $this->params = $params;
      $this->twig = $twig;
      $this->envioMasivo = $envioMasivo;

      parent::__construct();
  }
  protected function configure(){
    parent::configure();
    $this->setName('envio:mails')->setDescription('Envio de correos programados');
  }
  public function proveedoresOrdenados($destinatariosEntity){
    $tempral="";
    foreach($destinatariosEntity as $destinatarioEntity){
      $destinatariosObj[$destinatarioEntity['id']]['id']=$destinatarioEntity['id'];
      $destinatariosObj[$destinatarioEntity['id']]['integrante_grupo_id']=$destinatarioEntity['integrante_grupo_id'];
      $destinatariosObj[$destinatarioEntity['id']]['envio_id']=$envio['id'];
      $destinatariosObj[$destinatarioEntity['id']]['asociado_id']=$destinatarioEntity['asociado_id'];
      $destinatariosObj[$destinatarioEntity['id']]['proveedor_id']=$destinatarioEntity['proveedor_id'];
      $destinatariosObj[$destinatarioEntity['id']]['contacto_proveedor_id']=$destinatarioEntity['contacto_proveedor_id'];
    }
  }
  //https://www.easy365manager.com/office-365-ndr-error-codes/
  //https://support.microsoft.com/es-es/topic/soluci%C3%B3n-de-problemas-de-mensajes-de-registro-de-errores-en-microsoft-outlook-mobile-manager-4630d2f9-2dcd-0474-4112-a8e926c6a903
  protected function execute(InputInterface $input, OutputInterface $output) {
    set_time_limit(0);
    ini_set('memory_limit', '2048M');
    $output->writeln("<question>INICIO DEL ENVÍO:</question>");
    $em = $this->em;
    $emConect = $em->getConnection();
    $em2 = $this->container->get('doctrine')->getManager('contactosProveedor');
    $emConectContactos = $em2->getConnection();
    $asociados = $this->envioMasivo->getDroguerias($emConect);
    $contactosProveedor = $this->envioMasivo->getContactosProvDBContactos($emConectContactos);
    $proveedorObj = $this->envioMasivo->getProveedoresDBContactos($emConectContactos);
    $envioPendiente = array();
    $destinatariosObj = array();
    $envios = $emConect->query("SELECT e.id, e.asunto, e.contenido, e.cantidad_enviados, e.combinacion, e.columna_combinacion, e.adjuntos, e.agrupar_correspondencia, e.agrupar_proveedor, e.enviar_copia, u.nombre AS nombreEnvia, u.email emailEnvia, u.fecha_creado, u.id AS idAdmin, g.clasificacion 
      FROM envio e LEFT JOIN user u ON u.id=e.administrador_id LEFT JOIN grupo g ON g.id=e.grupo_id WHERE e.estado = 1");
    $envioActivo=false;
    foreach($envios as $envio){
      $envioActivo=true;
      $envioPendiente[$envio['id']]['id']=$envio['id'];
      $envioPendiente[$envio['id']]['adjuntos']=$envio['adjuntos'];
      $envioPendiente[$envio['id']]['contenido']=$envio['contenido'];
      $envioPendiente[$envio['id']]['asunto']=$envio['asunto'];
      $envioPendiente[$envio['id']]['combinacion']=$envio['combinacion'];
      $envioPendiente[$envio['id']]['columna_combinacion']=$envio['columna_combinacion'];
      $envioPendiente[$envio['id']]['clasificacion']=$envio['clasificacion'];
      $envioPendiente[$envio['id']]['nombreEnvia']=$envio['nombreEnvia'];
      $envioPendiente[$envio['id']]['emailEnvia']=$envio['emailEnvia'];
      $envioPendiente[$envio['id']]['enviar_copia']=$envio['enviar_copia'];
      $envioPendiente[$envio['id']]['cantidad_enviados']=$envio['cantidad_enviados'];
      #Arreglo de adjuntos, si los hay.
      $adjuntos = array();
      if($envio['adjuntos'] > 0){
        $fecha = new \DateTime($envio['fecha_creado']);
        $fecha = $fecha->format('Ymd');
        $carpeta = $envio['id'].'/'.$fecha.$envio['idAdmin'].'/otros';
        $ruta = $this->params->get('kernel.project_dir').'/public/adjuntos/'.$carpeta;
        if(is_dir($ruta)){
          if($rep = opendir($ruta)){
            $na=0;
            while($arc = readdir($rep)){
              if($arc != '..' && $arc !='.' && $arc !=''){
                $adjuntos[ $na ] = $ruta.'/'.$arc; 
                $na++;
              }
            }
          }
        }
      }
      $envioPendiente[$envio['id']]['archivosAdjuntos']=$adjuntos;
      #Arreglo de destinatarios.
      $destinatariosEntity = $emConect->query("SELECT ei.id, ei.integrante_grupo_id, ig.asociado_id, ig.proveedor_id, ig.contacto_proveedor_id FROM envio_integrante ei JOIN integrante_grupo ig ON ig.id =  ei.integrante_grupo_id WHERE ei.enviado=0 AND ei.envio_id=".$envio['id']);
      $cont=0;
      foreach($destinatariosEntity as $destinatarioEntity){
        $destinatariosObj[$destinatarioEntity['id']]['id']=$destinatarioEntity['id'];
        $destinatariosObj[$destinatarioEntity['id']]['integrante_grupo_id']=$destinatarioEntity['integrante_grupo_id'];
        $destinatariosObj[$destinatarioEntity['id']]['envio_id']=$envio['id'];
        $destinatariosObj[$destinatarioEntity['id']]['asociado_id']=$destinatarioEntity['asociado_id'];
        $destinatariosObj[$destinatarioEntity['id']]['proveedor_id']=$destinatarioEntity['proveedor_id'];
        $destinatariosObj[$destinatarioEntity['id']]['contacto_proveedor_id']=$destinatarioEntity['contacto_proveedor_id'];
        $cont++;
      }$output->writeln("<question>".$cont."</question>");
      #Validación si hay agrupación de envio.
      if($envio['agrupar_correspondencia'] OR $envio['agrupar_proveedor']){
        $envioActivo=false;
        if($envio['agrupar_correspondencia']){
          $transport = (new \Swift_SmtpTransport($this->params->get('conectorEmail'), $this->params->get('puertoEmail'), $this->params->get('seguridadSSLEmail')))->setStreamOptions(array('ssl' => array('verify_peer' => false,'allow_self_signed' => true)));
          $mailer = new \Swift_Mailer($transport);
          $numeroDestinatario=0;
          $enviadosObjIds=array();
          $enviadosObjDesrtino=array();
          foreach($destinatariosObj as $destinatario){
            if($numeroDestinatario==0){
              $message = new \Swift_Message();
              $message->setSubject($envio['asunto']);
              if($this->params->get('emailDev')){
                $message->setFrom(array($this->params->get('emailPredeterminado') => $this->params->get('emailNombre')));
              }else{
                $message->setFrom(array($envio['emailEnvia'] => $envio['nombreEnvia']));
              }
            }
            $numeroDestinatario++;
            if($destinatario['asociado_id']){
              $dest = $asociados[$destinatario['asociado_id']];
              if($envio['clasificacion']=='Asociado'){//Envio para asociados
                if($this->params->get('emailDev')){
                  $message->addBcc($this->params->get('emailDevBuzon'), $this->params->get('emailDevNombre'));                  
                }else{
                  $message->addBcc($dest['email_asociado'], $dest['asociado']);
                  $enviadosObjIds[]=$destinatario['id'];
                  $enviadosObjDesrtino[]=$dest['asociado'].'<'.$dest['email_asociado'].'>';
                }
                $dest['tipo']='Asociado';
              }else{
                if($this->params->get('emailDev')){
                  $message->addBcc($this->params->get('emailDevBuzon'), $this->params->get('emailDevNombre'));
                }else{
                  $message->addBcc($dest['email'], $dest['drogueria']);
                  $enviadosObjIds[]=$destinatario['id'];
                  $enviadosObjDesrtino[]=$dest['drogueria'].'<'.$dest['email'].'>';
                }
                $dest['tipo']='Drogueria';

                // se envia copia al creador del envio
                if($envio['enviar_copia'] == 1){
                  $message->addBcc($envio['emailEnvia'], $envio['nombreEnvia']);
                }

              }
              if($numeroDestinatario==$this->params->get('limiteEnvioDestinatarios')){
                $numeroDestinatario=0;
                $template = str_replace('../../../uploads/images', $this->params->get('carpetaUploads'),
                  $this->twig->render('envioDestinatario/email_masivo.html.twig',array('mensaje'=>$envio['contenido'],'destinatario'=>$dest,'destinatarioId'=>$destinatario['integrante_grupo_id'],'idEnvio'=>$envio['id'],'urlLectura'=>'')));
                $message->setBody($template, 'text/html');
                if($envio['adjuntos'] > 0){
                  foreach ($envioPendiente[$envio['id']]['archivosAdjuntos'] as $path) {
                    $attachment = \Swift_Attachment::fromPath($path);
                    $message->attach($attachment);
                  }
                }
                try{
                  $mailer->send($message);
                  //Marcar como enviado
                  if(!$this->params->get('emailDev')){
                    $sqlUpdate = "UPDATE envio SET cantidad_enviados=cantidad_enviados+".COUNT($enviadosObjIds)." WHERE id=".$envio['id'];
                    $emConect->query($sqlUpdate);
                    $sqlUpdate = "";
                    foreach ($enviadosObjIds as $enviadoId) {
                      $sqlUpdate .= "UPDATE envio_integrante SET enviado='1', fecha_envio='".date('Y-m-d H:i:s')."' WHERE id=".$enviadoId;
                    }
                    if($sqlUpdate!=""){
                      $emConect->query($sqlUpdate);
                    }
                  }
                }catch(\Exception $e){
                  #No es posibel marcar errores
                }
              }
            }
          }//Foreach destinatarios
          if($numeroDestinatario>0){
            $template = str_replace('../../../uploads/images', $this->params->get('carpetaUploads'),
              $this->twig->render('envioDestinatario/email_masivo.html.twig',array('mensaje'=>$envio['contenido'],'destinatario'=>$dest,'destinatarioId'=>$destinatario['integrante_grupo_id'],'idEnvio'=>$envio['id'],'urlLectura'=>'')));
            $message->setBody($template, 'text/html');
            if($envio['adjuntos'] > 0){
              foreach ($envioPendiente[$envio['id']]['archivosAdjuntos'] as $path) {
                $attachment = \Swift_Attachment::fromPath($path);
                $message->attach($attachment);
              }
            }
            try{
              $mailer->send($message);
              //Marcar como enviado
              if(!$this->params->get('emailDev')){
                $sqlUpdate = "UPDATE envio SET cantidad_enviados=cantidad_enviados+".COUNT($enviadosObjIds)." WHERE id=".$envio['id'];
                $emConect->query($sqlUpdate);
                $sqlUpdate = "";
                foreach ($enviadosObjIds as $enviadoId) {
                  $sqlUpdate .= "UPDATE envio_integrante SET enviado='1', fecha_envio='".date('Y-m-d H:i:s')."' WHERE id=".$enviadoId.";";
                }
                if($sqlUpdate!=""){
                  $emConect->query($sqlUpdate);
                }
              }
            }catch(\Exception $e){
              #No es posibel marcar errores
            }
          }//Fi envio destinatarios mayor a 0


          //----- notificacion de fin de envio ------
            //$this->notificacionEnvioTerminado($envio,$emConect);
            $finEnvio = $this->verificacionFinDeEnvio($envio['id'], $emConect);
            if($finEnvio){
              $output->writeln("<question>ENVIO COMPLETO</question>");
              $this->notificacionEnvioTerminado($envio,$emConect);
            }else{
              $output->writeln("<question>Envio incompleto</question>");
            }

          //-----------------------

        }else{
          $output->writeln("<info>Agrupa para Proveedor</info>");
          #Proveedores
          $ProveedoresIds=$emConect->query("SELECT DISTINCT ig.proveedor_id FROM envio_integrante ei LEFT JOIN integrante_grupo ig ON ig.id = ei.integrante_grupo_id WHERE ei.enviado=0 AND ei.envio_id=".$envio['id']);
          $transport = (new \Swift_SmtpTransport($this->params->get('conectorEmail'), $this->params->get('puertoEmail'), $this->params->get('seguridadSSLEmail')))->setStreamOptions(array('ssl' => array('verify_peer' => false,'allow_self_signed' => true)));
          
          foreach ($ProveedoresIds as $proveedor) {
            //var_dump($proveedor);exit;
            $output->writeln("<error>".$proveedor["proveedor_id"]."</error>");
            $mailer = new \Swift_Mailer($transport);
            $message = new \Swift_Message();
            $message->setSubject($envio['asunto']);
            if($this->params->get('emailDev')){
              $output->writeln("<error>Fron DEv</error>");
              $message->setFrom(array($this->params->get('emailPredeterminado') => $this->params->get('emailNombre')));
            }else{
              $message->setFrom(array($envio['emailEnvia'] => $envio['nombreEnvia']));
            }
            $enviadosObjIds=array();
            $enviadosObjIdsProv=array();
            $enviadosObjDesrtino=array();
            foreach($destinatariosObj as $destinatario){

              if($destinatario['proveedor_id']==$proveedor["proveedor_id"]){
                ///$output->writeln("<info>".count($destinatariosObj).'- '.$destinatario['proveedor_id'].'<'.$proveedor["proveedor_id"].'>'."</info>");
                if(!$destinatario['contacto_proveedor_id']){
                  
                  if( isset($proveedorObj[$destinatario['proveedor_id']]) ){
                    $dest = $proveedorObj[$destinatario['proveedor_id']];
                  }else{
                    continue;
                  }
                }else{
                  
                  if( isset($contactosProveedor[$destinatario['contacto_proveedor_id']]) ){
                    $dest = $contactosProveedor[$destinatario['contacto_proveedor_id']];

                    $dest['tipo'] ='Proveedor';
                    $dest['id'] = $dest['id_proveedor'];
                  }else{

                    $enviadosObjIds[]=$destinatario['id'];
                    $enviadosObjIdsProv[$proveedor["proveedor_id"]]=$destinatario['id'];
                    continue;
                  }
                }
                if($this->params->get('emailDev')){
                  $message->addCc($this->params->get('emailDevBuzon'), $this->params->get('emailDevNombre'));
                  continue;
                }else{
                  if(filter_var($dest['email'], FILTER_VALIDATE_EMAIL)){
                    $message->addCc($dest['email'], $dest['nombre']);
                  }
                  $enviadosObjIds[]=$destinatario['id'];
                  $enviadosObjIdsProv[$proveedor["proveedor_id"]]=$destinatario['id'];
                  $enviadosObjDesrtino[]=$dest['nombre'].'<'.$dest['email'].'>';
                  //$output->writeln("<info>".$dest['nombre'].'<'.$dest['email'].'>'."</info>");
                }
                //$enviadosObjIds[]=$destinatario['id'];
                //$enviadosObjIdsProv[]=$destinatario['id'];
                //$enviadosObjDesrtino[]=$dest['nombre'].'<'.$dest['email'].'>';
                unset($destinatariosObj[$destinatario['id']]);
              }//cierre if proveedor
            }//Cierre foreach destinatarios


            // se envia copia al creador del envio
            if($envio['enviar_copia'] == 1){
              $message->addBcc($envio['emailEnvia'], $envio['nombreEnvia']);
            }

            $contenido=$envio['contenido'];
            $permitidos=array('Nit','empresa','Codigo','direccion','telefono','fax','celular','clasificacion', 'nombre', 'cargo','email','codigo','nit');
            foreach($permitidos as $campo){
              if(isset($dest[$campo])){
                $contenido = str_replace("//$campo//", utf8_decode($dest[$campo]), $contenido);
              }
            }

            if($envio['combinacion']){
              $contenido = $this->envioMasivo->combinarCorrespondencia($envio, $dest, $contenido, $emConect);
            }

            $urlLectura = $this->params->get('urlApp').$this->router->generate('admin_envio_destinatario_lectura', ['idEnvio' => $envio['id'],'Tipo'=>$dest['tipo'],'destinatarioId'=>$dest['id']]);
            $template = str_replace('../../../uploads/images', $this->params->get('carpetaUploads'),$this->twig->render('envioDestinatario/email_masivo.html.twig',array('mensaje'=>$contenido,'destinatario'=>$dest,'destinatarioId'=>$destinatario['integrante_grupo_id'],'idEnvio'=>$envio['id'],'urlLectura'=>$urlLectura)));
            if($envio['adjuntos'] > 0){
              foreach ($envioPendiente[$envio['id']]['archivosAdjuntos'] as $path) {
                $attachment = \Swift_Attachment::fromPath($path);
                $message->attach($attachment);
              }
            }
            //$message->addBcc('alejandro@waplicaciones.co', 'Alejandro Ardila Ardila');
            
            //$message->addBcc($envio['emailEnvia'], $envio['nombreEnvia']);

            $message->setBody($template, 'text/html');
            try{
              $mailer->send($message);
              if(!$this->params->get('emailDev')){
                $sqlUpdate = "UPDATE envio SET cantidad_enviados=cantidad_enviados+".COUNT($enviadosObjIds)." WHERE id=".$envio['id'];
                $emConect->query($sqlUpdate);
                $sqlUpdate = "";
                foreach ($enviadosObjIds as $enviadoId) {
                  $sqlUpdate .= "UPDATE envio_integrante SET enviado='1', fecha_envio='".date('Y-m-d H:i:s')."' WHERE id=".$enviadoId.";";
                }
                if($sqlUpdate!=""){
                  $emConect->query($sqlUpdate);
                }
                unset($enviadosObjIds);
              }
              $output->writeln("<info>envia maail</info>");
            }catch(\Exception $e){
              $output->writeln("<info>Produce error</info>");
              #@@@@@@Hace falta definir bajo que ID se manje ates error....
              $sqlError = "INSERT INTO envio_error (envio_id, envio_integrante_id, fecha, error, reportado) VALUES ";
              $valuesError = "('".$envio['id']."','".$destinatario['id']."','".date('Y-m-d H:i:s')."','". addslashes($e->getMessage())."','0')";
              ##Auditoria Errores
              $pos = strpos($e->getMessage(), "Expected response code ");
              if($pos!==false){
                exit;
              }
              $emConect->query($sqlError.$valuesError);
            }
            unset($mailer);
          }//Cierre Foreach Proveedores


          //----- notificacion de fin de envio ------
            //$this->notificacionEnvioTerminado($envio,$emConect);
            $finEnvio = $this->verificacionFinDeEnvio($envio['id'], $emConect);
            if($finEnvio){
              $output->writeln("<question>ENVIO COMPLETO</question>");
              $this->notificacionEnvioTerminado($envio,$emConect);
            }else{
              $output->writeln("<question>Envio incompleto</question>");
            }
          //---------

        }//cierre else agrupar correspondencia
      }
    }
    #Si hay envios pendites se consultas los destinatarios pendientes por recibir
    
    if($envioActivo){
      $output->writeln("<info>Si hay envios</info>");
      $final=count($destinatariosObj);
      $transport = (new \Swift_SmtpTransport($this->params->get('conectorEmail'), $this->params->get('puertoEmail'), $this->params->get('seguridadSSLEmail')))
        ->setStreamOptions(array('ssl' => array(
          'verify_peer' => false,
          'allow_self_signed' => true
       )));
      $mailer = new \Swift_Mailer($transport);
      $enviadosObj = array();
      $erroresEnvio = array();
      for($j=0;$j<$final;$j++){
        $claveAleatoria = array_rand($destinatariosObj, 1);
        $destinatario = $destinatariosObj[$claveAleatoria];
        $envio = $envioPendiente[$destinatario['envio_id']];
        $contenido = $envio['contenido'];
        if($destinatario['asociado_id']){
          //Se hace envio al asociado
          $infoAsociado = $asociados[$destinatario['asociado_id']];
          $permitidos=array('zona','codigo','drogueria','nit','asociado','direcion','ciudad','ruta','depto','telefono','centro','email','emailAsociado', 'email_asociado', 'departamento', 'direccion');
          foreach($permitidos as $campo){
            if(isset($infoAsociado[$campo])){
              $contenido = str_replace("//$campo//", utf8_decode($infoAsociado[$campo]), $contenido);
            } 
          }
          if($envio['clasificacion'] == 'Asociados'){
            $dest['email']=$infoAsociado['email_asociado'];
            $dest['nombre']=$infoAsociado['asociado'];
            $dest['Codigo']=$infoAsociado['codigo'];
            $dest['Nit']=$infoAsociado['nit'];
            $dest['id']=$infoAsociado['id'];
            $dest['tipo']='Drogueria';
          }else{
            $dest['email']=$infoAsociado['email'];
            $dest['nombre']=$infoAsociado['drogueria'];
            $dest['Codigo']=$infoAsociado['codigo'];
            $dest['Nit']=$infoAsociado['nit'];
            $dest['id']=$infoAsociado['id'];
            $dest['tipo']='Asociado';
          }
        }else{
          if(!$destinatario['contacto_proveedor_id']){
            $output->writeln("<info>".$destinatario['proveedor_id']."</info>");
            $dest = $proveedorObj[$destinatario['proveedor_id']];
          }else{
            $output->writeln("<info>".$destinatario['contacto_proveedor_id']."</info>");
            
            if( isset( $contactosProveedor[$destinatario['contacto_proveedor_id']] ) ){
              $dest = $contactosProveedor[$destinatario['contacto_proveedor_id']];
            }else{
              continue;
            }
          }
          $permitidos=array('nit','nombre','empresa','cargo','telefono','celular','email1', 'email');
          foreach($permitidos as $campo){
            if(isset($dest[$campo])){
              $contenido = str_replace("//$campo//", utf8_decode($dest[$campo]), $contenido);
            }
          }
        }
        if($envio['combinacion']){
          $contenido = $this->envioMasivo->combinarCorrespondencia($envio, $dest, $contenido, $emConect);
        }
        $message = new \Swift_Message();
        $message->setSubject($envio['asunto']);
        if($this->params->get('emailDev')){
          $message->setFrom(array($this->params->get('emailPredeterminado') => $this->params->get('emailNombre')));
        }else{
          $message->setFrom(array($envio['emailEnvia'] => $envio['nombreEnvia']));
        }
        if($envio['clasificacion']=='Asociado'){//Envio para asociados
          if($this->params->get('emailDev')){
            $message->addBcc($this->params->get('emailDevBuzon'), $this->params->get('emailDevNombre'));
          }else{
            $message->addBcc($dest['email_asociado'], $dest['asociado']);
            $enviadosObjIds[]=$destinatario['id'];
            $enviadosObjDesrtino[]=$dest['asociado'].'<'.$dest['email_asociado'].'>';
          }
        }else{
          if($this->params->get('emailDev')){
            $message->addBcc($this->params->get('emailDevBuzon'), $this->params->get('emailDevNombre'));
          }else{
            $message->addBcc($dest['email'], $dest['drogueria']);
            $enviadosObjIds[]=$destinatario['id'];
            $enviadosObjDesrtino[]=$dest['drogueria'].'<'.$dest['email'].'>';

            $message->addBcc($envio['emailEnvia'], $envio['nombreEnvia']);
            
          }
        }

        // se envia copia al creador del envio
        if($envio['enviar_copia'] == 1){
          $message->addBcc($envio['emailEnvia'], $envio['nombreEnvia']);
        }
                
        $urlLectura = $this->params->get('urlApp').$this->router->generate('admin_envio_destinatario_lectura', ['idEnvio' => $envio['id'],'Tipo'=>$dest['tipo'],'destinatarioId'=>$dest['id']]);
        $template = str_replace('../../../uploads/images', $this->params->get('carpetaUploads'),
          $this->twig->render('envioDestinatario/email_masivo.html.twig'
          ,array(
            'mensaje'=>$contenido,
            'destinatario'=>$dest,
            'destinatarioId'=>$destinatario['integrante_grupo_id'],
            'idEnvio'=>$envio['id'],
            'urlLectura'=>$urlLectura
          )));
        $message->setBody($template, 'text/html');
        if($envio['adjuntos'] > 0){
          foreach ($envio['archivosAdjuntos'] as $path) {
            $attachment = \Swift_Attachment::fromPath($path);
            $message->attach($attachment);
          }
        }
        try{
          $mailer->send($message);
          if(!$this->params->get('emailDev')){
            $sqlUpdate = "UPDATE envio SET cantidad_enviados=cantidad_enviados+1 WHERE id=".$envio['id'];
            $emConect->query($sqlUpdate);
            $sqlUpdate = "UPDATE envio_integrante SET enviado='1', fecha_envio='".date('Y-m-d H:i:s')."' WHERE id=".$destinatario['id'];
            $emConect->query($sqlUpdate);
          }
          unset($destinatariosObj[$claveAleatoria]);
          $enviadosObj[$envio['id']]['id']=$envio['id'];
          $enviadosObj[$envio['id']]['enviado']=$dest['nombre']." - ".$dest['email'];
        }catch(\Exception $e){
          $sqlError = "INSERT INTO envio_error (envio_id, envio_integrante_id, fecha, error, reportado) VALUES ";
          $valuesError = "('".$envio['id']."','".$destinatario['id']."','".date('Y-m-d H:i:s')."','".addslashes($e->getMessage())."','0')";
          ##Auditoria Errores
          $emConect->query($sqlError.$valuesError);
        }


        //----- Verificacion de fin de Envio-----

          $finEnvio = $this->verificacionFinDeEnvio($envio['id'], $emConect);
          if($finEnvio){
            $output->writeln("<question>ENVIO COMPLETO</question>");
            $this->notificacionEnvioTerminado($envio,$emConect);
          }else{
            $output->writeln("<question>Envio incompleto</question>");
          }

        //--------


      }
    }
    $output->writeln("<question>FIN DE LA TAREA</question>");
    return Command::SUCCESS;
  }



  public function verificacionFinDeEnvio($idEnvio, $emConect){
    
    $sql= "SELECT COUNT(ei.id) AS cantPendientes 
      FROM envio_integrante ei 
      JOIN integrante_grupo ig ON ei.integrante_grupo_id = ig.id 
      WHERE ei.envio_id='".$idEnvio."' AND ei.enviado=0 ";

    $cantPendientes = $emConect->query($sql)->fetchAll()[0];

    if($cantPendientes['cantPendientes'] > 0){
      return false;
    }else{

      $sqlUpdate = "UPDATE envio SET estado=2 WHERE id=".$idEnvio;
      $emConect->query($sqlUpdate);

      return true;
    }


  }



  public function notificacionEnvioTerminado($envio,$emConect){

    $sqlCantEnviados = "SELECT cantidad_enviados FROM envio WHERE id=".$envio['id'];
    $cantEnviados = $emConect->query($sqlCantEnviados)->fetchAll()[0];
    
    $transport = (new \Swift_SmtpTransport($this->params->get('conectorEmail'), $this->params->get('puertoEmail'), $this->params->get('seguridadSSLEmail')))
    ->setStreamOptions(array(
      'ssl' => array(
        'verify_peer' => false,
        'allow_self_signed' => true
      )
    ));

    $mailer = new \Swift_Mailer($transport);

    $message = new \Swift_Message();
    $message->setSubject("Envio Terminado");

    if($this->params->get('emailDev')){
      $message->setFrom(array($this->params->get('emailPredeterminado') => $this->params->get('emailNombre')));
    }else{
      $message->setFrom(array($envio['emailEnvia'] => $envio['nombreEnvia']));
    }

    //$imageLogo = $message->embed(\Swift_Image::fromPath('images/logo-correos.png'));
    $imageLogo = "";

    $message->setTo($envio['emailEnvia'], $envio['nombreEnvia']);
    $message->addCc($envio['emailEnvia'], $envio['nombreEnvia']);
    $message->addBcc('alejandro@waplicaciones.co', 'Alejandro Ardila Ardila');

    $template = $this->twig->render('envio/email_envio_terminado.html.twig',array(
      'headerImg' =>$imageLogo,
      'nombre' =>$envio['nombreEnvia'],
      'nombreEnvio' =>$envio['asunto'],
      'nDestinatarios' => $cantEnviados['cantidad_enviados'],
    ));
    $message->setBody($template, 'text/html');

    try{
      $mailer->send($message);

    }catch(\Exception $e){
      echo $e->getMessage();
    }


  }




}