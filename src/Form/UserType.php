<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class UserType extends AbstractType{

  /**
   * @param FormBuilderInterface $builder
   * @param array $options
  */
  public function buildForm(FormBuilderInterface $builder, array $options){
      
    $builder
      ->add('usuario', TextType::class, array(
        'label' => 'Usuario',
        'label_attr' => ['class' => 'font-weight-bold'],
        'attr' => array(
          'class' => 'form-control form-control-sm',
          'required' => true
        )
      ))
      ->add('nombre', TextType::class, array(
        'label' => 'Nombre',
        'label_attr' => ['class' => 'font-weight-bold'],
        'attr' => array(
          'class' => 'form-control form-control-sm',
          'required' => true
        )
      ))
      ->add('email', EmailType::class, array(
        'label' => 'Correo Electrónico',
        'label_attr' => ['class' => 'font-weight-bold'],
        'attr' => array(
          'class' => 'form-control form-control-sm',
          'required' => true
        )
      ))
      /*->add('claveEmail', TextType::class, array(
        'label' => 'Clave De Correo Electrónico',
        'label_attr' => ['class' => 'font-weight-bold'],
        'attr' => array(
          'class' => 'form-control form-control-sm',
          'required' => true
        )
      ))*/
      ->add('estado', ChoiceType::class, array(
        'label' => 'Estado',
        'label_attr' => ['class' => 'font-weight-bold'],
        'attr' => array(
          'class' => 'form-control form-control-sm',
          'required' => true
        ),
        'choices' => array(
          'Activo'  => '1',
          'Inactivo'  => '2'
        ),
      ))
      ->add('tipoUsuario', ChoiceType::class, array(
        'label' => 'Tipo de usuario',
        'label_attr' => ['class' => 'font-weight-bold'],
        'attr' => array(
          'class' => 'form-control form-control-sm',
          'required' => true
        ),
        'choices' => array(
          'Administrador'  => '1',
          'Usuario'  => '0'
        ),
      ))
      ->add('save', SubmitType::class, array(
        'attr' => array('class' => 'btn btn-success'),
      ))
    ;
  }

   /**
    * @param OptionsResolver $resolver
   */
   public function configureOptions(OptionsResolver $resolver){
      $resolver->setDefaults([
         'data_class' => User::class,
         'attr' => array('id' => 'formUser'),
      ]);
   }
}