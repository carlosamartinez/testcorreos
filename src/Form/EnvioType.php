<?php

namespace App\Form;

use App\Entity\Envio;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class EnvioType extends AbstractType{

  /**
   * @param FormBuilderInterface $builder
   * @param array $options
  */
  public function buildForm(FormBuilderInterface $builder, array $options){
      
    $builder
      ->add('asunto')
      //->add('contenido')
      ->add('contenido', TextareaType::class, array(
        'label' => 'Contenido',
        'label_attr' => ['class' => 'font-weight-bold'],
        'attr' => array(
          'class' => 'form-control form-control-sm',
          'rows'  => '5',
          'required' => true
        )
      ))
      //->add('estado')
      /*->add('estado', ChoiceType::class, array(
        'choices'  => array(
            'Programado' => 1,
            'Activo' => 0
          )
        ))*/
      ->add('adjuntos')
      ->add('grupo',EntityType::class,array(
        'class' => 'App\Entity\Grupo',
        'choice_label' => 'nombre',
        'choice_attr' => function($choice, $key, $value) {
            // adds a class like attending_yes, attending_no, etc
            return ['data-tipo' => $choice->getClasificacion()];
        },
        'query_builder' => function (EntityRepository $er) use ( $options ) {
          return $er->createQueryBuilder('g')
          ->where('g.administrador IN ('.$options['idAdministrador'].')')
          ->orderBy('g.nombre', 'ASC');
        }
        ))
      /*->add('administrador',EntityType::class,array(
        'class' => 'App\Entity\User',
        'choice_label' => 'nombre',
        'query_builder' => function (EntityRepository $er) {
          return $er->createQueryBuilder('a')
          ->orderBy('a.nombre', 'ASC');
        }
        ))*/
      ->add('columnaCombinacion', ChoiceType::class, array(
          'choices'  => array(
              'No aplica' => null,
              'Codigo' => 'Codigo',
              'Nit' => 'Nit'
            )
          ))

      ->add('save', SubmitType::class, array(
        // 'label'    => 'Guardar',
        'attr' => array('class' => 'btn btn-success'),
      ))
    ;
  }

   /**
    * @param OptionsResolver $resolver
   */
   public function configureOptions(OptionsResolver $resolver){
      $resolver->setRequired('idAdministrador');
      $resolver->setDefaults([
         'data_class' => Envio::class,
         'idAdministrador' => null,
         'attr' => array('id' => 'formEnvio'),
      ]);
   }
}