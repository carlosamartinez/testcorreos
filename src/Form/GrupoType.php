<?php

namespace App\Form;

use App\Entity\Grupo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class GrupoType extends AbstractType{
    
  /**
   * @param FormBuilderInterface $builder   
   * @param array $options
  */
  public function buildForm(FormBuilderInterface $builder, array $options){
      
    $builder
        ->add('id', HiddenType::class)
        ->add('nombre', TextType::class, array(
            'label' => 'Nombre del Grupo',
            'label_attr' => ['class' => 'font-weight-bold'],
            'attr' => array(
                'class' => 'form-control form-control-sm',
                'required' => true
            )
        ))
        ->add('clasificacion', ChoiceType::class, array(
            'label' => 'Clasificación',
            'label_attr' => ['class' => 'font-weight-bold'],
            'attr' => array(
                'class' => 'form-control form-control-sm',
                'required' => true
            ),
            'choices' => array(
                'Droguerías'  => 'Droguerías',
                'Asociados'  => 'Asociados',
                'No Aplica'  => 'No Aplica'
            )
        ))
        ->add('integrantes', NumberType::class, array(
                'label' => 'Integrantes',
                'label_attr' => ['class' => 'font-weight-bold'],
                'attr' => array(
                    'class' => 'form-control form-control-sm',
                    'required' => true
                )
        ))
        ->add('descripcion', TextType::class, array(
                'label' => 'Descripción',
                'label_attr' => ['class' => 'font-weight-bold'],
                'attr' => array(
                    'class' => 'form-control form-control-sm',
                    'required' => true
                )
        ))
        ->add('save', SubmitType::class, array(
                'attr' => array('class' => 'btn btn-success'),
        ))
    ;
  }

   /**
    * @param OptionsResolver $resolver
   */
   public function configureOptions(OptionsResolver $resolver){
      $resolver->setDefaults([
         'data_class' => Grupo::class,
         'attr' => array('id' => 'formGrupo'),
      ]);
   }
}