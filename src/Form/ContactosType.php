<?php

namespace App\Form;

use App\Entity\Contactos;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ContactosType extends AbstractType{

  /**
   * @param FormBuilderInterface $builder
   * @param array $options
  */
  public function buildForm(FormBuilderInterface $builder, array $options){
      
    $builder
        ->add('nombreContacto', TextType::class, array(
          'label' => 'Nombre',
          'attr' => array(
            'class' => 'form-control form-control-sm',
            'required' => true
          )
        ))
        ->add('ciudad', TextType::class, array(
          'label' => 'Ciudad',
          'attr' => array(
            'class' => 'form-control form-control-sm',
            'required' => true
          )
        ))
        ->add('email', TextType::class, array(
          'label' => 'Email',
          'attr' => array(
            'class' => 'form-control form-control-sm',
            'required' => true
          )
        ))
        ->add('telefono', TextType::class, array(
          'label' => 'Telefono',
          'attr' => array(
            'class' => 'form-control form-control-sm',
            'required' => true
          )
        ))
        ->add('movil', TextType::class, array(
          'label' => 'Celular',
          'attr' => array(
            'class' => 'form-control form-control-sm',
            'required' => true
          )
        ))
        ->add('idProveedor', EntityType::class, array(
            'class' => 'App\Entity\Proveedores',
            'em' => 'contactosProveedor',
            'query_builder' => function (EntityRepository $e) use($options) {
                return $e->createQueryBuilder('p')
                    ->where('p.id = :id')
                    ->setParameter('id', $options['idProveedor']);
            },
            'choice_label' => 'nombre',
            'label' => 'Proveedor',
            'attr' => array(
              'class' => 'form-control form-control-sm'
            )
        ))
        ->add('idCargo', EntityType::class, array(
            'class' => 'App\Entity\Cargos',
            'em' => 'contactosProveedor',
            'choice_label' => 'nombre',
            'label' => 'Cargo',
            'attr' => array(
              'class' => 'form-control form-control-sm'
            )
        ))
        ->add('idArea', EntityType::class, array(
            'class' => 'App\Entity\Areas',
            'em' => 'contactosProveedor',
            'choice_label' => 'nombre',
            'label' => 'Area',
            'attr' => array(
              'class' => 'form-control form-control-sm'
            )
        ))
        ->add('save', SubmitType::class, array(
          // 'label'    => 'Guardar',
          'attr' => array('class' => 'btn btn-success'),
        ))
    ;
  }

   /**
    * @param OptionsResolver $resolver
   */
   public function configureOptions(OptionsResolver $resolver){
      $resolver->setDefaults([
         'data_class' => Contactos::class,
         'idProveedor' => null,
         'attr' => array('id' => 'formContactos'),
      ]);
   }
}