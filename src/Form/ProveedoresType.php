<?php

namespace App\Form;

use App\Entity\Proveedores;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ProveedoresType extends AbstractType{

  /**
   * @param FormBuilderInterface $builder
   * @param array $options
  */
  public function buildForm(FormBuilderInterface $builder, array $options){
      
    $builder
        ->add('nit', TextType::class, array(
          'label' => 'NIT',
          'attr' => array(
            'class' => 'form-control form-control-sm',
            'required' => true
          )
        ))
        ->add('codigo', TextType::class, array(
          'label' => 'Código',
          'attr' => array(
            'class' => 'form-control form-control-sm',
            'required' => true
          )
        ))
        ->add('nombre', TextType::class, array(
          'label' => 'Nombre-Razón Social',
          'attr' => array(
            'class' => 'form-control form-control-sm',
            'required' => true
          )
        ))
        ->add('representanteLegal', TextType::class, array(
          'label' => 'Representante Legal',
          'attr' => array(
            'class' => 'form-control form-control-sm',
            'required' => true
          )
        ))
        ->add('emailRepresentanteLegal', TextType::class, array(
          'label' => 'Email Representante Legal',
          'attr' => array(
            'class' => 'form-control form-control-sm',
            'required' => true
          )
        ))
        ->add('telefonoRepresentanteLegal', TextType::class, array(
          'label' => 'Teléfono Representante Legal',
          'attr' => array(
            'class' => 'form-control form-control-sm',
            'required' => true
          )
        ))
        ->add('save', SubmitType::class, array(
        // 'label'    => 'Guardar',
        'attr' => array('class' => 'btn btn-success'),
      ))
    ;
  }

   /**
    * @param OptionsResolver $resolver
   */
   public function configureOptions(OptionsResolver $resolver){
      $resolver->setDefaults([
         'data_class' => Proveedores::class,
         'attr' => array('id' => 'formProveedores'),
      ]);
   }
}