<?php

namespace App\Form;

//use App\Entity\User;
use App\Entity\ConfiguracionImagen;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class ConfiguracionType extends AbstractType{

  /**
   * @param FormBuilderInterface $builderTamaño maximo de las imagenes
   * @param array $options
  */
  public function buildForm(FormBuilderInterface $builder, array $options){

    $builder->add('tamanio', TextType::class, array(
        'label' => 'Tamaño maximo de las imagenes',
        'label_attr' => ['class' => 'font-weight-bold'],
        'attr' => array(    
          'class' => 'form-control form-control-sm',
          'required' => true
        )
      ))
      ->add('cantImgPaquete', TextType::class, array(
        'label' => 'cantidad de imagenes por paquete',
        'label_attr' => ['class' => 'font-weight-bold'],
        'attr' => array(
          'class' => 'form-control form-control-sm',
          'required' => true
        )
      ))
      /*->add('AceptarTerminosYcondiciones', CKEditorType::class, array(
        'label' => 'Términos y Condiciones',
        'label_attr' => ['class' => 'font-weight-bold'],
        'config' => array(
            //'skin' => 'office2013',
            'preset' => 'standard',
            'uiColor' => '#ffffff',
            'language' => 'es',
            'toolbar' => array(
               [ "Templates", "-", 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo', 'RemoveFormat', 'Find', 'Replace', '-', 'SelectAll', '-', 'Maximize', 'ShowBlocks' ],
               "/",
               [ 'Styles', 'Format', 'Font', 'FontSize', "-", 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ],
               "/",
               [ "Bold", 'Italic', 'Underline', 'Strike', "-", 'Subscript', 'Superscript', "-", "TextColor", 'BGColor', "-", 'NumberedList', 'BulletedList', "-", "Outdent", "Indent", "Blockquote", "-", "BidiLtr", "BidiRtl", "-" ]
            )
         ),
      ))*/

      ->add('AceptarTerminosYcondiciones', TextareaType::class, [
          'required' => false,
          'label' => 'Términos y Condiciones',
          'help' => '',
          'attr' => ['class' => ''],
      ])

      //->add('save', SubmitType::class, ['label' => 'Guardar Cambios'])

    ;
  }

   /**
    * @param OptionsResolver $resolver
   */
   public function configureOptions(OptionsResolver $resolver){
      $resolver->setDefaults([
         'data_class' => ConfiguracionImagen::class,
         'attr' => array('id' => 'formConfiguracion'),
      ]);
   }
}