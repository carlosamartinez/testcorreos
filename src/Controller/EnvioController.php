<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

// Services
use App\Services\Globales\GrillaGlobal;
use App\Services\Logs;
use App\Services\Globales\MenuPermisos;
use App\Services\GruposGeneral;

use App\Entity\Adjunto;
use App\Entity\Envio;
use App\Entity\IntegranteGrupo;
use App\Entity\Grupo;
use App\Form\EnvioType;



/**
* Controlador de Envio
*
* @category Envio
*
* @author Julian Andres Restrepo <j.restrepo@waplicaciones.co>
*
*/
class EnvioController extends AbstractController{

  private $menuPermisos;
  private $log;
  private $sMenuModulo = 'envio';
  private $sModuloVista = 'listado_envios';

  public function __construct(MenuPermisos $menuPermisos, Logs $log ){
    $this->menuPermisos = $menuPermisos;
    $this->log = $log;

  }

    /**
    * Genera la vista inicial para el listado de envios
    *
    * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
    * @author Julian Restrepo <j.restrepo@waplicaciones.com.co>
    * @return render('envio/index.html.twig') HTML
    **/
  public function index(Request $request){

    // Variables
    $session = $request->getSession();
    try{
    $this->menuPermisos->validarAccesoVista( $session, $this->sMenuModulo, $this->sModuloVista);
    } catch (\Throwable $th) {
      return $this->redirect($this->generateUrl('admin_login'));
    }

    $aPermisos = $this->menuPermisos->getPermisosModuloVista($session, $this->sMenuModulo, $this->sModuloVista);
    $aGridButtons = ( empty($aPermisos) ) ? array() : $this->menuPermisos->getGridButtons( $this->sMenuModulo,$session , $aPermisos );

    $dfColumnas = [
      ["headerClass" => "h6", 'headerName' => '#',         'field' => 'id',        'hide' => true],
      ["headerClass" => "h6", 'headerName' => 'Asunto',    'field' => 'asunto',    'width' => 500, 'pinned'=>'left'],
      ["headerClass" => "h6", 'headerName' => '#Adjuntos',    'field' => 'adjuntos',    'width' => 90,'cellClass' => 'text-center'],
      ["headerClass" => "h6", 'headerName' => '#Enviados',     'field' => 'cantidadEnviados',     'width' => 90,'cellClass' => 'text-center', 'pinned'=>'right'],
      ["headerClass" => "h6", 'headerName' => '#Envíos',     'field' => 'cantidadDestinatarios',     'width' => 90,'cellClass' => 'text-center', 'pinned'=>'right'],
      ["headerClass" => "h6", 'headerName' => 'Grupo',  'field' => 'grupo',  'width' => 300],
      ["headerClass" => "h6", 'headerName' => 'Estado',  'field' => 'estado',  'width' => 100, 'pinned'=>'left', "searchoptions" => [
            "type" => "combo", 
            "content" => [
               ["value" => 0, "text" => "Activo"],
               ["value" => 1, "text" => "Programado"],
               ["value" => 2, "text" => "Completado"],
            ]
         ]],
      ["headerClass" => "h6", 'headerName' => 'Creado',  'field' => 'fechaCreado',  'width' => 150, 'type' => "fecha", "searchoptions" => ["type" => "fecha", "format" => "YYYY-MM-DD"]],
      ["headerClass" => "h6", 'headerName' => 'Errores',  'field' => 'errores',  'width' => 70,'cellClass' => 'text-center', 'pinned'=>'right', 'cellRenderer' => 'agCellRender','cellRendererParams' => array(
            'grilla' => 'default', 'columna'  => 'erroresEnvios',
            'aData' => [
              'btnClass' => 'btn btn-custon-wa btn-sm border-0 font-weight-bold btnEnvios',
              'btnIconsClass' => 'far fa-eye'
            ]
          )],
      ["headerClass" => "h6", 'headerName' => 'Combinación',  'field' => 'combinacion',  'width' => 120],
      ["headerClass" => "h6", 'headerName' => 'Agrupación',  'field' => 'agrupacion',  'width' => 100],
      ["headerClass" => "h6", 'headerName' => 'Remitente',       'field' => 'administrador', 'width' => 250, 'cellClass' => 'text-left', 'search' => false, 'sortable' => false],
      ["headerClass" => "h6", 'headerName' => 'Ver',    'field' => '','width' => 70, 'cellClass' => 'text-center', 'search' => false, 'suppressSorting' => true, 'pinned'=>'right', 'cellRenderer' => 'agCellRender','cellRendererParams' => array(
            'grilla' => 'default', 'columna'  => 'envios',
            'aData' => [
              'btnClass' => 'btn btn-custon-wa btn-sm border-0 font-weight-bold btnEnvios',
              'btnIconsClass' => 'far fa-eye'
            ]
          )
      ],
    ];

    $this->log->setLogAdmin("LE1 Listado de Envios");

    return $this->render('envio/index.html.twig', array(
      'dfColumnas'    => json_encode($dfColumnas),
      'aGridButtons'  => json_encode($aGridButtons["iconos"]),
      'modulo'        => $this->sMenuModulo,
    ));
  }

    /**
    * Responde un JSON con la información requerida para mostrar el listado de envios
    * 
    * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
    * @param App\Services\GrillaGlobal $grillaGlobal para realizar los filtros de busqueda
    * @param $exportar Type=bool Valida si se filtran datos necesarios para descargar un archivo .csv
    * 
    * @return JSON
    */
  public function indexJson(Request $request, GrillaGlobal $grillaGlobal, $exportar = false){
    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $aJson = array();


      $session = $request->getSession();
      $em = $this->getDoctrine()->getManager();

      $aEquivalenciaColumnas = [
        'id'        => 'e.id',
        'asunto'    => 'e.asunto',
        'adjuntos'    => 'e.adjuntos',
        'cantidadEnviados'    => 'e.cantidadEnviados',
        'cantidadDestinatarios'    => 'e.cantidadDestinatarios',
        'combinacion'    => 'e.columnaCombinacion',
        'grupo'    => 'g.nombre',
        'administrador'     => 'a.nombre',
        'estado'     => 'e.estado',
        'fechaCreado'     => 'e.fechaCreado',
        'errores'     => 'e.errores',
        'agrupacion'     => 'e.agruparCorrespondencia',
      ];
      
      // Procedimiento Filtro Grilla
      $sIniFiltro = "";
      if (!$session->get('tipoUsuario')) $sIniFiltro = "a.id=".$session->get('id');
      $aDataGrilla = $grillaGlobal->realizarFiltro($aEquivalenciaColumnas, $sIniFiltro);

      // --- --- --- --- Total Filas --- --- --- --- //
      $numeroRegistros = 0;
      if( $aDataGrilla["paginaActual"] == 1 && $exportar === false ){
        $Contador = $em->createQuery("SELECT COUNT(e.id) AS numeroRegistros
        FROM App\Entity\Envio e
        JOIN App\Entity\Grupo g WITH e.grupo = g.id
        JOIN App\Entity\User a WITH e.administrador = a.id
        WHERE {$aDataGrilla["where"]} ");
        $Contador->setParameters($aDataGrilla["valoresWhere"]);
        $Contador = $Contador->getSingleResult();
        $numeroRegistros = $Contador['numeroRegistros'];
      }

      // DQL Data
      $queryUser = $em->createQuery("SELECT e.id ,e.asunto, e.adjuntos, e.cantidadEnviados, g.nombre AS grupo, a.nombre AS administrador, g.clasificacion, e.cantidadDestinatarios, e.columnaCombinacion, e.estado, e.fechaCreado, e.errores, e.agruparCorrespondencia, e.agruparProveedor
        FROM App\Entity\Envio e
        JOIN App\Entity\Grupo g WITH e.grupo = g.id
        JOIN App\Entity\User a WITH e.administrador = a.id
        WHERE {$aDataGrilla["where"]} 
        ORDER BY {$aDataGrilla["order"]} ");

      // Resultado
      $queryUser->setParameters($aDataGrilla["valoresWhere"]);

      if( $exportar === false ){
        $queryUser->setMaxResults($aDataGrilla["maximoFilas"]);
        $queryUser->setFirstResult($aDataGrilla["paginacion"]);
      }
      $aUser = $queryUser->getScalarResult();

        // --- --- --- Lógica --- --- --- //
        $aListUser = array();
        foreach( $aUser as $entiti ){

          if($entiti['estado'] == 0){
            $estado = 'Activo';
          }elseif ($entiti['estado'] == 1) {
            $estado = 'Programado';
          }else{
            $estado = 'Completado';
          }

          if($entiti['agruparCorrespondencia'] == 1){
            if($entiti['clasificacion'] == 'Asociados'){
              $agrupacion = 'Asociado';
            }else{
              $agrupacion = 'Droguería';
            }
          }elseif($entiti['agruparProveedor'] == 1){
            $agrupacion = 'Proveedor';
          }else{
            $agrupacion = '';
          }

          $aListUser [] = array(
            'id'       => $entiti['id'],
            'asunto'   => $entiti['asunto'],
            'adjuntos'   => $entiti['adjuntos'],
            'cantidadEnviados'    => $entiti['cantidadEnviados'],
            'cantidadDestinatarios'    => $entiti['cantidadDestinatarios'],
            'combinacion'    => $entiti['columnaCombinacion'],
            'grupo' => $entiti['grupo'],
            'administrador'   => $entiti['administrador'],
            'clasificacion'   => $entiti['clasificacion'],
            'codEstado'   => $entiti['estado'],
            'estado'   => $estado,
            'fechaCreado'   => $entiti['fechaCreado'],
            'errores'   => $entiti['errores'],
            'agrupacion'   => $agrupacion
          );
        }

        // Cierre de conexion y Respuesta
        $em->getConnection()->close();
        $response->setContent(json_encode(['totalRows' => $numeroRegistros, 'data' => $aListUser]));
    

    return $response;
  }



  /**
   * Accion para crear un Envio envio/mdlCrud
   * Si se detecta el envio del formulario se creara, de lo contrario se mostrara la vista envio/mdlCrud
   * @param object $request Objeto peticion de Symfony 4.2
   * @return vista envio/mdlCrud
   * @return object json resultado de la accion nuevo
   * @author Julian Restrepo <j.restrepo@waplicaciones.com.co>
   * @since 5.2
   * @category Correos\envio
  */
  public function enviosNew(Request $request, GruposGeneral $gruposGeneral): Response{
    
    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $aJson = array();
    $em = $this->getDoctrine()->getManager();


    if( $request->isXmlHttpRequest() ){

      $session = $request->getSession();
      $bAccesoAccion = $this->menuPermisos->getAccesoVistaAccion( $session, $this->sMenuModulo, $this->sModuloVista, 'nuevo-registro' );

      if($bAccesoAccion){

        if( !is_null($request->get('envio')) ){

          $aDataForm = $request->get('envio');
          $idGrupoForm = $aDataForm['grupo'];
          $emEnvio = new Envio();
          $form = $this->createForm(EnvioType::class, $emEnvio,['idAdministrador' => $session->get('id')]);
          $form->handleRequest($request);

          if( $form->isSubmitted() ){

              $Contador = $em->createQuery("SELECT COUNT(ig.id) AS numeroRegistros
              FROM App\Entity\IntegranteGrupo ig
              JOIN App\Entity\Grupo g WITH ig.grupo = g.id
              WHERE g.id = $idGrupoForm");

              $Contador = $Contador->getSingleResult();
              $numeroRegistros = $Contador['numeroRegistros'];

              $emEnvio->setAdministrador($em->getReference('App\Entity\User', $session->get('id')));
              $emEnvio->setCantidadEnviados(0);
              $emEnvio->setCantidadDestinatarios($numeroRegistros);
              $emEnvio->setFechaCreado(new \DateTime());
              $emEnvio->setEstado(0);

              if($request->get('agruparCorrespondencia')){
                $emEnvio->setAgruparCorrespondencia(1);
                $emEnvio->setAgruparProveedor(0);
              }else{
                if($request->get('agruparProveedor')){
                  $emEnvio->setAgruparProveedor(1);
                  $emEnvio->setAgruparCorrespondencia(0);
                }else{
                  $emEnvio->setAgruparProveedor(0);
                  $emEnvio->setAgruparCorrespondencia(0);
                }
              }

              if($request->get('enviarCopia')){
                $emEnvio->setEnviarCopia(1);
              }else{
                $emEnvio->setEnviarCopia(0);
              }

              

              $em->persist($emEnvio);
              $em->flush();

              //------ se verifica e inserta los destinatarios en envio_integrante----//

              $gruposGeneral->setEnvioIntegrante($emEnvio->getId(), $idGrupoForm);

              //---------//

              $this->log->setLogAdmin("LE2 Envio creado correctamente");

              $aJson['status'] = 1;
              $aJson['message'] = "Envio creado correctamente.";

          } else {
              $aJson['status'] = 0;
              $aJson['message'] = 'El formulario fue mal diligenciado, verifique e inténtelo de nuevo.';

              $this->log->setLogAdmin("LE2 Formulario de Creación de Envios fue mal diligenciado");
          }

        } else {
          
          $entity = new Envio();
          $form = $this->createForm(EnvioType::class, $entity,['idAdministrador' => $session->get('id')]);

          //data
          $aJson['entity'] = $entity;
          $aJson['accion'] = 'nuevo';
          $aJson['form']   = $form->createView();

          $this->log->setLogAdmin("LE2 visualizacion formulario de creación de envios");

          return $this->render('envio/mdlCrud.html.twig', $aJson);
        }
        
      }else{
        $aJson['status'] = 0;
        $aJson['message'] = 'Acción no habilitada, inicie sesión nuevamente he inténtelo otra vez.';

        $this->log->setLogAdmin("LE2 Formulario de creación de envios, acción no habilitada");        
      }

    } else {
      $aJson['status'] = 0;
      $aJson['message'] = 'Acción no valida';

      $this->log->setLogAdmin("LE2 Formulario de creación de envios, acción no valida");
    }

    $em->getConnection()->close();

    $response->setContent(json_encode($aJson));
    return $response;
  }


  /**
    * Responde una plantilla HTML envio/mdlCrud.html.twig con el formulario de edición de envios ó Responde un JSON con un estado y mensaje de la petición de edicion de un envio
    *
    * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
    * @author Julian Restrepo <j.restrepo@waplicaciones.com.co>
    * @since 5.2
    * @return Symfony\Component\HttpFoundation\Response JSON o render(envio/mdlCrud.html.twig)
    */
  public function enviosEdit(Request $request, GruposGeneral $gruposGeneral): Response{

    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $aJson = array();
    $em = $this->getDoctrine()->getManager();

    if( $request->isXmlHttpRequest() ){

      $session = $request->getSession();

      if( !is_null($request->get('envio')) ){

        $aFormData = $request->get('envio');


        $oEnvio = $em->getRepository(Envio::class)->findOneById($request->get('idEnvio'));

        $idGrupoForm = (int)$aFormData['grupo'];
        $idGrupoAnt = (int)$oEnvio->getGrupo()->getId();

        $editForm = $this->createForm(EnvioType::class, $oEnvio,['idAdministrador' => $session->get('id')]);
        $editForm->handleRequest($request);

          if( $editForm->isSubmitted() ){

             $Contador = $em->createQuery("SELECT COUNT(ig.id) AS numeroRegistros
              FROM App\Entity\IntegranteGrupo ig
              JOIN App\Entity\Grupo g WITH ig.grupo = g.id
              WHERE g.id = $idGrupoForm");

              $Contador = $Contador->getSingleResult();
              $numeroRegistros = $Contador['numeroRegistros'];

              $oEnvio->setCantidadDestinatarios($numeroRegistros);
              $oEnvio->setEstado(0);

              if($request->get('agruparCorrespondencia')){
                $oEnvio->setAgruparCorrespondencia(1);
                $oEnvio->setAgruparProveedor(0);
              }else{
                if($request->get('agruparProveedor')){
                  $oEnvio->setAgruparProveedor(1);
                  $oEnvio->setAgruparCorrespondencia(0);
                }else{
                  $oEnvio->setAgruparProveedor(0);
                  $oEnvio->setAgruparCorrespondencia(0);
                }
              }

              if($request->get('enviarCopia')){
                $oEnvio->setEnviarCopia(1);
              }else{
                $oEnvio->setEnviarCopia(0);
              }

              $em->persist($oEnvio);
              $em->flush();

              //------ se verifica e inserta los destinatarios en envio_integrante----//
              if($idGrupoForm != $idGrupoAnt){

                $gruposGeneral->deleteEnvioIntegrante($request->get('idEnvio'));
              }
              $gruposGeneral->setEnvioIntegrante($request->get('idEnvio'), $idGrupoForm);
              
              //---------//

              $this->log->setLogAdmin("LE3 Formulario de envios, editado correctamente");

              $aJson['status'] = 1;
              $aJson['message'] = "Envio editado correctamente.";
          }else{
              $aJson['status'] = 0;
              $aJson['message'] = "Envio editado correctamente.";

              $this->log->setLogAdmin("LE3 Formulario de envios, formulario mal diligenciado");

          }
      }else{

        // Entity manager y Formulario
        $oEnvio = $em->getRepository(Envio::class)->findOneById($request->get("idEnvio"));

        $editForm = $this->createForm(EnvioType::class, $oEnvio, ['idAdministrador' => $session->get('id')]);

        //data
        $aJson['entity'] = $oEnvio;
        $aJson['form']   = $editForm->createView();
        $aJson['accion'] = 'editar';
        $aJson['idEnvio'] = $request->get("idEnvio");

        $this->log->setLogAdmin("LE3 visualizacion de formulario de envios");

        return $this->render('envio/mdlCrudEditar.html.twig', $aJson);
      }

    }else{
      $aJson['status'] = 0;
      $aJson['message'] = 'Acción no valida';
      $this->log->setLogAdmin("LE3 Formulario de envios, acción no valida");
    }

    $em->getConnection()->close();

    $response->setContent(json_encode($aJson));
    return $response;
  }

  

  /**
  * Responde un JSON con un estado y mensaje de la petición de eliminación de un envio
  *
  * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
  * @author Julian Restrepo <j.restrepo@waplicaciones.com.co>
  * @since 5.2
  * @return Symfony\Component\HttpFoundation\Response JSON
  */
  public function enviosDelete(Request $request, GruposGeneral $gruposGeneral): Response{

    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $em = $this->getDoctrine()->getManager();
    $conexion= $em->getConnection();
    $session = $request->getSession();
    $aJson = array();

    if( $request->isXmlHttpRequest() ){

      
      $idEnvio = $request->get('idEnvio');

      try{
          
        $conexion->beginTransaction();

        $conexion->query(' DELETE FROM envio_integrante WHERE envio_id='.$idEnvio);
        $conexion->query(' DELETE FROM datos_envio WHERE envio_id='.$idEnvio);
        $conexion->query(' DELETE FROM copia_envio WHERE envio_id='.$idEnvio);
        $conexion->query(' DELETE FROM columnas_datos_envio WHERE envio_id='.$idEnvio);
        $conexion->query(' DELETE FROM envio WHERE id='.$idEnvio);

        

        $this->log->setLogAdmin("LE4 Eliminacion de envio");

        $conexion->commit();

        $gruposGeneral->deleteEnvioIntegrante($idEnvio);

        $aJson['status'] = 1;
        $aJson['message'] = 'Envio eliminado correctamente.';

      }catch(\Exception $e){

        $conexion->rollBack();        

        $aJson['status'] = 0;
        $aJson['error'] = $e->getMessage();
        $aJson['message'] = 'No es posible eliminar el envio. Comuniquese con el administrador.'; 

      }
      

    } else {
        $aJson['status'] = 0;
        $aJson['message'] = 'Acción no valida';

        $this->log->setLogAdmin("LE4 Elimnacion de envio, acción no valida");
    }

    //cierre de conexion
    $conexion->close();
    $response->setContent(json_encode($aJson));
    return $response;
  }


  public function previsualizar(Request $request){

    $response = new Response();
    $envieId = $request->get("envieId");
    
    $em = $this->getDoctrine()->getManager();
    $oEnvio = $em->getRepository(Envio::class)->findOneById($envieId);
    
    if($oEnvio){

      $adjunto = $em->getRepository(Adjunto::class)->findOneByEnvio($oEnvio);
      
      if($adjunto){
        
      }

      $carpetaAdmin = "/adjuntos". $fechaCreado.$administrador->getId();

      return $this->render('envio/mdlPrevisualizacion.html.twig', array(
        'asunto'     => $oEnvio->getAsunto(),
        'contenido'  => $oEnvio->getContenido(),
        'modulo'     => $oEnvio->getContenido(),
      ));
    }

    return $response;
  }


  /**
  * Muestra la plantilla para la previsualizacion del mensaje configurado en el envio
  *
  * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
  * @author Julian Restrepo <j.restrepo@waplicaciones.com.co>
  * @since 5.2
  * @return Symfony\Component\HttpFoundation\Response JSON
  */
  public function enviosVistaPrevia(Request $request){
    // Parámetros 
    $session = $request->getSession();
    $em = $this->getDoctrine()->getManager();
    $emp = $this->getDoctrine()->getManager('contactosProveedor');
    $envioId=$request->get('envioId');
    $tipoGrupo=$request->get('tipoGrupo');
    $destinatarioId=$request->get('destinatarioId');

    $entity = $em->getRepository('App\Entity\Envio')->findOneById($envioId);
    if(!$entity){
      throw $this->createNotFoundException('Unable to find Envio entity.');
    }
    if (!$tipoGrupo) {
      if($entity->getGrupo()->getClasificacion() == "Asociados"){
          $tipoGrupo = 1;
      }else{
          $tipoGrupo = 2;
      }
    }

    if (!$destinatarioId) {
      $integrante=$em->getRepository('App\Entity\IntegranteGrupo')->findOneByGrupo($entity->getGrupo()->getId());
      $destinatarioId= ($request->get('Tipo') =='Proveedor')?$integrante->getProveedorId():$integrante->getAsociadoId();
    }

    // Muestra el panel detalle, 
    $infoDetalle = $this->getInfoDetalleEnvio($entity, $destinatarioId);

    // Adjuntos documento
    $adjuntos=array();
    $na=0;
    $carpetaAdmin='';
    if($entity->getAdjuntos()>0){
        $administrador = $em->getRepository('App\Entity\User')->findOneById($session->get('id'));

        $fechaCreado = $administrador->getFechaCreado();
        $fechaCreado=$fechaCreado->format('Ymd');
        $carpetaAdmin =$fechaCreado.$administrador->getId();

        $ruta=$this->getParameter('kernel.project_dir').'/public/adjuntos/'.$entity->getId().'/'.$carpetaAdmin.'/otros';

        if (is_dir($ruta)) {
            if ($dh = opendir($ruta)) {
                while (($file = readdir($dh)) !== false) {
                    if(!is_dir($file)){
                        $adjuntos[$na]=$file;
                        $na++;
                    }
                }
                closedir($dh);
            }
        }

    }

    $destinatario=array();
    $contenido=$entity->getContenido();
    $usuarioTipo=$request->get('Tipo');
    switch($usuarioTipo){
        case 'asociado':
            $Query = $em->createQuery('SELECT c.zona, c.codigo, c.drogueria, c.nit, c.asociado, c.direcion, c.direcion AS direccion, c.ciudad, c.ruta, c.depto, c.depto AS departamento, c.telefono, c.centro, c.email, c.emailAsociado, c.emailAsociado AS email_asociado, c.id FROM App\Entity\Cliente c WHERE c.id = :clienteId  AND c.retirado=0');
            $Query->setParameter('clienteId', $destinatarioId);
            $destinatarioEntitie = $Query->getArrayResult();
            if ($destinatarioEntitie) {
                $destinatarioEntitie=$destinatarioEntitie[0];
                if($entity->getGrupo()->getClasificacion() == "Asociados"){
                  $permitidos=array('zona','nit','asociado','direcion','ciudad','ruta','depto','telefono','email','emailAsociado', 'email_asociado', 'departamento', 'direccion');
                }else{
                  $permitidos=array('zona','codigo','drogueria','nit','asociado','direcion','ciudad','ruta','depto','telefono','centro','email','emailAsociado', 'email_asociado', 'departamento', 'direccion');
                }
                foreach($permitidos as $campo){
                    if(isset($destinatarioEntitie[$campo]))
                        $contenido = str_replace("//$campo//", utf8_decode($destinatarioEntitie[$campo]), $contenido);
                }
                $destinatario['Codigo']=$destinatarioEntitie['codigo'];
                $destinatario['Nit']=$destinatarioEntitie['nit'];
                $destinatario['id']=$destinatarioEntitie['id'];
            }
        break;
        case 'proveedor':
            $aDestinatario = array_filter($infoDetalle["destinatarios"], function($detalle) use ($destinatarioId){return $detalle["id"] == $destinatarioId;});
            $aDestinatario = array_values($aDestinatario)[0];
            if($aDestinatario["tipo"] == "contacto"){
              $Query = $emp->createQuery('SELECT p.id, p.nit, p.nombre AS empresa, p.codigo, p.representanteLegal, p.emailRepresentanteLegal, p.telefonoRepresentanteLegal, cp.nombreContacto AS nombre,cp.ciudad,cp.email AS email1, cp.telefono, cp.movil, cp.fechaCreacion, cp.id AS idContacto 
                FROM App\Entity\Contactos cp 
                LEFT JOIN App\Entity\Proveedores p WITH cp.idProveedor = p.id 
                WHERE cp.id = :destinatarioId');
              $Query->setParameter('destinatarioId', $destinatarioId);
              $destinatarioEntitie = $Query->getArrayResult();
            }else if($aDestinatario["tipo"] == "proveedor"){
              $Query = $emp->createQuery('SELECT p.id, p.nit, p.nombre AS empresa, p.codigo, p.representanteLegal AS nombre, p.emailRepresentanteLegal AS email1, p.telefonoRepresentanteLegal 
                FROM App\Entity\Proveedores p 
                WHERE p.id = :destinatarioId');
              $Query->setParameter('destinatarioId', $destinatarioId);
              $destinatarioEntitie = $Query->getArrayResult();
            }
            
            $destinatarioEntitie=$destinatarioEntitie[0];
            $permitidos=array('nit','nombre','apellido','empresa','cargo','direccion','telefono','fax','celular','email1','email2','clasificacion','codigo');
            foreach($permitidos as $campo){
                if(isset($destinatarioEntitie[$campo]))
                    $contenido = str_replace("//$campo//", utf8_decode($destinatarioEntitie[$campo]), $contenido);
            }
            $destinatario['Codigo']=$destinatarioEntitie['codigo'];
            $destinatario['Nit']=$destinatarioEntitie['nit'];

            $destinatario['id']=$destinatarioEntitie['id'];
        break;
    }

    // Combinación
    if($entity->getCombinacion()==1 && $entity->getColumnaCombinacion()){
        $columna=$entity->getColumnaCombinacion();
        
        $cabezeraSabana = $em->getRepository('App\Entity\DatosEnvio')->findOneBy(array('envio' => $envioId));
        
        $condicion = $destinatario[$columna];

        $sabanaDatos = $em->createQuery("SELECT u FROM App\Entity\ColumnasDatosEnvio u WHERE u.envioId = '".$envioId."' AND u.identificador  = '$condicion'")->execute();
        
        $resultado = strpos($contenido, '//TABLA//');
        $control=0;
        if($resultado !== FALSE){
            //Crear la tabla con el contenido para enviar.
            $columnas = unserialize($cabezeraSabana->getDatos());
            $contColumnas = count($columnas) - 1;
            $posicion = 0;
            $informacionTabla = '<table class="tablaDatos"><tr>';
            for ($i = 0; $i <= $contColumnas; $i++) {
                $informacionTabla .= '<td>' . ucfirst(utf8_decode($columnas[$i])) . '</td>';
            }
            $informacionTabla .= '</tr>';
            foreach ($sabanaDatos as $dato) {
                $adjuntosCsv[] = $dato->getId();
                $data = unserialize($dato->getDatos());
                $contColumnas = count($data) - 1;
                $informacionTabla .= '<tr>';
                for ($i = 0; $i <= $contColumnas; $i++) {
                    $informacionTabla .= '<td>' . $data[$i] . '</td>';
                }
                $informacionTabla .= '</tr>';
            }
            $informacionTabla .= '</table>';
            $control++;
            $contenido = str_replace("//TABLA//", $informacionTabla, $contenido);
        }
        $resultado = strpos($contenido, '//ARCHIVO//');
        if($resultado !== FALSE){
            //Crear link para descargar el archivo.
            $linkArchivo='<a style="color:#E01F69;font-size:24px;" href="'.$this->generateUrl('admin_envio_destinatario_xls', array('envio_id'=>$envioId,'tipo_usuario'=>$usuarioTipo,'id_usuario'=>$destinatario['id'],'llave'=>md5($envioId.$usuarioTipo.$destinatario['id'])), true).'" target="_blank">Clic aqu&iacute; para descargar Archivo</a>';
            $control++;
            $contenido = str_replace("//ARCHIVO//", $linkArchivo, $contenido);
        }
        $resultadoC = strpos($contenido, '//COMBINAR//');
        if($resultadoC !== FALSE){
            foreach ($sabanaDatos as $dato) {
                $datosFila = unserialize($dato->getDatos());
                $cantidadDatos = count($datosFila) - 1;
                for($i = 0; $i <= $cantidadDatos; $i++){
                    $contenido = str_replace('//'.($i+1).'//', utf8_decode($datosFila[$i]), $contenido);
                }
            }
            $contenido = str_replace('//COMBINAR//', '', $contenido);
        }
    }

    //$auditoria->registralog('Vista previa mensaje', $session->get('id_usuario'));
    return $this->render('envio/vistaPreviaMensaje.html.twig', array(
      'entity'=>$entity,
      'contenido'=>$contenido,
      'destinatario'=>$destinatarioEntitie,
      'adjuntos'=>$adjuntos,
      'usuarioTipo'=>$usuarioTipo,
      'tipoGrupo'=>$tipoGrupo,
      'carpetaAdmin'=> $carpetaAdmin,
      "infoDetalle" => $infoDetalle,
      "destinatarioId" => $destinatarioId
    ));
}

  private function getInfoDetalleEnvio(Envio $oEnvio, string $destinatarioId = null){
    // Parámetros
    $em = $this->getDoctrine()->getManager();
    $emp = $this->getDoctrine()->getManager('contactosProveedor');

    // Variables
    $oGrupo = $oEnvio->getGrupo();
    $sClasificacion = $oGrupo->getClasificacion();
    $aDetalle = ["destinatarios" => [], "para" => []];
    $aDestinatarios = &$aDetalle["destinatarios"];
    
    if($oEnvio->getAgruparCorrespondencia() == 1){
      // Agrupado por asociado/droguería solo visualizar email
      return null;
    }else if($oEnvio->getAgruparProveedor() == 1){
      // Agrupado por proveedor, ver proveedores
      
      // Obtener Proveedores
      $aProveedores = $this->getProveedoresByGrupo($oGrupo);

      // Detalle
      foreach($aProveedores as $index => $oProveedor){
        $aDestinatarios[] = [
          "id" => $oProveedor->getId(),
          "tipo" => "proveedor",
          "nit" => $oProveedor->getNit(), 
          "nombre" => $oProveedor->getNombre(), 
        ];

        // Obtiene destinatarios del proveedor
        if($oProveedor->getId() == $destinatarioId){
          $sQueryProveedores = "SELECT DISTINCT ig.proveedorId FROM App\Entity\IntegranteGrupo ig WHERE ig.grupo = :grupo";
          $oIntegrantes = $em->getRepository(IntegranteGrupo::class)->findBy(["proveedorId" => $destinatarioId, "grupo" => $oGrupo]);
          foreach($oIntegrantes as $oIntegrante){
            if($oIntegrante->getContactoProveedorId()){
              $oContacto = $emp->find("App\Entity\Contactos", $oIntegrante->getContactoProveedorId());
              if(!$oContacto) 
                continue;

              $aDetalle["para"][] = [
                "empresa" => $oProveedor->getNombre(),
                "nombre" => $oContacto->getNombreContacto(),
                "email1" => $oContacto->getEmail(),
              ];
            }else{
              $aDetalle["para"][] = [
                "empresa" => $oProveedor->getNombre(),
                "nombre" => $oProveedor->getRepresentanteLegal(),
                "email1" => $oProveedor->getEmailRepresentanteLegal(),
              ];
            }
          }
        }
      }
    } else {
      // Sin Agrupar, aparecen en detalle los destinatarios

      if(in_array($sClasificacion, ["Droguerías", "Asociados"])){
        $sQueryIntegrantes = "SELECT c.id, c.codigo, c.drogueria, c.email, c.nit, c.asociado, c.emailAsociado FROM App\Entity\IntegranteGrupo ig JOIN App\Entity\Cliente as c WITH c.id = ig.asociadoId WHERE ig.grupo = :grupo";
        $aIntegrantes = $em->createQuery($sQueryIntegrantes)->setParameter("grupo", $oGrupo)->getResult();
        switch($sClasificacion){
          case "Droguerías":
            foreach($aIntegrantes as $aIntegrante){
              $aDestinatarios[] = [
                "id" => $aIntegrante["id"], 
                "tipo" => "drogueria",
                "codigo" => $aIntegrante["codigo"], 
                "drogueria" => $aIntegrante["drogueria"], 
                "email" => $aIntegrante["email"], 
              ];
            }
            break;
          case "Asociados":
            foreach($aIntegrantes as $aIntegrante){
              $aDestinatarios[] = [
                "id" => $aIntegrante["id"], 
                "tipo" => "asociado",
                "nit" => $aIntegrante["nit"], 
                "asociado" => $aIntegrante["asociado"], 
                "emailAsociado" => $aIntegrante["emailAsociado"], 
              ];
            }
          break;
        }
      }else if($sClasificacion == "No Aplica"){
        $aProveedores = $this->getProveedoresByGrupo($oGrupo);
        $oIntegrantes = $em->getRepository(IntegranteGrupo::class)->findByGrupo($oGrupo);

        foreach($oIntegrantes as $oIntegrante){
          if(!isset($aProveedores[$oIntegrante->getProveedorId()])){
            continue;
          }
          $oProveedor = $aProveedores[$oIntegrante->getProveedorId()];

          $aDestinatario = &$aDestinatarios[];
          $aDestinatario["id"] = $oProveedor->getId();
          $aDestinatario["tipo"] = "proveedor";
          $aDestinatario["nit"] = $oProveedor->getNit();
          $aDestinatario["proveedor"] = $oProveedor->getNombre();
          $aDestinatario["contacto"] = "";
          $aDestinatario["email"] = "";
          if($oIntegrante->getContactoProveedorId()){
            $oContacto = $emp->getRepository("App\Entity\Contactos")->find($oIntegrante->getContactoProveedorId());
            if($oContacto){
              $aDestinatario["id"] = $oContacto->getId();
              $aDestinatario["tipo"] = "contacto";
              $aDestinatario["contacto"] = $oContacto->getNombreContacto();
              $aDestinatario["email"] = $oContacto->getEmail();
            }
          }
        }
      }
    }
    return $aDetalle;
  }

  private function getProveedoresByGrupo(Grupo $oGrupo){
    // Parámetros
    $em = $this->getDoctrine()->getManager();
    $emp = $this->getDoctrine()->getManager('contactosProveedor');
    $aProveedores = [];

    // Query Proveedores
    $sQueryProveedores = "SELECT DISTINCT ig.proveedorId FROM App\Entity\IntegranteGrupo ig WHERE ig.grupo = :grupo";
    $aIntegrantes = $em->createQuery($sQueryProveedores)->setParameter("grupo", $oGrupo)->getResult();
    $aProveedoresId = array_map(function($aIntegrante){return $aIntegrante["proveedorId"];}, $aIntegrantes);
    $oProveedores = $emp->getRepository("App\Entity\Proveedores")->findById($aProveedoresId);
    foreach($oProveedores as $index => $oProveedor){
      $aProveedores[$oProveedor->getId()] = $oProveedor;
    }
    return $aProveedores;
  }


  public function confirmarProgramacion(Request $request,int $idEnvio){
    // Parámetros
    $em = $this->getDoctrine()->getManager();
    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');

    // Validar Petición HTTP
    if(!$request->isXmlHttpRequest()){
      $response->setContent(json_encode(["status" => 0, "message" => "Acción no válida"]));
      return $response;
    }

    // Empezamos proceso
    $em->getConnection()->beginTransaction(); // suspend auto-commit
    try {
      // Variables
      $oEnvio = $em->getRepository(Envio::class)->find($idEnvio);

      // Validaciones
      if(!$oEnvio){
        $response->setContent(json_encode(["status" => 0, "message" => "No fue posible encontrar el envío."]));
        return $response;
      }
      if($oEnvio->getEstado() != 0){
        $response->setContent(json_encode(["status" => 0, "message" => "El envío no se encuentra activo."]));
        return $response;
      }

      // Actualizamos Estado
      $oEnvio->setEstado(1);
      $em->persist($oEnvio);

      // Ejecutamos Procesos
      $em->flush();
      $em->getConnection()->commit();
    
      // Respuesta
      $aRespuesta = ['status' => 1, 'message' => "Envio confirmar programación"];
    }catch( \Doctrine\DBAL\DBALException $e ){
      $response->setStatusCode(Response::HTTP_BAD_REQUEST);
      $aRespuesta = ['status' => 0, 'message' => 'Hubo un error: [0] '.$e->getMessage()];
    }catch (\Exception $e) {
      $response->setStatusCode(Response::HTTP_BAD_REQUEST);
      $aRespuesta = ['status' => 0, 'message' => 'Hubo un error: [1] '.$e->getMessage()];
    }

    // Resultado
    $em->getConnection()->close();
    $response->setContent(json_encode($aRespuesta));
    return $response;
  }


  /**
  * Responde un JSON con un estado y mensaje de la petición de cancelacion de envio
  *
  * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
  * @author Julian Restrepo <j.restrepo@waplicaciones.com.co>
  * @since 5.2
  * @return Symfony\Component\HttpFoundation\Response JSON
  */
  public function cancelarEnvio(Request $request): Response{

    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $em = $this->getDoctrine()->getManager();
    $conexion= $em->getConnection();
    $session = $request->getSession();
    $aJson = array();

    $idEnvio = $request->get('idEnvio');
    $accion = $request->get('accion');

    $envio = $em->getRepository("App\Entity\Envio")->findOneById($idEnvio);

    if($envio){

      $envio->setEstado(0);

      $em->persist($envio);
      $em->flush();

      $aJson['status'] = 1;
      $aJson['message'] = 'Envío Pausado correctamente.';

      if($accion == 'reiniciar'){

        $sql = "UPDATE envio_integrante SET enviado=0, fecha_envio=NULL WHERE envio_id=".$idEnvio;
        $conexion->query($sql);

        $envio->setCantidadEnviados(0);
        $em->persist($envio);
        $em->flush();

        $aJson['message'] = 'Envío Reiniciado Correctamente.';

      }
    }

    $this->log->setLogAdmin("LE4 Cancelacion de Envío");

    //cierre de conexion
    $conexion->close();
    $response->setContent(json_encode($aJson));
    return $response;
  }

}