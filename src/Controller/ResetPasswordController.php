<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\HistoricoPassword;

use App\Form\ChangePasswordFormType;
use App\Form\ResetPasswordRequestFormType;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;

use App\Security\PasswordEncoder;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use SymfonyCasts\Bundle\ResetPassword\Controller\ResetPasswordControllerTrait;
use SymfonyCasts\Bundle\ResetPassword\Exception\ResetPasswordExceptionInterface;
use SymfonyCasts\Bundle\ResetPassword\ResetPasswordHelperInterface;

use App\Services\Mail;

/**
 * @Route("/reset-password")
 */
class ResetPasswordController extends AbstractController
{
    use ResetPasswordControllerTrait;

    private $resetPasswordHelper;
    private $mail;
    private $passwordEncoder;

    public function __construct(ResetPasswordHelperInterface $resetPasswordHelper, Mail $mail, PasswordEncoder $passwordEncoder)
    {
        $this->mail = $mail;
        $this->resetPasswordHelper = $resetPasswordHelper;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * Display & process form to request a password reset.
     *
     * @Route("", name="app_forgot_password_request")
     */
    public function request(Request $request): Response
    {

        $form = $this->createForm(ResetPasswordRequestFormType::class);
        $form->handleRequest($request);
        return $this->render('reset_password/request.html.twig', [
            'requestForm' => $form->createView(),   
        ]);
    }

    public function requestSending(Request $request, MailerInterface $mailer): Response
    {
        $form = $this->createForm(ResetPasswordRequestFormType::class);
        $form->handleRequest($request);
        $email = $request->get("email");
        return $this->processSendingPasswordResetEmail(
            $email,
            $mailer
        );

    }

    /**
     * Confirmation page after a user has requested a password reset.
     *
     * @Route("/check-email", name="app_check_email")
     */
    public function checkEmail(): Response
    {
        // We prevent users from directly accessing this page
        if (!$this->canCheckEmail()) {
            return $this->redirectToRoute('app_forgot_password_request');
        }

        return $this->render('reset_password/check_email.html.twig', [
            'tokenLifetime' => $this->resetPasswordHelper->getTokenLifetime(),
        ]);
    }

    /**
     * Validates and process the reset URL that the user clicked in their email.
     *
     * @Route("/reset/{token}", name="app_reset_password")
     */


    public function resetPasswordInApp(Request $request): Response {

        $response = new Response();
        $aJson = array();

        $user_id = $request->get("user");
        $em = $this->getDoctrine()->getManager();

        if($user_id){

            $user = $this->getDoctrine()->getRepository(User::class)->find($user_id);
            if($request->get('old_password') !== $this->passwordEncoder->decodePassword($user->getPassword())){
                $response->setStatusCode(406,"La contraseña actual no coincide");
                return $response->send();
            };
            $validar_contrasena = $this->validarContrasenasPrevias($request->get('password'),$user->getId());
            
            if(gettype($validar_contrasena) == "string"){
                $aJson['status'] = 406;
                $aJson['message'] = $validar_contrasena;
                $response->setContent(json_encode($aJson));
                return $response;
            }

            $encodedPassword = $this->passwordEncoder->encodePassword($request->get('password'));
            
            //$encodedPasswordsetPassword($encodedPassword);

            $user->setPassword($encodedPassword);

            $em->persist($user);
            $em->flush();

            //$this->insertarPasswordHistorico($request);

            $this->getDoctrine()->getManager()->flush();

            // The session is cleaned up after the password has been changed.
            $this->cleanSessionAfterReset();

            $aJson['status'] = 200;
            $aJson['message'] = "Contraseña Cambiada";

            $response->setContent(json_encode($aJson));
            return $response;

            //$response->setStatusCode(200,"Contraseña Cambiada");
            //return $response->send();
        }
        
        $aJson['status'] = 406;
        $aJson['message'] = "Hubo un error al cambiar tu clave";
        $response->setContent(json_encode($aJson));
        return $response;
    
        //$response->setStatusCode(406,"Hubo un error al cambiar tu clave");
        //return $response->send();
    }

    public function validarContrasenasPrevias($new_contrasena, $userId){
        
        $response = new Response();

        $em = $this->getDoctrine()->getManager();

        $oUser = $em->getRepository(User::class)->find($userId);

        if($this->passwordEncoder->decodePassword($oUser->getPassword()) == $new_contrasena){

            return "Ha ingresado la contraseña actual";
        }
        
        $contrasenas = $em->getRepository(HistoricoPassword::class)->findOneBy(['idUser' => $userId]);

        if(!isset($contrasenas)){
            $lista_contrasenas = array();

            array_unshift($lista_contrasenas,$oUser->getPassword());
            
            $contrasenas = new HistoricoPassword();

            $contrasenas->setFechaCambioClave(new \DateTime());
            $contrasenas->setClaves($lista_contrasenas);
            $contrasenas->setIdUser($oUser);

            $em->persist($contrasenas);
            $em->flush();

            return (bool)true;

        }

            $array_claves = $contrasenas->getClaves();
                
            foreach ($array_claves as $key => $value) {
                if($this->passwordEncoder->decodePassword($value) == $new_contrasena ){
                    
                    return "Contraseña ya usada anteriormente";

                }
            }
            array_unshift($array_claves,$oUser->getPassword());
            if(count($array_claves) > 3){
                array_pop($array_claves);
            }
            $contrasenas->setClaves($array_claves);
            $em->persist($contrasenas);
            $em->flush();
            return (bool)true;   
    }

    public function resetPassword(Request $request): Response
    {
        // The token is valid; allow the user to change their pas
        $response = new Response();
        $token = $request->get("token");
            // A password reset token should be used only once, remove it.
            if ($token) {

                try {
                    $user = $this->resetPasswordHelper->validateTokenAndFetchUser($token);
                } catch (ResetPasswordExceptionInterface $e) {
                    $this->addFlash('reset_password_error', sprintf(
                        'Hubo un problema procesando tu solicitud - %s',
                        $e->getReason()
                    ));
        
                    return $this->redirectToRoute('forgot_password');
                }
                
                $validar_contrasena = $this->validarContrasenasPrevias($request->get('plainPassword'),$user->getId());

                if(gettype($validar_contrasena) == "string"){
                    $aJson['status'] = 406;
                    $aJson['message'] = $validar_contrasena;
                    $response->setContent(json_encode($aJson));
                    return $response;
                }    

                $this->resetPasswordHelper->removeResetRequest($token);

                // Encode the plain password, and set it.
                $encodedPassword = $this->passwordEncoder->encodePassword($request->get('plainPassword'));
                
                $user->setPassword($encodedPassword);
                $this->getDoctrine()->getManager()->flush();

                // The session is cleaned up after the password has been changed.
                $this->cleanSessionAfterReset();
                $aJson['status'] = 200;
                $aJson['message'] = "volver al login";
                $response->setContent(json_encode($aJson));
                return $response;

            }else{
                return New Response(false);
            }


    }
     public function reset(Request $request, string $token = null): Response
    {

        if ($token) {
            // We store the token in session and remove it from the URL, to avoid the URL being
            // loaded in a browser and potentially leaking the token to 3rd party JavaScript.
            $this->storeTokenInSession($token);
            
        }
        $token = $this->getTokenFromSession();
        if (null === $token) {
            throw $this->createNotFoundException('Token de recuperacion de clave no encontrado.');
        }
        try {
            $user = $this->resetPasswordHelper->validateTokenAndFetchUser($token);
        } catch (ResetPasswordExceptionInterface $e) {
            $this->addFlash('reset_password_error', sprintf(
                'Hubo un problema procesando tu solicitud - %s',
                $e->getReason()
            ));

            return $this->redirectToRoute('forgot_password');
        }

        // The token is valid; allow the user to change their password.
        $form = $this->createForm(ChangePasswordFormType::class);
        $form->handleRequest($request);

        return $this->render('reset_password/reset.html.twig', [
            'resetForm' => $form->createView(),
            "token" => $token
        ]);
    }

    private function processSendingPasswordResetEmail(string $email, MailerInterface $mailer):Response
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'email' => $email
        ]);
        $this->setCanCheckEmailInSession();
        // Marks that you are allowed to see the app_check_email page.
        //$this->setCanCheckEmailInSession();

        // Do not reveal whether a user account was found or not.
        if (!$user) {
            return new Response(2);
        }

        try {
            $resetToken = $this->resetPasswordHelper->generateResetToken($user);
        } catch (ResetPasswordExceptionInterface $e) {
            // If you want to tell the user why a reset email was not sent, uncomment
            // the lines below and change the redirect to 'app_forgot_password_request'.
            // Caution: This may reveal if a user is registered or not.
            //
            // $this->addFlash('reset_password_error', sprintf(
            //     'There was a problem handling your password reset request - %s',
            //     $e->getReason()
            // ));
            //echo $e->getReason();exit();
            return new Response(409);
        }
        $path = $this->getParameter('kernel.project_dir');

        return $this->mail->forgotPassword($email, $user->getNombre(), $resetToken ,$this->resetPasswordHelper->getTokenLifetime() , $path );
       
    }
    public function resetPasswordInAppIndex(Request $request): Response
    {
        return $this->render('app/resetPassword.html.twig');
    }
}
