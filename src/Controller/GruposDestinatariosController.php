<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

// Services
use App\Services\Globales\GrillaGlobal;
use App\Services\Globales\MenuPermisos;
use App\Services\Logs;

/**
 * Controlador para visualizar los destinarios del cliente
 *
 * @author Anderson Barbosa <a.barbosa@waplicaciones.co>
 */
class GruposDestinatariosController extends AbstractController
{

  private $menuPermisos;
  private $sMenuModulo = 'grupos';
  private $sModuloVista = 'destinatarios_lista';
  private $log;
  public function __construct(MenuPermisos $menuPermisos, Logs $log)
  {
    $this->menuPermisos = $menuPermisos;
    $this->log = $log;
  }


  /**
   * Genera interfaz de los detinatarios proveedores y asociados
   *
   * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
   *
   * @return render('clientes/index.html.twig') HTML
   */
  public function index(Request $request)
  {
    // Parámetros
    $session = $request->getSession();

    // Validar Permisos
    try
    {
      $this
        ->menuPermisos
        ->validarAccesoVista($session, $this->sMenuModulo, $this->sModuloVista);
    }
    catch(\Throwable $th)
    {
      return $this->redirect($this->generateUrl('admin_login'));
    }

    $aPermisos = $this
      ->menuPermisos
      ->getPermisosModuloVista($session, $this->sMenuModulo, $this->sModuloVista);
    $aGridButtons = $this
      ->menuPermisos
      ->getGridButtons( $this->sMenuModulo,$session ,$aPermisos);

    $this->log->setLogAdmin("GD1 Listado Grupos Destinatarios");

    return $this->render('grupos/destinatarios.html.twig', array(
      'proveedores' => $this->getIndexParametrosProveedores(),
      'asociados' => $this->getIndexParametrosAsociados(),
      'aGridButtons' => json_encode($aGridButtons["iconos"]) ,
      'modulo' => $this->sMenuModulo,
    ));
  }


  private function getIndexParametrosProveedores(){
    return [
      "index" => [
        "json" => $signUpPage = $this->generateUrl('admin_grupo_destinatarios_proveedores_json'),
        "columnas" => [
          ['headerName' => '#',       'field' => 'id', 'hide' => true], 
          ['headerName' => 'Asignado'],
          ['headerName' => 'Nit',     'field' => 'nit', 'width' => 100, 'cellRenderer' => 'agGroupCellRenderer'], 
          ['headerName' => 'Código',  'field' => 'codigo', 'width' => 100], 
          ['headerName' => 'Empresa', 'field' => 'nombre'], 
          ['headerName' => 'Nombre',  'field' => 'representanteLegal']
        ],
        "buttons" => []
      ],
      "contactos" => [
        "json" => $signUpPage = $this->generateUrl('admin_grupo_destinatarios_proveedores_contactos_json'),
        "columnas" => [
          ['headerName' => '#',       'field' => 'id', 'hide' => true], 
          ['headerName' => 'Asignado'],
          ['headerName' => 'Nombre',  'field' => 'nombreContacto'], 
          ['headerName' => 'Ciudad',  'field' => 'ciudad'], 
          ['headerName' => 'Email',   'field' => 'email'], 
          ['headerName' => 'Teléfono','field' => 'telefono'], 
          ['headerName' => 'Móvil',   'field' => 'movil']
        ],
      ]
    ];
  }

  private function getIndexParametrosAsociados(){
    return [
      "index" => [
        "json" => $signUpPage = $this->generateUrl('admin_grupo_destinatarios_asociados_json'),
        "columnas" => [
          ['headerName' => '#',       'field' => 'id', 'width' => 200, 'hide' => true],
          [
            'headerName' => 'Permisos',
            'field' => 'esAsociado',
            'width' => 100,
            'cellClass' => ['text-center'],
            'cellRenderer' => 'agCellRender',
            'cellRendererParams' => [
              'grilla' => 'default',
              'columna'  => 'Asignado',
              'aData' => [
                'btnClass' => 'btn btn-outline-primary border-0 font-weight-bold btnGrillaPermisos',
                'btnIconsClass' => 'fas fa-key'
              ]
            ]
          ],
          ['headerName' => 'Departamento',       'field' => 'depto', 'width' => 130],
          ['headerName' => 'Ciudad',       'field' => 'ciudad', 'width' => 150],
          ['headerName' => 'Centro',       'field' => 'centro', 'width' => 130],
          ['headerName' => 'Código',       'field' => 'codigo', 'width' => 130],
          ['headerName' => 'Ruta',       'field' => 'ruta', 'width' => 130],
          ['headerName' => 'Asociado',       'field' => 'asociado', 'width' => 200],
          ['headerName' => 'Nit',       'field' => 'nit', 'width' => 150], 
          ['headerName' => 'Email',       'field' => 'email', 'width' => 310]
        ],
        "buttons" => []
      ],
    ];
  }
}