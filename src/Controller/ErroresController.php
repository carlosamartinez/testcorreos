<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

// Services
use App\Services\Globales\GrillaGlobal;
use App\Services\Logs;
use App\Services\Globales\MenuPermisos;

use App\Entity\Contactos;
use App\Entity\Cliente;
use App\Entity\Proveedores;
use App\Form\ContactosType;



/**
* Controlador de Errores de Envio
*
* @category EnvioError
*
* @author Julian Andres Restrepo <j.restrepo@waplicaciones.co>
*
*/
class ErroresController extends AbstractController{

  private $menuPermisos;
  private $log;
  private $sMenuModulo = 'envio';
  private $sModuloVista = 'listado_envios';

  public function __construct(MenuPermisos $menuPermisos, Logs $log ){
    $this->menuPermisos = $menuPermisos;
    $this->log = $log;

  }

    /**
    * Genera la vista inicial para el listado de errores de envios
    *
    * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
    * @author Julian Restrepo <j.restrepo@waplicaciones.com.co>
    * @return render('erroes/index.html.twig') HTML
    **/
  public function index(Request $request){

    // Variables
    $session = $request->getSession();

    $dfColumnas = [
      ["headerClass" => "h6", 'headerName' => '#',         'field' => 'id',        'hide' => true],
      ["headerClass" => "h6", 'headerName' => 'Envio',     'field' => 'asunto',     'width' => 250,        'hide' => true],
      ["headerClass" => "h6", 'headerName' => 'Destinatario',    'field' => 'destinatario',    'width' => 250],
      ["headerClass" => "h6", 'headerName' => 'Email Destinatario',    'field' => 'emailDestinatario',    'width' => 250, 'search' => false, 'suppressSorting' => true, 'sortable' => false],
      ["headerClass" => "h6", 'headerName' => 'Fecha',  'field' => 'fecha',  'width' => 150, 'type' => "fecha", "searchoptions" => ["type" => "fecha", "format" => "YYYY-MM-DD"]],
      ["headerClass" => "h6", 'headerName' => 'Error',       'field' => 'error', 'width' => 250, 'cellClass' => 'text-center']
    ];

    //$this->log->setLogAdmin("LU1 Listado de Usuarios de Administrador");

    return $this->render('envioErrores/index.html.twig', array(
      'dfColumnas'    => json_encode($dfColumnas),
      'modulo'        => $this->sMenuModulo,
    ));
  }

    /**
    * Responde un JSON con la información requerida para mostrar el listado de errores generados para un envio
    * 
    * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
    * @param App\Services\GrillaGlobal $grillaGlobal para realizar los filtros de busqueda
    * @param $exportar Type=bool Valida si se filtran datos necesarios para descargar un archivo .csv
    * @author Julian Restrepo <j.restrepo@waplicaciones.com.co>
    * @return JSON
    */
  public function indexJson(Request $request, GrillaGlobal $grillaGlobal, $exportar = false){
    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $aJson = array();


      $session = $request->getSession();
      $em = $this->getDoctrine()->getManager();
      $emContactos = $this->getDoctrine()->getManager('contactosProveedor');

      $aEquivalenciaColumnas = [
        'id'        => 'er.id',
        'asunto'    => 'e.asunto',
        'destinatario'    => 'ei.id',
        'fecha'    => 'er.fecha',
        'error'     => 'er.error'
      ];

      // Procedimiento Filtro Grilla
      $sIniFiltro = "er.envioId =".$request->get('idEnvio');
      $aDataGrilla = $grillaGlobal->realizarFiltro($aEquivalenciaColumnas, $sIniFiltro);

      // --- --- --- --- Total Filas --- --- --- --- //
      $numeroRegistros = 0;
      if( $aDataGrilla["paginaActual"] == 1 && $exportar === false ){
        $Contador = $em->createQuery("SELECT COUNT(er.id) AS numeroRegistros
        FROM App\Entity\EnvioError er
        JOIN App\Entity\Envio e WITH er.envioId = e.id
        LEFT JOIN App\Entity\EnvioIntegrante ei WITH er.envioIntegranteId = ei.id
        LEFT JOIN App\Entity\IntegranteGrupo ig WITH ei.integranteGrupoId = ig.id
        WHERE {$aDataGrilla["where"]} ");
        $Contador->setParameters($aDataGrilla["valoresWhere"]);
        $Contador = $Contador->getSingleResult();
        $numeroRegistros = $Contador['numeroRegistros'];
      }

      // DQL Data
      $queryUser = $em->createQuery("SELECT er.id, er.fecha, er.error, er.reportado, e.asunto, ig.asociadoId, ig.proveedorId, ig.contactoProveedorId
        FROM App\Entity\EnvioError er
        JOIN App\Entity\Envio e WITH er.envioId = e.id
        LEFT JOIN App\Entity\EnvioIntegrante ei WITH er.envioIntegranteId = ei.id
        LEFT JOIN App\Entity\IntegranteGrupo ig WITH ei.integranteGrupoId = ig.id
        WHERE {$aDataGrilla["where"]} 
        ORDER BY {$aDataGrilla["order"]} ");

      // Resultado
      $queryUser->setParameters($aDataGrilla["valoresWhere"]);

      if( $exportar === false ){
        $queryUser->setMaxResults($aDataGrilla["maximoFilas"]);
        $queryUser->setFirstResult($aDataGrilla["paginacion"]);
      }
      $aUser = $queryUser->getScalarResult();
      

        // --- --- --- Lógica --- --- --- //
        $aListUser = array();
        foreach( $aUser as $entiti ){
          $destinatario = null;
          $email = null;
          $nombre = null;
          if($entiti['asociadoId']){
  
            $asociado = $em->getRepository(Cliente::class)->findOneById($entiti['asociadoId']);
            $email = $asociado->getEmail();
            $nombre = $asociado->getAsociado();
          }else if($entiti['contactoProveedorId']){
            
            $contactoProveedor = $emContactos->getRepository(Contactos::class)->findOneById($entiti['contactoProveedorId']);
            $email = $contactoProveedor->getEmail();
            $nombre = $contactoProveedor->getNombre();
          }else if($entiti['proveedorId']){
  
            $proveedor = $emContactos->getRepository(Proveedores::class)->findOneById($entiti['proveedorId']);
            $email = $asociado->getEmailRepresentanteLegal();
            $nombre = $asociado->getNombre();
          }
          
          $aListUser[] = array(
            'id'       => $entiti['id'],
            'asunto'    => $entiti['asunto'],
            'destinatario'   => $nombre,
            'emailDestinatario'   => $email,
            'fecha'   => $entiti['fecha'],
            'error'   => $entiti['error']
          );
        }
        // Cierre de conexion y Respuesta
        $em->getConnection()->close();
        $response->setContent(json_encode(['totalRows' => $numeroRegistros, 'data' => $aListUser]));

    return $response;
  }

}