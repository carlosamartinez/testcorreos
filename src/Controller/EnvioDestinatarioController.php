<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Debug\ExceptionHandler;
use Symfony\Component\HttpFoundation\RedirectResponse;

// Services
use App\Services\Globales\GrillaGlobal;
use App\Services\Logs;
use App\Services\Globales\MenuPermisos;
use App\Services\EnvioMasivo;

use App\Entity\Envio;
use App\Entity\EnvioLectura;
use App\Entity\EnvioIntegrante;
use App\Entity\CopiaEnvio;
use App\Entity\EnvioError;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;



/**
* Controlador de Envio
*
* @category Envio
*
* @author Julian Andres Restrepo <j.restrepo@waplicaciones.co>
*
*/
class EnvioDestinatarioController extends AbstractController{

  private $menuPermisos;
  private $log;
  private $sMenuModulo = 'envioDestinatario';
  private $sModuloVista = 'listado_destinatarios';
  private $envioMasivo;

  public function __construct(MenuPermisos $menuPermisos, Logs $log, EnvioMasivo $envioMasivo){
    $this->menuPermisos = $menuPermisos;
    $this->log = $log;
    $this->envioMasivo = $envioMasivo;

  }

    /**
    * Genera la vista inicial para el listado de destinatarios de envios
    *
    * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
    * @author Julian Restrepo <j.restrepo@waplicaciones.com.co>
    * @return render('envioDestinatario/index.html.twig') HTML
    **/
  public function index(Request $request){

    // Variables
    $session = $request->getSession();


    $em = $this->getDoctrine()->getManager();
    $idEnvio=$request->get('idEnvio');

    $dfColumnasAsociado = [
      ["headerClass" => "h6", 'headerName' => '#',         'field' => 'id',        'hide' => true],
      ["headerClass" => "h6", 'headerName' => '',         'field' => 'id', 'width' => 50,'cellClass' => 'text-center', 'cellRenderer' => 'agCellRender','cellRendererParams' => array(
            'grilla' => 'default', 'columna'  => 'copiaMsg',
            'aData' => [
              'btnClass' => 'btn btn-outline-primary border-0 font-weight-bold btnCopiaMensaje',
              'btnIconsClass' => 'far fa-eye'
            ]
          )],
      ["headerClass" => "h6", 'headerName' => 'Reenviar',    'field' => 'id',    'width' => 100, 'cellClass' => 'text-center', 'cellRenderer' => 'agCellRender','cellRendererParams' => array(
            'grilla' => 'default', 'columna'  => 'reenviarMsg',
            'aData' => [
              'btnClass' => 'btn btn-outline-primary border-0 font-weight-bold btnReenviar',
              'btnIconsClass' => 'fa fa-reply-all'
            ]
          )],
      ["headerClass" => "h6", 'headerName' => 'Leido',    'field' => 'leido',    'width' => 60, 'cellClass' => 'text-center'],
      ["headerClass" => "h6", 'headerName' => 'Departamento',    'field' => 'depto',    'width' => 200],
      ["headerClass" => "h6", 'headerName' => 'Ciudad',    'field' => 'ciudad',    'width' => 100,'cellClass' => 'text-center'],
      ["headerClass" => "h6", 'headerName' => 'Centro',    'field' => 'centro',    'width' => 100,'cellClass' => 'text-center'],
      ["headerClass" => "h6", 'headerName' => 'Código',     'field' => 'codigo',     'width' => 100,'cellClass' => 'text-center'],
      ["headerClass" => "h6", 'headerName' => 'Droguería',  'field' => 'drogueria',  'width' => 300],
      ["headerClass" => "h6", 'headerName' => 'Nit',       'field' => 'nit', 'width' => 250, 'cellClass' => 'text-center'],
      ["headerClass" => "h6", 'headerName' => 'Email',       'field' => 'email', 'width' => 250, 'cellClass' => 'text-center'],
      ["headerClass" => "h6", 'headerName' => 'Fecha Envio',       'field' => 'fechaEnvio', 'width' => 250, 'cellClass' => 'text-center']
    ];


    $dfColumnasProveedor = [
      ["headerClass" => "h6", 'headerName' => '#',         'field' => 'id',        'hide' => true],
      ["headerClass" => "h6", 'headerName' => '',    'field' => 'id',    'width' => 50,'cellClass' => 'text-center', 'pinned'=>true, 'cellRenderer' => 'agCellRender','cellRendererParams' => array(
            'grilla' => 'default', 'columna'  => 'copiaMsgProv',
            'aData' => [
              'btnClass' => 'btn btn-outline-primary border-0 font-weight-bold btnCopiaMensaje',
              'btnIconsClass' => 'far fa-eye'
            ]
          )],
      ["headerClass" => "h6", 'headerName' => 'Reenviar',    'field' => 'id',    'width' => 100,'cellClass' => 'text-center', 'pinned'=>true, 'cellRenderer' => 'agCellRender','cellRendererParams' => array(
            'grilla' => 'default', 'columna'  => 'reenviarMsgProv',
            'aData' => [
              'btnClass' => 'btn btn-outline-primary border-0 font-weight-bold btnReenviar',
              'btnIconsClass' => 'fa fa-reply-all'
            ]
          )],
      ["headerClass" => "h6", 'headerName' => 'Leidos',     'field' => 'id',     'width' => 60,'cellClass' => 'text-center'],
      ["headerClass" => "h6", 'headerName' => 'Nit',  'field' => 'nit',  'width' => 120],
      ["headerClass" => "h6", 'headerName' => 'Código',       'field' => 'codigo', 'width' => 100, 'cellClass' => 'text-center'],
      ["headerClass" => "h6", 'headerName' => 'Empresa',       'field' => 'empresa', 'width' => 250, 'cellClass' => 'text-center'],
      ["headerClass" => "h6", 'headerName' => 'Nombre Representante',       'field' => 'nombreRep', 'width' => 250, 'cellClass' => 'text-center'],
      ["headerClass" => "h6", 'headerName' => 'Email Representante',       'field' => 'emailRep', 'width' => 250, 'cellClass' => 'text-center'],
      ["headerClass" => "h6", 'headerName' => 'Nombre Contacto',       'field' => 'nombre', 'width' => 250, 'cellClass' => 'text-center'],
      ["headerClass" => "h6", 'headerName' => 'Email',       'field' => 'email', 'width' => 250, 'cellClass' => 'text-center'],
      ["headerClass" => "h6", 'headerName' => 'Fecha Envio',       'field' => 'fechaEnvio', 'width' => 150, 'cellClass' => 'text-center'],
    ];

    $entity = $em->createQuery("SELECT g.id, g.clasificacion, e.asunto 
      FROM App\Entity\Envio e 
      JOIN App\Entity\Grupo g WITH e.grupo = g.id 
      WHERE e.id=".$idEnvio)->getSingleResult();

    $entityintegrante = $em->createQuery("SELECT a.id, ig.proveedorId 
      FROM App\Entity\IntegranteGrupo ig 
      LEFT JOIN App\Entity\Cliente a WITH ig.asociadoId = a.id 
      WHERE ig.grupo=".$entity['id'])->getResult();

    $asociado=false;
    $proveedor=false;

    if ($entityintegrante) {
      foreach ($entityintegrante as $value) {
        if ($value['id']) {
          $asociado=true;
        }
        if ($value['proveedorId']) {
          $proveedor=true;
        }
      }
    }

    if (!$entity) {
        throw $this->createNotFoundException('El grupo que intenta consultar no tiene Entidad.');
    }
    
    if($entity['clasificacion'] == "Asociados"){
        $tipoGrupo = 1;
    }else{
        $tipoGrupo = 2;
    }

    $this->log->setLogAdmin("LD1 Listado de Destinatarios");

    return $this->render('envioDestinatario/index.html.twig', array(
      'dfColumnasProveedor'    => json_encode($dfColumnasProveedor),
      'dfColumnasAsociado'    => json_encode($dfColumnasAsociado),
      'envioId'=>$idEnvio,
      'tipoGrupo'=>$tipoGrupo,
      'entity'=>$entity,
      'proveedor'=>$proveedor,
      'asociado'=>$asociado
    ));
  }


  /**
    * Responde un JSON con la información requerida para mostrar el listado de envios
    * 
    * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
    * @param App\Services\GrillaGlobal $grillaGlobal para realizar los filtros de busqueda
    * @param $exportar Type=bool Valida si se filtran datos necesarios para descargar un archivo .csv
    * @author Julian Restrepo <j.restrepo@waplicaciones.com.co>
    * @return JSON
    */
  public function proveedoresDestinatariosJson(Request $request, GrillaGlobal $grillaGlobal, $exportar = false){
    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $envioId = $request->get('idEnvio');
    $aJson = array();

    $session = $request->getSession();

    $em = $this->getDoctrine()->getManager();
    $emp = $this->getDoctrine()->getManager('contactosProveedor');

    $aEquivalenciaColumnas = [
      'id'        => 'p.id',
      'nit'    => 'p.nit',
      'codigo'    => 'p.codigo',
      'empresa'    => 'p.nombre',
      'nombreRep'    => 'p.representanteLegal',
      'emailRep'     => 'p.emailRepresentanteLegal',
      'nombre'     => 'c.nombreContacto',
      'email'     => 'c.email',
      'contactoId'     => 'c.id',
      'fechaEnvio'     => 'p.id'
    ];


    $integrantesEnvio = array();
    $envioIntegrante=$em->createQuery("SELECT ig.proveedorId, ig.contactoProveedorId, ei.enviado, ig.id as idIntegrante,ei.fechaEnvio 
      FROM App\Entity\EnvioIntegrante ei 
      JOIN App\Entity\IntegranteGrupo ig WITH ei.integranteGrupoId = ig.id 
      WHERE ei.envio=".$envioId)->getResult();

    $idsProveedorarray=array();
    $idsContactoProveedorarray=array();
    foreach ($envioIntegrante as $key => $envio) {
      $idsProveedorarray[]=$envio['proveedorId'];
      if ($envio['contactoProveedorId']) {
        $idsContactoProveedorarray[]=$envio['contactoProveedorId'];
      }
      $integrantesEnvio[$envio['proveedorId']]['enviado']=$envio['enviado'];
      $integrantesEnvio[$envio['proveedorId']]['integrante']=$envio['idIntegrante'];
      $integrantesEnvio[$envio['proveedorId']]['fechaEnvio']=$envio['fechaEnvio'];
    }

    $idsProveedor=array_unique($idsProveedorarray);
    $idsContactoProveedor=array_unique($idsContactoProveedorarray);

    $condicionContacto=' c.id IN ('.implode(',', $idsContactoProveedor).') ';
    if (!$idsProveedor) {
      $idsProveedor = array(0);
    }
    if (!$idsContactoProveedor) {
      $condicionContacto='p.id != 0';
    }


    $integrantesEnvio = array();
    $envioIntegrante=$em->createQuery("SELECT ig.proveedorId, ig.contactoProveedorId, ei.enviado, ei.id AS envioIntegranteId, ig.id as idIntegrante,ei.fechaEnvio 
      FROM App\Entity\EnvioIntegrante ei 
      JOIN App\Entity\IntegranteGrupo ig WITH ei.integranteGrupoId = ig.id 
      WHERE ei.envio=".$envioId)->getResult();

    $idsProveedor='';
    foreach ($envioIntegrante as $key => $envio) {
      if($key==0){
        $idsProveedor.=$envio['proveedorId'];
      }else{
        $idsProveedor.=','.$envio['proveedorId'];
      }
      $integrantesEnvio[$envio['proveedorId']]['enviado']=$envio['enviado'];
      $integrantesEnvio[$envio['proveedorId']]['integrante']=$envio['idIntegrante'];
      $integrantesEnvio[$envio['proveedorId']]['fechaEnvio']=$envio['fechaEnvio'];
      $integrantesEnvio[$envio['proveedorId']]['envioIntegranteId']=$envio['envioIntegranteId'];
    }
    $condicionprovedor="";
    if ($idsProveedor) {
      $condicionprovedor=" AND p.id IN (".$idsProveedor.") ";
    }
      

      

      // Procedimiento Filtro Grilla
      $sIniFiltro = $condicionContacto.$condicionprovedor;
      $aDataGrilla = $grillaGlobal->realizarFiltro($aEquivalenciaColumnas, $sIniFiltro);

      // --- --- --- --- Total Filas --- --- --- --- //
      $numeroRegistros = 0;
      if( $aDataGrilla["paginaActual"] == 1 && $exportar === false ){

        $Contador = $emp->createQuery("SELECT COUNT(p.id) AS numeroRegistros
        FROM App\Entity\Proveedores p
        JOIN App\Entity\Contactos c WITH c.idProveedor = p.id
        WHERE {$aDataGrilla["where"]} ");

        $Contador->setParameters($aDataGrilla["valoresWhere"]);
        $Contador = $Contador->getSingleResult();
        $numeroRegistros = $Contador['numeroRegistros'];
      }

      // DQL Data
      $queryUser = $emp->createQuery("SELECT p.nit, p.codigo, p.nombre, p.representanteLegal, p.emailRepresentanteLegal, c.nombreContacto, c.email, p.id,c.id AS contactoId 
        FROM App\Entity\Proveedores p
        LEFT JOIN App\Entity\Contactos c WITH c.idProveedor = p.id 
        WHERE {$aDataGrilla["where"]} 
        ORDER BY {$aDataGrilla["order"]} ");


      // Resultado
      $queryUser->setParameters($aDataGrilla["valoresWhere"]);

      if( $exportar === false ){
        $queryUser->setMaxResults($aDataGrilla["maximoFilas"]);
        $queryUser->setFirstResult($aDataGrilla["paginacion"]);
      }
      $aUser = $queryUser->getScalarResult();

        // --- --- --- Lógica --- --- --- //
        $aListUser = array();
        foreach( $aUser as $entiti ){

          if(isset($integrantesEnvio[$entiti['id']])){

            $aListUser [] = array(
              'id'       => $entiti['id'],
              'nit'   => $entiti['nit'],
              'codigo'   => $entiti['codigo'],
              'empresa'    => $entiti['nombre'],
              'nombreRep' => $entiti['representanteLegal'],
              'emailRep'   => $entiti['emailRepresentanteLegal'],
              'nombre'   => $entiti['nombreContacto'],
              'email'   => $entiti['email'],
              'contactoId'   => $entiti['contactoId'],
              'integrante'   => $integrantesEnvio[$entiti['id']]['integrante'],
              'envioIntegranteId'   => $integrantesEnvio[$entiti['id']]['envioIntegranteId'],
              'fechaEnvio'   => (isset($integrantesEnvio[$entiti['id']]['fechaEnvio']))?$integrantesEnvio[$entiti['id']]['fechaEnvio']->format('Y-m-d H:i:s'):''
            );


          }

          
        }

        // Cierre de conexion y Respuesta
        $em->getConnection()->close();
        $response->setContent(json_encode(['totalRows' => $numeroRegistros, 'data' => $aListUser]));
    

    return $response;
  }


  /**
  * Responde un JSON con el resultado de la operacion de agregar un proveedor como copia de destinatario
  * 
  * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
  * @param App\Services\GrillaGlobal $grillaGlobal para realizar los filtros de busqueda
  * @author Julian Restrepo <j.restrepo@waplicaciones.com.co>
  * @return JSON
  */
  public function proveedorDestinatariosAgregar(Request $request, GrillaGlobal $grillaGlobal){
        
        $em = $this->getDoctrine()->getManager();
        $proveedorId = $request->get('proveedorId');
        $envioId = $request->get('envioId');
        $session = $request->getSession();

        if ($request->get('multiple') == 'true') {

          $aDataGrilla = json_decode($this->proveedoresDestinatariosJson($request, $grillaGlobal, true)->getContent(), 1);

            $sql = "SELECT ig.proveedor_id AS id,cp.id AS asignado 
                FROM envio en 
                INNER JOIN grupo g ON g.id = en.grupo_id 
                INNER JOIN integrante_grupo ig ON g.id = ig.grupo_id AND ig.proveedor_id IS NOT NULL
                LEFT JOIN copia_envio cp ON cp.proveedor_id=ig.proveedor_id AND cp.envio_id= ".$envioId." WHERE en.id= ".$envioId;
            $entities= $em->getConnection()->query($sql)->fetchAll();

            $asignados = array();
            foreach($entities as $fila){
              if($fila['asignado']){
                $asignados[(int)$fila['proveedor_id']] = $fila['asignado'];
              }
              
            }


            $batchSize = 200;
            $i=0;
            $insert1='';
            $insert0='INSERT INTO copia_envio (asociado_id, proveedor_id, envio_id, activo) VALUES ';
            $registros=0;
            foreach($aDataGrilla["data"] as $d){
                if(!isset($asignados[(int)$d['id']])){
                    if($insert1!='')
                        $insert1.=', ';
                    $insert1.='(NULL,\''.$d['id'].'\', \''.$envioId.'\', \'1\')';
                    if (($i % $batchSize) == 0) {
                        $em->getConnection()->query($insert0.$insert1);
                        $insert1='';
                    }
                    $i++;
                }
            }
            if($i >1){
                $em->getConnection()->query($insert0.$insert1);
            }
        } else {
            $entidad = $em->getRepository('App\Entity\CopiaEnvio')->findOneBy(array('proveedorId' => $proveedorId, 'envio' => $envioId));
            if (!$entidad) {
                $integrante = new CopiaEnvio();
                $integrante->setActivo(1);
                $integrante->setEnvio($em->getReference('App\Entity\Envio',$envioId));
                $integrante->setProveedorId($proveedorId);
                $em->persist($integrante);
                $em->flush();
            }
        }
        //$auditoria->registralog('Agrega proveedores', $session->get('id_usuario'));
        $response = new Response(json_encode(array('resultado' => '1')));
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


    /**
    * Responde un JSON con el resultado de la operacion de eliminar un proveedor como copia de destinatario
    * 
    * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
    * @param App\Services\GrillaGlobal $grillaGlobal para realizar los filtros de busqueda
    * @author Julian Restrepo <j.restrepo@waplicaciones.com.co>
    * @return JSON
    */
    public function proveedorDestinatariosEliminar(Request $request){

        $em = $this->getDoctrine()->getManager();
        $proveedorId = $request->get('proveedorId');
        $envioId = $request->get('envioId');
        $session = $request->getSession();

        if ($request->get('multiple') == 'true') {

            $sql = "DELETE FROM copia_envio WHERE envio_id = '$envioId' AND proveedor_id IS NOT NULL AND asociado_id IS NULL";

            $em->getConnection()->query($sql);
        } else {

            $entidad = $em->getRepository('App\Entity\CopiaEnvio')->findOneBy(array('proveedorId' => $proveedorId, 'envio' => $envioId));

            if ($entidad) {
                $em->remove($entidad);
                $em->flush();
            }
        }
        //$auditoria->registralog('Elimina proveedores', $session->get('id_usuario'));
        $response = new Response(json_encode(array('resultado' => '1')));
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }



   /**
    * Responde un JSON con la información requerida para mostrar el listado destinatarios de tipo asociado
    * 
    * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
    * @param App\Services\GrillaGlobal $grillaGlobal para realizar los filtros de busqueda
    * @param $exportar Type=bool Valida si se filtran datos necesarios para descargar un archivo .csv
    * @author Julian Restrepo <j.restrepo@waplicaciones.com.co>
    * @return JSON
    */
  public function asociadosDestinatariosJson(Request $request, GrillaGlobal $grillaGlobal, $exportar = false){
    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $aJson = array();
    $envioId = $request->get('idEnvio');


      $session = $request->getSession();
      $em = $this->getDoctrine()->getManager();

      $aEquivalenciaColumnas = [
        'id'       => 'a.id',
        'depto'   => 'a.depto',
        'ciudad'   => 'a.ciudad',
        'centro'    => 'a.centro',
        'codigo' => 'a.codigo',
        'drogueria'   => 'a.drogueria',
        'nit'   => 'a.nit',
        'asociado'   => 'a.asociado',
        'email'   => 'a.email',
        'emailAsociado'   => 'a.emailAsociado',
        'asignado'   => 'cp.id',
        'enviado'   => 'ci.enviado',
        'idIntegrante'   => 'ig.id',
        'fechaEnvio'   => 'ci.fechaEnvio',
        'leido'   => 'a.id'
      ];


      // Procedimiento Filtro Grilla
      //$sIniFiltro = "a.id=".$session->get('id');
      $sIniFiltro = "en.id=".$envioId;
      $aDataGrilla = $grillaGlobal->realizarFiltro($aEquivalenciaColumnas, $sIniFiltro);

      // --- --- --- --- Total Filas --- --- --- --- //
      $numeroRegistros = 0;
      if( $aDataGrilla["paginaActual"] == 1 && $exportar === false ){

        $sql = "SELECT COUNT(DISTINCT a.id) AS numeroRegistros 
                FROM App\Entity\Envio en 
                INNER JOIN App\Entity\Grupo g WITH g.id = en.grupo
                INNER JOIN App\Entity\IntegranteGrupo ig WITH g.id = ig.grupo AND ig.activo=1
                INNER JOIN App\Entity\Cliente a WITH a.id=ig.asociadoId AND a.retirado=0
                WHERE {$aDataGrilla["where"]}";


        $Contador = $em->createQuery($sql);
        $Contador->setParameters($aDataGrilla["valoresWhere"]);
        $Contador = $Contador->getSingleResult();
        $numeroRegistros = $Contador['numeroRegistros'];
      }

      // DQL Data

      $sql = "SELECT a.id, a.depto,a.ciudad,a.centro,a.codigo,a.drogueria,a.nit,a.asociado,a.email, a.emailAsociado, cp.id AS asignado, ci.enviado AS enviado, ci.id AS envioIntegranteId, ig.id as idIntegrante,ci.fechaEnvio as fechaEnvio,COUNT(el.id) as leido 
          FROM App\Entity\Envio en 
          INNER JOIN App\Entity\Grupo g WITH g.id = en.grupo 
          INNER JOIN App\Entity\IntegranteGrupo ig WITH g.id = ig.grupo AND ig.activo=1
          INNER JOIN App\Entity\Cliente a WITH a.id=ig.asociadoId AND a.retirado=0
          LEFT JOIN App\Entity\CopiaEnvio cp WITH cp.asociadoId=a.id AND en.id=cp.envio
          LEFT JOIN App\Entity\EnvioIntegrante ci WITH ci.integranteGrupoId=ig.id AND ci.envio=$envioId
          LEFT JOIN App\Entity\EnvioLectura el WITH el.envioIntegrateId=ig.id AND el.envioId=$envioId    
          WHERE {$aDataGrilla["where"]}
          GROUP BY a.id
          ORDER BY {$aDataGrilla["order"]}";

        $queryUser = $em->createQuery($sql);

      // Resultado
      $queryUser->setParameters($aDataGrilla["valoresWhere"]);

      if( $exportar === false ){
        $queryUser->setMaxResults($aDataGrilla["maximoFilas"]);
        $queryUser->setFirstResult($aDataGrilla["paginacion"]);
      }
      $aUser = $queryUser->getScalarResult();

        // --- --- --- Lógica --- --- --- //
        $aListUser = array();
        foreach( $aUser as $entiti ){



          $aListUser [] = array(
            'id'       => $entiti['id'],
            'depto'   => $entiti['depto'],
            'ciudad'   => $entiti['ciudad'],
            'centro'    => $entiti['centro'],
            'codigo' => $entiti['codigo'],
            'drogueria'   => $entiti['drogueria'],
            'nit'   => $entiti['nit'],
            'asociado'   => $entiti['asociado'],
            'email'   => $entiti['email'],
            'emailAsociado'   => $entiti['emailAsociado'],
            'asignado'   => $entiti['asignado'],
            'enviado'   => $entiti['enviado'],
            'idIntegrante'   => $entiti['idIntegrante'],
            'fechaEnvio'   => $entiti['fechaEnvio'],
            'leido'   => $entiti['leido'],
            'envioIntegranteId'   => $entiti['envioIntegranteId'],
            'tipoGrupo'   => $request->get('tipoGrupo')
          );
        }

        // Cierre de conexion y Respuesta
        $em->getConnection()->close();
        $response->setContent(json_encode(['totalRows' => $numeroRegistros, 'data' => $aListUser]));
    

    return $response;
  }


  /**
    * Responde un JSON con el resultado de la operacion de agregar un asociado como copia de destinatario
    * 
    * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
    * @param App\Services\GrillaGlobal $grillaGlobal para realizar los filtros de busqueda
    * 
    * @return JSON
    */
  public function asociadosDestinatariosAgregar(Request $request , GrillaGlobal $grillaGlobal){

      set_time_limit(0);

      $response = new Response();
      $response->headers->set('Content-Type', 'application/json');

      $em = $this->getDoctrine()->getManager();

      $asociadoId = $request->get('asociadoId');
      $envioId = $request->get('envioId');
      $session = $request->getSession();

      
      if($request->get('multiple') == 'true'){

          $aDataGrilla = json_decode($this->asociadosDestinatariosJson($request, $grillaGlobal, true)->getContent(), 1);

          $batchSize = 200;
          $i=0;
          $insert1='';
          $insert0='INSERT INTO copia_envio (asociado_id, proveedor_id, envio_id, activo) VALUES ';
          $registros=0;
          foreach($aDataGrilla["data"] as $d){
              if(!$d['asignado']){
                  if($insert1!='')
                      $insert1.=', ';
                  $insert1.='(\''.$d['id'].'\', NULL, \''.$envioId.'\', \'1\')';
                  if (($i % $batchSize) == 0) {
                      $em->getConnection()->query($insert0.$insert1);
                      $insert1='';
                  }
                  $i++;
              }
          }
          if($i >1){
              $em->getConnection()->query($insert0.$insert1);
          }
      }else{

          $entidad = $em->getRepository('App\Entity\CopiaEnvio')->findOneBy(array('asociadoId' => $asociadoId, 'envio' => $envioId));
          if(!$entidad){           
              $integrante = new CopiaEnvio();
              $integrante->setActivo(1);
              
              $integrante->setEnvio($em->getReference('App\Entity\Envio',$envioId) );
              $integrante->setAsociadoId($asociadoId);
              
              $em->persist($integrante);
              $em->flush();
          }
      }


      $aJson['resultado'] ='1';
      $response->setContent(json_encode($aJson));

      return $response;
  }


  /**
  * Responde un JSON con el resultado de la operacion de eliminar un asociado como copia de destinatario
  * 
  * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
  * @param App\Services\GrillaGlobal $grillaGlobal para realizar los filtros de busqueda
  * 
  * @return JSON
  */
  public function asociadosDestinatariosEliminar(Request $request){
        
        $em = $this->getDoctrine()->getManager();
        $asociadoId = $request->get('asociadoId');
        $envioId = $request->get('envioId');
        $session = $request->getSession();

        
        if ($request->get('multiple') == 'true') {

            $sql = "DELETE FROM copia_envio WHERE envio_id = '$envioId' AND asociado_id IS NOT NULL AND proveedor_id IS NULL";

            $em->getConnection()->query($sql)->execute();
            
            $response = new Response(json_encode(array('resultado' => '1')));
            $response->setStatusCode(200);
            $response->headers->set('Content-Type', 'application/json');
            
        } else {

            $entidad_del = $em->getRepository('App\Entity\CopiaEnvio')->findOneBy(array('asociadoId' => $asociadoId, 'envio' => $envioId));
            if($entidad_del){

                $em->remove($entidad_del);
                $em->flush();

                $response = new Response(json_encode(array('resultado' => '1')));
                $response->setStatusCode(200);
                $response->headers->set('Content-Type', 'application/json');
            }else{
                $response = new Response(json_encode(array('resultado' => '0')));
                $response->headers->set('Content-Type', 'application/json');
            }
        }
        //$auditoria->registralog('Eliminacion asociados', $session->get('id_usuario'));
        return $response;
    }


    /**
  * Responde un archivo XLS con la informacion de los destinatarios del envio
  * 
  * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
  * @param App\Services\GrillaGlobal $grillaGlobal para realizar los filtros de busqueda
  * 
  * @return XLS
  */
    public function asociadosDestinatariosXls(Request $request, GrillaGlobal $grillaGlobal){
      ini_set("memory_limit", "1024M");
      set_time_limit(0);

      $envioId = $request->get('envioId');
      $session = $request->getSession();
      $em = $this->getDoctrine()->getManager();

      $nameFile='';

      $aEquivalenciaColumnas = [
        'id'       => 'a.id',
        'depto'   => 'a.depto',
        'ciudad'   => 'a.ciudad',
        'centro'    => 'a.centro',
        'codigo' => 'a.codigo',
        'drogueria'   => 'a.drogueria',
        'nit'   => 'a.nit',
        'asociado'   => 'a.asociado',
        'email'   => 'a.email',
        'emailAsociado'   => 'a.emailAsociado',
        'asignado'   => 'cp.id',
        'enviado'   => 'ci.enviado',
        'idIntegrante'   => 'ig.id',
        'fechaEnvio'   => 'ci.fechaEnvio',
        'leido'   => 'a.id'
      ];

      // Procedimiento Filtro Grilla
      //$sIniFiltro = "a.id=".$session->get('id');
      $sIniFiltro = "en.id=".$envioId;
      $aDataGrilla = $grillaGlobal->realizarFiltro($aEquivalenciaColumnas, $sIniFiltro);



      if ($request->get('detalle')) { 
        $nameFile='detalle';

        // DQL Data

        $sql = "SELECT a.id, a.depto,a.ciudad,a.centro,a.codigo,a.drogueria,a.nit,a.asociado,a.email, a.emailAsociado, ci.enviado AS enviado, ig.id as idIntegrante,ci.fechaEnvio as fechaEnvio,el.fecha,el.ip 
            FROM App\Entity\Envio en 
            INNER JOIN App\Entity\Grupo g WITH g.id = en.grupo 
            INNER JOIN App\Entity\IntegranteGrupo ig WITH g.id = ig.grupo AND ig.activo=1
            INNER JOIN App\Entity\Cliente a WITH a.id=ig.asociadoId AND a.retirado=0
            LEFT JOIN App\Entity\EnvioIntegrante ci WITH ci.integranteGrupoId=ig.id AND ci.envio=$envioId
            LEFT JOIN App\Entity\EnvioLectura el WITH el.envioIntegrateId=ig.id AND el.envioId=$envioId    
            WHERE {$aDataGrilla["where"]}
            ORDER BY {$aDataGrilla["order"]}";

          $queryUser = $em->createQuery($sql);

        // Resultado
        $queryUser->setParameters($aDataGrilla["valoresWhere"]);

        $asociado = $queryUser->getScalarResult();


        $titulos = array(1 => 'Drogueria',2 => 'Nit',3 => 'Email',4 => 'Codigo',5 => 'Ciudad',6 => 'Centro',7 => 'F.Envio',8 => 'F.Lectura',9 => 'IP');

        $excelService = New Spreadsheet();
        $activeSheet = $excelService->getActiveSheet();

        $excelService->getActiveSheet()->setTitle('Asociado');

        $columna = "A";
        $fila=1;
        $activeSheet->getColumnDimension('A')->setWidth(10);
        $activeSheet->getColumnDimension('B')->setWidth(30);
        $activeSheet->getColumnDimension('C')->setWidth(30);
        $activeSheet->getColumnDimension('D')->setWidth(25);
        $activeSheet->getColumnDimension('E')->setWidth(25);
        $activeSheet->getColumnDimension('F')->setWidth(25);
        $activeSheet->getColumnDimension('G')->setWidth(25);
        $activeSheet->getColumnDimension('H')->setWidth(25);
        $activeSheet->getColumnDimension('I')->setWidth(25);

        $activeSheet->setCellValue('A1', 'Drogueria');
        $activeSheet->setCellValue('B1', 'Nit');
        $activeSheet->setCellValue('C1', 'Email');
        $activeSheet->setCellValue('D1', 'Codigo');
        $activeSheet->setCellValue('E1', 'Ciudad');
        $activeSheet->setCellValue('F1', 'Centro');
        $activeSheet->setCellValue('G1', 'F.Envio');
        $activeSheet->setCellValue('H1', 'F.Lectura');
        $activeSheet->setCellValue('I1', 'IP');


        $fila++;

        foreach ($asociado as $key => $value) {

          $activeSheet->setCellValue('A'.$fila, $value['drogueria']);
          $activeSheet->setCellValue('B'.$fila, $value['nit']);
          $activeSheet->setCellValue('C'.$fila, $value['email']);
          $activeSheet->setCellValue('D'.$fila, $value['codigo']);
          $activeSheet->setCellValue('E'.$fila, $value['ciudad']);
          $activeSheet->setCellValue('F'.$fila, $value['centro']);
          $activeSheet->setCellValue('G'.$fila, $value['fechaEnvio']);
          $activeSheet->setCellValue('H'.$fila, $value['fecha']);
          $activeSheet->setCellValue('A'.$fila, $value['ip']);

          $fila++;

        }

      }else{
        $nameFile='consolidado';

        $sql = "SELECT a.id, a.depto,a.ciudad,a.centro,a.codigo,a.drogueria,a.nit,a.asociado,a.email, a.emailAsociado, ci.enviado AS enviado, ig.id as idIntegrante,ci.fechaEnvio as fechaEnvio,COUNT(el.id) as leido,el.fecha,el.ip  
          FROM App\Entity\Envio en 
          INNER JOIN App\Entity\Grupo g WITH g.id = en.grupo 
          INNER JOIN App\Entity\IntegranteGrupo ig WITH g.id = ig.grupo AND ig.activo=1
          INNER JOIN App\Entity\Cliente a WITH a.id=ig.asociadoId AND a.retirado=0
          LEFT JOIN App\Entity\EnvioIntegrante ci WITH ci.integranteGrupoId=ig.id AND ci.envio=$envioId
          LEFT JOIN App\Entity\EnvioLectura el WITH el.envioIntegrateId=ig.id AND el.envioId=$envioId    
          WHERE {$aDataGrilla["where"]}
          GROUP BY a.id
          ORDER BY {$aDataGrilla["order"]}";

          $queryUser = $em->createQuery($sql);

          // Resultado
          $queryUser->setParameters($aDataGrilla["valoresWhere"]);

          $asociado = $queryUser->getScalarResult();


        $titulos = array(1 => '# de veces leida',2 => 'Drogueria',3 => 'Nit',4 => 'Email',5 => 'Codigo',6 => 'Ciudad',7 => 'Centro',8 => 'F.Envio',9 => 'F.Lectura',10 => 'IP');
        
        $excelService = New Spreadsheet();
        $activeSheet = $excelService->getActiveSheet();

        $excelService->getActiveSheet()->setTitle('Asociado');

        $columna = "A";
        $fila=1;
        $activeSheet->getColumnDimension('A')->setWidth(10);
        $activeSheet->getColumnDimension('B')->setWidth(30);
        $activeSheet->getColumnDimension('C')->setWidth(30);
        $activeSheet->getColumnDimension('D')->setWidth(25);
        $activeSheet->getColumnDimension('E')->setWidth(25);
        $activeSheet->getColumnDimension('F')->setWidth(25);
        $activeSheet->getColumnDimension('G')->setWidth(25);
        $activeSheet->getColumnDimension('H')->setWidth(25);
        $activeSheet->getColumnDimension('I')->setWidth(25);
        $activeSheet->getColumnDimension('J')->setWidth(25);

        $activeSheet->setCellValue('A1', '# Lecturas');
        $activeSheet->setCellValue('B1', 'Drogueria');
        $activeSheet->setCellValue('C1', 'Nit');
        $activeSheet->setCellValue('D1', 'Email');
        $activeSheet->setCellValue('E1', 'Codigo');
        $activeSheet->setCellValue('F1', 'Ciudad');
        $activeSheet->setCellValue('G1', 'Centro');
        $activeSheet->setCellValue('H1', 'F.Envio');
        $activeSheet->setCellValue('I1', 'F.Lectura');
        $activeSheet->setCellValue('J1', 'IP');


        $fila++;


        foreach ($asociado as $key => $value) {

          $activeSheet->setCellValue('A'.$fila, $value['leido']);
          $activeSheet->setCellValue('B'.$fila, $value['drogueria']);
          $activeSheet->setCellValue('C'.$fila, $value['nit']);
          $activeSheet->setCellValue('D'.$fila, $value['email']);
          $activeSheet->setCellValue('E'.$fila, $value['codigo']);
          $activeSheet->setCellValue('F'.$fila, $value['ciudad']);
          $activeSheet->setCellValue('G'.$fila, $value['centro']);
          $activeSheet->setCellValue('H'.$fila, $value['fechaEnvio']);
          $activeSheet->setCellValue('I'.$fila, $value['fecha']);
          $activeSheet->setCellValue('J'.$fila, $value['ip']);

          $fila++;

        }
        unset($datos);
      }

      $writer = new Xlsx($excelService);
      $fileName= $nameFile.".xlsx";
        
      $writer->save($fileName);
      header('Content-type: application/vnd.ms-excel');
      header('Content-Disposition: attachment; filename='.$fileName);
      $writer->save('php://output');

      exit();
    }



public function reenviarMensaje( Request $request ){
    
    set_time_limit(500);

    $session = $request->getSession();
    $autenticacionMail=FALSE;
    $info='';

    $transport = (new \Swift_SmtpTransport($this->getParameter('conectorEmail'), $this->getParameter('puertoEmail'), $this->getParameter('seguridadSSLEmail')))
      ->setStreamOptions(array('ssl' => array(
        'verify_peer' => false,
        'allow_self_signed' => true
     )));

    
    try{
      $resultado =$transport->start();
      $autenticacionMail=TRUE;
    }catch(\Swift_TransportException $exentionMailer){
      $info=$exentionMailer->getMessage();
      $resultado = 6;
    }catch(\Exeption $e){
      $info=$e->getMessage();
      $resultado = 7;
    }
    
    $mailer = new \Swift_Mailer($transport);
    $correoAlEnvio='';
    if ($autenticacionMail) {

      $em = $this->getDoctrine()->getManager();
      $emConect = $em->getConnection();
      
      $emp = $this->getDoctrine()->getManager('contactosProveedor');
      $emContacConect = $emp->getConnection();
      
      $emsp = $this->getDoctrine()->getManager('sipproveedores');
      $emspConect = $emsp->getConnection();

      $entity = $em->getRepository('App\Entity\Envio')->find($request->get('envioId'));

      if (!$entity)
          throw $this->createNotFoundException('La entidad <Envio> no ha sido encontrada.');
      

      //------- Se verifica si el destinatario esta en envio integrante --------//
      if($request->get('envioIntegranteId') > 0){

        $Enviado = $em->getRepository('App\Entity\EnvioIntegrante')->findOneById($request->get('envioIntegranteId'));

        if(!$Enviado){

          $Enviado = new EnvioIntegrante();
          $Enviado->setEnvio($em->getReference('App\Entity\Envio', $request->get('envioId')));
          $Enviado->setIntegranteGrupoId($em->getReference('App\Entity\IntegranteGrupo', $rquest->get('integranteGrupoId') ));
          $Enviado->setEnviado(0);
          $Enviado->setReenviado(0);

          $em->persist($Enviado);
          $em->flush();

        }

      }else{

        $Enviado = new EnvioIntegrante();
        $Enviado->setEnvio($em->getReference('App\Entity\Envio', $request->get('envioId')));
        $Enviado->setIntegranteGrupoId($em->getReference('App\Entity\IntegranteGrupo', $rquest->get('integranteGrupoId') ));
        $Enviado->setEnviado(0);
        $Enviado->setReenviado(0);

        $em->persist($Enviado);
        $em->flush();

      }
      //----------------------------//


      $adjuntos=array();
      $na=0;


      $fecha = $entity->getAdministrador()->getFechaCreado();
      $fecha = $fecha->format('Ymd');
      $idAdmin = $entity->getAdministrador()->getId();
      $carpeta = $entity->getId().'/'.$fecha.$idAdmin.'/otros';

      $ruta=$this->getParameter('kernel.project_dir').'/public/adjuntos/'.$carpeta;
      
      if( $entity->getAdjuntos() > 0 ){

        if ( is_dir($ruta) ) {

          if ( $rep = opendir( $ruta ) ) { //Abrimos el directorio

            while ($arc = readdir($rep)) {  //Leemos el arreglo de archivos contenidos en el directorio: readdir recibe como parametro el directorio abierto

              if($arc != '..' && $arc !='.' && $arc !=''){

                $adjuntos[ $na ] = $ruta.'/'.$arc; 
                $na++;

              }
              
            }
          
          }//end if
        }//end if
      }//end if

      $contenido=$entity->getContenido();

      $destinatario=array();

      /************SE BUSCAN PROVEEDORES BLOQUEADOS SIPPROVEEDORES*************/
      $arrayProvBloqueados = $this->envioMasivo->provedoresBloqueadosSip($emspConect);
      /*************************/

      /**********SE BUSCAN LOS CARGOS SIN SEGUIMIENTO ****************/
      $arrayProvBloqueados = $this->envioMasivo->cargosSinSeguimiento($emContacConect);
      /**************************/
      
      //**SI VIENE DE REENVIO** --> Obtener los datos del destinatario

      if( $request->get('reenvioProveedorId') || $request->get('reenvioAsociadoId') ){
          
        //Asociado
          
        if($request->get('reenvioAsociadoId')){
            $entityDestinatario = $em->getRepository('App\Entity\IntegranteGrupo')->findOneBy(array('asociadoId'=>$request->get('reenvioAsociadoId')));

            $Query = $em->createQuery('SELECT c FROM App\Entity\Cliente c WHERE c.id = '.$request->get('reenvioAsociadoId').' AND c.retirado=0');
            
            $destinatarioEntitie = $Query->getArrayResult();
            if ($destinatarioEntitie) {
                
                $destinatarioEntitie=$destinatarioEntitie[0];

                $permitidos=array('zona','codigo','drogueria','nit','asociado','direcion','ciudad','ruta','depto','telefono','centro','email','emailAsociado');

                foreach($permitidos as $campo){

                    if(isset($destinatarioEntitie[$campo]))
                        $contenido = str_replace("//$campo//", utf8_decode($destinatarioEntitie[$campo]), $contenido);

                }

                if($entity->getGrupo()->getClasificacion()=='Asociados'){

                  $destinatario['email']=$destinatarioEntitie['emailAsociado'];
                  $destinatario['nombre']=$destinatarioEntitie['asociado'];
                  $destinatario['Codigo']=$destinatarioEntitie['codigo'];
                  $destinatario['Nit']=$destinatarioEntitie['nit'];
                  $destinatario['id']=$destinatarioEntitie['id'];
                  $destinatario['tipo']='Asociado';
                }else{

                  $destinatario['email']=$destinatarioEntitie['email'];
                  $destinatario['nombre']=$destinatarioEntitie['drogueria'];
                  $destinatario['Codigo']=$destinatarioEntitie['codigo'];
                  $destinatario['Nit']=$destinatarioEntitie['nit'];
                  $destinatario['id']=$destinatarioEntitie['id'];
                  $destinatario['tipo']='Drogueria';
                }

                

                $destinatario['cargo']=0;
            }
        //-->proveedor**
        }elseif($request->get('reenvioProveedorId')){     


          $entityDestinatario = $em->getRepository('App\Entity\IntegranteGrupo')->findOneBy(array('id'=> $request->get('integranteGrupoId') ));

          if ($entityDestinatario->getContactoProveedorId()) {

            $Query = $emp->createQuery('SELECT p.nit, cp.nombreContacto AS nombre, p.nombre AS empresa, cp.telefono AS telefono, cp.movil AS celular, cp.email AS email1, p.codigo, p.id, car.id AS cargoId 
              FROM App\Entity\Contactos cp 
              LEFT JOIN App\Entity\Proveedores p WITH cp.idProveedor = p.id 
              LEFT JOIN App\Entity\Cargos car WITH cp.idCargo = car.id
              WHERE cp.id = '.$entityDestinatario->getContactoProveedorId());

          }else{
            $Query = $emp->createQuery('SELECT p.nit,  p.representanteLegal AS nombre, p.nombre AS empresa, p.telefonoRepresentanteLegal AS telefono, p.telefonoRepresentanteLegal AS celular, p.emailRepresentanteLegal AS email1, p.codigo, p.id 
              FROM App\Entity\Proveedores p 
              WHERE p.id = '.$entityDestinatario->getProveedorId());
          }

          $destinatarioEntitie = $Query->getArrayResult();
          $destinatarioEntitie=$destinatarioEntitie[0];

          $permitidos=array('nit','nombre','empresa','telefono','celular','email1');

          foreach($permitidos as $campo){

              if(isset($destinatarioEntitie[$campo]))
                  $contenido = str_replace("//$campo//", utf8_decode($destinatarioEntitie[$campo]), $contenido);

          }
          
          $destinatario['email']=$destinatarioEntitie['email1'];
          $destinatario['nombre']=$destinatarioEntitie['nombre'];
          $destinatario['Codigo']=$destinatarioEntitie['codigo'];
          $destinatario['Nit']=$destinatarioEntitie['nit'];
          $destinatario['id']=$destinatarioEntitie['id'];
          $destinatario['tipo']='Proveedor';

          if( isset($destinatarioEntitie['cargoId']) ){
            $destinatario['cargo']=$destinatarioEntitie['cargoId'];
          }else{
            $destinatario['cargo']=0;
          }
        }

      }

      if($entity->getAgruparCorrespondencia() == 0 && $entity->getAgruparProveedor() == 0){

        if($entity->getCombinacion()==1){

          $infoEnvio['id'] = $request->get('envioId');
          $infoEnvio['columna_combinacion'] = $entity->getColumnaCombinacion();

          $contenido = $this->envioMasivo->combinarCorrespondencia($infoEnvio, $destinatario, $contenido, $emConect);

        }

      }
      
      $message = '';

      if($entityDestinatario->getAsociadoId() || ($destinatario['tipo'] == "Proveedor" && (!isset($arrayProvBloqueados[isset($destinatario['nit'])])) ) ){

        if( !isset( $arrayCargoSinSeguimiento[ $destinatario['cargo'] ] )  ){

            //Si es reenvio; ponemos un prefijo en el asunto.
            $message = new \Swift_Message('Reenviado: '.$entity->getAsunto() );
                
            //$message->setFrom(array($session->get('emailAdmin') => $session->get('nombreAdmin')))->setContentType('text/html');
            $message->setFrom(array('soporte1@waplicaciones.co' => 'Waplicaciones SAS' ))->setContentType('text/html');
                
            if($entityDestinatario->getAsociadoId()){
              $correoAlEnvio=$destinatario['email'];
              //$message->setTo($destinatario['email'],$destinatario['nombre']);
              $message->setTo('j.restrepo@waplicaciones.co',$destinatario['nombre']);
              //$message->setTo('julianrd19@hotmail.com',$destinatario['nombre']);
              
            }else{

              if(filter_var($destinatario['email'], FILTER_VALIDATE_EMAIL)){
                $correoAlEnvio=$destinatario['email'];
                //$message->setTo($destinatario['email'],$destinatario['nombre']);
                $message->setTo('j.restrepo@waplicaciones.co',$destinatario['nombre']);
                //$message->setTo('julianrd19@hotmail.com',$destinatario['nombre']);


              }
                
            }

            // Copia del envio.  
            if($destinatario['tipo']=='Proveedor'){
              $copia_envio = $em->getRepository('App\Entity\CopiaEnvio')->findOneBy(array('proveedorId'=>$destinatario['id'],'envio'=>$entity->getId()));
            }else{

              $copia_envio = $em->getRepository('App\Entity\CopiaEnvio')->findOneBy(array('asociadoId'=>$destinatario['id'],'envio'=>$entity->getId()));

            }

            //Copia envío administrador con sesión activa
            
            if($copia_envio){
              //$message->addCc($session->get('emailAdmin'),$session->get('nombreAdmin'));
            }
              
            
            if($entity->getAgruparCorrespondencia() == 1 || $entity->getAgruparProveedor() == 1){
              $urlLectura = '';
            }else{
              $urlLectura = $this->getParameter('urlApp').$this->generateUrl('admin_envio_destinatario_lectura', ['idEnvio' => $entity->getId(),'Tipo'=>$destinatario['tipo'],'destinatarioId'=>$entityDestinatario->getId()]);
            }

            $template= str_replace('../../../uploads/images',  $this->getParameter('carpetaUploads') ,
                    $this->renderView('envioDestinatario/email_send.html.twig'
                    ,array(
                      'mensaje'=>$contenido,
                      'destinatario'=>$destinatario,
                      'destinatarioId'=>$entityDestinatario->getId(),
                      'idEnvio'=>$entity->getId(),
                      'urlLectura' => $urlLectura
                    )));

            $message->setBody($template);
            
            foreach ($adjuntos as $path) {

              $attachment = \Swift_Attachment::fromPath($path);
              $message->attach($attachment);

            }
            try{

              $mailer->send($message);

              $resultado=1;

              $Enviado->setReenviado($Enviado->getReenviado()+1);
              $Enviado->setFechaReenvio(new \DateTime());
              $em->persist($Enviado);
              $em->flush();


            }catch(\Exception $e){
              $info=$e->getMessage();
              $resultado=2;

              $errorEnvio = new EnvioError();

              $errorEnvio->setEnvioId($request->get('envioId'));
              $errorEnvio->setEnvioIntegranteId($Enviado->getId());
              $errorEnvio->setFecha(new \DateTime());
              $errorEnvio->setError($info);
              $errorEnvio->setReportado(0);

              $em->persist($errorEnvio);
              $em->flush();


            }catch(\ContextErrorException $e){
              $info=$e->getMessage();
              $resultado=2;

              $errorEnvio = new EnvioError();

              $errorEnvio->setEnvioId($request->get('envioId'));
              $errorEnvio->setEnvioIntegranteId($Enviado->getId());
              $errorEnvio->setFecha(new \DateTime());
              $errorEnvio->setError($info);
              $errorEnvio->setReportado(0);

              $em->persist($errorEnvio);
              $em->flush();
            }


        }else{

          $resultado=1;
          
          $info = "Email no Enviado. ".$destinatario['nombre']." No Habilitado para envio de correos. ";
        }


      }else{
          $resultado=1;
          
          $info = "Email no Enviado. Proveedor ".$destinatario['nombre']." Bloqueado por informacion desactualizada.";
      }
      
      
    }

    $response = new Response(json_encode(array('resultado' => $resultado,'info' => $info)));
    $response->setStatusCode(200);
    $response->headers->set('Content-Type', 'application/json');
    return $response;

}// end action


    /**
     * REcibe solicitud desde el email enviado y construye un CSV con los datos correspondientes al destinatario que reliza la solicitud.
     * @param int $id Identificador del envio.
     * @return \Symfony\Component\HttpFoundation\Response mixted json('Resultado de la operacion')
     * @author Julian Restrepo <j.restrepo@waplicaciones.co>
     * @category Correos\EnvioDesitnatario
     */
    public function envioDestinatarioXls(Request $request) {
      //echo "entra";exit();
      ini_set("memory_limit", "1024M");
      ini_set('max_execution_time', 0);

      $llave1=$request->get('llave');
      if(!$llave1){
          echo "Se requiere llave de acceso";exit();
      }
      $em = $this->getDoctrine()->getManager();
      $emp = $this->getDoctrine()->getManager('contactosProveedor');

      $envio = $em->getRepository('App\Entity\Envio')->find($request->get('envio_id'));
      if(!$envio){
          throw $this->createNotFoundException('La informaci&oacute;n requerida ya no esta disponible.');
      }
      if($request->get('tipo_usuario')== 'asociado' || $request->get('tipo_usuario')== 'Asociado' || $request->get('tipo_usuario')== 'drogueria' || $request->get('tipo_usuario')== 'Drogueria'){

          $datosUsuario = $em->getRepository('App\Entity\Cliente')->find($request->get('id_usuario'));
          $asociado = true;
      }else{
          if($request->get('tipo_usuario')== 'proveedor' || $request->get('tipo_usuario') == 'Proveedor'){
            $datosUsuario = $emp->createQuery('SELECT p.id, p.nit, p.codigo 
              FROM App\Entity\Proveedores p 
              WHERE p.id = :destinatarioId')->setParameter('destinatarioId', $request->get('id_usuario'))->getResult();
          }else{
            $datosUsuario = $emp->createQuery("SELECT p.id, p.codigo, cp.id AS idContacto, p.nit 
            FROM App\Entity\Contactos cp 
            LEFT JOIN App\Entity\Proveedores p WITH cp.idProveedor = p.id 
            WHERE cp.id=".$request->get('id_usuario'))->getResult();
          }
          $asociado = false;
          $datosUsuario = $datosUsuario[0];
          
      }
      if(!$datosUsuario){
          
          echo "Lo sentimos, no encontramos información para entregarle.";exit();
      }
      $nameFile='';
      $idDestinatario=0;
      if ($envio->getColumnaCombinacion() == 'Codigo') {
          
          if($asociado){
              $parametroBusqueda = $datosUsuario->getCodigo();
              $posicion = 0;
              $nameFile=$datosUsuario->getCodigo();
              $idDestinatario=$datosUsuario->getId();
          }else{
              $parametroBusqueda = $datosUsuario['codigo'];
              $posicion = 0;
              $nameFile=$datosUsuario['codigo'];
              $idDestinatario=$datosUsuario['id'];
          }
          
          
      }else{
          
          if($asociado){
              $parametroBusqueda = $datosUsuario->getNit();
              $posicion = 0;
              $nameFile=$datosUsuario->getNit();
              $idDestinatario=$datosUsuario->getId();
          }else{
              $parametroBusqueda = $datosUsuario['nit'];
              $posicion = 0;
              $nameFile=$datosUsuario['nit'];
              $idDestinatario=$datosUsuario['id'];
          }
          
          
          
      }
      //$llave1=md5($llave1);
      $llave2=md5($envio->getId().$request->get('tipo_usuario').$idDestinatario);
      if($llave1!=$llave2){
        
        if(isset($datosUsuario['idContacto'])){
          $llave2 = md5($envio->getId().$request->get('tipo_usuario').$datosUsuario['idContacto']);
          if($llave1!=$llave2){
            echo "Llave de acceso no corresponde.";exit();
            
          }
        
        }else{
          echo "Llave de acceso no corresponde.";exit();

        }
      }

      $condicion = $parametroBusqueda;
      $informacion = $em->createQuery("SELECT u 
        FROM App\Entity\ColumnasDatosEnvio u 
        WHERE u.envioId = '".$request->get('envio_id')."' AND u.identificador  = '$condicion'")->execute();

      if(!$informacion){
          $informacion = $em->createQuery("SELECT u 
          FROM App\Entity\ColumnasDatosEnvio u 
          JOIN App\Entity\EnvioIntegrante ei WITH ei.envio = u.envioId
          JOIN App\Entity\IntegranteGrupo ig WITH ei.integranteGrupoId = ig.id
          WHERE ig.contactoProveedorId = '".$request->get('id_usuario')."' AND u.envioId = '".$request->get('envio_id')."' ")->execute();
      }
      
      $datosEnvio = $em->getRepository('App\Entity\DatosEnvio')->findOneBy(array('envio' => $request->get('envio_id')));
      
      
      

      $excelService = New Spreadsheet();
      $activeSheet = $excelService->getActiveSheet();

      $titulosColumnas = @unserialize($datosEnvio->getDatos());
      $cantidadColumnas = count($titulosColumnas) - 1;

      $cont = "A";
      for ($i = 0; $i <= $cantidadColumnas; $i++) {

        $activeSheet->setCellValue("$cont" . '1', ucfirst(trim($titulosColumnas[$i])));

          $cont++;
      }
      $num = 2;

      foreach ($informacion as $info) {

          $datos = @unserialize($info->getDatos());
          $contadorDatos = count($datos) - 1;
          $cont = "A";

          for ($i = 0; $i <= $contadorDatos; $i++) {

              $pesos=strpos(trim($datos[$i]), '$');

              if($pesos !== false){

                $activeSheet->setCellValue("$cont" . $num, str_replace(array(' ','.','$',','),array('','','','.'),trim($datos[$i])) );

                  //$activeSheet->getStyle("$cont" . $num)->getNumberFormat()->setFormatCode('"$"#,##0.00_-');
              }else{
                  $activeSheet->setCellValue("$cont" . $num, ucfirst(trim($datos[$i])));
              }
              $cont++;
          }
          $num++;
      }

      $writer = new Xlsx($excelService);
      $fileName= $nameFile.".xlsx";
        
      //$writer->save($fileName);
      header('Content-type: application/vnd.ms-excel');
      header('Content-Disposition: attachment; filename='.$fileName);
      $writer->save('php://output');

      exit();
  }


  public function envioDestinatarioLectura(Request $request){

    header('Content-type: image/png');
    echo gzinflate(base64_decode('6wzwc+flkuJiYGDg9fRwCQLSjCDMwQQkJ5QH3wNSbCVBfsEMYJC3jH0ikOLxdHEMqZiTnJCQAOSxMDB+E7cIBcl7uvq5rHNKaAIA'));

    $em = $this->getDoctrine()->getManager();
    $emp = $this->getDoctrine()->getManager('contactosProveedor');

    $idEnvio            =$request->get('idEnvio');
    $destinatarioId     = $request->get('destinatarioId');
    $tipoDestinatario   =$request->get('Tipo');

    $entityEnvio = $em->getRepository('App\Entity\Envio')->find($idEnvio);

    if($entityEnvio){

            $destinatario=array();
            if($tipoDestinatario=='Drogueria' || $tipoDestinatario=='Asociado'){

                $entityDestino = $em->getRepository('App\Entity\IntegranteGrupo')->find($destinatarioId); 

                if($tipoDestinatario=='Drogueria'){
                    $destinatario['nombre']=$entityDestino->getAsociado()->getDrogeria();
                    $destinatario['email']=$entityDestino->getAsociado()->getEmail();
                }else{
                    $destinatario['nombre']=$entityDestino->getAsociado()->getAsociado();
                    $destinatario['email']=$entityDestino->getAsociado()->getEmailAsociado();
                }
            }
            if($tipoDestinatario=='Proveedor'){

                $entityDestino = $em->getRepository('App\Entity\IntegranteGrupo')->find($destinatarioId);

                if ($entityDestino) {
                  $datosProveedor= $emp->getRepository('App\Entity\Proveedores')->find($entityDestino->getProveedorId());

                  $destinatario['nombre']=$datosProveedor->getNombre().' '.$datosProveedor->getRepresentanteLegal();
                  $destinatario['email']=$datosProveedor->getEmailRepresentanteLegal();
                }
            }
            $lecturas = $em->getRepository('App\Entity\EnvioIntegrante')->findOneBy(array('integranteGrupoId'=>$destinatarioId,'envio'=>$idEnvio));
            if($lecturas){
                $lecturas->setEnviado($lecturas->getEnviado()+1);
                $em->persist($lecturas);
            }
            if($destinatario){

                $lectura=new EnvioLectura();
                $lectura->setAsunto($entityEnvio->getAsunto());
                $lectura->setIntegrante($destinatario['nombre']);
                $lectura->setEmail($destinatario['email']);
                $lectura->setFecha(new \DateTime());
                $lectura->setIp($request->getClientIp());
                $lectura->setEnvioId($em->getReference('App\Entity\Envio', $idEnvio));
                $lectura->setEnvioIntegrateId($destinatarioId);
                $em->persist($lectura);
                $em->flush();

            }
        //}
    }
    exit();
  }// end if

  /**
  * Responde un archivo XLS con la información de los destinatarios del envio proveedores
  * 
  * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
  * 
  * @return XLS
  */
  public function proveedorDestinatariosXls(Request $request){
    // XLS
    ini_set("memory_limit", "1024M");
    set_time_limit(0);

    // PARÁMETROS
    $em = $this->getDoctrine()->getManager();
    $session = $request->getSession();
    $envioId = $request->get('envioId');

    if ($request->get('detalle')) { 
      $nameFile='detalle';

      // Query para obtener el detalle 
      $qEnvios = $em->createQuery("SELECT el.ip, el.fecha AS fechaLectura, ei.fechaEnvio, ig.proveedorId, ig.contactoProveedorId
        FROM App\Entity\Envio en 
        JOIN App\Entity\EnvioLectura el WITH el.envioId = en.id
        JOIN App\Entity\EnvioIntegrante ei WITH ei.integranteGrupoId = el.envioIntegrateId
        JOIN App\Entity\IntegranteGrupo ig WITH ig.id = el.envioIntegrateId
        WHERE en.id = :envioId");
      $qEnvios->setParameter("envioId", $envioId);
      $aEnvios = $qEnvios->getScalarResult();

      // Información BD Proveedores
      $aInfoProveedores = $this->getInfoContactosAndProveedor($aEnvios);
      $aContactos = $aInfoProveedores["contactos"];
      $aProveedores = $aInfoProveedores["proveedores"];
      
      // -- XLS Envios --
      $excelService = New Spreadsheet();
      $activeSheet = $excelService->getActiveSheet();
      $excelService->getActiveSheet()->setTitle('Proveedor - Detalle');

      // Header
      $activeSheet->setCellValue('A1', 'Proveedor');
      $activeSheet->setCellValue('B1', 'Nit');
      $activeSheet->setCellValue('C1', 'Código');
      $activeSheet->setCellValue('D1', 'Nombre Representante');
      $activeSheet->setCellValue('E1', 'Email Representante');
      $activeSheet->setCellValue('F1', 'Nombre Contacto');
      $activeSheet->setCellValue('G1', 'Email');
      $activeSheet->setCellValue('H1', 'F.Envio');  
      $activeSheet->setCellValue('I1', 'F.Lectura');
      $activeSheet->setCellValue('J1', 'IP');

      // Content
      $nFila = 2;
      foreach($aEnvios as $aEnvio){
        if (!empty($aEnvio["contactoProveedorId"])) {
          if(!isset($aContactos[$aEnvio["contactoProveedorId"]]))
            continue;
          $aContacto = $aContactos[$aEnvio["contactoProveedorId"]];
          $aProveedor = $aProveedores[$aContacto["proveedorId"]];
          $activeSheet->setCellValue("A{$nFila}", $aProveedor["nombre"]);
          $activeSheet->setCellValue("B{$nFila}", $aProveedor["nit"]);
          $activeSheet->setCellValue("C{$nFila}", $aProveedor["codigo"]);
          $activeSheet->setCellValue("D{$nFila}", $aProveedor["representanteLegal"]);
          $activeSheet->setCellValue("E{$nFila}", $aProveedor["emailRepresentanteLegal"]);
          $activeSheet->setCellValue("F{$nFila}", $aContacto["nombreContacto"]);
          $activeSheet->setCellValue("G{$nFila}", $aContacto["email"]);
        }else if(empty($aEnvio["contactoProveedorId"]) && isset($aProveedores[$aEnvio["proveedorId"]])){
          $aProveedor = $aProveedores[$aEnvio["proveedorId"]];
          $activeSheet->setCellValue("A{$nFila}", $aProveedor["nombre"]);
          $activeSheet->setCellValue("B{$nFila}", $aProveedor["nit"]);
          $activeSheet->setCellValue("C{$nFila}", $aProveedor["codigo"]);
          $activeSheet->setCellValue("D{$nFila}", $aProveedor["representanteLegal"]);
          $activeSheet->setCellValue("E{$nFila}", $aProveedor["emailRepresentanteLegal"]);
        }else if(!isset($aProveedores[$aEnvio["proveedorId"]])){
          continue;
        }

        $activeSheet->setCellValue("H{$nFila}", $aEnvio["fechaEnvio"]);
        $activeSheet->setCellValue("I{$nFila}", $aEnvio["fechaLectura"]);
        $activeSheet->setCellValue("J{$nFila}", $aEnvio["ip"]);
        $nFila++;
      }
    }else{
      $nameFile='consolidado';

      // Query para obtener el detalle 
      $qEnvios = $em->createQuery("SELECT el.ip, el.fecha AS fechaLectura, ei.fechaEnvio, ig.proveedorId, ig.contactoProveedorId, COUNT(el.envioIntegrateId) AS leido
        FROM App\Entity\Envio en 
        JOIN App\Entity\EnvioLectura el WITH el.envioId = en.id
        JOIN App\Entity\EnvioIntegrante ei WITH ei.integranteGrupoId = el.envioIntegrateId
        JOIN App\Entity\IntegranteGrupo ig WITH ig.id = el.envioIntegrateId
        WHERE en.id = :envioId
        GROUP BY el.envioIntegrateId");
      $qEnvios->setParameter("envioId", $envioId);
      $aEnvios = $qEnvios->getScalarResult();

      // Información BD Proveedores
      $aInfoProveedores = $this->getInfoContactosAndProveedor($aEnvios);
      $aContactos = $aInfoProveedores["contactos"];
      $aProveedores = $aInfoProveedores["proveedores"];

      // -- XLS Envios --
      $excelService = New Spreadsheet();
      $activeSheet = $excelService->getActiveSheet();
      $excelService->getActiveSheet()->setTitle('Proveedor - Consolidado');

      // Header
      $activeSheet->setCellValue('A1', '# De Veces Leídas');
      $activeSheet->setCellValue('B1', 'Proveedor');
      $activeSheet->setCellValue('C1', 'Nit');
      $activeSheet->setCellValue('D1', 'Código');
      $activeSheet->setCellValue('E1', 'Nombre Representante');
      $activeSheet->setCellValue('F1', 'Email Representante');
      $activeSheet->setCellValue('G1', 'Nombre Contacto');
      $activeSheet->setCellValue('H1', 'Email');
      $activeSheet->setCellValue('I1', 'F.Envio');
      $activeSheet->setCellValue('J1', 'F.Lectura');
      $activeSheet->setCellValue('K1', 'IP');

      // Content
      $nFila = 2;
      foreach($aEnvios as $aEnvio){
        if (!empty($aEnvio["contactoProveedorId"])) {
          if(!isset($aContactos[$aEnvio["contactoProveedorId"]]))
            continue;
          $aContacto = $aContactos[$aEnvio["contactoProveedorId"]];
          $aProveedor = $aProveedores[$aContacto["proveedorId"]];
          $activeSheet->setCellValue("B{$nFila}", $aProveedor["nombre"]);
          $activeSheet->setCellValue("C{$nFila}", $aProveedor["nit"]);
          $activeSheet->setCellValue("D{$nFila}", $aProveedor["codigo"]);
          $activeSheet->setCellValue("E{$nFila}", $aProveedor["representanteLegal"]);
          $activeSheet->setCellValue("F{$nFila}", $aProveedor["emailRepresentanteLegal"]);
          $activeSheet->setCellValue("G{$nFila}", $aContacto["nombreContacto"]);
          $activeSheet->setCellValue("H{$nFila}", $aContacto["email"]);    
        }else if(empty($aEnvio["contactoProveedorId"]) && isset($aProveedores[$aEnvio["proveedorId"]])){
          $aProveedor = $aProveedores[$aEnvio["proveedorId"]];
          $activeSheet->setCellValue("B{$nFila}", $aProveedor["nombre"]);
          $activeSheet->setCellValue("C{$nFila}", $aProveedor["nit"]);
          $activeSheet->setCellValue("D{$nFila}", $aProveedor["codigo"]);
          $activeSheet->setCellValue("E{$nFila}", $aProveedor["representanteLegal"]);
          $activeSheet->setCellValue("F{$nFila}", $aProveedor["emailRepresentanteLegal"]);
        }else if(!isset($aProveedores[$aEnvio["proveedorId"]])){
          continue;
        }

        $activeSheet->setCellValue("A{$nFila}", $aEnvio["leido"]);
        $activeSheet->setCellValue("I{$nFila}", $aEnvio["fechaEnvio"]);
        $activeSheet->setCellValue("J{$nFila}", $aEnvio["fechaLectura"]);
        $activeSheet->setCellValue("K{$nFila}", $aEnvio["ip"]);
        $nFila++;
      }
    }

    // Print XLSX
    $writer = new Xlsx($excelService);
    $fileName= $nameFile."_".$envioId.".xlsx";

    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename='.$fileName);
    $writer->save('php://output');
    exit();
  }


  /**
   * Obtiene información de contactos y proveedor de un resultado query
   * es importante que el resultado tenga los parametros "contactoProveedorId" y "proveedorId"
   */
  private function getInfoContactosAndProveedor($aQueryResult){
    $emp = $this->getDoctrine()->getManager('contactosProveedor');

    // Funciones locales
    $getIdProveedor = function($aData){
      return $aData["proveedorId"];
    };

    // Información Contactos
    $aContactos = [];
    $aContactosId = array_map(function($aResult){return $aResult["contactoProveedorId"];}, $aQueryResult);
    $aContactosId = array_unique(array_filter($aContactosId));
    $aQueryContactos = $emp->createQuery("SELECT c.id, c.nombreContacto, c.email, IDENTITY(c.idProveedor) as proveedorId FROM App\Entity\Contactos as c WHERE c.id IN (:contactosId)")->setParameter(":contactosId", $aContactosId)->getScalarResult();
    foreach($aQueryContactos as $aQueryContacto)
      $aContactos[$aQueryContacto["id"]] = $aQueryContacto;

    // Información Proveedor
    $aProveedores = [];
    $aProveedoresIdEnvios = array_map($getIdProveedor, $aQueryResult);
    $aProveedoresIdContactos = array_map($getIdProveedor, $aContactos);
    $aProveedoresId = array_merge($aProveedoresIdEnvios, $aProveedoresIdContactos);
    $aProveedoresId = array_unique(array_filter($aProveedoresId));
    $aQueryProveedores = $emp->createQuery("SELECT p.id, p.nombre, p.nit, p.codigo, p.representanteLegal, p.emailRepresentanteLegal FROM App\Entity\Proveedores as p WHERE p.id IN (:proveedoresId)")->setParameter(":proveedoresId", $aProveedoresId)->getScalarResult();
    foreach($aQueryProveedores as $aQueryProveedor)
      $aProveedores[$aQueryProveedor["id"]] = $aQueryProveedor;

    return ["contactos" => $aContactos, "proveedores" => $aProveedores];
  }
}