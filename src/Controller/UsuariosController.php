<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
//bundles
use Nzo\UrlEncryptorBundle\Encryptor\Encryptor;
// Services
use App\Services\Globales\GrillaGlobal;
use App\Services\Logs;
use App\Security\PasswordEncoder;
use App\Services\Mail;
// User
use App\Entity\User;
use App\Services\Utilidades;
use App\Services\Globales\MenuPermisos;
use App\Form\UserType;

/**
 * Controlador de Usuarios
 *
 * @category USUARIOS
 *
 * @author Pablo J. T. Bravo <p.trivino@waplicaciones.co> && Julian Andres Restrepo <j.restrepo@waplicaciones.co>
 *
 */
class UsuariosController extends AbstractController
{

  private $menuPermisos;
  private $sMenuModulo = 'user';
  private $sModuloVista = 'listado_user';
  private $mail;
  private $passwordEncoder;

  public function __construct(MenuPermisos $menuPermisos, Logs $log, Mail $mail, PasswordEncoder $passwordEncoder)
  {
    $this->menuPermisos = $menuPermisos;
    $this->log = $log;
    $this->mail = $mail;
    $this->passwordEncoder = $passwordEncoder;
  }

  /**
   * Responde un documento HTML con la lista de usuarios
   *
   * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
   *
   * @return render('usuarios/index.html.twig') HTML
   **/
  public function index(Request $request)
  {

    // Variables
    $session = $request->getSession();
    try {
      $this->menuPermisos->validarAccesoVista($session, $this->sMenuModulo, $this->sModuloVista);
    } catch (\Throwable $th) {
      return $this->redirect($this->generateUrl('admin_login'));
    }
    $this->menuPermisos->validarAccesoVista($session, $this->sMenuModulo, $this->sModuloVista);
    $aPermisos = $this->menuPermisos->getPermisosModuloVista($session, $this->sMenuModulo, $this->sModuloVista);
    $aGridButtons = (empty($aPermisos)) ? array() : $this->menuPermisos->getGridButtons($this->sMenuModulo, $session, $aPermisos);

    $dfColumnas = [
      ["headerClass" => "h6", 'headerName' => '#',         'field' => 'id',         'hide' => true],
      ["headerClass" => "h6", 'headerName' => 'Nombre',    'field' => 'nombre',     'width' => 180],
      ["headerClass" => "h6", 'headerName' => 'Usuario',    'field' => 'usuario',     'width' => 180],
      ["headerClass" => "h6", 'headerName' => 'Email',     'field' => 'email',      'width' => 200],
      ["headerClass" => "h6", 'headerName' => 'Rol',       'field' => 'roles',      'width' => 170, 'cellClass' => 'text-center', 'hide' => true, 'search' => false, 'sortable' => false],
      ["headerClass" => "h6", 'headerName' => 'Estado',    'field' => 'estado',     'width' => 110, 'cellClass' => 'text-center'],
      ["headerClass" => "h6", 'headerName' => 'Tipo',      'field' => 'tipoUsuario', 'width' => 110, 'cellClass' => 'text-center'],
    ];

    // Agregar columna cambio de permisos
    if ($aGridButtons["modificar_permisos"]) {
      $dfColumnas[] = [
        "headerClass" => "h6",
        'headerName' => 'Permisos',  'field' => 'id',    'width' => 100, /*'pinned' => 'left',*/ 'suppressSorting' => true, 'search' => false, 'cellClass' => 'text-center',
        'cellRenderer' => 'agCellRender', 'cellRendererParams' => array(
          'grilla' => 'default', 'columna'  => 'permisos',
          'aData' => [
            'btnClass' => 'btn btn-custon-wa btn-sm border-0 font-weight-bold btnGrillaPermisos',
            'btnIconsClass' => 'fas fa-key'
          ]
        )
      ];
    }

    $this->log->setLogAdmin("LU1 Listado de Usuarios de Administrador");

    return $this->render('usuarios/index.html.twig', array(
      'dfColumnas'    => json_encode($dfColumnas),
      'aGridButtons'  => json_encode($aGridButtons["iconos"]),
      'modulo'        => $this->sMenuModulo,
    ));
  }

  /**
   * Responde un JSON con la información requerida para mostrar la tabla usuarios
   * 
   * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
   * @param App\Services\GrillaGlobal $grillaGlobal para realizar los filtros de busqueda
   * @param $exportar Type=bool Valida si se filtran datos necesarios para descargar un archivo .csv
   * 
   * @return JSON
   */
  public function indexJson(Request $request, GrillaGlobal $grillaGlobal, $exportar = false)
  {
    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $aJson = array();

    if ($request->isXmlHttpRequest()) {

      $session = $request->getSession();
      $em = $this->getDoctrine()->getManager();

      $aEquivalenciaColumnas = [
        'id'           => 'us.id',
        'nombre'       => 'us.nombre',
        'email'        => 'us.email',
        'estado'       => 'us.estado',
        'roles'        => 'us.roles',
        'usuario'      => 'us.usuario',
        'tipoUsuario'  =>  'us.tipoUsuario',
      ];

      // Procedimiento Filtro Grilla
      $sIniFiltro = "";
      $aDataGrilla = $grillaGlobal->realizarFiltro($aEquivalenciaColumnas, $sIniFiltro);

      // --- --- --- --- Total Filas --- --- --- --- //
      $numeroRegistros = 0;
      if ($aDataGrilla["paginaActual"] == 1 && $exportar === false) {
        $Contador = $em->createQuery("SELECT COUNT(us.id) AS numeroRegistros
        FROM App\Entity\User us
        WHERE {$aDataGrilla["where"]} ");
        $Contador->setParameters($aDataGrilla["valoresWhere"]);
        $Contador = $Contador->getSingleResult();
        $numeroRegistros = $Contador['numeroRegistros'];
      }

      // DQL Data
      $queryUser = $em->createQuery("SELECT us.id, us.nombre, us.email, us.estado, us.usuario, us.roles, us.tipoUsuario 
        FROM App\Entity\User us
        WHERE {$aDataGrilla["where"]} 
        ORDER BY {$aDataGrilla["order"]} ");

      // Resultado
      $queryUser->setParameters($aDataGrilla["valoresWhere"]);

      if ($exportar === false) {
        $queryUser->setMaxResults($aDataGrilla["maximoFilas"]);
        $queryUser->setFirstResult($aDataGrilla["paginacion"]);
      }
      $aUser = $queryUser->getScalarResult();

      // --- --- --- Lógica --- --- --- //
      $aListUser = array();
      foreach ($aUser as $entiti) {


        $aListUser[] = array(
          'id'           => $entiti['id'],
          'nombre'       => $entiti['nombre'],
          'email'        => $entiti['email'],
          'usuario'      => $entiti['usuario'],
          'estado'       => ($entiti['estado'] == 2) ? 'Inactivo' : 'Activo',
          'roles'        => ($entiti['roles'] == '["ROLE_ADMIN"]') ? 'Administrador' : 'Mensajero',
          'tipoUsuario'  => ($entiti['tipoUsuario']) ? 'Administrador' : 'Usuario',
        );
      }

      // Cierre de conexion y Respuesta
      $em->getConnection()->close();
      $response->setContent(json_encode(['totalRows' => $numeroRegistros, 'data' => $aListUser]));
    } else {
      $aJson['status'] = 0;
      $aJson['message'] = "Acción no valida";
      $response->setContent(json_encode($aJson));
    }

    return $response;
  }

  /**
   * Accion para crear Cliente cliente/mdlCrud
   * Si se detecta el envio del formulario se creara, de lo contrario se mostrara la vista cliente/mdlCrud
   * @param object $request Objeto peticion de Symfony 4.2
   * @return vista cliente/mdlCrud -> rol Usuarios
   * @return object json resultado de la accion nuevo
   * @author Nestor Quintero <n.quintero@waplicaciones.com.co>
   * @since 4.2
   * @category AdministradorImagenes\usuarios
   */
  public function userNew(Request $request, Utilidades $utilidades, PasswordEncoder $encoder): Response
  {
    // public function userNew(Request $request, RegistroActividad $regActividad): Response{
    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $aJson = array();

    if ($request->isXmlHttpRequest()) {

      $session = $request->getSession();
      $bAccesoAccion = $this->menuPermisos->getAccesoVistaAccion($session, $this->sMenuModulo, $this->sModuloVista, 'nuevo-registro');

      if ($bAccesoAccion) {

        $em = $this->getDoctrine()->getManager();

        if (!is_null($request->get('user'))) {


          $aDataForm = $request->get('user');
          // $sConfirmPassword = $request->get('confirm_password');

          $oUser = $em->getRepository('App\Entity\User')->findOneByUsuario($aDataForm['usuario']);

          if (!$oUser) {
            // if( !$oUser && ($sConfirmPassword == $aDataForm['password']) ){

            $emUser = new User();
            $form = $this->createForm(UserType::class, $emUser);
            $form->handleRequest($request);

            if ($form->isSubmitted()) {

              $sUserRol = $request->get('user_rol');
              $sPassword = $utilidades->getTxtPassword();

              $sPasswordEncoded = $this->passwordEncoder->encodePassword($sPassword);
              $decrypted = $this->passwordEncoder->decodePassword($sPasswordEncoded);
              $aJsonRole = ['ROLE_ADMIN'];

              $emUser->setPassword($sPasswordEncoded);
              $emUser->setRole($aJsonRole[0]);
              $emUser->setRoles($aJsonRole);
              $emUser->setClaveEmail('');
              //$emUser->setRestorePassword(0);
              $path = $this->getParameter('kernel.project_dir');
              $this->mail->sendMail($decrypted, $aDataForm["email"], $aDataForm["usuario"], $aDataForm["nombre"], $path);

              $em->persist($emUser);
              $em->flush();
              $this->_setDefaultPermissions($em, $emUser->getId());
              $this->log->setLogAdmin("LU2 Usuarios de Administrador, agregado correctamente");

              $aJson['status'] = 1;
              $aJson['message'] = "Usuario agregado correctamente.";
            } else {
              $aJson['status'] = 0;
              $aJson['message'] = 'El formulario fue mal diligenciado, verifique e inténtelo de nuevo.';

              $this->log->setLogAdmin("LU2 Formulario de Creación de Usuarios de Administrador fue mal Diligenciado");
            }
          } else {
            $aJson['status'] = 0;
            $aJson['message'] = 'El nombre de usuario ya se encuentra registrado, verifique he inténtelo de nuevo.';

            $this->log->setLogAdmin("LU2 Formulario de Creación de Usuarios de Administrador, el nombre de usuario ya se encuentra registrado");
          }
        } else {
          // Entity manager y Formulario
          $entity = new User();
          $form = $this->createForm(UserType::class, $entity);

          //cierre de conexion

          //data
          $aJson['entity'] = $entity;
          $aJson['accion'] = 'nuevo';
          $aJson['form']   = $form->createView();

          $this->log->setLogAdmin("LU2 Formulario de Creación de Usuarios de Administrador, usuario Vacio");

          return $this->render('usuarios/mdlCrud.html.twig', $aJson);
        }

        //cierre de conexion
        $em->getConnection()->close();
      } else {
        $aJson['status'] = 0;
        $aJson['message'] = 'Acción no habilitada, inicie sesión nuevamente he inténtelo otra vez.';

        $this->log->setLogAdmin("LU2 Formulario de Creación de Usuarios de Administrador, acción no habilitada");
      }
    } else {
      $aJson['status'] = 0;
      $aJson['message'] = 'Acción no valida';

      $this->log->setLogAdmin("LU2 Formulario de Creación de Usuarios de Administrador, acción no valida");
    }


    $response->setContent(json_encode($aJson));
    return $response;
  }

  /**
   * Responde una plantilla HTML usuarios/mdlCrud.html.twig con el formulario de edición de usuarios ó Responde un JSON con un estado y mensaje de la petición de edicion de un usuario 
   *
   * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
   *
   * @return Symfony\Component\HttpFoundation\Response JSON o render(usuarios/mdlCrud.html.twig)
   */
  public function userEdit(Request $request): Response
  {

    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $aJson = array();

    if ($request->isXmlHttpRequest()) {

      $session = $request->getSession();
      $bAccesoAccion = $this->menuPermisos->getAccesoVistaAccion($session, $this->sMenuModulo, $this->sModuloVista, 'editar-registro');

      if ($bAccesoAccion) {

        $em = $this->getDoctrine()->getManager();
        if (!is_null($request->get('user'))) {

          $aFormData = $request->get('user');
          $aUser = ($aFormData['usuario'] == $request->get('usuario')) ? array() :
            $em->getRepository('App\Entity\User')->findByUsuario($aFormData['usuario']);

          $emUser = $em->getRepository(User::class)->findOneById($request->get('id_user'));
          $editForm = $this->createForm(UserType::class, $emUser);
          $editForm->handleRequest($request);

          if ($editForm->isSubmitted() && !isset($aUser[0])) {

            $sUserRol = $request->get('user_rol');
            //$aJsonRole = ($sUserRol == 'admin') ? ['ROLE_ADMIN'] : ['ROLE_MENSAJERO'];
            $aJsonRole = ['ROLE_ADMIN'];
            $aFormData["tipoUsuario"] !== 1 ?: array_push($aJsonRole, "ROLE_SUPER_ADMIN");
            $emUser->setRole($aJsonRole[0]);
            $emUser->setRoles($aJsonRole);
            $emUser->setTipoUsuario($aFormData["tipoUsuario"]);
            $emUser->setClaveEmail($emUser->getClaveEmail());

            $em->persist($emUser);
            $em->flush();

            if($aFormData["tipoUsuario"] == 0){
              $this->_deleteDefaultPermissions($em, $emUser->getId());

              $this->_setDefaultPermissions($em, $emUser->getId());
            }

            

            $this->log->setLogAdmin("LU3 Formulario de Usuarios de Administrador, editado correctamente");

            $aJson['status'] = 1;
            $aJson['message'] = "Usuario editado correctamente.";
          } else {
            $aJson['status'] = 0;

            if (!isset($aUser[0])) {
              $aJson['message'] = 'El formulario fue mal diligenciado, verifique e inténtelo de nuevo.';
              $this->log->setLogAdmin("LU3 Formulario de Usuarios de Administrador, el formulario fue mal diligenciado");
            } else {
              $aJson['message'] = 'El nombre de usuario ya se encuentra registrado, verifique he inténtelo de nuevo.';
              $this->log->setLogAdmin("LU3 Formulario de Usuarios de Administrador, el nombre de usuario ya se encuentra registrada");
            }
          }
        } else {

          // Entity manager y Formulario
          $oUser = $em->getRepository(User::class)->findOneById($request->get("idUser"));
          $editForm = $this->createForm(UserType::class, $oUser);


          //data
          $aJson['entity'] = $oUser;
          $aJson['form']   = $editForm->createView();
          $aJson['accion'] = 'editar';
          $aJson['rol_actual'] = $oUser->getRoles()[0];
          $aJson['usuario'] = $oUser->getUsuario();
          $aJson['idUser'] = $request->get("idUser");

          // $this->log->setLogAdmin("LU3 Formulario de Usuarios de Administrador, usuario Vacio");

          return $this->render('usuarios/mdlCrud.html.twig', $aJson);
        }

        //cierre de conexion
        $em->getConnection()->close();
      } else {
        $aJson['status'] = 0;
        $aJson['message'] = 'Acción no habilitada, inicie sesión nuevamente he inténtelo otra vez.';
        $this->log->setLogAdmin("LU3 Formulario de Usuarios de Administrador, acción no habilitada");
      }
    } else {
      $aJson['status'] = 0;
      $aJson['message'] = 'Acción no valida';
      $this->log->setLogAdmin("LU3 Formulario de Usuarios de Administrador, acción no valida");
    }

    $response->setContent(json_encode($aJson));
    return $response;
  }

  /**
   * Responde un JSON con un estado y mensaje de la petición de eliminación de un usuario
   *
   * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
   *
   * @return Symfony\Component\HttpFoundation\Response JSON
   */
  public function userDelete(Request $request): Response
  {

    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $aJson = array();

    if ($request->isXmlHttpRequest()) {

      $session = $request->getSession();
      $bAccesoAccion = $this->menuPermisos->getAccesoVistaAccion($session, $this->sMenuModulo, $this->sModuloVista, 'eliminar-registro');
      if ($bAccesoAccion) {

        $em = $this->getDoctrine()->getManager();

        $idUser = (int)$request->get('idUser');
        $emUser = $em->getRepository(User::class)->findOneById($idUser);

        $em->remove($emUser);
        $em->flush();

        //cierre de conexion
        $em->getConnection()->close();

        $aJson['status'] = 1;
        $aJson['message'] = 'Usuario eliminado correctamente.';

        $this->log->setLogAdmin("LU4 Formulario de Usuarios de Administrador, eliminado correctamente");
      } else {
        $aJson['status'] = 0;
        $aJson['message'] = 'Acción no habilitada, inicie sesión nuevamente he inténtelo otra vez.';

        $this->log->setLogAdmin("LU4 Formulario de Usuarios de Administrador, acción no habilitada");
      }
    } else {
      $aJson['status'] = 0;
      $aJson['message'] = 'Acción no valida';

      $this->log->setLogAdmin("LU4 Formulario de Usuarios de Administrador, acción no valida");
    }

    $response->setContent(json_encode($aJson));
    return $response;
  }

  /**
   * Retorna la plantilla de edición de permisos del usuario
   *
   * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
   *
   * @return render('usuarios/mdlPermisos.html.twig') HTML TWIG
   */
  public function userPermisos(Request $request): Response
  {
    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $aJson = array();

    if ($request->isXmlHttpRequest()) {

      $session = $request->getSession();
      $bAccesoAccion = $this->menuPermisos->getAccesoVistaAccion($session, $this->sMenuModulo, $this->sModuloVista, 'modificar_permisos');

      if ($bAccesoAccion) {

        $em = $this->getDoctrine()->getManager();
        if (!is_null($request->get('permisos'))) {

          $aPermisos = $request->get('permisos');
          $nIdUser = $request->get('user_id');
          // Eliminar Permisos actuales
          $bResult = $em->createQuery('DELETE App\Entity\MenuPermisos accp WHERE accp.idUser = ' . $nIdUser)->getResult();
          // Registrar los nuevos permisos
          $sInsertPermisos = ' INSERT INTO menu_permisos (id_user, id_accion, permiso) VALUES ';
          $sValuesInsertPermisos = '';
          foreach ($aPermisos as $key => $permiso) {
            if ($sValuesInsertPermisos != '') $sValuesInsertPermisos .= ', ';
            $sValuesInsertPermisos .= "({$nIdUser}, {$key}, {$permiso})";
          }

          $conn = $em->getConnection();
          $oInsertar = $conn->prepare($sInsertPermisos . $sValuesInsertPermisos);
          $oInsertar->execute();

          $aJson['status'] = 1;
          $aJson['message'] = 'Permisos actualizados correctamente.';

          $this->log->setLogAdmin("LU6 Formulario de Usuarios de Administrador, permisos actualizados");
        } else {

          // Objeto Usuario
          $oUser = $em->getRepository(User::class)->findOneById($request->get("idUser"));

          //data
          $aJson['idUser'] = $request->get("idUser");
          $aJson['menuPermisos'] = $this->menuPermisos->getMenu($oUser->getRoles()[0], $oUser->getId())['menu'];

          //cierre de conexion
          $em->getConnection()->close();

          $this->log->setLogAdmin("LU6 Formulario de Usuarios de Administrador, permisos vacio");

          return $this->render('usuarios/mdlPermisos.html.twig', $aJson);
        }
        //cierre de conexion
        $em->getConnection()->close();
      } else {
        $aJson['status'] = 0;
        $aJson['message'] = 'Acción no habilitada, inicie sesión nuevamente he inténtelo otra vez.';

        $this->log->setLogAdmin("LU6 Formulario de Usuarios de Administrador, acción no habilitada");
      }
    } else {
      $aJson['status'] = 0;
      $aJson['message'] = 'Acción no valida';

      $this->log->setLogAdmin("LU6 Formulario de Usuarios de Administrador, acción no valida");
    }

    $response->setContent(json_encode($aJson));
    return $response;
  }

  private function _setDefaultPermissions($connection, $nIdUser)
  {
    $aPermisos = array(47, 49, 50, 51, 52, 55, 54, 56, 58, 59, 60, 63, 64, 65, 66, 67, 48);

    $sInsertPermisos = ' INSERT INTO menu_permisos (id_user, id_accion, permiso) VALUES ';
    $sValuesInsertPermisos = '';
    foreach ($aPermisos as $action) {
      if ($sValuesInsertPermisos != '') $sValuesInsertPermisos .= ', ';
      $sValuesInsertPermisos .= "({$nIdUser}, {$action}, 1)";
    }

    $conn = $connection->getConnection();
    $oInsertar = $conn->prepare($sInsertPermisos . $sValuesInsertPermisos);
    $oInsertar->execute();
  }

  private function _deleteDefaultPermissions($connection, $nIdUser)
  {

    $sqlDelete="DELETE FROM menu_permisos WHERE id_user=".$nIdUser;

    $conn = $connection->getConnection();
    $conn->query($sqlDelete);

  }
}
