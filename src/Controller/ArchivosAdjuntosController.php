<?php

namespace App\Controller;

use symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use App\Entity\Envio;

use App\Services\Logs;
use App\Services\Globales\MenuPermisos;


class ArchivosAdjuntosController extends AbstractController
{

  private $menuPermisos;
  private $log;

  public function __construct(MenuPermisos $menuPermisos, Logs $log ){
    $this->menuPermisos = $menuPermisos;
    $this->log = $log;

  }

  /**
  * Renderiza el formulario para la carga de archivos.
  * @param object $request Objeto peticion de Symfony 5, entidad Envio
  * @return 
  * @author Julián Restrepo <j.restrepo@waplicaciones.co>
  * @since 3
  * @category Correos\ArchivosAdjuntos
  */
  public function formSubirArchivos( Request $request){

    $session=$request->getSession();
    $em = $this->getDoctrine()->getManager();
    $session->set('envio',$request->get('idEnvio'));

    $infoAdmin = $em->getRepository('App\Entity\User')->findOneById($session->get('id'));

    $hayExcepcion=$this->verificarCarpetas($session, $infoAdmin, $request->get('idEnvio') );

    $data=$this->archivosDisponibles( $session );

    $peso = $this->pesoArchivos( $session );

    $this->log->setLogAdmin("LA1 Carga de archivos adjuntos ");

    return $this->render('envio/adminArchivos.html.twig',array(
      'idEnvio'=> $request->get('idEnvio'),
      'hayExcepcion'=>$hayExcepcion,
      'peso' => $peso
    ));

  }// end action

  /**
  * Carga el archivo csv "combinacion.csv"
  * @param object $request Objeto peticion de Symfony 5
  * @return 
  * @author Julián Restrepo <j.restrepo@waplicaciones.co>
  * @since 3
  * @category Correos\ArchivosAdjuntos
  */
  public function csv(Request $request){

    set_time_limit (0);
    $em = $this->getDoctrine()->getManager();
    $fs = new Filesystem();
    $archivo='combinacion.csv';
    //$auditoria = $this->get('utilidadesAdministrador');

    $session=$request->getSession();
    $ruta = $session->get('rutaCSV');

    $response = new Response;
    $response->headers->set('Content-Type', 'application/json');
    $json = array();
           
    $response->setStatusCode( 200 );

    try{

      //Obtener el archivo
      $file = $request->files->get('csv');

      //verificar si hay archivo existente
      if( $fs->exists($ruta.$archivo) )
        $fs->remove($ruta.$archivo);

      //Mover el archivo a la carpeta
      $fs->copy( $file->getRealPath() , $ruta.$archivo );
      $json['status'] = 1;
      
      /*********Asignar estado de combinacion al envio********/
      
      $envio = $em->getRepository('App\Entity\Envio')->findOneById($request->get('idEnvio'));
      if($envio){
          $envio->setCombinacion(1);
          $em->persist($envio);
          $em->flush();
      
        //$auditoria->registralog('Procesa convinacion Csv', $session->get('id_usuario'));
        /*******Procesar Sabana*******/
        $sabanaInicial = true;

        $this->procesarSabana($ruta.$archivo,$envio->getId(),$session,$sabanaInicial);
      
      }
      
    }catch( \IOExceptionInterface $e ){
      $json['descripcion']=$e->getMessage();
      $response->setStatusCode( 500 );
      $json['status'] = 2;

    }catch(\UnexpectedValueException $e){
      $json['descripcion']=$e->getMessage();
      $json['status'] = 3;
      $response->setStatusCode( 500 );
    } catch( \Exception $e ){
      $json['descripcion']=$e->getMessage();
      $json['status'] = 3;
      $response->setStatusCode( 500 );
    }

    $response->setContent(json_encode($this->utf8_converter($json)));
    return $response;

  }

  function utf8_converter($array){
    array_walk_recursive($array, function(&$item, $key){
        if(!mb_detect_encoding($item, 'utf-8', true)){
                $item = utf8_encode($item);
        }
    });
 
    return $array;
}

  /**
  * Carga demás archivos adjuntos.
  * @param object $request Objeto peticion de Symfony 5
  * @return 
  * @author Julián restrepo <j.restrepo@waplicaciones.com.co>
  * @since 3
  * @category Correos\ArchivosAdjuntos
  */
  public function otros(Request $request){

    set_time_limit ( 0 );

    $fs = new Filesystem();
   
    $session=$request->getSession();
    $ruta = $session->get('rutaOtros');
    //$auditoria = $this->get('utilidadesAdministrador');

    $response = new Response;
    $response->headers->set('Content-Type', 'application/json');
    $json = array();
           
    $response->setStatusCode( 200 );
    try{

      $peso = $this->pesoArchivos( $session );

      //Obtener el archivo
      $files = $request->files->get('adjuntos');
      $contaArchivos=0;

      //Mover los archivos 
      foreach ($files as  $file){

        $peso+= number_format(( filesize ( $file->getRealPath() )  / 1000000 ),1,'.','');  
        if( $peso <= 2 ){
          $contaArchivos++;
          $fs->copy( $file->getRealPath(), $ruta.$file->getClientOriginalName() );
        }else{
          continue;
        }

      }// end foreach
      
      $this->log->setLogAdmin("LA2 Carga otros archivos adjuntos");

      $this->pesoArchivos( $session );
      
      $json['status'] = 1;
    }catch( IOExceptionInterface $e ){

      $response->setStatusCode( 500 );
      $json['status'] = 2;

    }catch( \Exception $e ){

      $json['status'] = 2;
      $response->setStatusCode( 500 );

    }

    $response->setContent(json_encode($json));
    return $response;

  }

  /**
  * Verifica la existencia de la carpeta donde se guardarán los archivos a cargar.
  * @param object $request Objeto peticion de Symfony 5
  * @return 
  * @author Julián restrepo <j.restrepo@waplicaciones.com.co>
  * @since 3
  * @category Correos\ArchivosAdjuntos
  */
  private function verificarCarpetas($session, $administrador, $idEnvio){

    //verificar existencia de directorio
    $fs = new Filesystem();
    //$ruta=$this->get('kernel')->getRootDir().'/../public';
    $ruta=$this->getParameter('kernel.project_dir').'/public';

    try{

      if( !$fs->exists($ruta.'/adjuntos') )
        $fs->mkdir($ruta.'/adjuntos');

      //verificar existencia de la carpeta del administrador
      $fecha = $administrador->getFechaCreado();
      $fecha=$fecha->format('Ymd');
      $carpeta = $idEnvio.'/'.$fecha.$administrador->getId();

      if( !$fs->exists($ruta.'/adjuntos/'.$carpeta) ){

        $fs->mkdir($ruta.'/adjuntos/'.$carpeta.'/csv/');
        $fs->mkdir($ruta.'/adjuntos/'.$carpeta.'/otros/');

      }

      $session->set('rutaCSV',$ruta.'/adjuntos/'.$carpeta.'/csv/');
      $session->set('rutaOtros',$ruta.'/adjuntos/'.$carpeta.'/otros/');

      return false;

    }catch( IOExceptionInterface $e ){

      return true;

    }catch( \Exception $e ){

      return true;
    }

  }

  /**
  * Recupera y renderiza a la vista los archivos cargados disponibles.
  * @param object $request Objeto peticion de Symfony 5
  * @return 
  * @author Julián restrepo <j.restrepo@waplicaciones.com.co>
  * @since 3
  * @category Correos\ArchivosAdjuntos
  */
  public function listar( Request $request ){

    $session = $request->getSession();
    
    $data=$this->archivosDisponibles( $session );

    $csv=$data['csv'];
    $otros=$data['otros'];
    $peso=0;
    foreach ($otros as  $value) 
      $peso+=(int)$value['peso'];
    
    
    $this->log->setLogAdmin("LA3 Listado de archivos cargados");

    return $this->render( 'envio/listaArchivos.html.twig',array( 'csv'=>$csv, 'otros'=>$otros, 'peso'=>$peso ));

  }// end action

  /**
  * Recupera la ruta y el nombre de los archivos que el Administrador ha cargado al sistema.
  * @param object $request Objeto peticion de Symfony 5
  * @return 2 array. Uno con información del csv y otro con ifnormación de los archivos adjuntos.
  * @author Julián restrepo <j.restrepo@waplicaciones.com.co>
  * @since 3
  * @category Correos\ArchivosAdjuntos
  */
  private function archivosDisponibles( $session ){

    $rutaCSV = $session->get('rutaCSV');
    $rutaOtros = $session->get('rutaOtros');

    $infoCSV = $infoOtros = array();

    if( !is_null( $rutaCSV ) && $rutaCSV != '' ){

      $fs = new Filesystem();
      $archivo='combinacion.csv';

      try{

        //verificar si hay archivo existente
        if( $fs->exists( $rutaCSV.$archivo ) ){
          $infoCSV['archivo'] = $archivo;
          $infoCSV['peso'] =  filesize ( $rutaCSV.$archivo );
        }else{
          $infoCSV['archivo'] =' No se pudo recuperar el archivo. ';
        }

      }catch( \Exception $e ){
        $infoCSV['error'] =' No se pudo recuperar el archivo. ';
      }
    }

    if( !is_null( $rutaOtros ) && $rutaOtros != '' ){

      $rep  = opendir($rutaOtros);    //Abrimos el directorio

      while ($arc = readdir($rep)) {  //Leemos el arreglo de archivos contenidos en el directorio: readdir recibe como parametro el directorio abierto

        if($arc != '..' && $arc !='.' && $arc !=''){
          $infoOtros[$arc]['nombre'] = $arc; 
          $infoOtros[$arc]['peso'] = filesize ( $rutaOtros.$arc );
        }
      }
      closedir($rep);         //Cerramos el directorio
      clearstatcache();     //Limpia la caché de estado de un archivo

    }

    return array( 'csv'=>$infoCSV, 'otros'=>$infoOtros );
   
  }//end function

  /**
  * Elimina archivos 
  * @param object $request Objeto peticion de Symfony 5
  * @return 
  * @author Julián restrepo <j.restrepo@waplicaciones.com.co>
  * @since 3
  * @category Correos\ArchivosAdjuntos
  */
  public function eliminar(Request $request){

    $session = $request->getSession();
    
    $archivo = $request->get('archivo');
    $opcion = $request->get('opcion');

    if( $opcion == 1 ){
      $ruta = $session->get('rutaCSV');
    }else{
      $ruta = $session->get('rutaOtros');
    }

    $fs = new Filesystem();

    $response = new Response;
    
    try{

      $this->log->setLogAdmin("LA4 Eliminacion de archivos adjuntos");

      $fs->remove( $ruta.$archivo );
      $response->setStatusCode( 200 );
      $this->pesoArchivos( $session );
    }catch(\Exception $e){
      $response->setStatusCode( 500 );
    }

    return $response;
  }

  /**
  * Calcula el peso total en MB de los archivos disponibles.
  * @param object $request Objeto peticion de Symfony 5
  * @return 
  * @author Julián restrepo <j.restrepo@waplicaciones.com.co>
  * @since 3
  * @category Correos\ArchivosAdjuntos
  */
  private function pesoArchivos( $session ){

    $rutaOtros = $session->get('rutaOtros');
    $peso = 0;
    $contaArchivos = 0;
    if( !is_null( $rutaOtros ) && $rutaOtros != '' ){

      $rep  = opendir($rutaOtros);    //Abrimos el directorio

      while ($arc = readdir($rep)) {  //Leemos el arreglo de archivos contenidos en el directorio: readdir recibe como parametro el directorio abierto
        if($arc != '..' && $arc !='.' && $arc !=''){
          $peso+=(int) filesize ( $rutaOtros.$arc );  
          $contaArchivos++;
        }
      }
      closedir($rep);         //Cerramos el directorio
      clearstatcache();     //Limpia la caché de estado de un archivo

    }

    $em = $this->getDoctrine()->getManager();
    $envioId = $session->get('envio');

    $em->createQuery("UPDATE App\Entity\Envio e SET e.adjuntos='".$contaArchivos."' WHERE e.id=".$envioId)->execute();


    return $peso = ( $peso >0 )? number_format(($peso / 1000000),1,'.','') :0;

  }
  
  
  /**
     * Procesa y registra la sabana de datos enviada por el usuario.
     * @param type $ruta
     * @param type $envio
     * @author Julian Restrepo <j.restrepo@waplicaciones.co>
     * @category ArchivosAdjuntos
     */
    protected function procesarSabana($ruta, $envio,$session,$inicial) {
        ini_set('memory_limit', '2048M');
        set_time_limit(0);
        $em = $this->getDoctrine()->getManager();
        if($inicial){
            $Eliminar0="DELETE FROM `columnas_datos_envio` WHERE `envio_id` = $envio";
            $em->getConnection()->query($Eliminar0);
            $Eliminar1="DELETE FROM `datos_envio` WHERE `envio_id` = $envio";
            $em->getConnection()->query($Eliminar1);
        }
        
        $cont = 1;$batchSize = 20;
        $insert1="INSERT INTO `columnas_datos_envio` (`fecha_creado` ,`envio_id` ,`datos` ,`creador_id` ,`fecha_modificado` ,`identificador`)
                        VALUES ";
        $sql="";
        $fpB = fopen($ruta, "r");
    
        if ($fpB){
            while ($data = fgetcsv($fpB, 4000, ";")){
              if($cont === 1){
                  $v=array();
                  
                  foreach($data as $d){
                      $v[]=  utf8_encode(stripcslashes($d));
                  }              

                  $SQLC=" INSERT INTO `datos_envio` (`fecha_creado` ,`codigo_destinatario` , `tipo_usuario` ,`envio_id` ,`creador_id` ,`datos` ,`fecha_modificado` ,`identificador`)
                      VALUES ( '".date('Y-m-d H:i:s')."', 1 , NULL , '".$envio."', '".$session->get('id')."', '".serialize($v)."' , '".date('Y-m-d H:i:s')."', '".$data[0]."')";
                  $em->getConnection()->query($SQLC);
                  unset($v);
              }else{
                  $v=array();
                  foreach($data as $d){
                      $v[]=  utf8_encode(stripcslashes($d));
                  }
                  
                  if($sql!="")
                      $sql.=",";
                  $sql.=" ( '".date('Y-m-d H:i:s')."', '".$envio."', '".serialize($v)."', '".$session->get('id')."', '".date('Y-m-d H:i:s')."', '".$data[0]."')";
                  if (($cont % $batchSize) == 0) {                   
                      $em->getConnection()->query($insert1.$sql);
                      $sql="";
                  }
                  unset($v);
              }
              $cont++;
            }  
            if($sql!="")
                $em->getConnection()->query($insert1.$sql);
        }
        /*$em->flush();
        $em->clear();*/
    }


}//end class
