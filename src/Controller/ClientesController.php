<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

// Services
use App\Services\Globales\GrillaGlobal;
use App\Services\Globales\MenuPermisos;
use App\Services\Logs;

/**
 * Controlador para visualizar los clientes
 *
 * @author Anderson Barbosa <a.barbosa@waplicaciones.co>
 */
class ClientesController extends AbstractController
{

  private $menuPermisos;
  private $sMenuModulo = 'client';
  private $sModuloVista = 'listado';
  private $sModuloVista2 = 'listado_2';
  private $log;
  public function __construct(MenuPermisos $menuPermisos, Logs $log)
  {
    $this->menuPermisos = $menuPermisos;
    $this->log = $log;
  }

  /**
   * Genera interfaz con el listado de los clientes
   *
   * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
   *
   * @return render('clientes/index.html.twig') HTML
   */
  public function index(Request $request)
  {
    // Parámetros
    $session = $request->getSession();

    // Validar Permisos
    try
    {
      $this
        ->menuPermisos
        ->validarAccesoVista($session, $this->sMenuModulo, $this->sModuloVista);
    }
    catch(\Throwable $th)
    {
      return $this->redirect($this->generateUrl('admin_login'));
    }

    $aPermisos = $this
      ->menuPermisos
      ->getPermisosModuloVista($session, $this->sMenuModulo, $this->sModuloVista);
    $aGridButtons = $this
      ->menuPermisos
      ->getGridButtons( $this->sMenuModulo,$session ,$aPermisos);

    $dfColumnas = [
      ['headerName' => '#', 'field' => 'id', 'hide' => true], 
      ['headerName' => 'Código', 'field' => 'codigo', 'width' => 50, 'pinned'=>'left'], 
      ['headerName' => 'Droguería', 'field' => 'drogueria', 'pinned'=>'left'], 
      ['headerName' => 'Nit', 'field' => 'nit', 'width' => 60], 
      ['headerName' => 'Asociado', 'field' => 'asociado', 'pinned'=>'left'], 
      ['headerName' => 'Ciudad', 'field' => 'ciudad', 'width' => 120], 
      ['headerName' => 'Teléfono', 'field' => 'telefono', 'width' => 100], 
      ['headerName' => 'Tipo', 'field' => 'tipoCliente', 'width' => 50],
      ['headerName' => 'Estado', 'field' => 'retirado', 'width' => 50, "searchoptions" => [
            "type" => "combo", 
            "content" => [
               ["value" => 0, "text" => "Activo"],
               ["value" => 1, "text" => "Retirado"]
            ]
         ]
      ]
    ];

    $this->log->setLogAdmin("LC1 Listado Clientes");

    return $this->render('clientes/index.html.twig', array(
      'dfColumnas' => json_encode($dfColumnas) ,
      'aGridButtons' => json_encode($aGridButtons["iconos"]) ,
      'modulo' => $this->sMenuModulo,
    ));
  }

  /**
   * Responde un JSON con la información requerida para mostrar la tabla clientes
   *
   * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
   * @param App\Services\GrillaGlobal $grillaGlobal para realizar los filtros de busqueda
   * @param $exportar Type=bool Valida si se filtran datos necesarios para descargar un archivo .csv
   *
   * @return JSON
   */
  public function indexJson(Request $request, GrillaGlobal $grillaGlobal, $exportar = false)
  {
    // Parámetros
    $session = $request->getSession();
    $em = $this->getDoctrine()->getManager();
    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');

    // Validar Petición HTTP
    if(!$request->isXmlHttpRequest()){
      $response->setContent(json_encode([
        "status" => 0,
        "message" => "Acción no válida"
      ]));
      return $response;
    }

    // Grilla
    $aEquivalenciaColumnas = ["id" => "c.id", "codigo" => "c.codigo", "drogueria" => "c.drogueria", "nit" => "c.nit", "asociado" => "c.asociado", "ciudad" => "c.ciudad", "telefono" => "c.telefono", "tipoCliente" => "c.tipoCliente", "retirado" => "c.retirado"];
    $aDataGrilla = $grillaGlobal->realizarFiltro($aEquivalenciaColumnas);

    // Total Registros
    $totalRegistros = 0;
    if ($aDataGrilla["paginaActual"] == 1 && $exportar === false)
    {
      $Contador = $em->createQuery("SELECT COUNT(c.id) AS totalRegistros
        FROM App\Entity\Cliente as c
        WHERE {$aDataGrilla["where"]} ");
      $Contador->setParameters($aDataGrilla["valoresWhere"]);
      $Contador = $Contador->getSingleResult();
      $totalRegistros = $Contador['totalRegistros'];
    }

    // DQL Query
    $queryClientes = $em->createQuery("SELECT {$aDataGrilla["columnas"]}
      FROM App\Entity\Cliente as c
      WHERE {$aDataGrilla["where"]} 
      ORDER BY {$aDataGrilla["order"]} ");
    $queryClientes->setParameters($aDataGrilla["valoresWhere"]);
    $queryClientes->setMaxResults($aDataGrilla["maximoFilas"]);
    $queryClientes->setFirstResult($aDataGrilla["paginacion"]);
    $aClientesEnt = $queryClientes->getScalarResult();
    // --- --- --- Lógica --- --- --- //
    $aClientes = array();
    foreach( $aClientesEnt as $entiti ){
      if($entiti['retirado'] == 1){
        $retirado = 'Retirado';
      }else{
        $retirado = 'Activo';
      }
      $aClientes [] = array(
        'id'       => $entiti['id'],
        'codigo'   => $entiti['codigo'],
        'drogueria'   => $entiti['drogueria'],
        'nit'    => $entiti['nit'],
        'asociado'    => $entiti['asociado'],
        'ciudad' => $entiti['ciudad'],
        'telefono'   => $entiti['telefono'],
        'tipoCliente'   => $entiti['tipoCliente'],
        'retirado'   => $retirado
      );
    }

    // Resultado
    $em->getConnection()->close();
    $response->setContent(json_encode(['totalRows' => $totalRegistros, 'data' => $aClientes]));
    return $response;
  }

}

