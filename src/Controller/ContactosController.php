<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

// Services
use App\Services\Globales\GrillaGlobal;
use App\Services\Logs;
use App\Services\Globales\MenuPermisos;

use App\Entity\Contactos;
use App\Form\ContactosType;



/**
* Controlador de Contactos
*
* @category Contactos
*
* @author Julian Andres Restrepo <j.restrepo@waplicaciones.co>
*
*/
class ContactosController extends AbstractController{

  private $menuPermisos;
  private $log;
  private $sMenuModulo = 'contactosProveedor';
  private $sModuloVista = 'listado_contactos';

  public function __construct(MenuPermisos $menuPermisos, Logs $log ){
    $this->menuPermisos = $menuPermisos;
    $this->log = $log;

  }

    /**
    * Genera la vista inicial para el listado de contactos del proveedor
    *
    * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
    * @author Julian Restrepo <j.restrepo@waplicaciones.com.co>
    * @return render('contactos/index.html.twig') HTML
    **/
  public function index(Request $request){

    // Variables
    $session = $request->getSession();

    $dfColumnas = [
      ["headerClass" => "h6", 'headerName' => '#',         'field' => 'id',        'hide' => true],
      ["headerClass" => "h6", 'headerName' => 'Nombre',     'field' => 'nombre',     'width' => 250],
      ["headerClass" => "h6", 'headerName' => 'Cargo',    'field' => 'cargo',    'width' => 250],
      ["headerClass" => "h6", 'headerName' => 'Ciudad',  'field' => 'ciudad',  'width' => 150],
      ["headerClass" => "h6", 'headerName' => 'Email',       'field' => 'email', 'width' => 250, 'cellClass' => 'text-center'],
      ["headerClass" => "h6", 'headerName' => 'Telefono',    'field' => 'telefono','width' => 110, 'cellClass' => 'text-center']
    ];

    //$this->log->setLogAdmin("LU1 Listado de Usuarios de Administrador");

    return $this->render('contactos/index.html.twig', array(
      'dfColumnas'    => json_encode($dfColumnas),
      'modulo'        => $this->sMenuModulo,
    ));
  }

    /**
    * Responde un JSON con la información requerida para mostrar el listado de contactos del proveedor
    * 
    * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
    * @param App\Services\GrillaGlobal $grillaGlobal para realizar los filtros de busqueda
    * @param $exportar Type=bool Valida si se filtran datos necesarios para descargar un archivo .csv
    * @author Julian Restrepo <j.restrepo@waplicaciones.com.co>
    * @return JSON
    */
  public function indexJson(Request $request, GrillaGlobal $grillaGlobal, $exportar = false){
    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $aJson = array();


      $session = $request->getSession();
      $em = $this->getDoctrine()->getManager('contactosProveedor');

      $aEquivalenciaColumnas = [
        'id'        => 'c.id',
        'nombre'    => 'c.nombreContacto',
        'cargo'    => 'ca.nombre',
        'ciudad'    => 'c.ciudad',
        'email'     => 'c.email',
        'telefono'  => 'c.telefono'
      ];

      // Procedimiento Filtro Grilla
      $sIniFiltro = "c.idProveedor =".$request->get('idProveedor');
      $aDataGrilla = $grillaGlobal->realizarFiltro($aEquivalenciaColumnas, $sIniFiltro);

      // --- --- --- --- Total Filas --- --- --- --- //
      $numeroRegistros = 0;
      if( $aDataGrilla["paginaActual"] == 1 && $exportar === false ){
        $Contador = $em->createQuery("SELECT COUNT(c.id) AS numeroRegistros
        FROM App\Entity\Contactos c
        JOIN App\Entity\Proveedores p WITH c.idProveedor = p.id
        JOIN App\Entity\Cargos ca WITH c.idCargo = ca.id
        WHERE {$aDataGrilla["where"]} ");
        $Contador->setParameters($aDataGrilla["valoresWhere"]);
        $Contador = $Contador->getSingleResult();
        $numeroRegistros = $Contador['numeroRegistros'];
      }

      // DQL Data
      $queryUser = $em->createQuery("SELECT c.id, c.nombreContacto AS nombre, ca.nombre AS cargo, c.ciudad, c.email, c.telefono
        FROM App\Entity\Contactos c
        JOIN App\Entity\Proveedores p WITH c.idProveedor = p.id
        JOIN App\Entity\Cargos ca WITH c.idCargo = ca.id
        WHERE {$aDataGrilla["where"]} 
        ORDER BY {$aDataGrilla["order"]} ");

      // Resultado
      $queryUser->setParameters($aDataGrilla["valoresWhere"]);

      if( $exportar === false ){
        $queryUser->setMaxResults($aDataGrilla["maximoFilas"]);
        $queryUser->setFirstResult($aDataGrilla["paginacion"]);
      }
      $aUser = $queryUser->getScalarResult();

        // --- --- --- Lógica --- --- --- //
        $aListUser = array();
        foreach( $aUser as $entiti ){

          $aListUser[] = array(
            'id'       => $entiti['id'],
            'nombre'    => $entiti['nombre'],
            'cargo'   => $entiti['cargo'],
            'ciudad'   => $entiti['ciudad'],
            'email'   => $entiti['email'],
            'telefono'    => $entiti['telefono'],
          );
        }
        // Cierre de conexion y Respuesta
        $em->getConnection()->close();
        $response->setContent(json_encode(['totalRows' => $numeroRegistros, 'data' => $aListUser]));

    return $response;
  }

  /**
   * Accion para crear un contacto de proveedor contactos/mdlCrud
   * Si se detecta el envio del formulario se creara, de lo contrario se mostrara la vista contactos/mdlCrud
   * @param object $request Objeto peticion de Symfony 4.2
   * @return vista contactos/mdlCrud
   * @return object json resultado de la accion nuevo
   * @author Julian Restrepo <j.restrepo@waplicaciones.com.co>
   * @since 5.2
   * @category Correos\contactos
  */
  public function contactosNew(Request $request): Response{
    
    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $aJson = array();
    $em = $this->getDoctrine()->getManager('contactosProveedor');

    if( $request->isXmlHttpRequest() ){

      $session = $request->getSession();

      if( !is_null($request->get('contactos')) ){
          
        $aDataForm = $request->get('contactos');

        $emUser = new Contactos();
        $form = $this->createForm(ContactosType::class, $emUser,['idProveedor' => $aDataForm['idProveedor']]);
        $form->handleRequest($request);

        if( $form->isSubmitted() ){

            $emUser->setFechaCreacion(new \DateTime());

            $em->persist($emUser);
            $em->flush();

            //$this->log->setLogAdmin("LU2 Usuarios de Administrador, agregado correctamente");

            $aJson['status'] = 1;
            $aJson['message'] = "Contacto agregado correctamente.";

        } else {
            $aJson['status'] = 0;
            $aJson['message'] = 'El formulario fue mal diligenciado, verifique e inténtelo de nuevo.';

            //$this->log->setLogAdmin("LU2 Formulario de Creación de Usuarios de Administrador fue mal Diligenciado");
        }
      } else {
        
        $entity = new Contactos();
        $form = $this->createForm(ContactosType::class, $entity, ['idProveedor' => $request->get('idProveedor')]);

        //data
        $aJson['entity'] = $entity;
        $aJson['accion'] = 'nuevo';
        $aJson['form']   = $form->createView();

        //$this->log->setLogAdmin("LU2 Formulario de Creación de Usuarios de Administrador, usuario Vacio");

        return $this->render('contactos/mdlCrud.html.twig', $aJson);
      }

    } else {
      $aJson['status'] = 0;
      $aJson['message'] = 'Acción no valida';

      //$this->log->setLogAdmin("LU2 Formulario de Creación de Usuarios de Administrador, acción no valida");
    }

    $em->getConnection()->close();

    $response->setContent(json_encode($aJson));
    return $response;
  }

    /**
    * Responde una plantilla HTML contactos/mdlCrud.html.twig con el formulario de edición de contacots ó Responde un JSON con un estado y mensaje de la petición de edicion de un contacto
    *
    * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
    *
    * @return Symfony\Component\HttpFoundation\Response JSON o render(contactos/mdlCrud.html.twig)
    */
  public function contactosEdit(Request $request): Response{

    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $aJson = array();
    $em = $this->getDoctrine()->getManager('contactosProveedor');

    if( $request->isXmlHttpRequest() ){

      $session = $request->getSession();

      if( !is_null($request->get('contactos')) ){

        $aFormData = $request->get('contactos');

        $emUser = $em->getRepository(Contactos::class)->findOneById($request->get('idContacto'));

        $editForm = $this->createForm(ContactosType::class, $emUser,['idProveedor' => $request->get('idProveedor')]);
        $editForm->handleRequest($request);

          if( $editForm->isSubmitted() && !isset($aUser[0]) ){

              $em->persist($emUser);
              $em->flush();

              //$this->log->setLogAdmin("LU3 Formulario de Usuarios de Administrador, editado correctamente");

              $aJson['status'] = 1;
              $aJson['message'] = "Contacto editado correctamente.";
          }else{
              $aJson['status'] = 0;

              //$this->log->setLogAdmin("LU3 Formulario de Usuarios de Administrador, la Cedula ya se encuentra registrada");

          }
      }else{

        // Entity manager y Formulario
        $oUser = $em->getRepository(Contactos::class)->findOneById($request->get("idContacto"));

        $editForm = $this->createForm(ContactosType::class, $oUser, ['idProveedor' => $request->get('idProveedor')]);

        //data
        $aJson['entity'] = $oUser;
        $aJson['form']   = $editForm->createView();
        $aJson['accion'] = 'editar';
        $aJson['idProveedor'] = $request->get("idProveedor");
        $aJson['idContacto'] = $request->get("idContacto");

        // $this->log->setLogAdmin("LU3 Formulario de Usuarios de Administrador, usuario Vacio");

        return $this->render('contactos/mdlCrud.html.twig', $aJson);
      }

    }else{
      $aJson['status'] = 0;
      $aJson['message'] = 'Acción no valida';
      //$this->log->setLogAdmin("LU3 Formulario de Usuarios de Administrador, acción no valida");
    }

    $em->getConnection()->close();

    $response->setContent(json_encode($aJson));
    return $response;
  }

    /**
    * Responde un JSON con un estado y mensaje de la petición de eliminación de un contacto
    *
    * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
    *
    * @return Symfony\Component\HttpFoundation\Response JSON
    */
  public function contactosDelete(Request $request): Response{

    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $em = $this->getDoctrine()->getManager('contactosProveedor');
    $conexion= $em->getConnection();
    $aJson = array();

    if( $request->isXmlHttpRequest() ){

      $session = $request->getSession();
      $idUser = $request->get('idContacto');

      try{
      
        $conexion->beginTransaction();
        $conexion->query('DELETE FROM contactos WHERE id='.$idUser);

        //$response->setStatusCode(200);
        $conexion->commit();

        $aJson['status'] = 1;
        $aJson['message'] = 'Contacto eliminado correctamente.';

      }catch(\Exception $e){
        $conexion->rollBack();

        $aJson['status'] = 0;
        $aJson['message'] = 'No es posible eliminar el contacto. Comuniquese con el administrador.';
      }

      //$this->log->setLogAdmin("LU4 Formulario de Usuarios de Administrador, eliminado correctamente");
      

    } else {
        $aJson['status'] = 0;
        $aJson['message'] = 'Acción no valida';

        //$this->log->setLogAdmin("LU4 Formulario de Usuarios de Administrador, acción no valida");
    }

    //cierre de conexion
    $conexion->close();
    $response->setContent(json_encode($aJson));
    return $response;
  }


    /**
    * Retorna un archivo CSV con el listado de proveedores
    *
    * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
    *
    * @return Symfony\Component\HttpFoundation\Response
    */
  public function proveedorCsv(Request $request, GrillaGlobal $grillaGlobal): Response{

    // Variables
    $session = $request->getSession();

    $sFileCsv = "";
    $aDataGrilla = json_decode($this->indexJson($request, $grillaGlobal, true)->getContent(), 1);

    //$this->log->setLogAdmin("LRA5 Listado de Logs de Administrador");

    $sFileCsv = utf8_decode("NIT;CODIGO;NOMBRE;REPRESENTANTE LEGAL;EMAIL REPRESENTANTE LEGAL;TELEFONO REPRESENTANTE LEGAL");
    $sFileCsv .= "\r\n";
    foreach( $aDataGrilla["data"] as $aData ){

      $sFileCsv .= utf8_decode($aData['nit']).";".utf8_decode($aData['codigo']).";".utf8_decode($aData['nombre']).";".utf8_decode($aData['representanteLegal']).";".utf8_decode($aData['emailRepresentanteLegal']).";".utf8_decode($aData['telefonoRepresentanteLegal']).";";
      $sFileCsv .= "\r\n";
    }

    $response = new Response($sFileCsv);
    $response->headers->set('Content-Type', 'text/csv');
    $response->setStatusCode(200);
    $response->headers->set('Content-Disposition', 'attachment; filename=proveedores.csv; charset=UTF-8');

    return $response;
    
  }


  /**
    * Responde un JSON con un estado y mensaje de la verificacion del email del contacto
    *
    * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
    *
    * @return Symfony\Component\HttpFoundation\Response JSON
    */
  public function verificacionEmail(Request $request): Response{

    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $em = $this->getDoctrine()->getManager('contactosProveedor');
    $conexion= $em->getConnection();
    $session = $request->getSession();
    $aJson = array();

    if( $request->isXmlHttpRequest() ){

      
      $idContacto = $request->get('idContacto');
      $email = $request->get('email');

      if($idContacto != ''){
        $infoContacto = $em->createQuery("SELECT c.id FROM App\Entity\Contactos c WHERE c.email='".$email."' AND c.id !=".$idContacto)->getScalarResult();

        if($infoContacto){
          $aJson['status'] = 0;
          $aJson['message'] = 'El email ['.$email.'] ya fue registrado para otro contacto.';
        }else{
          $aJson['status'] = 1;
          $aJson['message'] = 'El email esta libre para el registro.';
        }

      }else{

        $infoContacto = $em->getRepository('App\Entity\Contactos')->findOneBy(array('email'=>$email));

        if($infoContacto){
          $aJson['status'] = 0;
          $aJson['message'] = 'El email ['.$email.'] ya fue registrado para otro contacto.';
        }else{
          $aJson['status'] = 1;
          $aJson['message'] = 'El email esta libre para el registro.';
        }

      }      

    } else {
        $aJson['status'] = 0;
        $aJson['message'] = 'Acción no valida';
    }

    //cierre de conexion
    $conexion->close();
    $response->setContent(json_encode($aJson));
    return $response;
  }


}