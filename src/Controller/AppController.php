<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route; 
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bundle\FrameworkBundle\Controller\RedirectController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use App\Security\PasswordEncoder;

use Twig\Environment;
use App\Entity\Drogueria;
use App\Entity\User;
use App\Services\Mail;


/**
* Controlador de inicio de sesión de Shopping de Precios
*
* @category Login
* @author Julian Andres Restrepo <j.restrepo@waplicaciones.co>
*/
class AppController extends AbstractController{


	private $twig;
	private $passwordEncoder;
    private $mail;

	public function __construct(Environment $twig, PasswordEncoder $passwordEncoder, Mail $mail){
        $this->twig = $twig;
        $this->mail = $mail;
        $this->passwordEncoder = $passwordEncoder;
    }
    /**
    * Valida la sesión y redirige segun resultado
    *
    * @param Symfony\Component\HttpFoundation\Request $request Peticion HTTP, que debe contener la variable SESSION con la información de sesión del administrador.
    * @param App\Services\AuthenticationUtils $authenticationUtils Complemento de symfony que controla la autenticación de los usuarios
    *
    * @return render('security/login.html.twig') HTML
    */
    public function login(AuthenticationUtils $authenticationUtils,Request $request): Response{
        
        if($request->getSession()->get("id")){
            return $this->redirect($this->generateUrl('admin_usuarios'));
        }
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }
    
    /**
    * Cierra la sesión existente
    *
    * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
    * @param App\Services\AuthenticationUtils $authenticationUtils Complemento de symfony que controla la autenticación de los usuarios
    *
    * @return render('security/login.html.twig') HTML
    */
    public function logout(Request $request){
      $session = $request->getSession();
      $session->invalidate();
      return $this->redirect($this->generateUrl('admin_login'));
      //throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
