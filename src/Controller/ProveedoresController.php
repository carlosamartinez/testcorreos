<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

// Services
use App\Services\Globales\GrillaGlobal;
use App\Services\Logs;
use App\Services\Globales\MenuPermisos;

use App\Entity\Proveedores;
use App\Form\ProveedoresType;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


/**
* Controlador de Proveedores
*
* @category Proveedores
*
* @author Julian Andres Restrepo <j.restrepo@waplicaciones.co>
*
*/
class ProveedoresController extends AbstractController{

  private $menuPermisos;
  private $log;
  private $sMenuModulo = 'proveedores';
  private $sModuloVista = 'listado_proveedores';

  public function __construct(MenuPermisos $menuPermisos, Logs $log ){
    $this->menuPermisos = $menuPermisos;
    $this->log = $log;

  }

    /**
    * Genera la vista inicial para el listado de proveedores
    *
    * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
    * @author Julian Restrepo <j.restrepo@waplicaciones.com.co>
    * @return render('proveedores/index.html.twig') HTML
    **/
  public function index(Request $request){

    // Variables
    $session = $request->getSession();
    try{
    $this->menuPermisos->validarAccesoVista( $session, $this->sMenuModulo, $this->sModuloVista);
    } catch (\Throwable $th) {
      return $this->redirect($this->generateUrl('admin_login'));
    }

    $aPermisos = $this->menuPermisos->getPermisosModuloVista($session, $this->sMenuModulo, $this->sModuloVista);
    $aGridButtons = ( empty($aPermisos) ) ? array() : $this->menuPermisos->getGridButtons( $this->sMenuModulo,$session , $aPermisos );

    $dfColumnas = [
      ["headerClass" => "h6", 'headerName' => '#',         'field' => 'id',        'hide' => true],
      ['headerName' => '','field' => 'expand','maxWidth' => 40, 'width' => 40, 'cellClass' => 'grid-cell-checkbox-detail', 'cellRenderer' => 'agGroupCellRenderer','sortable' => false, 'resizable' => false],
      ["headerClass" => "h6", 'headerName' => 'Nit',    'field' => 'nit',    'width' => 180],
      ["headerClass" => "h6", 'headerName' => 'Código',    'field' => 'codigo',    'width' => 100],
      ["headerClass" => "h6", 'headerName' => 'Nombre',     'field' => 'nombre',     'width' => 250],
      ["headerClass" => "h6", 'headerName' => 'Representante legal',  'field' => 'representanteLegal',  'width' => 250],
      ["headerClass" => "h6", 'headerName' => 'Email representante',       'field' => 'emailRepresentanteLegal', 'width' => 250, 'cellClass' => 'text-center', 'search' => false, 'sortable' => false],
      ["headerClass" => "h6", 'headerName' => 'Teléfono representante',    'field' => 'telefonoRepresentanteLegal','width' => 200, 'cellClass' => 'text-center']
    ];

    $dfColumnasContactos = [
      ["headerClass" => "h6", 'headerName' => '#',         'field' => 'id',        'hide' => true],
      ["headerClass" => "h6", 'headerName' => 'Nombre',     'field' => 'nombre',     'width' => 250],
      ["headerClass" => "h6", 'headerName' => 'Cargo',    'field' => 'cargo',    'width' => 250],
      ["headerClass" => "h6", 'headerName' => 'Ciudad',  'field' => 'ciudad',  'width' => 150],
      ["headerClass" => "h6", 'headerName' => 'Email',       'field' => 'email', 'width' => 250, 'cellClass' => 'text-center'],
      ["headerClass" => "h6", 'headerName' => 'Teléfono',    'field' => 'telefono','width' => 110, 'cellClass' => 'text-center']
    ];

    $this->log->setLogAdmin("LP1 Listado de Proveedores");

    return $this->render('proveedores/index.html.twig', array(
      'dfColumnas'    => json_encode($dfColumnas),
      'dfColumnasContactos'    => json_encode($dfColumnasContactos),
      'aGridButtons'  => json_encode($aGridButtons["iconos"]),
      'modulo'        => $this->sMenuModulo,
    ));
  }

    /**
    * Responde un JSON con la información requerida para mostrar el listado de proveedores
    * 
    * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
    * @param App\Services\GrillaGlobal $grillaGlobal para realizar los filtros de busqueda
    * @param $exportar Type=bool Valida si se filtran datos necesarios para descargar un archivo .csv
    * @author Julian Restrepo <j.restrepo@waplicaciones.com.co>
    * @return JSON
    */
  public function indexJson(Request $request, GrillaGlobal $grillaGlobal, $exportar = false){
    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $aJson = array();


      $session = $request->getSession();
      $em = $this->getDoctrine()->getManager('contactosProveedor');

      $aEquivalenciaColumnas = [
        'id'        => 'p.id',
        'nit'    => 'p.nit',
        'codigo'    => 'p.codigo',
        'nombre'    => 'p.nombre',
        'representanteLegal'    => 'p.representanteLegal',
        'emailRepresentanteLegal'     => 'p.emailRepresentanteLegal',
        'telefonoRepresentanteLegal'  => 'p.telefonoRepresentanteLegal'
      ];

      // Procedimiento Filtro Grilla
      $sIniFiltro = "";
      $aDataGrilla = $grillaGlobal->realizarFiltro($aEquivalenciaColumnas, $sIniFiltro);

      // --- --- --- --- Total Filas --- --- --- --- //
      $numeroRegistros = 0;
      if( $aDataGrilla["paginaActual"] == 1 && $exportar === false ){
        $Contador = $em->createQuery("SELECT COUNT(DISTINCT(p.nit)) AS numeroRegistros
        FROM App\Entity\Proveedores p
        JOIN App\Entity\Contactos c WITH c.idProveedor = p.id AND c.idArea = 2
        WHERE {$aDataGrilla["where"]} ");
        $Contador->setParameters($aDataGrilla["valoresWhere"]);
        $Contador = $Contador->getSingleResult();
        $numeroRegistros = $Contador['numeroRegistros'];
      }

      // DQL Data
      $queryUser = $em->createQuery("SELECT p.id, p.nit, p.codigo, p.nombre, p.representanteLegal, p.emailRepresentanteLegal, p.telefonoRepresentanteLegal, '' AS expand
        FROM App\Entity\Proveedores p
        JOIN App\Entity\Contactos c WITH c.idProveedor = p.id AND c.idArea = 2
        WHERE {$aDataGrilla["where"]} 
        GROUP BY p.nit
        ORDER BY {$aDataGrilla["order"]} ");

      // Resultado
      $queryUser->setParameters($aDataGrilla["valoresWhere"]);

      if( $exportar === false ){
        $queryUser->setMaxResults($aDataGrilla["maximoFilas"]);
        $queryUser->setFirstResult($aDataGrilla["paginacion"]);
      }
      $aUser = $queryUser->getScalarResult();

        // --- --- --- Lógica --- --- --- //
        $aListUser = array();
        foreach( $aUser as $entiti ){

          $aListUser [] = array(
            'id'       => $entiti['id'],
            'nit'   => $entiti['nit'],
            'codigo'   => $entiti['codigo'],
            'nombre'    => $entiti['nombre'],
            'representanteLegal' => $entiti['representanteLegal'],
            'emailRepresentanteLegal'   => $entiti['emailRepresentanteLegal'],
            'telefonoRepresentanteLegal'    => $entiti['telefonoRepresentanteLegal'],
            'expand'    => $entiti['expand'],
            'urlContactos'=> $this->generateUrl('admin_contactos_proveedores', ['idProveedor' => $entiti["id"] ])
          );
        }

        // Cierre de conexion y Respuesta
        $em->getConnection()->close();
        $response->setContent(json_encode(['totalRows' => $numeroRegistros, 'data' => $aListUser]));
    

    return $response;
  }

  /**
   * Accion para crear Proveedor proveedor/mdlCrud
   * Si se detecta el envio del formulario se creara, de lo contrario se mostrara la vista proveedor/mdlCrud
   * @param object $request Objeto peticion de Symfony 4.2
   * @return vista proveedor/mdlCrud
   * @return object json resultado de la accion nuevo
   * @author Julian Restrepo <j.restrepo@waplicaciones.com.co>
   * @since 4.2
   * @category Correos\proveedores
  */
  public function proveedorNew(Request $request): Response{
    
    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $aJson = array();
    $em = $this->getDoctrine()->getManager('contactosProveedor');

    if( $request->isXmlHttpRequest() ){

      $session = $request->getSession();
      $bAccesoAccion = $this->menuPermisos->getAccesoVistaAccion( $session, $this->sMenuModulo, $this->sModuloVista, 'nuevo-registro' );

      if($bAccesoAccion){

        if( !is_null($request->get('proveedores')) ){
          
          $aDataForm = $request->get('proveedores');
          
          $oUser = $em->getRepository('App\Entity\Proveedores')->findOneByNit($aDataForm['nit']);

          if( $oUser ){
            $aJson['status'] = 0;
            $aJson['message'] = 'El NIT ya se encuentra registrado, verifique he inténtelo de nuevo.';

            $this->log->setLogAdmin("LP2 Formulario de Creación de Proveedores, NIT registrado previamente");
            
          }else{

            $emUser = new Proveedores();
            $form = $this->createForm(ProveedoresType::class, $emUser);
            $form->handleRequest($request);

            if( $form->isSubmitted() ){

                $emUser->setFechaUltimaModificacion(new \DateTime());

                $em->persist($emUser);
                $em->flush();

                $this->log->setLogAdmin("LP2 Proveedor agregado correctamente");

                $aJson['status'] = 1;
                $aJson['message'] = "Proveedor agregado correctamente.";

            } else {
                $aJson['status'] = 0;
                $aJson['message'] = 'El formulario fue mal diligenciado, verifique e inténtelo de nuevo.';

                $this->log->setLogAdmin("LP2 Formulario de Creación de Proveedores fue mal diligenciado");
            }
            
          }
        } else {
          
          $entity = new Proveedores();
          $form = $this->createForm(ProveedoresType::class, $entity);

          //data
          $aJson['entity'] = $entity;
          $aJson['accion'] = 'nuevo';
          $aJson['form']   = $form->createView();

          $this->log->setLogAdmin("LP2 Visualizacion de formulario de Creación de Proveedores");

          return $this->render('proveedores/mdlCrud.html.twig', $aJson);
        }
        
      }else{
        $aJson['status'] = 0;
        $aJson['message'] = 'Acción no habilitada, inicie sesión nuevamente he inténtelo otra vez.';

        $this->log->setLogAdmin("LP2 Formulario de Creación de Proveedores, acción no habilitada");        
      }

    } else {
      $aJson['status'] = 0;
      $aJson['message'] = 'Acción no valida';

      $this->log->setLogAdmin("LU2 Formulario de Creación de Proveedores, acción no valida");
    }

    $em->getConnection()->close();

    $response->setContent(json_encode($aJson));
    return $response;
  }

    /**
    * Responde una plantilla HTML proveedores/mdlCrud.html.twig con el formulario de edición de proveeodres ó Responde un JSON con un estado y mensaje de la petición de edicion de un proveedor
    *
    * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
    *
    * @return Symfony\Component\HttpFoundation\Response JSON o render(usuarios/mdlCrud.html.twig)
    */
  public function proveedorEdit(Request $request): Response{

    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $aJson = array();
    $em = $this->getDoctrine()->getManager('contactosProveedor');

    if( $request->isXmlHttpRequest() ){

      $session = $request->getSession();
      $bAccesoAccion = $this->menuPermisos->getAccesoVistaAccion( $session, $this->sMenuModulo, $this->sModuloVista, 'editar-registro' );

      if($bAccesoAccion){
        
        
        if( !is_null($request->get('proveedores')) ){

          $aFormData = $request->get('proveedores');

          $emUser = $em->getRepository(Proveedores::class)->findOneById($request->get('id_proveedor'));

          $editForm = $this->createForm(ProveedoresType::class, $emUser);
          $editForm->handleRequest($request);

            if( $editForm->isSubmitted() ){

                $emUser->setFechaUltimaModificacion(new \DateTime());

                $em->persist($emUser);
                $em->flush();

                $this->log->setLogAdmin("LP3 Formulario de Proveedores, editado correctamente");

                $aJson['status'] = 1;
                $aJson['message'] = "Proveedor editado correctamente.";
            }else{
                $aJson['status'] = 0;

                $this->log->setLogAdmin("LP3 Formulario de Proveedores, formulario mal diligenciado");
            }
        }else{

          // Entity manager y Formulario
          $oUser = $em->getRepository(Proveedores::class)->findOneById($request->get("idProveedor"));
          $editForm = $this->createForm(ProveedoresType::class, $oUser);

          //data
          $aJson['entity'] = $oUser;
          $aJson['form']   = $editForm->createView();
          $aJson['accion'] = 'editar';
          $aJson['idProveedor'] = $request->get("idProveedor");

          $this->log->setLogAdmin("LP3 Visualizacion del formulario de Proveedores");

          return $this->render('proveedores/mdlCrud.html.twig', $aJson);
        }

        //cierre de conexion
        
      }else{
        $aJson['status'] = 0;
        $aJson['message'] = 'Acción no habilitada, inicie sesión nuevamente he inténtelo otra vez.';
        $this->log->setLogAdmin("LP3 Formulario de Proveedores, acción no habilitada");
      }

    }else{
      $aJson['status'] = 0;
      $aJson['message'] = 'Acción no valida';
      $this->log->setLogAdmin("LP3 Formulario de Proveedores, acción no valida");
    }

    $em->getConnection()->close();

    $response->setContent(json_encode($aJson));
    return $response;
  }

    /**
    * Responde un JSON con un estado y mensaje de la petición de eliminación de un proveedor
    *
    * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
    *
    * @return Symfony\Component\HttpFoundation\Response JSON
    */
  public function proveedorDelete(Request $request): Response{

    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $em = $this->getDoctrine()->getManager('contactosProveedor');
    $conexion= $em->getConnection();
    $aJson = array();

    if( $request->isXmlHttpRequest() ){

      $session = $request->getSession();
      $bAccesoAccion = $this->menuPermisos->getAccesoVistaAccion( $session, $this->sMenuModulo, $this->sModuloVista, 'eliminar-registro' );
      if($bAccesoAccion){

        $idUser = $request->get('idProveedor');

        try{
        
          $conexion->beginTransaction();
          $conexion->query('DELETE FROM proveedores WHERE id='.$idUser);

          $conexion->commit();

          $aJson['status'] = 1;
          $aJson['message'] = 'Proveedor eliminado correctamente.';

          $this->log->setLogAdmin("LP4 Proveedor eliminado correctamente");

        }catch(\Exception $e){
          $conexion->rollBack();

          $aJson['status'] = 0;
          $aJson['message'] = 'No es posible eliminar el proveedor ya que tiene vinculados contactos.';

          $this->log->setLogAdmin("LP4 No fue posible eliminar los datos del Proveedor");
        }

        

      }else{ 
        $aJson['status'] = 0;
        $aJson['message'] = 'Acción no habilitada, inicie sesión nuevamente he inténtelo otra vez.';

        $this->log->setLogAdmin("LP4 Eliminacion de datos del Proveedor, acción no habilitada");
      }

    } else {
        $aJson['status'] = 0;
        $aJson['message'] = 'Acción no valida';

        $this->log->setLogAdmin("LP4 Eliminacion de datos del Proveedor, acción no valida");
    }

    //cierre de conexion
    $conexion->close();
    $response->setContent(json_encode($aJson));
    return $response;
  }


    /**
    * Retorna un archivo CSV con el listado de proveedores
    *
    * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
    *
    * @return Symfony\Component\HttpFoundation\Response
    */
  public function proveedorCsv(Request $request, GrillaGlobal $grillaGlobal): Response{

    // Variables
    $session = $request->getSession();

    $sFileCsv = "";
    $aDataGrilla = json_decode($this->indexJson($request, $grillaGlobal, true)->getContent(), 1);

    $this->log->setLogAdmin("LP5 Exportacion de datos del Proveedor");

    $excelService = new Spreadsheet();
    $writer = new Xlsx($excelService);
    

    $activeSheet = $excelService->getActiveSheet();

    $activeSheet->setCellValue("A1", "NIT");
    $activeSheet->setCellValue("B1", "CODIGO");
    $activeSheet->setCellValue("C1", "NOMBRE");
    $activeSheet->setCellValue("D1", "REPRESENTANTE LEGAL");
    $activeSheet->setCellValue("E1", "EMAIL REPRESENTANTE");
    $activeSheet->setCellValue("F1", "TELEFONO REPRESENTANTE LEGAL");
    $count = 2 ;
    foreach( $aDataGrilla["data"] as $aData ){

      $activeSheet->setCellValue("A".$count, utf8_decode($aData['nit']));
      $activeSheet->setCellValue("B".$count, utf8_decode($aData['codigo']));
      $activeSheet->setCellValue("C".$count, utf8_decode($aData['nombre']));
      $activeSheet->setCellValue("D".$count, utf8_decode($aData['representanteLegal']));
      $activeSheet->setCellValue("E".$count, utf8_decode($aData['emailRepresentanteLegal']));
      $activeSheet->setCellValue("F".$count, utf8_decode($aData['telefonoRepresentanteLegal']));
      
      $count++;
    }
    $fileName = "proveedoresListado.xls";

    $writer->save($fileName);
    // We'll be outputting an excel file
    header('Content-type: application/vnd.ms-excel');

    // It will be called file.xls
    header('Content-Disposition: attachment; filename=' . $fileName);
    // Write file to the browser
    $writer->save('php://output');

    exit();
    
  }


}