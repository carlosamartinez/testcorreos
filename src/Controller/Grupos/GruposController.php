<?php
namespace App\Controller\Grupos;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

// Services
use App\Services\Globales\GrillaGlobal;
use App\Services\Globales\MenuPermisos;
use App\Entity\Grupo;
use App\Entity\User;
use App\Services\Logs;
use App\Form\GrupoType;

//BUNDLES 
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
/**
 * Controlador para visualizar los grupos
 *
 * @author Pablo Bravo <p.trivino@waplicaciones.co>
 */
class GruposController extends AbstractController
{

  private $menuPermisos;
  private $sMenuModulo = 'grupos';
  private $sModuloVista = 'listado_grupos';
  private $log;
  public function __construct(MenuPermisos $menuPermisos, Logs $log)
  {
    $this->menuPermisos = $menuPermisos;
    $this->log = $log;
  }

  /**
   * Responde un documento HTML con la lista de grupos
   *
   * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
   *
   * @return render('grupos/index.html.twig') HTML
   */
  public function index(Request $request)
  {
    // Parámetros
    $session = $request->getSession();

    // Validar Permisos
    try
    {
      $this
        ->menuPermisos
        ->validarAccesoVista($session, $this->sMenuModulo, $this->sModuloVista);
    }
    catch(\Throwable $th)
    {
      return $this->redirect($this->generateUrl('admin_login'));
    }

    $aPermisos = $this
      ->menuPermisos
      ->getPermisosModuloVista($session, $this->sMenuModulo, $this->sModuloVista);
    $aGridButtons = $this
      ->menuPermisos 
      ->getGridButtons( $this->sMenuModulo,$session ,$aPermisos);

      $dfColumnas = [
      ["headerClass" => "h6", 'headerName' => '#',             'field' => 'id', 'hide' => true], 
      ["headerClass" => "h6", 'headerName' => 'Título',        'field' => 'nombre', 'width' => 280], 
      ["headerClass" => "h6", 'headerName' => 'Fecha creación','field' => 'fechaCreado', 'width' => 150], 
      ["headerClass" => "h6", 'headerName' => 'Administrador', 'field' => 'administrador', 'width' => 200, 'hide' => true], 
      ["headerClass" => "h6", 'headerName' => 'Clasificación', 'field' => 'clasificacion', 'width' => 150], 
      ["headerClass" => "h6", 'headerName' => 'Integrantes',   'field' => 'integrantes', 'width' => 130], 
      ["headerClass" => "h6", 'headerName' => 'Descripción',   'field' => 'descripcion', 'width' => 310], 
    ];

    $this->log->setLogAdmin("LC1 Listado Clientes");

    return $this->render('grupos/index.html.twig', array(
      'dfColumnas' => json_encode($dfColumnas) ,
      'aGridButtons' => json_encode($aGridButtons["iconos"]) ,
      'modulo' => $this->sMenuModulo,
    ));
  }

  /**
   * Responde un JSON con la información requerida para mostrar la tabla clientes
   *
   * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
   * @param App\Services\GrillaGlobal $grillaGlobal para realizar los filtros de busqueda
   * @param $exportar Type=bool Valida si se filtran datos necesarios para descargar un archivo .csv
   *
   * @return JSON
   */
  public function indexJson(Request $request, GrillaGlobal $grillaGlobal, $exportar = false)
  {
    // Parámetros
    $session = $request->getSession();
    $em = $this->getDoctrine()->getManager();
    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');

    // Validar Petición HTTP
    if(!$request->isXmlHttpRequest()){
      $response->setContent(json_encode([
        "status" => 0,
        "message" => "Acción no válida"
      ]));
      return $response;
    }

    // Grilla
    $aEquivalenciaColumnas = ["id" => "g.id", 
                              "nombre" => "g.nombre", 
                              "fechaCreado" => "g.fechaCreado", 
                              "administrador" => "u.id", 
                              "clasificacion" => "g.clasificacion", 
                              "integrantes" => "g.integrantes", 
                              "descripcion" => "g.descripcion"
                            ];
    $aDataGrilla = $grillaGlobal->realizarFiltro($aEquivalenciaColumnas);

    // Total Registros
    $totalRegistros = 0;
    if ($aDataGrilla["paginaActual"] == 1 && $exportar === false)
    {
      $Contador = $em->createQuery("SELECT COUNT(g.id) AS totalRegistros
        FROM App\Entity\Grupo as g
        WHERE {$aDataGrilla["where"]} ");
      $Contador->setParameters($aDataGrilla["valoresWhere"]);
      $Contador = $Contador->getSingleResult();
      $totalRegistros = $Contador['totalRegistros'];
    }

    // DQL Query
    $queryClientes = $em->createQuery("SELECT g.id, g.nombre, g.fechaCreado, u.id as administrador, g.clasificacion, g.integrantes, g.descripcion 
      FROM App\Entity\Grupo as g
      LEFT JOIN App\Entity\User as u WITH g.administrador = u.id 
      WHERE {$aDataGrilla["where"]} 
      ORDER BY {$aDataGrilla["order"]} ");
    $queryClientes->setParameters($aDataGrilla["valoresWhere"]);
    $aCLientes = $queryClientes->getScalarResult();

    // Resultado
    $em->getConnection()->close();
    $response->setContent(json_encode(['totalRows' => $totalRegistros, 'data' => $aCLientes]));
    return $response;
  }

  /**
   * Accion para crear un nuevo grupo
   * Si se detecta el envio del formulario se creara, de lo contrario se mostrara la vista de nuevo grupo
   * @param object $request Objeto peticion de Symfony 4.2
   * @return render(html.t) 
   * @return object json resultado de la accion nuevo
   * @since 4.2
   * @category AdministradorImagenes\usuarios
  */
  public function grupoModif(Request $request): Response{
    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $aJson = array();
    $em = $this->getDoctrine()->getManager();

    if( $request->isXmlHttpRequest() ){

      $session = $request->getSession();
      $bAccesoAccion = $this->menuPermisos->getAccesoVistaAccion( $session, $this->sMenuModulo, $this->sModuloVista, 'nuevo-registro' );

      if($bAccesoAccion){
        $oGrupo = $em->getRepository('App\Entity\Grupo')->findOneById($request->get("id"));

        if( !is_null($request->get('grupo')) ){
          
            $aDataForm = $request->get('grupo');
            !isset($aDataForm["id"]) && empty($aDataForm["id"]) ? : $oGrupo = $em->getRepository('App\Entity\Grupo')->findOneById($aDataForm["id"]);
            if( !$oGrupo){

                $emGrupo = new Grupo();
                $form = $this->createForm(GrupoType::class, $emGrupo);

            }else{

                $emGrupo = $oGrupo;
                $form = $this->createForm(GrupoType::class, $emGrupo);

            }

            $form->handleRequest($request);
                
            if( $form->isSubmitted() ){

                $editFilter = !$oGrupo ? "" : "AND g.id <> " . $aDataForm["id"];

                $grupoFind = $em->createQuery("SELECT g.id
                    FROM App\Entity\Grupo as g
                    WHERE g.nombre = '{$aDataForm["nombre"]}' {$editFilter}
                ")->getScalarResult();

                if($grupoFind){

                    $aJson['status'] = 0;
                    $aJson['message'] = 'El nombre de grupo ya se encuentra registrado, verifique he inténtelo de nuevo.';

                    $this->log->setLogAdmin("LG2 Formulario de creación de grupo, el nombre de usuario ya se encuentra registrado");

                }else{

                    $oGrupo ? :$emGrupo->setFechaCreado(new \DateTime());
                    $oGrupo ? :$emGrupo->setAdministrador($em->getRepository(User::Class)->find($session->get('id')));
                    $emGrupo->setNombre($aDataForm["nombre"]);
                    $emGrupo->setClasificacion($aDataForm["clasificacion"]);
                    $emGrupo->setDescripcion($aDataForm["descripcion"]);

                    $em->persist($emGrupo);
                    $em->flush();

                    $this->log->setLogAdmin($oGrupo ? "LG2 Grupo editado correctamente" :"LU2 Grupo agregado correctamente");

                    $aJson['status'] = 1;
                    $aJson['message'] = $oGrupo ? "LG2 Grupo editado correctamente" :"LU2 Grupo agregado correctamente";
                }

            }else{

                $aJson['status'] = 0;
                $aJson['message'] = 'El formulario fue mal diligenciado, verifique e inténtelo de nuevo.';

                $this->log->setLogAdmin("LG2 Formulario de creación de grupo fue mal diligenciado");
            }
            
        }else{
            if( !$oGrupo ){

                $emGrupo = new Grupo();

            }else{

                $emGrupo = $oGrupo;

            }
            $form = $this->createForm(GrupoType::class, $emGrupo);

            //data
            $aJson['entity'] = $emGrupo;
            $aJson['accion'] = 'nuevo';
            $aJson['form']   = $form->createView();

            $this->log->setLogAdmin("LG2 Formulario de Creación de Grupos");

            return $this->render('grupos/mdlCrud.html.twig', $aJson);

        }
      }else{
        $aJson['status'] = 0;
        $aJson['message'] = 'Acción no habilitada, inicie sesión nuevamente he inténtelo otra vez.';

        $this->log->setLogAdmin("LG2 Formulario de Creación de Usuarios de Administrador, acción no habilitada");        
      }

    } else {
      $aJson['status'] = 0;
      $aJson['message'] = 'Acción no valida';

      $this->log->setLogAdmin("LG2 Formulario de Creación de Usuarios de Administrador, acción no valida");
    }


    $response->setContent(json_encode($aJson));
    return $response;
  }


    /**
    * Responde un JSON con un estado y mensaje de la petición de eliminación de un usuario
    *
    * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
    *
    * @return Symfony\Component\HttpFoundation\Response JSON
    */
  public function grupoDelete(Request $request): Response{

    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $aJson = array();

    if( $request->isXmlHttpRequest() ){

      $session = $request->getSession();
      $bAccesoAccion = $this->menuPermisos->getAccesoVistaAccion( $session, $this->sMenuModulo, $this->sModuloVista, 'eliminar-registro' );
      if($bAccesoAccion){

        $em = $this->getDoctrine()->getManager();

        $idGrupo = (int)$request->get('idGrupo');
        $emUser = $em->getRepository(Grupo::class)->findOneById($idGrupo);

        $em->remove($emUser);
        $em->flush();

        //cierre de conexion
        $em->getConnection()->close();

        $aJson['status'] = 1;
        $aJson['message'] = 'Grupo eliminado correctamente.';

        $this->log->setLogAdmin("LG4 Formulario de Grupos, eliminado correctamente");

      }else{ 
        $aJson['status'] = 0;
        $aJson['message'] = 'Acción no habilitada, inicie sesión nuevamente he inténtelo otra vez.';

        $this->log->setLogAdmin("LG4 Formulario de Grupos, acción no habilitada");
      }

    } else {
        $aJson['status'] = 0;
        $aJson['message'] = 'Acción no valida';

        $this->log->setLogAdmin("LG4 Formulario de Grupo, acción no valida");
    }

    $response->setContent(json_encode($aJson));
    return $response;
  }

    /**
    * Retorna la plantilla de edición de permisos del usuario
    *
    * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
    *
    * @return render('usuarios/mdlPermisos.html.twig') HTML TWIG
    */
    public function userPermisos(Request $request): Response{
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $aJson = array();

        if( $request->isXmlHttpRequest() ){

            $session = $request->getSession();
            $bAccesoAccion = $this->menuPermisos->getAccesoVistaAccion( $session, $this->sMenuModulo, $this->sModuloVista, 'modificar_permisos' );

            if($bAccesoAccion){

                $em = $this->getDoctrine()->getManager();
                if( !is_null($request->get('permisos')) ){

                    $aPermisos = $request->get('permisos');
                    $nIdUser = $request->get('user_id');
                    // Eliminar Permisos actuales
                    $bResult = $em->createQuery('DELETE App\Entity\MenuPermisos accp WHERE accp.idUser = '.$nIdUser)->getResult();
                    // Registrar los nuevos permisos
                    $sInsertPermisos = ' INSERT INTO menu_permisos (id_user, id_accion, permiso) VALUES ';
                    $sValuesInsertPermisos = '';

                    foreach( $aPermisos as $key => $permiso ){

                        if($sValuesInsertPermisos != '') $sValuesInsertPermisos .= ', ';
                        $sValuesInsertPermisos .= "({$nIdUser}, {$key}, {$permiso})";
                    }

                    $conn = $em->getConnection();
                    $oInsertar = $conn->prepare($sInsertPermisos.$sValuesInsertPermisos);
                    $oInsertar->execute();

                    $aJson['status'] = 1;
                    $aJson['message'] = 'Permisos actualizados correctamente.';

                    $this->log->setLogAdmin("LU6 Formulario de Usuarios de Administrador, permisos actualizados");

                }else{

                    // Objeto Usuario
                    $oUser = $em->getRepository(User::class)->findOneById($request->get("idUser"));
                    
                    //data
                    $aJson['idUser'] = $request->get("idUser");
                    $aJson['menuPermisos'] = $this->menuPermisos->getMenu( $oUser->getRoles()[0], $oUser->getId() )['menu'];

                    //cierre de conexion
                    $em->getConnection()->close();

                    $this->log->setLogAdmin("LU6 Formulario de Usuarios de Administrador, permisos vacio");
                    
                    return $this->render('usuarios/mdlPermisos.html.twig', $aJson);
                }
                //cierre de conexion
                $em->getConnection()->close();
            }else{
                $aJson['status'] = 0;
                $aJson['message'] = 'Acción no habilitada, inicie sesión nuevamente he inténtelo otra vez.';

                $this->log->setLogAdmin("LU6 Formulario de Usuarios de Administrador, acción no habilitada");
            }

        }else{
            $aJson['status'] = 0;
            $aJson['message'] = 'Acción no valida';

            $this->log->setLogAdmin("LU6 Formulario de Usuarios de Administrador, acción no valida");
        }

        $response->setContent(json_encode($aJson));
        return $response;
    }
        
    /** 
     * Genera archivo xls con los destinatarios asignados al grupo seleccionado.
     * 
     */
    public function downloadReport(Request $request){
        $session = $request->getSession();

        $grupoId=$request->get('grupoId');
        
        $excelService = New Spreadsheet();
        $activeSheet = $excelService->getActiveSheet();

        
        $em = $this->getDoctrine()->getManager();
        $emp = $this->getDoctrine()->getManager('contactosProveedor');
        //Hoja de estudiantes:
        $columna = "A";
        $fila=1;
        $activeSheet->getColumnDimension('A')->setWidth(10);
        $activeSheet->getColumnDimension('B')->setWidth(30);
        $activeSheet->getColumnDimension('C')->setWidth(30);
        $activeSheet->getColumnDimension('D')->setWidth(25);
        $activeSheet->getColumnDimension('E')->setWidth(25);
        $activeSheet->getColumnDimension('E')->setWidth(25);
        $activeSheet->getColumnDimension('F')->setWidth(25);
        
        /*$entitiesE = $em->createQuery("SELECT a.nit AS aNit, a.drogueria AS Drogueria, a.email AS aEmail, a.asociado AS aNombre, p.nit AS pNit, p.empresa AS Empresa, p.email1 AS pEmail, p.nombre AS pNombre, p.apellido AS pApellido, ig.id AS asignado 
                FROM WABackendBundle:IntegranteGrupo ig 
                LEFT JOIN ig.asociado a 
                LEFT JOIN ig.proveedor p
                WHERE ig.grupo=".$grupoId)->getResult();*/
        
        $entitiesE = $em->createQuery("SELECT a.nit AS aNit,a.codigo AS aCodigo, a.drogueria AS Drogueria, a.email AS aEmail, a.asociado AS aNombre, ig.id AS asignado, ig.proveedorId AS proveedorId,ig.contactoProveedorId AS contactoProveedorId
                FROM App\Entity\IntegranteGrupo ig 
                LEFT JOIN App\Entity\Cliente a WITH ig.asociadoId = a.id
                WHERE ig.grupo=".$grupoId)->getResult();
        
        $contactosProveedor = $emp->createQuery("SELECT p.nit, p.codigo, p.nombre, p.representanteLegal, p.emailRepresentanteLegal, c.nombreContacto, c.email, p.id,c.id AS contactoId, car.nombre AS cargoNombre
                FROM App\Entity\Proveedores p 
                LEFT JOIN App\Entity\Contactos c WITH c.idProveedor = p.id
                LEFT JOIN c.idCargo car ")->getResult();
        $contactosArray = array();
        foreach($contactosProveedor as $contactos){
            $contactosArray[$contactos['id']]['id'] = $contactos['id'];
            isset($contactosArray[$contactos['id']]['pNit']) ?$contactosArray[$contactos['id']]['pNit'] = $contactos['nit'] : "" ;
            $contactosArray[$contactos['id']]['Empresa'] = $contactos['nombre'];
            $contactosArray[$contactos['id']]['pEmail'] = $contactos['email'];
            $contactosArray[$contactos['id']]['pNombre'] = $contactos['nombreContacto'];
            $contactosArray[$contactos['contactoId']]['nombreContacto'] = $contactos['nombreContacto'];
            $contactosArray[$contactos['contactoId']]['email'] = $contactos['email'];
            $contactosArray[$contactos['contactoId']]['cargo'] = $contactos['cargoNombre'];
            //echo $contactos['contactoId'].' - '.$contactos['cargoNombre'];
        }//exit();
        $titulocolum='Nit';
        if ($request->get('clasificacion')=='Droguerías') {
            $titulocolum='Codigo';
        }
        $activeSheet->setCellValue($columna.$fila, $titulocolum);$columna++;
        if ($entitiesE[0]['Drogueria']) {
            $activeSheet->setCellValue($columna.$fila, 'Drogueria');$columna++;
        }else{
            $activeSheet->setCellValue($columna.$fila, 'Empresa');$columna++;
            $activeSheet->setCellValue($columna.$fila, 'Cargo');$columna++;
        }
        $activeSheet->setCellValue($columna.$fila, 'Nombre');$columna++;
        $activeSheet->setCellValue($columna.$fila, 'Email');$columna++;
        $FilaInicial=$fila;
        
        if ($entitiesE) {
            foreach($entitiesE as $p){
                $fila++;
                $columna = "A";
                $valcolum=$p['aNit'];
                if ($request->get('clasificacion')=='Droguerías') {
                    $valcolum=$p['aCodigo'];
                }
                if(isset($contactosArray[$p['proveedorId']]['pNit']))$activeSheet->setCellValue($columna.$fila, $p['aNit']? $valcolum : $contactosArray[$p['proveedorId']]['pNit'] );$columna++;
                if ($p['Drogueria']) {
                    $activeSheet->setCellValue($columna.$fila, $p['Drogueria']);$columna++;
                    $activeSheet->setCellValue($columna.$fila, $p['aNombre']);$columna++;
                    $activeSheet->setCellValue($columna.$fila, $p['aEmail']);$columna++;
                }else{
                    if(isset($contactosArray[$p['proveedorId']]['pNit']))$activeSheet->setCellValue($columna.$fila, $p['aNit']? $valcolum : $contactosArray[$p['proveedorId']]['pNit'] );$columna++;
                    if(isset($contactosArray[$p['proveedorId']]['Empresa']))$activeSheet->setCellValue($columna.$fila, $contactosArray[$p['proveedorId']]['Empresa']);$columna++;
                    if (isset($contactosArray[$p['contactoProveedorId']])) {
                        $cargo='';
                        if(isset($contactosArray[$p['contactoProveedorId']]['cargo'])){
                            $cargo=$contactosArray[$p['contactoProveedorId']]['cargo'];
                        }
                        $activeSheet->setCellValue($columna.$fila, $cargo);$columna++;
                        $nombreContacto='';
                        if(isset($contactosArray[$p['contactoProveedorId']]['nombreContacto'])){
                            $nombreContacto=$contactosArray[$p['contactoProveedorId']]['nombreContacto'];
                        }
                        $activeSheet->setCellValue($columna.$fila, $nombreContacto);$columna++;
                        $emailContacto='';
                        if(isset($contactosArray[$p['contactoProveedorId']]['email'])){
                            $emailContacto=$contactosArray[$p['contactoProveedorId']]['email'];
                        }
                        $activeSheet->setCellValue($columna.$fila, $emailContacto);$columna++;
                    }else{
                        $activeSheet->setCellValue($columna.$fila, '');$columna++;
                        !isset($contactosArray[$p['proveedorId']]['pNombre']) ?:$activeSheet->setCellValue($columna.$fila, $contactosArray[$p['proveedorId']]['pNombre']);$columna++;
                        !isset($contactosArray[$p['proveedorId']]['pEmail']) ?:$activeSheet->setCellValue($columna.$fila, $contactosArray[$p['proveedorId']]['pEmail'] );$columna++;
                    }
                }
            }
            $hoja=0;
            $activeSheet->getTabColor()->setRGB('00FF00');
            $excelService->getActiveSheet()->setTitle($entitiesE[0]['aEmail']? 'Asociado' : 'Proveedor');
        }

        $writer = new Xlsx($excelService);
        $fileName= "file.xls";
        
        $writer->save($fileName);
        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');

        // It will be called file.xls
        header('Content-Disposition: attachment; filename='.$fileName);
        // Write file to the browser
        $writer->save('php://output');

        exit();
    }
    

    public function asociadosIndex(Request $request){

        $sMenuModuloVista = "grupos_destinatarios";

        $session = $request->getSession();
        
        $dfColumnasAsociados = [
            ["headerClass" => "h6", 'headerName' => '#',       'field' => 'id', 'width' => 200, 'hide' => true],
            ["headerClass" => "h6", 'headerName' => 'Permisos',  'field' => 'esAsociado',    'width' => 100, 'cellClass' => ['text-center'],
            'cellRenderer' => 'agCellRender', 'cellRendererParams' => array(
                    'grilla' => 'default', 'columna'  => 'Asignado','aData' => [
                        'btnClass' => 'btn btn-outline-primary border-0 font-weight-bold btnGrillaPermisos',
                        'btnIconsClass' => 'fas fa-key'
                    ]
                )
            ],
            ["headerClass" => "h6", 'headerName' => 'Departamento',       'field' => 'depto', 'width' => 130],
            ["headerClass" => "h6", 'headerName' => 'Ciudad',       'field' => 'ciudad', 'width' => 150],
            ["headerClass" => "h6", 'headerName' => 'Centro',       'field' => 'centro', 'width' => 130],
            ["headerClass" => "h6", 'headerName' => 'Código',       'field' => 'codigo', 'width' => 130],
            ["headerClass" => "h6", 'headerName' => 'Ruta',       'field' => 'ruta', 'width' => 130],
            ["headerClass" => "h6", 'headerName' => 'Asociado',       'field' => 'asociado', 'width' => 200],
            ["headerClass" => "h6", 'headerName' => 'Nit',       'field' => 'nit', 'width' => 150], 
            ["headerClass" => "h6", 'headerName' => 'Email',       'field' => 'email', 'width' => 310]
        ];

        $this->log->setLogAdmin("LC1 Listado Clientes");
        return $this->render('grupos/destinatarios.html.twig', array(
          'dfColumnas' => json_encode($dfColumnasAsociados) ,
          'idGrupo' => $request->get("idGrupo")
        ));

    }


    public function asociadosIndexJson(Request $request, GrillaGlobal $grillaGlobal, $exportar = false){
        
        // Parámetros
        $idGrupo = $request->get("idGrupo");
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
        $response = new Response();

        // Validar Petición HTTP
        if(!$request->isXmlHttpRequest()){
        $response->setContent(json_encode([
            "status" => 0,
            "message" => "Acción no válida"
        ]));
        return $response;
        }

        // Grilla

        $aEquivalenciaColumnas = [
            "id" => "c.id",
            "depto" => "c.depto",
            "ciudad" => "c.ciudad",
            "centro" => "c.centro",
            "codigo" => "c.codigo",
            "ruta" => "c.ruta",
            "asociado" => "c.asociado",
            "nit" => "c.nit",
            "email" => "c.email",
            "esAsociado" => "ig.id"
        ];
        $aDataGrilla = $grillaGlobal->realizarFiltro($aEquivalenciaColumnas);

        // Total Registros
        
        $totalRegistros = 0;
        if ($aDataGrilla["paginaActual"] == 1 && $exportar === false)
        {
        $Contador = $em->createQuery("SELECT COUNT(c.id) AS totalRegistros
            FROM App\Entity\Cliente c 
            LEFT JOIN App\Entity\IntegranteGrupo ig WITH ig.asociadoId = c.id AND ig.grupo = {$idGrupo} 
            WHERE {$aDataGrilla["where"]} ");
        $Contador->setParameters($aDataGrilla["valoresWhere"]);
        $Contador = $Contador->getSingleResult();
        $totalRegistros = $Contador['totalRegistros'];
        }

        // DQL Query
        $queryClientes = $em->createQuery("SELECT c.id ,c.depto ,c.ciudad ,c.centro ,c.codigo ,c.ruta ,c.asociado ,c.nit ,c.email, ig.id as esAsociado
            FROM App\Entity\Cliente c 
            LEFT JOIN App\Entity\IntegranteGrupo ig WITH ig.asociadoId = c.id AND ig.grupo = {$idGrupo} 
            WHERE {$aDataGrilla["where"]} 
            ORDER BY {$aDataGrilla["order"]} ");
        $queryClientes->setParameters($aDataGrilla["valoresWhere"]);
        $aCLientes = $queryClientes->getScalarResult();

        // Resultado
        $em->getConnection()->close();
        $response->setContent(json_encode(['totalRows' => $totalRegistros, 'data' => $aCLientes]));
        return $response;
    }


    public function cargarDestinatarios(Request $request){

        $grupoId = $request->get("grupoId");
        if(isset($_FILES["form"])){
            $em = $this->getDoctrine()->getManager();

            $queryClientes = $em->createQuery( "SELECT c.id , ig.asociadoId as asociadoId 
                FROM App\Entity\Cliente c 
                LEFT JOIN App\Entity\IntegranteGrupo ig WITH ig.asociadoId = c.id AND ig.grupo = {$idGrupo} 
            ")->getScalarResult();

            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
            //    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            //    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xml();
            //    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Ods();
            //    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Slk();
            //    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Gnumeric();
            //    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
            /** Load $inputFileName to a Spreadsheet Object  **/
            $path = $this->getParameter('kernel.project_dir');
            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($_FILES["form"]["tmp_name"]["archivo"]);

            $worksheet = $spreadsheet->getActiveSheet()->toArray(null,true,true,true);
            $nitFromFile = [];

            foreach($worksheet as $key => $element){
                $nitFromFile[$element["A"]] = $element;
            }

            foreach($queryClientes as $key => $element ){
                if(isset($nitFromFile[$element["nit"]]));
            }

            dump($work1sheet);die;
            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter( $spreadsheet , "Xlsx" );
            $writer->save($_FILES["form"]["name"]["archivo"]);

            move_uploaded_file($_FILES["form"]["tmp_name"]["archivo"],$path."/public/".$_FILES["form"]["name"]["archivo"]);
            $spreadsheet = $reader->load($_FILES["form"]["name"]["archivo"]);
            
            dump($spreadsheet);die;

        }else{
            return $this->render('grupos/mdlCargarDestinatarios.html.twig', [
                "grupoId" => $grupoId
            ]);
        };
    }
    public function descargarFormatoAsociados(Request $request){

        $excelService = New Spreadsheet();
        $activeSheet = $excelService->getActiveSheet();
        $activeSheet->setCellValue("A1", 'Nit');
        $activeSheet->setCellValue("B1", 'Drogueria');
        $activeSheet->setCellValue("C1", 'Nombre');
        $activeSheet->setCellValue("D1", 'Email');
        $writer = new Xlsx($excelService);
        $fileName= "FormatoAsociados.xls";
        
        $writer->save($fileName);
        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');

        // It will be called file.xls
        header('Content-Disposition: attachment; filename='.$fileName);
        // Write file to the browser
        $writer->save('php://output');

        exit();

    }

    public function agregarTodosAsociados(Request $request){

        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $response = new Response();

        $grupoId = $request->get("grupoId");
        $queryClientes = $em->createQuery("SELECT c.id 
            FROM App\Entity\Cliente c 
            LEFT JOIN App\Entity\IntegranteGrupo ig WITH ig.asociadoId = c.id AND ig.grupo <> {$grupoId} ");
        $aCLientes = $queryClientes->getResult();

        if(sizeof($aCLientes) < 1){

            $resJson["status"] = 206;
            $resJson["message"] = "Grupo ya tiene todos los asociados";
        }

        $qIntegranteGrupoList = "";
        $qIntegranteGrupo="INSERT INTO integrante_grupo (asociado_id, proveedor_id, grupo_id, tipo_integrante_id, activo) VALUES ";

        foreach($aCLientes as $key => $element){
            
            if($qIntegranteGrupoList != "" ) $qIntegranteGrupoList.= ",";
            $qIntegranteGrupoList.= "({$element["id"]}, NULL, {$grupoId}, NULL, '1')" ;
            $element["id"];

            if($key % 200 == 0 && $key > 0){

                dump($qIntegranteGrupo.$qIntegranteGrupoList);
                dump("alto ahí");die;
                $conn->query($qIntegranteGrupo.$qIntegranteGrupoList);
                $qIntegranteGrupoList = "";
            }

        };
        if($qIntegranteGrupoList != ""){

            dump($qIntegranteGrupo.$qIntegranteGrupoList);
            dump("alto ahí");die;
            $conn->query($qIntegranteGrupo.$qIntegranteGrupoList);
        }

        $resJson["status"] = 200;
        $resJson["message"] = "Todos asociados agregados";

        $response->setContent(json_encode($resJson));
        return $response;

    }

    /**
     * Accion para eliminar los asociados a un grupo
     * 
     * @param object $request Objeto peticion de Symfony 5.1
     * @return array 
     * @return object json resultado de la accion nuevo
     * @since 4.2
     * @category AdministradorImagenes\usuarios
     */
    public function eliminarTodosAsociados(Request $request){

        $response = new Response();
        $grupoId = $request->get("grupoId");
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        
        dump("DELETE FROM integrante_grupo AS ig WHERE ig.grupo_id = {$grupoId} ");die;
        
        $queryClientes = $conn->query( "DELETE FROM App\Entity\IntegranteGrupo ig WHERE ig.grupo = {$grupoId} ");
        $resJson["status"] = 200;
        $resJson["message"] = "Asociados Eliminados";

        $response->setContent(json_encode($resJson));
        return $response;
    }

}

