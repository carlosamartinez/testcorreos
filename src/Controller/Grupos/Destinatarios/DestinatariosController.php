<?php
namespace App\Controller\Grupos\Destinatarios;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

// Base Controller
use App\Controller\Grupos\DestinatariosBaseController;

// Services
use App\Services\Globales\GrillaGlobal;
use App\Services\Globales\MenuPermisos;
use App\Services\Logs;

//BUNDLES 
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

// entity
use App\Entity\Grupo;

/**
 * Controlador para visualizar los destinarios del grupo
 *
 * @author Anderson Barbosa <a.barbosa@waplicaciones.co>
 */
class DestinatariosController extends DestinatariosBaseController
{
  private $sMenuModulo = 'grupos';
  private $sModuloVista = 'destinatarios_lista';
  private $idGrupo;

  /**
   * Genera interfaz de los detinatarios proveedores y asociados
   *
   * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
   *
   * @return render('clientes/index.html.twig') HTML
   */
  public function index(Request $request, $idGrupo)
  {
    $this->idGrupo = $idGrupo;

    // Parámetros
    $session = $request->getSession();
    $em =$this->getDoctrine()->getManager();

    // Validamos Grupo
    $oGrupo = $em->find("App\Entity\Grupo", $idGrupo);
    if(!$oGrupo){
      $this->addFlash('error', $this->getMsgBadGrupo("El grupo ". $idGrupo ." no existe."));
      return $this->redirect($this->generateUrl('admin_grupo'));
    }

    // Validar si el grupo pertenece al usuario
    if(!$this->checkIdGrupo($idGrupo)){
      $this->addFlash('error', $this->getMsgBadGrupo($idGrupo));
      return $this->redirect($this->generateUrl('admin_grupo'));
    }

    $infoGrupo = $em->getRepository('App\Entity\Grupo')->findOneById($idGrupo);

    if($infoGrupo->getClasificacion()=='Asociados'){
      $tipoGrilla= 1;
    }elseif($infoGrupo->getClasificacion()=='No Aplica'){
      $tipoGrilla= 2;
    }else{
      $tipoGrilla= 0;
    }

    // Success
    $this->log->setLogAdmin("GD1 Listado Grupos Destinatarios");
    return $this->render('grupos/destinatarios.html.twig', array(
      'idGrupo' => $idGrupo,
      'infoGrupo' => $infoGrupo,
      'tipoGrupo' => (($infoGrupo->getClasificacion()=='Asociados')?1:0),
      'tipoGrilla' => $tipoGrilla,
      'infoProveedores' => $this->getIndexParametrosProveedores(),
      'infoAsociados' => $this->getIndexParametrosAsociados()
    )); 
  }


  /**
   * Obtiene información de la grilla para proveedores
   */
  private function getIndexParametrosProveedores(){
    $params = ['idGrupo' => $this->idGrupo];
    return [
      "index" => [ 
        "urlAsignar" => $this->generateUrl('admin_grupo_destinatarios_proveedores_asignarProveedor', $params),
        "urlAsignarTodos" => $this->generateUrl('admin_grupo_destinatarios_proveedores_asignarTodos', $params),
        "urlBorrarTodos" => $this->generateUrl('admin_grupo_destinatarios_proveedores_eliminarTodos', $params),
        "urlCargarDestinatariosHtml" => $this->generateUrl('admin_grupo_destinatarios_proveedores_cargarDestinatarios', $params),
        "urlJson" => $this->generateUrl('admin_grupo_destinatarios_proveedores_json', $params),
        "columnas" => [
          ['headerName' => '#',       'field' => 'id', 'hide' => true], 
          [
            'headerName' => '',
            'field' => 'expand',
            'maxWidth' => 40, 
            'cellClass' => 'grid-cell-checkbox-detail', 
            'cellRenderer' => 'agGroupCellRenderer',
            'sortable' => false, 
            'resizable' => false
          ],
          [
            'headerName' => 'Asig.', 
            'field' => 'asignado',
            'maxWidth' => 55 , 
            'sortable' => false, 
            'resizable' => false,
            'cellRenderer' => 'asignadoCellRenderer',
          ],
          ['headerName' => 'Nit',     'field' => 'nit', 'width' => 40], 
          ['headerName' => 'Código',  'field' => 'codigo', 'width' => 30], 
          ['headerName' => 'Empresa', 'field' => 'nombre'], 
          ['headerName' => 'Nombre',  'field' => 'representanteLegal'],
          ['headerName' => 'Email',  'field' => 'emailRepresentanteLegal', 'width' => 120], 
        ],
        "buttons" => []
      ],
      "contactos" => [
        "columnas" => [
          ['headerName' => '#',       'field' => 'id', 'hide' => true], 
          [
            'headerName' => 'Asig.', 
            'field' => 'asignado',
            'maxWidth' => 55, 
            'sortable' => false, 
            'resizable' => false,
            'cellRenderer' => 'asignadoCellRenderer',
          ],
          ['headerName' => 'Nombre',  'field' => 'nombreContacto'], 
          ['headerName' => 'Ciudad',  'field' => 'ciudad'], 
          ['headerName' => 'Email',   'field' => 'email'], 
          ['headerName' => 'Teléfono','field' => 'telefono'], 
          ['headerName' => 'Móvil',   'field' => 'movil']
        ],
      ]
    ];
  }

  /**
   * Obtiene información de la grilla para asociados
   */
  private function getIndexParametrosAsociados(){
    return [
      "index" => [
        "urlJson" => $this->generateUrl('admin_grupo_destinatarios_asociados_json', ['idGrupo' => $this->idGrupo]),
        "columnas" => [
          ['headerName' => '#',       'field' => 'id', 'width' => 200, 'hide' => true],
          [
            'headerName' => 'Asig.',
            'field' => 'esAsociado',
            'width' => 55,
            'cellClass' => ['text-center'],
            'cellRenderer' => 'agCellRender',
            'cellRendererParams' => [
              'grilla' => 'default',
              'columna'  => 'Asignado',
              'aData' => [
                'btnClass' => 'btn btn-outline-primary border-0 font-weight-bold btnGrillaPermisos',
                'btnIconsClass' => 'fas fa-key'
              ]
            ]
          ],
          ['headerName' => 'Departamento',       'field' => 'depto', 'width' => 130],
          ['headerName' => 'Ciudad',       'field' => 'ciudad', 'width' => 150],
          ['headerName' => 'Centro',       'field' => 'centro', 'width' => 130],
          ['headerName' => 'Código',       'field' => 'codigo', 'width' => 130],
          ['headerName' => 'Ruta',       'field' => 'ruta', 'width' => 130],
          ['headerName' => 'Asociado',       'field' => 'asociado', 'width' => 250],
          ['headerName' => 'Nit',       'field' => 'nit', 'width' => 150], 
          ['headerName' => 'Email',       'field' => 'email', 'width' => 310]
        ],
        "buttons" => []
      ],
    ];
  }
  /**
     * Lists all Grupo entities.
     *
     */
    public function procesarArchivoActionProveedor_ant(Request $request){
      $session = $request->getSession();
      /*$permisos = $session->get('permisos');
      $auditoria = $this->get('auditoria');
      if(!$auditoria->seguridad_http('grupo',3,$permisos))
          return $this->redirect($this->generateUrl('admin_login'));*/
          
      $response = new Response();
      $resp = array();
      
      if($request->getMethod() == 'POST'){
        
              $idGrupo = $request->get('grupoId');
              $em = $this->getDoctrine()->getManager();
              $conn = $em->getConnection();

              $connp = $this->getDoctrine()->getManager('contactosProveedor')->getConnection(); 
              $errores_proveedor_email=array();

              $errores_asociados = array();
              $files=$request->files->get('form');

              if($files['archivo']){
                  $file = $files['archivo'];

                  $file->move('./uploads/',$file->getClientOriginalName());
                  $filename = './uploads/'.$file->getClientOriginalName();

                  $excelObj = \PhpOffice\PhpSpreadsheet\IOFactory::load($filename);

                  $sheetData = $excelObj->getActiveSheet()->toArray(null,true,true,true);

                  $update="UPDATE integrante_grupo SET activo = '0' WHERE grupo_id = ".$idGrupo;

                  $idsCliente = array();
                  $idsProveedorContacto = array();

                  $idsProveedor = $idsProveedorC = array();
                  $grupo = $em->getRepository(Grupo::class)->find($idGrupo);

                  $proveedoresArray=array();
                  $contadorCont=$contadorProve=$contadorActuCont=$contadorActuProve=0;

                  $sqlProveedores = "SELECT c.id, p.id AS idProveedor, p.nit, p.codigo, p.email_representante_legal, c.email FROM proveedores p JOIN contactos c ON c.id_proveedor = p.id ";
                  $proveedoresEntitie=$connp->query($sqlProveedores)->fetchAll();

                  $tipoResgistroProveedor=($request->get('tipoRegistro')=='Email')?'email':'id';
                  foreach($proveedoresEntitie as $proveedorEntitie){
                      $proveedoresArray[$proveedorEntitie[$tipoResgistroProveedor]][0]=$proveedorEntitie['id'];
                      $proveedoresArray[$proveedorEntitie[$tipoResgistroProveedor]][1]=$proveedorEntitie['nit'];
                      $proveedoresArray[$proveedorEntitie[$tipoResgistroProveedor]][2]=$proveedorEntitie['codigo'];
                      $proveedoresArray[$proveedorEntitie[$tipoResgistroProveedor]][3]=$proveedorEntitie['email'];
                      $proveedoresArray[$proveedorEntitie[$tipoResgistroProveedor]][4]=$proveedorEntitie['idProveedor'];
                  }
                  $destinatarioGrupoAArray=array();
                  $destinatarioGrupoPArray=array();

                  /********SE ELIMINAN LOS INTEGRANTES DEL GRUPO ANTES DE INSERTAR LOS NUEVOS DEL ARCHIVO*********/

                  $sqlIntegrantes = "SELECT id, contacto_proveedor_id, proveedor_id FROM integrante_grupo WHERE grupo_id=".$idGrupo;
                  $datosIntegrantes = $conn->query($sqlIntegrantes);

                  $arrayRespaldoContacto = array();
                  $arrayRespaldoProveedor = array();

                  foreach($datosIntegrantes as $datIntegrante){
                      if(is_null($datIntegrante['contacto_proveedor_id'])){
                          $arrayRespaldoProveedor[$datIntegrante['proveedor_id']] =$datIntegrante['id'] ;
                      }else{
                          $arrayRespaldoContacto[$datIntegrante['proveedor_id']][$datIntegrante['contacto_proveedor_id']] = $datIntegrante['id'];
                      }
                  }

                  $eliminacionIntegrantes = "DELETE FROM integrante_grupo WHERE grupo_id=".$idGrupo;
                  $conn->query($eliminacionIntegrantes);

                  /********************/

                  //dump($sheetData);exit();
                  foreach($sheetData as $n_fila=>$data){         
                      if($n_fila > 1){
                          $proveedor_email =(isset($data['A']))?trim($data['A']):NULL;                            
                          if($proveedor_email != ''){
                              if($request->get('tipoRegistro')=='Email'){
                                      
                                  if (filter_var($proveedor_email, FILTER_VALIDATE_EMAIL)) {
                                      if(isset($proveedoresArray[$proveedor_email])){
                                          $idsProveedorContacto[] = $proveedoresArray[$proveedor_email][0];
                                          $idsProveedor[$proveedoresArray[$proveedor_email][0]] = $proveedoresArray[$proveedor_email][4];
                                          $idsProveedorC[] = $proveedoresArray[$proveedor_email][4];
                                      }
                                  }else{
                                      $errores_proveedor_email[] = $proveedor_email;
                                  }

                              }else{
                                  foreach ($proveedoresArray as $proveedorA){
                                      $provModificado = $proveedorA[1];
                                      $provModificado = explode("-", $provModificado);
                                      if (count($provModificado)>1) {
                                          $provModificado = $provModificado[0];
                                      }else{
                                          $provModificado = $proveedorA[1];
                                      }
                                      if(preg_replace('/\D/', '', $provModificado)==$proveedor_email){
                                          $idsProveedorContacto[] = $proveedorA[0];
                                          $idsProveedor[$proveedorA[0]] = $proveedorA[4];
                                          $idsProveedorC[] = $proveedorA[4];
                                      }

                                  }

                                  $sqlProveedores = "SELECT p.id FROM proveedores p WHERE p.nit LIKE '%".$proveedor_email."%' GROUP BY p.codigo ";
                                  $idproveedoresEntitie=$connp->query($sqlProveedores)->fetchAll();

                                  foreach ($idproveedoresEntitie as $value) {
                                      $idsProveedorC[] = $value['id'];
                                  }
                              }
                          }
                      }
                  }
                  $insertados= [];
                  $actualizados= [];
                  //** Recorro e inserto los integrantes de tipo sociados **//
                  foreach (array_unique($idsProveedorC) as $id) {
                      if(isset($destinatarioGrupoPArray[$id]['proveedor'])){
                          $update="UPDATE integrante_grupo SET activo = '1' WHERE id = ".$destinatarioGrupoPArray[$id]['proveedor'];
                          $actualizados[$destinatarioGrupoPArray[$id]['proveedor']] = $destinatarioGrupoPArray[$id]['proveedor'];
                          $conn->query($update);
                          $contadorActuProve++;
                          
                      }else{
                          if(isset( $arrayRespaldoProveedor[$id] )){

                              $insert="INSERT INTO integrante_grupo (id, asociado_id, proveedor_id, contacto_proveedor_id, grupo_id, tipo_integrante_id, activo) VALUES ('".$arrayRespaldoProveedor[$id]."' , NULL,'".$id."', NULL, '".$idGrupo."', NULL, '1')";
                              $insertados[$arrayRespaldoProveedor[$id]] = $arrayRespaldoProveedor[$id];

                          }else{

                              $insert="INSERT INTO integrante_grupo (asociado_id, proveedor_id, contacto_proveedor_id, grupo_id, tipo_integrante_id, activo) VALUES ( NULL,'".$id."', NULL, '".$idGrupo."', NULL, '1')";
                              $insertados[$id] = $id;

                          }
                          //$insert="INSERT INTO integrante_grupo (asociado_id, proveedor_id, contacto_proveedor_id, grupo_id, tipo_integrante_id, activo) VALUES ( NULL,'".$id."', NULL, '".$idGrupo."', NULL, '1')";
                          $conn->query($insert);
                          $contadorProve++;

                          $destinatarioGrupoPArray[$id]['proveedor']=$id;
                      }
                  }
                  foreach ($idsProveedorContacto as $id) {
                      if(isset($destinatarioGrupoPArray[$id]['contacto'])){
                          $update="UPDATE integrante_grupo SET activo = '1' WHERE id = ".$destinatarioGrupoPArray[$id]['contacto'];
                          $actualizados[$destinatarioGrupoPArray[$id]['contacto']] = $destinatarioGrupoPArray[$id]['contacto'];
                          $conn->query($update);
                          $contadorActuCont++;
                      }else{

                          if(isset($arrayRespaldoContacto[ $idsProveedor[$id] ][ $id ] ) ){
                              $insert="INSERT INTO integrante_grupo (id, asociado_id, proveedor_id, contacto_proveedor_id, grupo_id, tipo_integrante_id, activo) VALUES ( '".$arrayRespaldoContacto[ $idsProveedor[$id] ][ $id ]."', NULL,'". $idsProveedor[$id] ."', '".$id."', '".$idGrupo."', NULL, '1')";
                              $insertados[$arrayRespaldoContacto[ $idsProveedor[$id] ][ $id ]] = $arrayRespaldoContacto[ $idsProveedor[$id] ][ $id ];

                          }else{
                              $insert="INSERT INTO integrante_grupo (asociado_id, proveedor_id, contacto_proveedor_id, grupo_id, tipo_integrante_id, activo) VALUES ( NULL,'". $idsProveedor[$id] ."', '".$id."', '".$idGrupo."', NULL, '1')";
                              $insertados[$idsProveedor[$id]] = $idsProveedor[$id];

                          }
                          //$insert="INSERT INTO integrante_grupo (asociado_id, proveedor_id, contacto_proveedor_id, grupo_id, tipo_integrante_id, activo) VALUES ( NULL,'". $idsProveedor[$id] ."', '".$id."', '".$idGrupo."', NULL, '1')";
                          $conn->query($insert);
                          $contadorCont++;

                          $destinatarioGrupoPArray[$id]['contacto']=$id;
                      }
                  }
              }
              //Respuesta
              $resp['status'] = 1;
              $resp['actualizados'] = $actualizados;
              $resp['insertados'] = $insertados;
              if($errores_proveedor_email){
                  $resp['errores_proveedor_email'] = implode('|', $errores_proveedor_email);
              }
              if($errores_asociados){
                  $resp['erroresAsociados'] = implode('|', $errores_asociados);
              }
              
              
              $Contador = $em->createQuery("SELECT COUNT(ig.id) AS contador FROM App\Entity\IntegranteGrupo ig WHERE ig.grupo=".$idGrupo)->getSingleResult();
              $numRegistros = $Contador['contador'];

              if($grupo) {
                  $grupo->setIntegrantes($numRegistros);
                  $em->persist($grupo);
                  $em->flush();
              }
              
      }
      if(is_file($filename)){
          unlink($filename);
      }
      $response->setContent(json_encode($resp));
      return $response;
  }



  public function previsualizarArchivoProveedores(Request $request){
      $session = $request->getSession();
          
      $response = new Response();
      $resp = array();
      
      if($request->getMethod() == 'POST'){
        
              $idGrupo = $request->get('grupoId');
              $em = $this->getDoctrine()->getManager();
              $emp = $this->getDoctrine()->getManager('contactosProveedor');
              $conn = $em->getConnection();
              $connp = $emp->getConnection(); 

              $errores_proveedor_email=array();

              $errores_asociados = array();
              $files=$request->files->get('form');

              if($files['archivo']){
                  $file = $files['archivo'];

                  $file->move('./uploads/',$file->getClientOriginalName());
                  $filename = './uploads/'.$file->getClientOriginalName();

                  $excelObj = \PhpOffice\PhpSpreadsheet\IOFactory::load($filename);

                  $sheetData = $excelObj->getActiveSheet()->toArray(null,true,true,true);

                  $tipoResgistroProveedor=($request->get('tipoRegistro')=='Email')?'email':'id';

                  // ----- se traen los integrantes previos
                  $integrantesPrevios = $em->createQuery("SELECT ig.id, ig.proveedorId, ig.contactoProveedorId 
                    FROM App\Entity\IntegranteGrupo ig 
                    WHERE ig.grupo=".$idGrupo." AND ig.activo =1")->getResult();


                  $arrayIntegrantesPrevios = array();
                  foreach($integrantesPrevios as $integrante){

                    if($integrante['contactoProveedorId']){

                      $sql = "SELECT p.id, p.nit, p.nombre, p.email_representante_legal AS email, c.nombre_contacto, c.email AS emailContacto 
                      FROM proveedores p 
                      LEFT JOIN contactos c ON p.id = c.id_proveedor 
                      WHERE p.id =".$integrante['proveedorId']." AND c.id =".$integrante['contactoProveedorId'];
                      
                      $datosProveedores = $connp->query($sql)->fetchAll();

                      foreach ($datosProveedores as $key => $datosProveedor) {
                        
                        if (!filter_var($datosProveedor['emailContacto'], FILTER_VALIDATE_EMAIL))continue;
                        $arrayIntegrantesPrevios[] = array(
                          'idIG' => $integrante['id'],
                          'idContacto' => $integrante['contactoProveedorId'],
                          'nombre' => $datosProveedor['nombre_contacto'],
                          'email' => $datosProveedor['emailContacto'],
                          'proveedor' => $datosProveedor['nombre'],
                          'idProveedor' => $datosProveedor['id'],
                        );
                      }

                      

                    }else{


                    }

                  }
                  // -------- fin integrantes previos

                  //--- integrantes del archivo

                  $arrayIntegrantesArchivo = array();
                  $arrayNoEncontrados = array();
                  foreach($sheetData as $n_fila=>$data){
                    if($n_fila > 1){

                      $emailNit = (isset($data['A']))?trim($data['A']):NULL;
                      if($emailNit != ''){

                        if($request->get('tipoRegistro')=='Email'){
                          $where = " c.email ='".$emailNit."'";
                        }else{
                          $where = " p.nit LIKE '%".$emailNit."%' ";
                        }


                        $sql = "SELECT p.id, p.nit, p.nombre, p.email_representante_legal AS email,c.id AS idContacto, c.nombre_contacto, c.email AS emailContacto 
                        FROM proveedores p 
                        LEFT JOIN contactos c ON p.id = c.id_proveedor 
                        WHERE ".$where;
                        
                        $datosProveedor = $connp->query($sql)->fetchAll();

                        $encontrado = false;
                        foreach($datosProveedor as $dp){
                          $encontrado = true;
                          $arrayIntegrantesArchivo[] = array(
                            'nombre' => $dp['nombre_contacto'],
                            'email' => $dp['emailContacto'],
                            'proveedor' => $dp['nombre'],
                            'idContacto' => $dp['idContacto'],
                            'idProveedor' => $dp['id'],
                          );

                        }

                        if($encontrado == false){

                          $arrayNoEncontrados[] = array(
                            'fila'=>$n_fila,
                            'dato'=>$emailNit,
                            'motivo'=>'Contacto no encontrado',
                          );

                        }

                      }  
                      
                    }

                  }

              }
              //Respuesta
              $resp['status'] = 1;
              $resp['integrantesPrevios'] = $arrayIntegrantesPrevios;
              $resp['integrantesArchivo'] = $arrayIntegrantesArchivo;
              $resp['noEncontrados'] = $arrayNoEncontrados;
              
      }
      if(is_file($filename)){
          unlink($filename);
      }
      $response->setContent(json_encode($resp));
      return $response;
  }


  public function procesarArchivoActionProveedor(Request $request){
      $session = $request->getSession();
          
      $response = new Response();
      $resp = array();
      
      $idGrupo = $request->get('grupoId');
      $em = $this->getDoctrine()->getManager();
      $conn = $em->getConnection();

      $connp = $this->getDoctrine()->getManager('contactosProveedor')->getConnection(); 
      $errores_proveedor_email=array();

      $errores_asociados = array();

      $contactosInsertar = $request->get('resultadoCargue');
      $contactosInsertar = json_decode($contactosInsertar, true);


      // ----- se traen los integrantes previos
      $integrantesPrevios = $em->createQuery("SELECT ig.id, ig.proveedorId, ig.contactoProveedorId 
        FROM App\Entity\IntegranteGrupo ig 
        WHERE ig.grupo=".$idGrupo)->getResult();


      $arrayIntegrantesPrevios = array();
      foreach($integrantesPrevios as $integrante){

        $arrayIntegrantesPrevios[$integrante['proveedorId']][$integrante['contactoProveedorId']] = $integrante['id'];

      }
      // -------- fin integrantes previos


      $cont =0;
      $sqlInsert = "INSERT INTO integrante_grupo (proveedor_id, contacto_proveedor_id, grupo_id, activo) VALUES ";
      $values = "";
      foreach($contactosInsertar as $contacto){

        if(isset( $arrayIntegrantesPrevios[$contacto['idProveedor']][$contacto['idContacto']] )){

          //actualizamos
          $sqlUpdate = "UPDATE integrante_grupo SET activo = 1 WHERE id=".$arrayIntegrantesPrevios[$contacto['idProveedor']][$contacto['idContacto']];

        }else{

          //insertamos
          
          if($values != ""){
            $values .= ", ";
          }
          $idContacto = $contacto['idContacto'] == "" ? "NULL" : " '".$contacto['idContacto']."' ";
          $values .= "('".$contacto['idProveedor']."',".$idContacto.",'".$idGrupo."','1')";

          $cont++;

          if($cont > 100){
            $conn->query($sqlInsert.$values);
            $cont =0;
            $values = "";
          }

        }

      }

      if($values != ""){
        $conn->query($sqlInsert.$values);
        $cont =0;
        $values = "";
      }

      $Contador = $em->createQuery("SELECT COUNT(ig.id) AS contador FROM App\Entity\IntegranteGrupo ig WHERE ig.grupo=".$idGrupo)->getSingleResult();
      $numRegistros = $Contador['contador'];

      $sqlUpdateGrupo = "UPDATE grupo SET integrantes='".$numRegistros."' WHERE id=".$idGrupo;

      $conn->query($sqlUpdateGrupo);


      $resp['status'] = 1;


      $response->setContent(json_encode($resp));
      return $response;
  }

}