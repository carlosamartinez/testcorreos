<?php
namespace App\Controller\Grupos\Destinatarios;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

// Base Controller
use App\Controller\Grupos\DestinatariosBaseController;

// Services
use App\Services\Globales\GrillaGlobal;
use App\Services\Globales\MenuPermisos;
use App\Services\Logs;

// Entity
use App\Entity\IntegranteGrupo;
use App\Entity\Proveedores;
use App\Entity\Contactos;

/**
 * Controlador para gestionar la lógica de negocio en destinatarios proveedores
 *
 * @author Anderson Barbosa <a.barbosa@waplicaciones.co>
 */
class DestinatariosProveedoresController extends DestinatariosBaseController
{

  public function proveedoresJson(Request $request, GrillaGlobal $grillaGlobal, int $idGrupo, $exportar = false) : Response{
    // Parámetros
    $emCp = $this->getDoctrine()->getManager('contactosProveedor');
    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');

    // Validar Petición HTTP y Grupo
    if($validateResponse = $this->checkHttpRequest($request, $idGrupo ) && !$exportar){
      return $validateResponse;
    }

    // Grilla
    $aEquivalenciaColumnas = ["id" => "p.id", "expand" => "''", "nit" => "p.nit", "codigo" => "p.codigo", "nombre" => "p.nombre", "representanteLegal" => "p.representanteLegal", "emailRepresentanteLegal" => "p.emailRepresentanteLegal"];
    $aDataGrilla = $grillaGlobal->realizarFiltro($aEquivalenciaColumnas);

    // Total Registros
    $totalRegistros = 0;
    if ($aDataGrilla["paginaActual"] == 1)
    {
      $Contador = $emCp->createQuery("SELECT COUNT(p.id) AS totalRegistros
        FROM App\Entity\Proveedores as p
        WHERE {$aDataGrilla["where"]} ");
      $Contador->setParameters($aDataGrilla["valoresWhere"]);
      $Contador = $Contador->getSingleResult();
      $totalRegistros = $Contador['totalRegistros'];
    }

    // DQL Query
    $queryProveedores = $emCp->createQuery("SELECT {$aDataGrilla["columnas"]}
      FROM App\Entity\Proveedores as p
      WHERE {$aDataGrilla["where"]} 
      ORDER BY {$aDataGrilla["order"]} ");
    $queryProveedores->setParameters($aDataGrilla["valoresWhere"]);
    if(!$exportar){
      $queryProveedores->setMaxResults($aDataGrilla["maximoFilas"]);
      $queryProveedores->setFirstResult($aDataGrilla["paginacion"]);
    }
    $aProveedores = $queryProveedores->getScalarResult();

    // Lógica de negocio
    foreach($aProveedores as &$aProveedor){
      // Ruta Contactos
      $aProveedor["urlContactos"] = $this->generateUrl('admin_grupo_destinatarios_proveedores_contactos_json', [
        'idGrupo' => $idGrupo, 
        'idProveedor' => $aProveedor["id"]
      ]);

      // Valida si algúno de los contactos ha sido asignado
      $aIntegrantes = $this->getIntegrantes($idGrupo, $aProveedor["id"]);
      $aProveedor["asignado"] = count($aIntegrantes) > 0;
    }

    // Resultado
    $emCp->getConnection()->close();
    $response->setContent(json_encode(['totalRows' => $totalRegistros, 'data' => $aProveedores]));
    return $response;
  }

  public function proveedoresContactosJson(Request $request, GrillaGlobal $grillaGlobal, int $idGrupo, int $idProveedor) : Response{
    // Parámetros
    $emCp = $this->getDoctrine()->getManager('contactosProveedor');
    
    // Validar Petición HTTP y Grupo
    if($validateResponse = $this->checkHttpRequest($request, $idGrupo)){
      return $validateResponse;
    }

    // Grilla
    $sFiltro = "c.idProveedor = :idProveedor";
    $aEquivalenciaColumnas = [
      "id" => "c.id", 
      "nombreContacto" => "c.nombreContacto", 
      "ciudad" => "c.ciudad", 
      "email" => "c.email", 
      "telefono" => "c.telefono", 
      "movil" => "c.movil"
    ];

    $aDataGrilla = $grillaGlobal->realizarFiltro($aEquivalenciaColumnas, $sFiltro);
    $aDataGrilla["valoresWhere"]["idProveedor"] = $idProveedor;

    // Total Registros
    $totalRegistros = 0;
    if ($aDataGrilla["paginaActual"] == 1)
    {
      $Contador = $emCp->createQuery("SELECT COUNT(c.id) AS totalRegistros
        FROM App\Entity\Contactos as c
        WHERE {$aDataGrilla["where"]} ");
      $Contador->setParameters($aDataGrilla["valoresWhere"]);
      $Contador = $Contador->getSingleResult();
      $totalRegistros = $Contador['totalRegistros'];
    }

    // DQL Query
    $queryContactos = $emCp->createQuery("SELECT {$aDataGrilla["columnas"]}, IDENTITY(c.idProveedor) as idProveedor
      FROM App\Entity\Contactos as c
      WHERE {$aDataGrilla["where"]} 
      ORDER BY {$aDataGrilla["order"]} ");
    $queryContactos->setParameters($aDataGrilla["valoresWhere"]);
    $queryContactos->setMaxResults($aDataGrilla["maximoFilas"]);
    $queryContactos->setFirstResult($aDataGrilla["paginacion"]);
    $aContactosDql = $queryContactos->getScalarResult();

    // Integrantes (Asignados)
    $aIntegrantes = $this->getIntegrantes($idGrupo, $idProveedor);

    // Lógica de negocio, relaciona para validar si contacto es asignado con integrante
    $aContactos = array_map(function($aContacto) use ($aIntegrantes){
      $aContacto["asignado"] = in_array($aContacto["id"], $aIntegrantes);
      return $aContacto;
    }, $aContactosDql);
    
    // Resultado
    $emCp->getConnection()->close();
    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $response->setContent(json_encode(['totalRows' => $totalRegistros, 'data' => $aContactos]));
    return $response;
  }

  public function asignarProveedor(Request $request, int $idGrupo) : Response{
    // Parámetros
    $em = $this->getDoctrine()->getManager();
    $conn = $em->getConnection();

    // Variables
    $idProveedor = $request->get("idProveedor");
    $idContacto = $request->get("idContacto");

    // Validar Petición HTTP y Grupo
    if($validateResponse = $this->checkHttpRequest($request, $idGrupo)){
      return $validateResponse;
    }

    if($idContacto == ''){

      // Validamos si esta creado en integrantes
      $oIntegrantes = $em->getRepository(IntegranteGrupo::class)->findBy(["grupo" => $idGrupo, "proveedorId" => $idProveedor]);
      if(!$oIntegrantes){
        // Crear
        $responseAsignados = $this->asignarTodos($request, $idGrupo, $idProveedor);
        $newStatus = true;
      }else{
        
        //------ verificamos si el grupo esta en uso en un envio-----
        $envio = $em->getRepository("App\Entity\Envio")->findOneBy(array('grupo' => $idGrupo, 'estado' => 1));

        if($envio){
          $newStatus = true;
          $aRespuesta = ['status' => 0, 'message' => 'No es posible eliminar el integrante ya que se encuentra activo en un envío '];

          $response = new Response();
          $responseAsignados = $response->setContent(json_encode($aRespuesta));
        }else{
          // Eliminar
          $responseAsignados = $this->eliminarTodos($request, $idGrupo, $idProveedor);
          $newStatus = false;
        }

      }

    }else{

      if($request->get('accion') == 'insertar'){

        $integrante = $em->getRepository(IntegranteGrupo::class)->findOneBy(array('grupo'=>$idGrupo, 'proveedorId'=>$idProveedor, 'contactoProveedorId'=>$idContacto));

        if(!$integrante){
          $integrante = new IntegranteGrupo();
          $integrante->setGrupo($em->getReference("App\Entity\Grupo", $idGrupo));
          $integrante->setProveedorId($idProveedor);
          $integrante->setContactoProveedorId($idContacto);
        }

        $integrante->setActivo(1);
        
        $em->persist($integrante);
        $em->flush();

        $newStatus = true;
        $aRespuesta = ['status' => 1, 'message' => "Contacto se ha agregado con éxito al grupo."];


      }else{

        //------ verificamos si el grupo esta en uso en un envio-----
        $envio = $em->getRepository("App\Entity\Envio")->findOneBy(array('grupo' => $idGrupo, 'estado' => 1));

        if($envio){
          $newStatus = true;
          $aRespuesta = ['status' => 0, 'message' => 'No es posible eliminar el integrante ya que se encuentra activo en un envío '];

        }else{

          $integrante = $em->getRepository(IntegranteGrupo::class)->findOneBy(array('grupo'=>$idGrupo, 'proveedorId'=>$idProveedor, 'contactoProveedorId'=>$idContacto));

          if($integrante){

            $integrante->setActivo(0);
            $em->persist($integrante);
            //$em->remove($integrante);
            $em->flush();
          }

          $newStatus = false;
          $aRespuesta = ['status' => 1, 'message' => "Contacto se ha eliminado con éxito del grupo."];

        }

        
    

      }

      //---- Se actualiza la cantidad de contactos 
      $Contador = $em->createQuery("SELECT COUNT(ig.id) AS contador FROM App\Entity\IntegranteGrupo ig WHERE ig.grupo=".$idGrupo)->getSingleResult();
      $numRegistros = $Contador['contador'];

      $sqlUpdateGrupo = "UPDATE grupo SET integrantes='".$numRegistros."' WHERE id=".$idGrupo;

      $conn->query($sqlUpdateGrupo);
      //----------

      $response = new Response();
      $response->headers->set('Content-Type', 'application/json');
      $response->setContent(json_encode(['status' => 1, 'newSatus' => $newStatus]));
      return $response;


    }


    //---- Se actualiza la cantidad de contactos 
    $Contador = $em->createQuery("SELECT COUNT(ig.id) AS contador FROM App\Entity\IntegranteGrupo ig WHERE ig.grupo=".$idGrupo)->getSingleResult();
    $numRegistros = $Contador['contador'];

    $sqlUpdateGrupo = "UPDATE grupo SET integrantes='".$numRegistros."' WHERE id=".$idGrupo;

    $conn->query($sqlUpdateGrupo);
    //----------

    
    $resultAsignado = json_decode($responseAsignados->getContent());
    if($resultAsignado->status === 0){
      //return $resultAsignado;
      $response->setContent($responseAsignados->getContent());
      return $response;
    }

    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $response->setContent(json_encode(['status' => 1, 'newSatus' => $newStatus]));
    return $response;
  }

  public function asignarProveedorContacto(Request $request, $idGrupo) : Response{
    // Parámetros
    $emCp = $this->getDoctrine()->getManager('contactosProveedor');
    
    // Validar Petición HTTP y Grupo
    if($validateResponse = $this->checkHttpRequest($request, $idGrupo)){
      return $validateResponse;
    }

    

    // Resultado
    $emCp->getConnection()->close();
    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $response->setContent(json_encode(['status' => 1, 'message' => "Los Proveedores se han agregado con éxito al grupo."]));
    return $response;
  }

  

  /**
   * Asigna a todos los contactos al grupo
   */
  public function asignarTodos(Request $request, $idGrupo, $idProveedor = null) : Response{

    set_time_limit(0);

    // Validar Petición HTTP y Grupo
    if($validateResponse = $this->checkHttpRequest($request, $idGrupo)){
      return $validateResponse;
    }

    // Eliminamos todos los registros para volver a crear
    /*$responseRemove = $this->eliminarTodos($request, $idGrupo, $idProveedor);
    $resultRemove = json_decode($responseRemove->getContent());
    if($resultRemove->status === 0){
      return $responseRemove;
    }*/

    // Parámetros
    $em = $this->getDoctrine()->getManager();
    $conn = $em->getConnection();
    $emCp = $this->getDoctrine()->getManager('contactosProveedor');
    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');

    $sqlIntegrantes = "SELECT ig.id, ig.proveedor_id, ig.contacto_proveedor_id FROM integrante_grupo ig WHERE ig.grupo_id='".$idGrupo."' AND ig.proveedor_id IS NOT NULL";
    $intg = $conn->query($sqlIntegrantes);

    $arrayIntegranteProv = array();
    foreach($intg as $intP){
      if($intP['contacto_proveedor_id']){
        $arrayIntegranteProv[$intP['proveedor_id']][$intP['contacto_proveedor_id']] = $intP['id'];
      }else{
        $arrayIntegranteProv[$intP['proveedor_id']]['proveedor_solo'] = $intP['id'];
      }
      
    }

    // Empezamos proceso
    
    try {
      // Grupo
      $oGrupo = $em->find("App\Entity\Grupo", $idGrupo);

      // Consultamos los proveedores
      if($idProveedor === null){
        $aProveedores = $emCp->createQuery("SELECT p.id FROM App\Entity\Proveedores as p")->getScalarResult();
        $aProveedoresIds = array_map(function($aProveedor){return $aProveedor["id"];}, $aProveedores);
      }else{
        $aProveedoresIds = [$idProveedor];
      }

      $sqlInsert = "INSERT INTO integrante_grupo (activo, grupo_id, proveedor_id, contacto_proveedor_id) VALUES ";
      $cont =0;
      $values ="";
      foreach($aProveedoresIds as $id){

        

        // Consultamos todos los contactos de los proveedores
        $qContactos = $emCp->createQuery("SELECT c.email ,c.id as contactoId, IDENTITY(c.idProveedor) as proveedorId FROM App\Entity\Contactos as c WHERE c.idProveedor =:idProveedor ");
        $qContactos->setParameter("idProveedor", $id);
        $aContactos = $qContactos->getResult();


        // Creamos nuevos registros
        foreach($aContactos as $aContacto){

          if(isset( $arrayIntegranteProv[$id][$aContacto["contactoId"]] )){

            $sqlUpdate = "UPDATE integrante_grupo SET activo=1 WHERE id=".$arrayIntegranteProv[$id][$aContacto["contactoId"]];
            $conn->query($sqlUpdate);

          }else{

            if (!filter_var($aContacto["email"], FILTER_VALIDATE_EMAIL))continue;

            if($values != ""){
              $values .= ", ";
            }

            $values.="('1','".$idGrupo."','".intval($aContacto["proveedorId"])."','".$aContacto["contactoId"]."')";

            $cont++;

          }

          

          

          
        }

        if(isset( $arrayIntegranteProv[$id]['proveedor_solo'] )){

          $sqlUpdate = "UPDATE integrante_grupo SET activo=1 WHERE id=".$arrayIntegranteProv[$id]['proveedor_solo'];
          $conn->query($sqlUpdate);

        }else{

          if($values != ""){
            $values .= ", ";
          }

          $values.="('1','".$idGrupo."','".intval($id)."',NULL)";

          $cont++;

          if($cont > 100){
            $conn->query($sqlInsert.$values);
            $values ="";
            $cont =0;
          }

        }        

      }

      if($values !=""){
        $conn->query($sqlInsert.$values);
        $values ="";
        $cont =0;
      }


      // Respuesta
      $aRespuesta = ['status' => 1, 'message' => "Los Proveedores se han agregado con éxito al grupo."];
    }catch( \Doctrine\DBAL\DBALException $e ){
      
      $response->setStatusCode(Response::HTTP_BAD_REQUEST);
      $aRespuesta = ['status' => 0, 'message' => 'Hubo un error: [0] '.$e->getMessage()];
    }catch (\Exception $e) {
      
      $response->setStatusCode(Response::HTTP_BAD_REQUEST);
      $aRespuesta = ['status' => 0, 'message' => 'Hubo un error: [1] '.$e->getMessage()];
    }

    //---- Se actualiza la cantidad de contactos 
    $Contador = $em->createQuery("SELECT COUNT(ig.id) AS contador FROM App\Entity\IntegranteGrupo ig WHERE ig.grupo=".$idGrupo)->getSingleResult();
    $numRegistros = $Contador['contador'];

    $sqlUpdateGrupo = "UPDATE grupo SET integrantes='".$numRegistros."' WHERE id=".$idGrupo;

    $conn->query($sqlUpdateGrupo);
    //----------

    // Resultado
    $emCp->getConnection()->close();
    $response->setContent(json_encode($aRespuesta));
    return $response;
  }

  /**
   * Evento para eliminar todos los asignados en el grupo
   */
  public function eliminarTodos(Request $request, $idGrupo, $idProveedor = null) : Response{
    // Parámetros
    $em = $this->getDoctrine()->getManager();
    $conn = $em->getConnection();
    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');

    // Validar Petición HTTP y Grupo
    if($validateResponse = $this->checkHttpRequest($request, $idGrupo)){
      return $validateResponse;
    }

    //------ verificamos si el grupo esta en uso en un envio-----
    $envio = $em->getRepository("App\Entity\Envio")->findOneBy(array('grupo' => $idGrupo, 'estado' => 1));

    if($envio){

      $aRespuesta = ['status' => 1, 'message' => 'No es posible eliminar el integrante ya que se encuentra activo en un envío '];

    }else{

      // Empezamos proceso
      $em->getConnection()->beginTransaction(); // suspend auto-commit
      try {
        // Filtro Proveedor
        $sWhereProveedor = ($idProveedor !== null) ? "i.proveedorId = :idProveedor" : "i.proveedorId IS NOT NULL";

        // Eliminamos todos los asignanos
        $queryDelete = $em->createQuery("DELETE FROM App\Entity\IntegranteGrupo as i WHERE i.grupo = :idGrupo AND {$sWhereProveedor} ");
        $queryDelete->setParameter("idGrupo", $idGrupo);
        if($idProveedor !== null)
          $queryDelete->setParameter("idProveedor", $idProveedor);
        $queryDelete->execute();

        // Ejecutamos Procesos
        $em->flush();
        $em->getConnection()->commit();

        // Respuesta
        $aRespuesta = ['status' => 1, 'message' => "Los Proveedores se han eliminado con éxito al grupo."];
      }catch( \Doctrine\DBAL\DBALException $e ){
        $em->getConnection()->rollBack();
        $response->setStatusCode(Response::HTTP_BAD_REQUEST);
        $aRespuesta = ['status' => 0, 'message' => 'Hubo un error: [0] '.$e->getMessage()];
      }catch (\Exception $e) {
        $em->getConnection()->rollBack();
        $response->setStatusCode(Response::HTTP_BAD_REQUEST);
        $aRespuesta = ['status' => 0, 'message' => 'Hubo un error: [1] '.$e->getMessage()];
      }

      //---- Se actualiza la cantidad de contactos 
      $Contador = $em->createQuery("SELECT COUNT(ig.id) AS contador FROM App\Entity\IntegranteGrupo ig WHERE ig.grupo=".$idGrupo)->getSingleResult();
      $numRegistros = $Contador['contador'];

      $sqlUpdateGrupo = "UPDATE grupo SET integrantes='".$numRegistros."' WHERE id=".$idGrupo;

      $conn->query($sqlUpdateGrupo);
      //----------

    }



    

    // Resultado
    $em->getConnection()->close();
    $response->setContent(json_encode($aRespuesta));
    return $response;
  }

  public function cargarDestinatariosHtml(Request $request, $idGrupo){
    // Parámetros
    $emCp = $this->getDoctrine()->getManager('contactosProveedor');
    
    // Validar Petición HTTP y Grupo
    if($validateResponse = $this->checkHttpRequest($request, $idGrupo)){
      return $validateResponse;
    }

    // Resultado
    $emCp->getConnection()->close();
    return $this->render('grupos/modals/proveedor-cargar-destinatarios.html.twig',array(
      "idGrupo" => $idGrupo
    ));
  }

  /**
   * Obtiene los integrantes(Asignados) del grupo y proveedor.
   *
   * @param int $idGrupo Id Grupo
   * @param int $idProveedor Id Proveedor
   * @return array
   */
  private function getIntegrantes(int $idGrupo, int $idProveedor){
    $em = $this->getDoctrine()->getManager();

    $queryIntegrantes = $em->createQuery("SELECT i.contactoProveedorId as asignado
      FROM App\Entity\IntegranteGrupo as i
      WHERE i.proveedorId = :idProveedor AND i.grupo = :idGrupo AND i.activo = 1");
    $queryIntegrantes->setParameters(["idProveedor" => $idProveedor, "idGrupo" => $idGrupo]);
    $aIntegrantesDql = $queryIntegrantes->getScalarResult();
    $aIntegrantes = array_map(function($aIntegrante){return $aIntegrante["asignado"];}, $aIntegrantesDql);
    return $aIntegrantes;
  }

  /**
  * Retorna un archivo CSV con el listado de proveedores
  *
  * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
  *
  * @return Symfony\Component\HttpFoundation\Response
  */
  public function destinatariosCsv(Request $request, GrillaGlobal $grillaGlobal): Response{

      // Variables
      $session = $request->getSession();
      $spreadsheet = new Spreadsheet();
      $sheet = $spreadsheet->getActiveSheet();
      $sheet->setCellValue('A1', 'Hello World !');

      $writer = new Xlsx($spreadsheet);

      $sFileCsv = "";

      $aDataGrilla = json_decode($this->proveedoresJson($request, $grillaGlobal, $request->get("idGrupo"), true)->getContent(), 1);

      $this->log->setLogAdmin("LP5 Exportacion de datos del Proveedor");
      $sheet->setCellValue('A1', 'NIT !');
      $sheet->setCellValue('B1', 'NOMBRE !');
      $sheet->setCellValue('C1', 'NOMBRE CONTACTO');
      $sheet->setCellValue('D1', 'EMAIL CONTACTO');

      $sFileCsv .= "\r\n";
      $em = $this->getDoctrine()->getManager();
      $queryintegranteGrupo = $em->createQuery(
        "SELECT g.id
         FROM App\Entity\IntegranteGrupo as ig 
         JOIN App\Entity\Grupo as g WITH g.id = ig.grupo 
         WHERE g.id = {$request->get("idGrupo")} AND ig.contactoProveedorId IS NULL
        "
      );
      $aIntegranteGrupo = [];
      foreach($queryintegranteGrupo as $element){
        $aIntegranteGrupo[$element["id"]] = $element;
      }

      $emCp = $this->getDoctrine()->getManager('contactosProveedor');

      $queryProveedoresCp = $emCp->createQuery(
        "SELECT cp, p.id as idProveedor 
         FROM App\Entity\Contactos as cp 
         JOIN App\Entity\Proveedores as p WITH p.id = cp.idProveedor
        "
      );
      $qProveedores = $queryProveedoresCp->getScalarResult();
      $aProveedor = [];
      foreach($qProveedores as $element){
        
        $aProveedor[$element["idProveedor"]][] = $element;
      }
      $count = 2 ;
      foreach( $aDataGrilla["data"] as $key => $aData ){
        if($aData["asignado"] == false)continue;
        $sheet->setCellValue('A'.$count , utf8_decode($aData['nit']));
        $sheet->setCellValue('B'.$count , utf8_decode($aData['nombre']));

        if( isset($aProveedor[$aData['id']]) ){
          foreach($aProveedor[$aData['id']] as $element){
            $count++;
            $sheet->setCellValue('C'.$count , $element["cp_nombreContacto"]);
            $sheet->setCellValue('D'.$count , $element["cp_email"]);
            
          };
          if( isset($aIntegranteGrupo[$aData['id']]) ){
            $sheet->setCellValue('C'.$count , $aData['representanteLegal']);
            $sheet->setCellValue('D'.$count , $aData["emailRepresentanteLegal"]);
          
          }
        }else{
          $count++;
            $sheet->setCellValue('C'.$count , $aData['representanteLegal']);
            $sheet->setCellValue('D'.$count , $aData["emailRepresentanteLegal"]);

        }
        $count++;
      }
      $fileName = "Proveedores.xls";

      $writer->save($fileName);
      // We'll be outputting an excel file
      header('Content-type: application/vnd.ms-excel');

      // It will be called file.xls
      header('Content-Disposition: attachment; filename=' . $fileName);
      // Write file to the browser
      $writer->save('php://output');

      exit();
    }
 
}