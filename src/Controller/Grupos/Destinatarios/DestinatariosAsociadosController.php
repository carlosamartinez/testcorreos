<?php
namespace App\Controller\Grupos\Destinatarios;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

// Services
use App\Services\Globales\GrillaGlobal;
use App\Services\Globales\MenuPermisos;
use App\Services\Logs;

/**
 * Controlador para gestionar la lógica de negocio en destinatarios asociados
 *
 * @author Anderson Barbosa <a.barbosa@waplicaciones.co>
 */
class DestinatariosAsociadosController extends AbstractController
{
  private $log;

  public function __construct(MenuPermisos $menuPermisos, Logs $log)
  {
    $this->log = $log;
  }

  public function asociadosJson(Request $request, GrillaGlobal $grillaGlobal, int $idGrupo, $exportar=false) : Response{
    // Parámetros
    //$idGrupo = $request->get("idGrupo");
    $session = $request->getSession();
    $em = $this->getDoctrine()->getManager();
    $response = new Response();

    // Validar Petición HTTP
    if(!$request->isXmlHttpRequest()){
    $response->setContent(json_encode([
        "status" => 0,
        "message" => "Acción no válida"
    ]));
    return $response;
    }

    // Grilla

    $aEquivalenciaColumnas = [
        "id" => "c.id",
        "depto" => "c.depto",
        "ciudad" => "c.ciudad",
        "centro" => "c.centro",
        "codigo" => "c.codigo",
        "ruta" => "c.ruta",
        "asociado" => "c.asociado",
        "nit" => "c.nit",
        "email" => ($request->get('tipoGrupo')==1)?"c.emailAsociado":"c.email",
        "esAsociado" => "ig.id"
    ];
    $aDataGrilla = $grillaGlobal->realizarFiltro($aEquivalenciaColumnas);

    // Total Registros
    
    $totalRegistros = 0;
    
    $Group=' GROUP BY c.codigo ';
    $distincy=' COUNT(DISTINCT c.codigo) ';

    if($request->get('tipoGrupo')==1){
      $Group=' GROUP BY c.nit ';
      $distincy=' COUNT(DISTINCT c.nit) ';
    }


    if ($aDataGrilla["paginaActual"] == 1 && $exportar === false)
    {
      $Contador = $em->createQuery("SELECT ".$distincy." AS totalRegistros
          FROM App\Entity\Cliente c 
          LEFT JOIN App\Entity\IntegranteGrupo ig WITH ig.asociadoId = c.id AND ig.activo=1 AND ig.grupo = {$idGrupo} 
          WHERE {$aDataGrilla["where"]} AND c.retirado = 0 ");
      $Contador->setParameters($aDataGrilla["valoresWhere"]);
      $Contador = $Contador->getSingleResult();
      $totalRegistros = $Contador['totalRegistros'];
    }

    // DQL Query
    $queryClientes = $em->createQuery("SELECT c.id ,c.depto ,c.ciudad ,c.centro ,c.codigo ,c.ruta ,c.asociado ,c.nit ,c.email, ig.id as esAsociado, c.emailAsociado
        FROM App\Entity\Cliente c 
        LEFT JOIN App\Entity\IntegranteGrupo ig WITH ig.asociadoId = c.id AND ig.activo=1 AND ig.grupo = {$idGrupo} 
        WHERE {$aDataGrilla["where"]} AND c.retirado = 0 {$Group} 
        ORDER BY {$aDataGrilla["order"]} ");
    $queryClientes->setParameters($aDataGrilla["valoresWhere"]);
    $aCLientes = $queryClientes->getScalarResult();

    $listadoClientes = array();
    foreach($aCLientes as $key => $cliente){
      $listadoClientes[$key]['id'] = $cliente['id'];
      $listadoClientes[$key]['depto'] = $cliente['depto'];
      $listadoClientes[$key]['ciudad'] = $cliente['ciudad'];
      $listadoClientes[$key]['centro'] = $cliente['centro'];
      $listadoClientes[$key]['codigo'] = $cliente['codigo'];
      $listadoClientes[$key]['ruta'] = $cliente['ruta'];
      $listadoClientes[$key]['asociado'] = $cliente['asociado'];
      $listadoClientes[$key]['nit'] = $cliente['nit'];
      $listadoClientes[$key]['esAsociado'] = $cliente['esAsociado'];
      if($request->get('tipoGrupo')==1){
        $listadoClientes[$key]['email'] = $cliente['emailAsociado'];
      }else{
        $listadoClientes[$key]['email'] = $cliente['email'];
      }
    }

    // Resultado
    $em->getConnection()->close();
    $response->setContent(json_encode(['totalRows' => $totalRegistros, 'data' => $listadoClientes]));
    return $response;
  }

  private function checkIdGrupo() : bool{

  }

}