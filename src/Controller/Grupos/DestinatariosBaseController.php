<?php
namespace App\Controller\Grupos;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

// Services
use App\Services\Globales\MenuPermisos;
use App\Services\Logs;

// Enitity
Use App\Entity\Grupo;

/**
 * Controlador base para destinatarios
 *
 * @author Anderson Barbosa <a.barbosa@waplicaciones.co>
 */
class DestinatariosBaseController extends AbstractController
{
  public $menuPermisos;
  public $log;
  private $security;

  public function __construct(Security $security, MenuPermisos $menuPermisos, Logs $log)
  {
    $this->security = $security;
    $this->menuPermisos = $menuPermisos;
    $this->log = $log;
  }

  /**
   * Válida si el grupo a consultar pertenece al usuario
   * @return validación
   */
  public function checkIdGrupo(int $idGrupo) : bool{
    // Parámetros
    $em = $this->getDoctrine()->getManager();
    $user = $this->security->getUser();

    // Validamos grupo
    $oGrupo = $em->getRepository(Grupo::class)->find($idGrupo);
    if(!$oGrupo)
      return false;
    
    // Si es "administrador" permite el ingreso, tipoUsuario:true => administrador
    if($user->getTipoUsuario())
      return true;
    
    // Validamos si el "usuario" pertenece a este grupo
    $grupoAdminId = $oGrupo->getAdministrador()->getid();
    return ($grupoAdminId == $user->getId());
  }

  public function getMsgBadGrupo(int $idGrupo) : string{
    return "El grupo \"{$idGrupo}\" no pertenece al usuario";
  }

  public function checkHttpRequest(Request $request, int $idGrupo) : ?Response{
    // Parámetros
    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');

    // Validar Petición HTTP
    if(!$request->isXmlHttpRequest()){
      $response->setContent(json_encode(["status" => 0, "message" => "Acción no válida"]));
      return $response;
    }
    
    // Validar Grupo
    if(!$this->checkIdGrupo($idGrupo)){
      $response->setStatusCode(Response::HTTP_BAD_REQUEST);
      $response->setContent(json_encode(['status' => 0, 'message' => $this->getMsgBadGrupo($idGrupo)]));
      return $response;
    }

    return null;
  }
}