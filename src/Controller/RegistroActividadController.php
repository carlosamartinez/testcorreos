<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
// Services
use App\Services\Globales\GrillaGlobal;
use App\Services\Globales\MenuPermisos;
use App\Services\Logs;

/**
* Controlador de registro de actividad de Correos
*
* @category LOGS

* @author Pablo J. T. Bravo <p.trivino@waplicaciones.co> && Julian Andres Restrepo <j.restrepo@waplicaciones.co>
*/
class RegistroActividadController extends AbstractController{

  private $menuPermisos;
  private $sMenuModulo = 'registro_actividad';
  private $sModuloVista = 'listado';
  private $log;
  public function __construct(MenuPermisos $menuPermisos, Logs $log){
    $this->menuPermisos = $menuPermisos;
    $this->log = $log;
  }

    /**
    * Responde un documento HTML con la lista actvidad del usuario administrador
    *
    * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
    * @param App\Services\Logs Servicio que registra la actividad de los usuarios
    *
    * @return render('registroActividad/index.html.twig') HTML
    */
  public function registroActividad(Request $request, Logs $log){

    // Variables
    $session = $request->getSession();
    $sListado = $request->get('listado');
    $this->sModuloVista = 'listado_admin';
    try{
      $this->menuPermisos->validarAccesoVista( $session, $this->sMenuModulo, $this->sModuloVista );
    } catch (\Throwable $th) {
      return $this->redirect($this->generateUrl('admin_login'));
    }
    $aPermisos = $this->menuPermisos->getPermisosModuloVista($session, $this->sMenuModulo, $this->sModuloVista);
    $aGridButtons = ( empty($aPermisos) ) ? array() : $this->menuPermisos->getGridButtons( $this->sMenuModulo,$session , $aPermisos );

      $dfColumnas = $this->definicionColumnasAdmin();
      $this->log->setLogAdmin("LRA1 Listado de Logs de Administrador");


    return $this->render('registroActividad/index.html.twig', [
      'dfColumnas' => json_encode($dfColumnas),
      'aGridButtons' => json_encode($aGridButtons["iconos"]),
      'listado' => $sListado
    ]);
  }

    /**
    * Responde un JSON con la información requerida para mostrar la tabla adecuadamente
    * 
    * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
    * @param App\Services\GrillaGlobal $grillaGlobal para realizar los filtros de busqueda
    * @param $exportar Type=bool Valida si se filtran datos necesarios para descargar un archivo .csv
    * 
    * @return JSON
    */
  public function registroActividadJson(Request $request, GrillaGlobal $grillaGlobal, $exportar = false){

    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $aJson = array();

    if( $request->isXmlHttpRequest() || $exportar == true ){

      $session = $request->getSession();
      $em = $this->getDoctrine()->getManager();
      $sListado = $request->get('listado');  

      $aEquivalenciaColumnas = [
        'id'          => 'ra.id',
        'userNombre'  => 'us.nombre',
        'fecha'       => 'ra.fechaIngreso',
        'actividad'   => 'ra.actividad',
        'ipUsuario'   => 'ra.ip'
      ];

      // Procedimiento Filtro Grilla
      $sIniFiltro = "";
      $aDataGrilla = $grillaGlobal->realizarFiltro($aEquivalenciaColumnas, $sIniFiltro);

      // --- --- --- --- Total Filas --- --- --- --- //
      $numeroRegistros = 0;
      if( $aDataGrilla["paginaActual"] == 1 && $exportar === false ){
        $Contador = $em->createQuery("SELECT COUNT(ra.id) AS numeroRegistros
          FROM App\Entity\LogAdministrador ra
          JOIN App\Entity\User us WITH us.id = ra.administradorId
          WHERE {$aDataGrilla["where"]} "
        );
        $Contador->setParameters($aDataGrilla["valoresWhere"]);
        $Contador = $Contador->getSingleResult();
        $numeroRegistros = $Contador['numeroRegistros'];
      }

      // DQL Data
      $queryRegistroActividad = $em->createQuery("SELECT ra.id, us.nombre, ra.fechaIngreso, ra.actividad, ra.ip, us.id AS idUsuario
        FROM App\Entity\LogAdministrador ra
        JOIN App\Entity\User us WITH us.id = ra.administradorId
        WHERE {$aDataGrilla["where"]} 
        ORDER BY {$aDataGrilla["order"]} ");

      // Resultado
      $queryRegistroActividad->setParameters($aDataGrilla["valoresWhere"]);

      if( $exportar === false ){
        $queryRegistroActividad->setMaxResults($aDataGrilla["maximoFilas"]);
        $queryRegistroActividad->setFirstResult($aDataGrilla["paginacion"]);
      }
      $aRegistroActividad = $queryRegistroActividad->getScalarResult();

      // --- --- --- Lógica --- --- --- //
      $aListRegistroActividad = array();
      //$date = new DateTime();
      foreach( $aRegistroActividad as $entiti ){
        $aListRegistroActividad [] = array(
          'id'          => $entiti['id'],
          'userNombre'  => $entiti['nombre'],
          'fecha'       => $entiti['fechaIngreso'],
          'actividad'   => $entiti['actividad'],
          'ipUsuario'   => $entiti['ip'],
          'idUsuario'   => $entiti['idUsuario'],
        );
      }

      // Cierre de conexion y Respuesta
      $em->getConnection()->close();
      $response->setContent(json_encode(['totalRows' => $numeroRegistros, 'data' => $aListRegistroActividad]));

    }else{
      $aJson['status'] = 0;
      $aJson['msg'] = "Acción no valida";
      $response->setContent(json_encode($aJson));
    }

    return $response;
  }

    /**
    * Responde un archivo .csv con la información de registro de los usuarios administradores
    * 
    * @param Symfony\Component\HttpFoundation\Request $request Contiene los datos que vienen por peticion HTTP además de los datos de sesión.
    * @param App\Services\GrillaGlobal $grillaGlobal para realizar los filtros de busqueda
    * @param $exportar Type=bool Valida si se filtran datos necesarios para descargar un archivo .csv
    * 
    * @return JSON
    */
  public function registroActividadDescargaLogsCsv(Request $request, GrillaGlobal $grillaGlobal, $exportar = false){

    // Variables
    $session = $request->getSession();
    $sListado = $request->get('listado');
    $em = $this->getDoctrine()->getManager();

    $sFileCsv = "";

    $aEquivalenciaColumnas = [
      'id'          => 'ra.id',
      'userNombre'  => 'us.nombre',
      'fecha'       => 'ra.fechaIngreso',
      'actividad'   => 'ra.actividad',
      'ipUsuario'   => 'ra.ip'
    ];

    // Procedimiento Filtro Grilla
    $sIniFiltro = "";
    $aDataGrilla = $grillaGlobal->realizarFiltro($aEquivalenciaColumnas, $sIniFiltro);
 

    // DQL Data
    $queryRegistroActividad = $em->createQuery("SELECT ra.id, us.nombre, ra.fechaIngreso, ra.actividad, ra.ip, us.id AS idUsuario
      FROM App\Entity\LogAdministrador ra
      JOIN App\Entity\User us WITH us.id = ra.administradorId
      WHERE {$aDataGrilla["where"]} 
      ORDER BY {$aDataGrilla["order"]} ");

    // Resultado
    $queryRegistroActividad->setParameters($aDataGrilla["valoresWhere"]);

    $queryRegistroActividad->setMaxResults(50000);
    $aRegistroActividad = $queryRegistroActividad->getScalarResult();
    // --- --- --- Lógica --- --- --- //
    $aListRegistroActividad = array();
    //$date = new DateTime();
    foreach( $aRegistroActividad as $entiti ){
      $aListRegistroActividad [] = array(
        'id'          => $entiti['id'],
        'userNombre'  => $entiti['nombre'],
        'fecha'       => $entiti['fechaIngreso'],
        'actividad'   => $entiti['actividad'],
        'ipUsuario'   => $entiti['ip'],
        'idUsuario'   => $entiti['idUsuario'],
      );
    }

    $dfColumnsLogs = $this->definicionColumnasAdmin();
    $sNombreCsv = 'admin_logs';

    $this->log->setLogAdmin("LRA5 Listado de Logs de Administrador");

    foreach ($dfColumnsLogs as $dfColumn) {
      if( isset($dfColumn['hide']) && $dfColumn['hide'] != true ) $sFileCsv .= utf8_decode($dfColumn["headerName"]).";";
      else if(!isset($dfColumn['hide'])) $sFileCsv .= utf8_decode($dfColumn["headerName"]).";";
    }
    $sFileCsv .= "\r\n";
    foreach( $aListRegistroActividad as $aData ){
      foreach( $dfColumnsLogs as $dfColumn ){
        if( !isset($dfColumn['hide'])) $sFileCsv .= utf8_decode($aData[$dfColumn["field"]]).";";
      }
      $sFileCsv .= "\r\n";
    }

    $response = new Response($sFileCsv);
    $response->headers->set('Content-Type', 'text/csv');
    $response->setStatusCode(200);
    $response->headers->set('Content-Disposition', 'attachment; filename='.$sNombreCsv.'.csv; charset=UTF-8');

    return $response;
  }

    /**
    * Declara el nombre de las columnas y las funcionalidades que se mostrarán en el listado de registro de actividad
    * 
    * @return JSON
    */
  private function definicionColumnasAdmin(){
    return $dfColumnas = [
      ["headerClass" => "h6", 'headerName' => '#',           'field' => 'id',          'hide' => true],
      ["headerClass" => "h6", 'headerName' => 'Usuario',     'field' => 'userNombre',  'width' => 110],
      ["headerClass" => "h6", 'headerName' => 'Fecha',       'field' => 'fecha',       'width' => 140, 'type' => "fecha", "searchoptions" => ["type" => "fecha", "format" => "YYYY-MM-DD"]],
      ["headerClass" => "h6", 'headerName' => 'Actividad',   'field' => 'actividad',   'width' => 260],
      ["headerClass" => "h6", 'headerName' => 'IP Usuario',  'field' => 'ipUsuario',   'width' => 110],
    ];
  }
}
