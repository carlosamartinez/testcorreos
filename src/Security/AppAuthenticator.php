<?php

namespace App\Security;

use App\Entity\User;
use App\Entity\Menu;
use App\Services\Globales\MenuPermisos;
use Nzo\UrlEncryptorBundle\Encryptor\Encryptor;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Security\PasswordEncoder;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Guard\PasswordAuthenticatedInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class AppAuthenticator extends AbstractFormLoginAuthenticator implements PasswordAuthenticatedInterface{

    use TargetPathTrait;
    public const LOGIN_ROUTE = 'admin_login';

    private $entityManager;
    private $urlGenerator;
    private $csrfTokenManager;
    private $menuPermisos;
    private $pathGenerate;
    private $encryptor;
    private $passwordEncoder;

    public function __construct(EntityManagerInterface $entityManager, UrlGeneratorInterface $urlGenerator, CsrfTokenManagerInterface $csrfTokenManager, PasswordEncoder $passwordEncoder, MenuPermisos $menuPermisos,Encryptor $encryptor){
      $this->entityManager = $entityManager;
      $this->urlGenerator = $urlGenerator;
      $this->passwordEncoder = $passwordEncoder;
      $this->csrfTokenManager = $csrfTokenManager;
      $this->menuPermisos = $menuPermisos;
      $this->encryptor = $encryptor;
    }

    public function supports(Request $request)
    {
        return self::LOGIN_ROUTE === $request->attributes->get('_route')
            && $request->isMethod('POST');
    }

    public function getCredentials(Request $request)
    {
        $credentials = [
            'usuario' => $request->request->get('usuario'),
            'password' => $request->request->get('password'),
            'csrf_token' => $request->request->get('_csrf_token'),
        ];
        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['usuario']
        );

        return $credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider){

        $token = new CsrfToken('authenticate', $credentials['csrf_token']);
        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }

        $user = $this->entityManager->getRepository(User::class)->findOneBy(['usuario' => $credentials['usuario']]);

        if (!$user) {
            // fail authentication with a custom error
            throw new CustomUserMessageAuthenticationException('Email no encontrado.');
        }

        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return $this->passwordEncoder->isPasswordValid($user, $credentials['password']);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
    */
    public function getPassword($credentials): ?string{
      return $credentials['password'];
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey){

      /*if( $targetPath = $this->getTargetPath($request->getSession(), $providerKey) ){
        return new RedirectResponse($targetPath);
      }*/

      $oUser = $token->getUser();
      if( $oUser->getEstado() == 1 ){
        $aDataMenuPermisos = $this->menuPermisos->getMenu( $oUser->getRoles()[0], $oUser->getId());
        //$sPathGenerate = $aDataMenuPermisos['pathGenerate'];
        if( empty($aDataMenuPermisos) || $aDataMenuPermisos['pathDefault'] == '' ){
          return new RedirectResponse($this->urlGenerator->generate('admin_logout'));
          //throw new CustomUserMessageAuthenticationException('El usuario ingresado no tiene permisos de acceso a la aplicación, contacte al administrador del sitio.');
        }else{
          // variables de sesion

          $datosUsuario = $this->entityManager->getRepository(User::class)->findOneById($oUser->getId());

          $session = $request->getSession();
          $session->set('pathDefault', $aDataMenuPermisos['pathDefault'] );
          // var_dump( '<pre>', $aDataMenuPermisos['menu'] );exit();
          $session->set('menuPermisos',  $aDataMenuPermisos['menu'] );
          //$session->set('routeActiveDefault', $sPathGenerate );
          $session->set('id', $oUser->getId() );
          $session->set('usuario', $oUser->getUsuario() );
          $session->set('nombre', $oUser->getNombre() );
          $session->set('administrador', $datosUsuario );
          $session->set('nombreAdmin', $datosUsuario->getNombre());
          $session->set('emailAdmin', $datosUsuario->getEmail());
          $session->set('emailClave', $datosUsuario->getClaveEmail());
          $session->set('fechaCreado', $datosUsuario->getFechaCreado());
          $session->set('tipoUsuario', $datosUsuario->getTipoUsuario());
          /*$session->set('autenticado',true);
          $session->set('last_time', time());*/

          $semail = $request->request->get('email');
          $user = $this->entityManager->getRepository(User::class)->findOneBy(['email' => $semail ]);

          return new RedirectResponse($this->urlGenerator->generate($aDataMenuPermisos['pathDefault']));
        }
      }else{
        //echo "entra 4";exit();
        //throw new CustomUserMessageAuthenticationException('El usuario no se encuentra activo.');
        return new RedirectResponse($this->urlGenerator->generate('admin_logout'));
      } 

      if( $targetPath = $this->getTargetPath($request->getSession(), $providerKey) ){
        return new RedirectResponse($targetPath);
      }else{
        return new RedirectResponse( $this->urlGenerator->generate($aDataMenuPermisos['pathDefault']) );
      }      
      // else return new RedirectResponse($this->urlGenerator->generate('admin_login'));
      // For example : return new RedirectResponse($this->urlGenerator->generate('some_route'));
      // throw new \Exception('TODO: provide a valid redirect inside '.__FILE__);
    }



    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
      ///echo "error autenticacion";exit();

        /*$usuario = $request->request->get('_username');
        //$referer = $request->headers->get('referer');

        $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);
        $referer = $this->container->get('router')->generate('drogueria_login', array('error' => $exception->getMessageKey() ));

        $clave=$request->request->get('_password');
        $asociado = $this->em->getRepository('App\Entity\Cliente')->findOneBy(array('codigo' => $usuario));
        if($asociado){
            $aceptacionContrato = $this->em->getRepository('App\Entity\AceptacionContrato')->findOneBy(array('codigoDrogeria' => $usuario));
            if(!$aceptacionContrato){
                if(($asociado->getCodigo() == $usuario) && ($asociado->getNit() == $clave)){
                    $session = $request->getSession();
                    $session->set('idDrogueria', $asociado->getId());
                    $session->set('codigoDrogueria', $asociado->getCodigo());
                    $this->em->getConnection()->close();
                    return new RedirectResponse($this->container->get('router')->generate("drogueria_terminos_uso"));
                }
            }
        }
        $request->getSession()->getFlashBag()->add('error', $exception->getMessage());
        $this->em->getConnection()->close();
        return new RedirectResponse($referer);*/

    }

  protected function getLoginUrl(){
    return $this->urlGenerator->generate(self::LOGIN_ROUTE);
  }

}
