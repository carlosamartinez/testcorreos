<?php
namespace App\Security;

use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Nzo\UrlEncryptorBundle\Encryptor\Encryptor;

class PasswordEncoder implements PasswordEncoderInterface
{
    private $encryptor;

    public function __construct(Encryptor $encryptor){
        $this->encryptor = $encryptor;
    }

    public function encodePassword(string $raw, ?string $salt = null)
    {   
        return $this->encryptor->encrypt($raw);
    }
    public function decodePassword(string $raw)
    {   
        return $this->encryptor->decrypt($raw);
    }
    /*public function decodePassword(string $encoded, string $raw, ?string $salt)
    {
        return $this->encryptor->decrypt($encrypt_Password);
    }*/
    public function isPasswordValid( $encoded,  $raw, ?string $salt = null)
    {
        if($raw == $this->encryptor->decrypt($encoded->getPassword())){
            return true;
        }
        return false;
       
    }

    public function needsRehash(string $encoded): bool
    {
        return true;
    }
}