export default class MdlCambiarPassword {

  constructor(){}
  init(){
    

    $('#seeoldpass').click(()=>{
        $('#PreviousPassword').get(0).type = $('#PreviousPassword').get(0).type == "password" ? "text" : "password";
    });
    $('#seepass2').click(()=>{
        $('#InputPassword2').get(0).type = $('#InputPassword2').get(0).type == "password" ? "text" : "password";
    });
    $('#seepass1').click(()=>{
        $('#InputPassword1').get(0).type = $('#InputPassword1').get(0).type == "password" ? "text" : "password";
    });

    $('#CambiarClave').click(()=>{
        console.log("!!!!")
        if ($('#InputPassword1').val() != $('#InputPassword2').val()) {
            Swal.fire({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 5000 ,
                timerProgressBar: true
            }).showValidationMessage(
                `Las contraseñas deben coincidir`
            );

            return;
        }else if(($('#InputPassword1').val()).length < 8){
            Swal.fire({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 5000 ,
                timerProgressBar: true
            }).showValidationMessage( 
                `La contraseña debe contener minimo 8 carácteres`
            );
            return;

        }else if(($('#InputPassword1').val()).toLowerCase() == $('#InputPassword1').val()){

            Swal.fire({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 5000 ,
                timerProgressBar: true
            }).showValidationMessage(
                `La contraseña debe contener al menos una letra en mayuscula`
            );

            return;
        }else if(($('#InputPassword1').val()).toUpperCase() === $('#InputPassword1').val()){
            Swal.fire({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 5000 ,
                timerProgressBar: true
            }).showValidationMessage( 
                `La contraseña debe contener al menos una letra en minuscula`
            );
            return;
        }else if(!/\d+/.test($('#InputPassword1').val())){
            Swal.fire({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 5000 ,
                timerProgressBar: true
            }).showValidationMessage( 
                `La contraseña debe contener al menos un numero`
            );
            return;
            
        }
        $.ajax({
            type: "post",
            url: _urlCambiarClave,
            data: {"old_password": $('#PreviousPassword').val(),"password": $('#InputPassword1').val(),"user" : user }, 
            success: function(data){
                
                    Swal.fire({
                        icon: 'success' ,
                        title: 'Contraseña cambiada',
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 5000 ,
                        timerProgressBar: true
                    });
                    $(".modal-body").empty();
                    $(".modal-body").append('<div class="d-flex justify-content-center"> <div class="p-2 bd-highlight">Volveras al login... </div></div>');
                    $("#IniciarSeS").click(()=>{
                    })
                    setTimeout(function() {                     
                        location.href = "/admin/logout";
                    }, 2000);

                    return;
                
                
            },
            error: function (request, status, error) {
                
                if (request.status == 406){
                    Swal.fire({
                        icon: 'warning' ,
                        title: 'La contraseña actual no es correcta',
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 5000 ,
                        timerProgressBar: true
                    });
                    return;
                }
                Swal.fire({
                    icon: 'error' ,
                    title: 'Ocurrio un error, intentalo mas tarde',
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 5000 ,
                    timerProgressBar: true
                });  
            }
          })

        
    })
  }

}