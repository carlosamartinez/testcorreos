// Css
import '../../css/app/signin.css';
// Js
import swal from 'sweetalert2';

// Funciones Locales
$(function (){

    // Clear Cache
    $.ajaxSetup({ cache: false });

    $('#btn-olvidoContrasenia').on('click', function () {
        window.location.href =  "/reset-password";
    });

  // Loading Hide
  $('#loading').hide();
});