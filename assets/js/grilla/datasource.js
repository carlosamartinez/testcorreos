import $ from "jquery";
import { alertaNotificar } from "../globales/alertas";
import Mensajes from "../globales/mensajes";

function Datasource() { }
Datasource.prototype.rowCount = 0;
Datasource.prototype.totalRows = 0;
Datasource.prototype.totalPages = 0;
Datasource.prototype.executeReadySizeColumns = false;
Datasource.prototype.globalFilterQuery = "";
Datasource.prototype.filters = [];
Datasource.prototype.gridOptions = {};
Datasource.prototype.urlGetData = { GRILLA_LISTAR_JSON: '', ORDER_SORD: "DESC", ORDER_SIDX: "id", ADD_DATA: {} }; // URL to AJAX
Datasource.prototype.globalFilterQuery = "";
/**
  * Metodo que es llamado cada vez que se requiere solicitar nuevas filas desde el servidor
  * @param {{startRow: number, endRow: number, sortModel: any, filterModel: any, context: any}} params
  * @return void
*/
Datasource.prototype.getRows = function (params) {
    // console.log("%cInfo params en getRows: ", 'color: green', params);
    // console.log("%c Contenido this: ", 'color: #023A78', this);
    var request = params.request;
    var instance = this;
    let mensajes = new Mensajes();
    var ApiRestURLS = instance.urlGetData;
    var rows = request.endRow - request.startRow;
    var page = request.endRow / rows;
    var sord = "sord[]=" + ((ApiRestURLS.ORDER_SORD != undefined) ? ApiRestURLS.ORDER_SORD : "ASC");
    var sidx = "sidx[]=" + ApiRestURLS.ORDER_SIDX;
    var filterQuery = "";
    if (request.sortModel.length > 0) {
        sord = "";
        sidx = "";
        request.sortModel.forEach(function (data) {
            sord += "&sord[]=" + data.sort.toUpperCase();
            sidx += "&sidx[]=" + data.colId;
        });
        sord = sord.substring(1);
        sidx = sidx.substring(1);
    }
    this.filters.forEach(function (filter) {
        filterQuery += `&filter[]=${filter.field}&filterOperator[]=${filter.operator}&filterValue[]=${filter.value}`;
    });

    let addData = '';
    if (!$.isEmptyObject(this.urlGetData.ADD_DATA)) {
        $.each(this.urlGetData.ADD_DATA, function (campo, valor) {
            addData += `&${campo}=${valor}`;
        });
    }

    this.globalFilterQuery = "?" + sord + "&" + sidx + "&rows=" + rows + "&page=" + page + filterQuery + addData;


    // ALTERNATIVA SOLUCIÓN BUG-AG-GRID,
    // En AgGrid no funciona el evento "failCallback" para ocultar "loading" por lo tanto se plantea 
    // una alternativa es utilizar successCallback con los atributos vacios... totalRows y totalPages
    // Por defecto estan en 0 lo cual no causa problemas en la navegación.
    // @autor Anderson Barbosa - 2021-04-21

    this.gridOptions.api.showLoadingOverlay();
    $.ajax({
        url: ApiRestURLS.GRILLA_LISTAR_JSON + this.globalFilterQuery,
        dataType: "json",
        method: "GET",
        success: function (result) {
            instance.gridOptions.api.hideOverlay();

            if (result.totalRows != 0) {
                instance.totalRows = result.totalRows;
                instance.totalPages = Math.ceil(instance.totalRows / rows);
            }

            params.successCallback(result.data, instance.totalRows);
            if (result.data.length == 0) {
                params.failCallback();
                instance.gridOptions.api.showNoRowsOverlay();
            }else if(instance.executeReadySizeColumns){
                // Cuando genera la primera información de la grilla, este debe acomodarla
                instance.executeReadySizeColumns = false;
                instance.gridOptions.api.sizeColumnsToFit();
            }
        },
        error: function (err) {
            // params.failCallback();
            params.successCallback([], 0);
            console.log("err", err);

            let sMsgError = '';
            if(typeof err.responseJSON.message === 'string')
                sMsgError = err.responseJSON.message;
 
            alertaNotificar({title: `${ mensajes.errorGlobal }`, text: sMsgError, icon: 'error'});
            instance.gridOptions.api.showNoRowsOverlay();
        }
    });
};


export default Datasource;