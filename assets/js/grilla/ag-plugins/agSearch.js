import $ from "jquery";

// datetimepicker
global.moment = require('moment');
require('tempusdominus-bootstrap-4');

function Ag_Search(idFrmSearch, gridOptions) {
    this.gridOptions = gridOptions; 
    this.idFrmSearch = idFrmSearch; 
} 

// RETORNA CONTENEDOR DEL MODAL
Ag_Search.prototype.getContainerSearch = function(){
    
    let form = $(`<form id="${ this.idFrmSearch }"></form>`);
    let container = $(`<div class="container-fluid"></div>`).appendTo( form );
    
    const addRow = () => {
        let row = this.getRowFilter();
        row.appendTo( container );
    };
    
    // Boton Adicionar
    let hRow_sumbitButton = $(`<div class="row"><div class="col-sm-4"><div class="form-group"></div></div><div class="col-sm-8"><div class="form-group"></div></div></div>`);
    let hRow_formGroup = hRow_sumbitButton.find(".form-group");

    let btnTipoBusqueda = $("<select>").addClass("form-control").append( $("<option>", {value:"AND", text:".:: TODO ::."}), $("<option>",{value:"OR", text:".:: CUALQUIER ::."}) ).appendTo( hRow_formGroup.eq(0) );
    let btnAdd = $("<button type='button' >").addClass("btn btn-success").append( $("<i class='fa fa-plus'></i>") ).appendTo( hRow_formGroup.eq(1) );
    btnAdd.click( addRow );
    container.append( hRow_sumbitButton );
    
    // Agrega Fila
    addRow();
    
    this.containerInModal = form;
    
    return form;
};

Ag_Search.prototype.getColumn = function(nCell){
    let column = $(`<div class="col-sm-${nCell}"></div>`);
    let group = $(`<div class="form-group"></div>`).appendTo(column);
    return {
        "column" : column,
        "group" : group
    };
};

/**
* Contenido Fila
*/
Ag_Search.prototype.getRowFilter = function(){

    let oRow = $("<div class='row'></div>");
    
    // Parametro
    let col_parametro = this.getColumn(4);
    let columnData = this.getColumnData(); // Obtenemos Columnas
    let field_parametro = $("<select>").addClass("form-control").appendTo( col_parametro.group );
    let defaultFilter = {type: "default"}; // Filtro defecto (primera columna)

    $.each(columnData, function(indice, valor) {
        let option = $("<option>", valor);
        if(valor.filter){
            option.data("filter", valor.filter);
            defaultFilter = (indice === 0) ? valor.filter : defaultFilter;
        }
        field_parametro.append( option );
    });
    field_parametro.change( (e) => { this.changeCamps(field_parametro); } );
    oRow.append( col_parametro.column ); 
    
    // Campos Default
    oRow.append( this.typeCamps(defaultFilter) );
    
    // Eliminar
    let col_eliminar= this.getColumn(1);
    let field_eliminar = $("<button type='button'>").addClass("btn btn-danger").append( $("<i class='fa fa-times'></i>") ).appendTo( col_eliminar.group );
    field_eliminar.click( (e) => { if(oRow.siblings().length > 1){ oRow.remove(); } else{ this.clearInModal() } } );
    oRow.append( col_eliminar.column );
    
    return oRow;
};

/**
* Obtenemos las columnas para renderizarlo en la selección de campo
*/
Ag_Search.prototype.getColumnData = function(){
    let columnsData = [];
    let columnsGrid = this.gridOptions.columnApi.getAllColumns(); // Obtenemos Columnas

    // Ordenamiendo por nombre de columnas
    columnsGrid.sort(function(a, b) {
        if (a.colDef.headerName > b.colDef.headerName) {
            return 1;
        }
        if (a.colDef.headerName < b.colDef.headerName) {
            return -1;
        }
        // a must be equal to b
        return 0;
    });
    //, cellRenderer: columnDefault.cellRenderer
    // Recorremos columnas
    columnsGrid.forEach(function(dataColumn){
        let columnDefault = dataColumn.colDef;
        let sNombre = columnDefault.headerName;
        let bSearch = (typeof columnDefault.search === "boolean") ? columnDefault.search : true;
        if(dataColumn.visible === true && sNombre != "" && bSearch){
            let oData = {value: columnDefault.field, text: sNombre};
            if (columnDefault.searchoptions) {
                oData.filter = columnDefault.searchoptions;
            }
            columnsData.push(oData);
        }
    });  

    return columnsData;
}

/**
* Al momento de seleccionar otro campo, restrablece las otras celdas con su respectiva configuración
*/
Ag_Search.prototype.changeCamps = function(field_parametro){
    let combo = field_parametro;
    let selected = combo.find("option:selected");
    let oParent = combo.parent().parent();
    let oRow = oParent.parent();
    let aParents = oRow.children();
    
    // Eliminamos Campos
    aParents[1].remove();
    aParents[2].remove();
    
    let oFilter = ( selected.data("filter") != undefined ) ? selected.data("filter") : {type: "default"};
    
    oParent.after( this.typeCamps(oFilter) );
};

/**
* Cada columna cuenta con su propia configuración de campo
*/
Ag_Search.prototype.typeCamps = function(oFilter){

    let instance = this;
    
    // Parametros Iniciales
    // let data_operador = [ {value: "eq", text: "Igual"}, {value: "gte", text: "Mayor o Igual"}, {value: "lte", text: "Menor o Igual"}, {value: "like", text: "Contiene"} ];

    // Variables
    let field_valor, data_operador;
    let operadorDateAndNumber = [ 
        {value: "eq", text: "Igual "},
        {value: "ne", text: "No igual a"},
        {value: "nu", text: "Is null"},
        {value: "nn", text: "Is not null"},
        {value: "lt", text: "Menor que"},
        {value: "le", text: "Menor o igual que"},
        {value: "gt", text: "Mayor que"},
        {value: "ge", text: "Mayor o igual a"},
        {value: "cn", text: "Contiene"},
        {value: "nc", text: "No Contiene"}
    ];

    
    switch(oFilter.type){
        case "number":
            data_operador = operadorDateAndNumber;
            field_valor = $("<input type='text' placeholder='1234567890...' />").addClass("form-control").keypress(function(e){
                if(!(e.which == 13 || e.which == 8 || e.which ==9 || (e.which >47 && e.which < 58) || e.which ==0)){
                    return false;
                }
            });
            break;
        case "fecha":
            data_operador = operadorDateAndNumber;
            let sIdFecha = instance.idFrmSearch + "-" + Math.floor((Math.random() * 1000) + 1); // Es necesario para que funcione la fecha
            field_valor = $("<input />", {type: "text", class: "form-control", id: sIdFecha, placeholder: "YYYY-MM-DD H:M",attr:{"data-toggle": "datetimepicker", "data-target": `#${sIdFecha}`}, data:{toggle: "datetimepicker", target: `#${sIdFecha}`} });
            field_valor.datetimepicker({
                locale: 'es',
                format: 'YYYY-MM-DD HH:mm',
                collapse: true,
                icons: {
                    time: 'fa fa-clock-o',
                    date: 'fa fa-calendar',
                    up: 'fa fa-arrow-up',
                    down: 'fa fa-arrow-down',
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    today: 'fa fa-screenshot',
                    clear: 'fa fa-trash',
                    close: 'fa fa-remove'
                }
            }); 
            field_valor.focusout(function(){
                field_valor.datetimepicker("hide");
            })
            break;
        case "combo":
            data_operador = [ {value: "eq", text: "Igual"} ];
            field_valor = $("<select>").addClass("form-control");
            $.each(oFilter.content, function(indice, valor) {
                field_valor.append( $("<option>", valor) );
            });
            break;
        default:
            field_valor = $("<input type='text' placeholder='Valor...' />").addClass("form-control"); // CAMPO VALOR
            data_operador = [ 
                {value: "eq", text: "Igual"},
                {value: "ne", text: "No igual a"},
                {value: "nu", text: "Is Null"},
                {value: "nn", text: "Is Not Null"},
                {value: "bw", text: "Empiece por"},
                {value: "bn", text: "No empiece por"},
                {value: "ew", text: "Termina por"},
                {value: "en", text: "No termina por"},
                {value: "cn", text: "Contiene"},
                {value: "nc", text: "No Contiene"}
            ];
            break;
    }
    
    // Operador
    let col_operador= this.getColumn(3);
    let field_operador = $("<select>").addClass("form-control").appendTo( col_operador.group );
    $.each(data_operador, function(indice, valor) {
        field_operador.append( $("<option>", valor) );
    });
    
    // Valor
    let col_valor= this.getColumn(4);
    field_valor.appendTo( col_valor.group );
    
    return [col_operador.column, col_valor.column];
    
};

Ag_Search.prototype.clearInModal = function(){
    
    var oContainer = this.containerInModal;
    
    oContainer.find(".row").each(function(indice, campo){
        if(indice > 1){
            $(this).remove();
        }
    });
    
    document.getElementById( this.idFrmSearch ).reset(); 
    
};

export { Ag_Search };