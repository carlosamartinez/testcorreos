import { enviarDatos } from "../../globales/utilidades";
import { alertaNotificar } from "../../globales/alertas";

export default class AgPreferenciasGrilla{
	constructor( idGrilla ){
        this.colaPreferencias = []; // Columnas Grilla
		this.initTipoGrilla = idGrilla;
        this.isSyncEnProceso = false;
        this.timeUltimaSolicitudGuardado = new Date().getTime();
        this.tiempoConsulta; // Tiempo restante para realizar guardado de las preferencias
    }

    guardarPreferencias(columnState) {
    	// Variables
    	let instance = this;

    	// Guardamos Columnas
        this.colaPreferencias.push(columnState);

        // Tiempo para realizar solicitud
        let timeActual = new Date().getTime();
        let diffTimeActalVsUltimaSolicitudEnMs = timeActual - this.timeUltimaSolicitudGuardado;
        this.timeUltimaSolicitudGuardado = timeActual;

        // Validación de tiempo
        if (diffTimeActalVsUltimaSolicitudEnMs > 1000 || (diffTimeActalVsUltimaSolicitudEnMs < 1000 && !this.isSyncEnProceso)) {
            this.isSyncEnProceso = true; // En Proceso...
            
            // Tiempo para "Borrar Preferencias Columna"
            let nTime = (columnState.length > 0) ? 2500 : 0;
            
            // Reiniciamos Tiempo
            if(this.tiempoConsulta !== undefined){
                window.clearTimeout(this.tiempoConsulta);
            }
            
            // Envía Información
            this.tiempoConsulta = window.setTimeout(() => {
                
                // Ordena la información a guardar
                let enviarPreferencias = this.colaPreferencias.pop().map(function(aColumnas){
                    return {
                        colId: aColumnas.colId,
                        hide: aColumnas.hide,
                        pinned: aColumnas.pinned,
                        width: aColumnas.width
                    }
                });

                enviarDatos(_urlPreferenciaGrilla, { "modulo": _urlModulo, "column_state": enviarPreferencias, "tipoGrilla": this.initTipoGrilla }, {}, false).then(function(result){
                	// Restablecemos Proceso
                	instance.isSyncEnProceso = false;
                	delete instance.colaPreferencias;
                	instance.colaPreferencias = [];

                	// Mensaje
                    alertaNotificar({title: `Parámetros guardados.`, icon: 'success'});
                }, function(err){
                    alertaNotificar({title: `No fue posible guardar los parametros.`, icon: 'error'});
                });
            }, nTime);
        }
    }


	// _urlPreferenciaGrilla
}