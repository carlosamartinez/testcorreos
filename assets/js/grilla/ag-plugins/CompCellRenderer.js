// cell renderer class
function CompCellRenderer() {}

// init method gets the details of the cell to be rendere
CompCellRenderer.prototype.init = function(params) {
   
   this.eGui = document.createElement('div');
   this.eGui.className = "p-0 m-0 align-middle";

   var sElement = '';
   var rowData = params.node.data;
   var aForm = params.form;

   var sModulo = params.modulo;
   var sCampo = params.campo;

   if(rowData){

      if(typeof(aForm) != "undefined"){

         $.each(aForm, function(indexForm, dataElement){

            if(aInfoPlanilla.estado == 1){
               if(indexForm == "select"){
                  sElement += `<select class="${dataElement.class}">`;
                  sElement += `<option value="2" ${(rowData.estado == 1 || rowData.estado == 2) ? "selected" : ""}>Sin Entregar</option>`;
                  $.each(dataElement.option, function(indexOption, element){
                        sElement += `<option value="${indexOption}" ${(indexOption == rowData.estado) ? "selected" : ""}>${element}</option>`;
                  });
                  sElement += `</select>`;
               }
            }else{
               if(rowData.estado == 1 || rowData.estado == 2){
                  sElement = "Sin Entregar";
               }else{
                  $.each(dataElement.option, function(indexOption, element){
                     sElement += (indexOption == rowData.estado) ? element : "";
                  });
               }
            }

         });

      }else{
         sElement = "No se han definido Elementos";
      }

      if(typeof(sModulo) != undefined && sModulo == 'inventario'){
         if(typeof(sCampo) != undefined && sCampo == 'estado'){
            if(rowData.estado == '1') sElement = '<b class="text-success">Habilitado</b>';
            else if(rowData.estado == '2') sElement = '<b class="text-warning">Deshabilitado</b>';
            else if(rowData.estado == '3') sElement = '<b class="text-danger">Eliminado</b>';
            else sElement = '<b class="text-danger">Ha ocurrido un problema, contacte con administración.</b>';
         }else{
            sElement = 'Campo no encontrado.';
         }
      }else{
         sElement = 'Falta referencia al campo.';
      }
    }

   // console.log(params);

   this.eGui.innerHTML = sElement;

};

CompCellRenderer.prototype.getGui = function() {
   return this.eGui;
};


export { CompCellRenderer };