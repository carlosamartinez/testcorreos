export class PaginationStatusBar{
  init(params) {
    this.params = params;

    console.log("this.params", this.params);

    this.eGui = document.createElement('button');

    this.buttonListener = this.onButtonClicked.bind(this);
    this.eGui.addEventListener('click', this.buttonListener);
    this.eGui.innerHTML = 'Click Me For Selected Row Count';
    this.eGui.style.padding = '1px';
  }

  getGui() {
    return this.eGui;
  }

  destroy() {
    this.eButton.removeEventListener('click', this.buttonListener);
  }

  onButtonClicked() {
    alert('Selected Row Count: ' + this.params.api.getSelectedRows().length);
  }
}