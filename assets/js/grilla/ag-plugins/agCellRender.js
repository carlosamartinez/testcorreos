// cell renderer class
function AgCellRender() {}

AgCellRender.prototype.init = function(params){

  this.eGui = document.createElement('span');
  var rowData = params.node.data;
  // cellRendererParams 
  var sGrilla = params.grilla;
  var sColumna = params.columna;
  var sValidate = params.validate;
  var aDataRenderParams = params.aData;
  // Valores defecto
  var sHtmlRender = 'Faltan Parámetros';
    

    if(rowData){

        // --- --- Acciones por defecto
        if(typeof(sGrilla) != 'undefined' && sGrilla == 'default'){

            if(typeof(sColumna) != 'undefined' && sColumna == 'estado'){
                sHtmlRender = "<strong class='text-danger'>Inactivo</strong>";
                //console.log(JSON.stringify(rowData));
                if(rowData.estado == 1 ){
                    sHtmlRender = "<strong class='text-success'>Activo</strong>";
                }else if(rowData.estado == 3 ){
                    sHtmlRender = "<strong class='text-success'>En ejecución...</strong>";

                }
            }


            if(typeof(sColumna) != 'undefined' && sColumna == 'contactos'){
    
                let btnClass = aDataRenderParams.btnClass;
                let btnId = aDataRenderParams.btnId;
                let btnIconsClass = (typeof(aDataRenderParams.btnIconsClass) != 'undefined') ? aDataRenderParams.btnIconsClass : '';
                sHtmlRender = `<button class='${btnClass}' id='btnContacto_${rowData.id}' data-toggle="tooltip" data-placement="top" title="Ver contactos del proveedor" data-idproveedor='${rowData.id}'><i class='${btnIconsClass}'> </i></button>`;
            }

            if(typeof(sColumna) != 'undefined' && sColumna == 'envios'){
    
                let btnClass = aDataRenderParams.btnClass;
                let btnId = aDataRenderParams.btnId;
                let btnIconsClass = (typeof(aDataRenderParams.btnIconsClass) != 'undefined') ? aDataRenderParams.btnIconsClass : '';
                sHtmlRender = `<button class='${btnClass}' id='btnEnvio_${rowData.id}' data-toggle="tooltip" data-placement="top" title="Ver envios" data-idenvio='${rowData.id}'><i class='${btnIconsClass}'> </i></button>`;
            }

            if(typeof(sColumna) != 'undefined' && sColumna == 'copiaMsg'){

                let btnId = aDataRenderParams.btnId;
                let btnIconsClass = (typeof(aDataRenderParams.btnIconsClass) != 'undefined') ? aDataRenderParams.btnIconsClass : '';

                if(rowData.asignado){
                    var btnClass = "btn btn-sm btn-success selectAsociado";
                    var icono ="fas fa-check";
                    var tipo = 'quitar';
                    
                }else{
                    var btnClass = "btn btn-sm btn-warning selectAsociado";
                    var icono ="fas fa-exclamation-triangle";
                    var tipo = 'agregar';
                }  

                sHtmlRender = `<button class='${btnClass}' alt='${tipo}' id='asignarAsociado_${rowData.id}' data-toggle="tooltip" data-placement="top" title="Recibir copia de mensaje." ><i class='${icono}'> </i></button>`;
                
            }

            if(typeof(sColumna) != 'undefined' && sColumna == 'reenviarMsg'){

                let btnClass = aDataRenderParams.btnClass;
                let btnId = aDataRenderParams.btnId;
                let btnIconsClass = (typeof(aDataRenderParams.btnIconsClass) != 'undefined') ? aDataRenderParams.btnIconsClass : '';
                
                var envioIntegranteId = 0;
                if(rowData.envioIntegranteId && rowData.envioIntegranteId != 'undefined'){
                    envioIntegranteId = rowData.envioIntegranteId;
                }

                sHtmlRender = `<button class='btn btn-sm btn-primary enlaceE reenviarmsg' alt='${tipo}' id='reenviarAsociado_${rowData.id}' data-toggle="tooltip" data-placement="top" data-enviointegranteid="${envioIntegranteId}" data-integrantegrupoid="${rowData.idIntegrante}" title="Reenviar mensaje a asociado." ><i class='${btnIconsClass}'> </i></button>`;
                
                
            }


            if(typeof(sColumna) != 'undefined' && sColumna == 'copiaMsgProv'){

                let btnId = aDataRenderParams.btnId;
                let btnIconsClass = (typeof(aDataRenderParams.btnIconsClass) != 'undefined') ? aDataRenderParams.btnIconsClass : '';

                if(rowData.id){
                    var btnClass = "btn btn-sm btn-success selectProveedor";
                    var icono ="fas fa-check";
                    var tipo = 'quitar';
                    
                }else{
                    var btnClass = "btn btn-sm btn-warning selectProveedor";
                    var icono ="fas fa-exclamation-triangle";
                    var tipo = 'agregar';
                }  

                sHtmlRender = `<button class='${btnClass}' alt='${tipo}' id='asignarproveedor_${rowData.id}' data-toggle="tooltip" data-placement="top" title="Recibir copia de mensaje." ><i class='${icono}'> </i></button>`;
                
            }

            if(typeof(sColumna) != 'undefined' && sColumna == 'reenviarMsgProv'){

                let btnClass = aDataRenderParams.btnClass;
                let btnId = aDataRenderParams.btnId;
                let btnIconsClass = (typeof(aDataRenderParams.btnIconsClass) != 'undefined') ? aDataRenderParams.btnIconsClass : '';
                var envioIntegranteId = 0;
                var integranteGrupoId = 0;
                if(rowData.envioIntegranteId && rowData.envioIntegranteId != 'undefined'){
                    envioIntegranteId = rowData.envioIntegranteId;
                }

                if(rowData.integrante && rowData.integrante != 'undefined'){
                    integranteGrupoId = rowData.integrante;
                }

                sHtmlRender = `<button class='btn btn-sm btn-primary enlaceE reenviarmsg' alt='${tipo}' id='reenviarProveedor_${rowData.id}' data-toggle="tooltip" data-placement="top" data-enviointegranteid="${envioIntegranteId}" data-integrantegrupoid="${integranteGrupoId}" title="Reenviar mensaje a proveedor y recibir copia." ><i class='${btnIconsClass}'> </i></button>`;
                
                
            }

            
            // buttons default
            if(typeof(sColumna) != 'undefined' && sColumna == 'Asignado'){

                if(rowData.esAsociado == null){
                    sHtmlRender = `<input type="checkbox" class="agregar-cliente text-danger" data-id-cliente="${rowData.id}">`;
                }else{
                    sHtmlRender = `<input type="checkbox" checked class="agregar-cliente text-success" data-id-cliente="${rowData.id}">`;
                };
            }

            // buttons default
            if(typeof(sColumna) != 'undefined' && sColumna == 'permisos'){

                if(rowData.tipoUsuario == 'Administrador') {
                    let btnClass = (typeof(aDataRenderParams.btnClass) != 'undefined') ? aDataRenderParams.btnClass : 'btn btn-secondary';
                    let btnText = (typeof(aDataRenderParams.btnText) != 'undefined') ? aDataRenderParams.btnText + '&nbsp;' : '';
                    let btnIconsClass = (typeof(aDataRenderParams.btnIconsClass) != 'undefined') ? aDataRenderParams.btnIconsClass : '';

                    sHtmlRender = `<button class='${btnClass}'>${btnText}`;
                    if(btnIconsClass != '' && typeof(btnIconsClass) == 'string'){
                        sHtmlRender += `<i class='${btnIconsClass}'></i>`;
                    }else{
                        btnIconsClass.forEach(
                        element => sHtmlRender += `<i class='${element}'></i>&nbsp;`
                        );
                        sHtmlRender = sHtmlRender.slice(0, -6);
                    }
                    sHtmlRender += '</button>';
                } else{
                    sHtmlRender = null;
                };
            }

            if(typeof(sColumna) != 'undefined' && sColumna == 'buttons'){
                
                let bConstruir = true;

                
                if(bConstruir == true && (typeof(aDataRenderParams.btnText) != 'undefined' || typeof(aDataRenderParams.btnIconsClass) != 'undefined') ){

                    let btnClass = (typeof(aDataRenderParams.btnClass) != 'undefined') ? aDataRenderParams.btnClass : 'btn btn-secondary';
                    let btnText = (typeof(aDataRenderParams.btnText) != 'undefined') ? aDataRenderParams.btnText + '&nbsp;' : '';
                    let btnIconsClass = (typeof(aDataRenderParams.btnIconsClass) != 'undefined') ? aDataRenderParams.btnIconsClass : '';

                    sHtmlRender = `<button class='${btnClass} btn-sm'>${btnText}`;
                    if(btnIconsClass != '' && typeof(btnIconsClass) == 'string'){
                        sHtmlRender += `<i class='${btnIconsClass}'></i>`;
                    }else{
                        btnIconsClass.forEach(
                        element => sHtmlRender += `<i class='${element}'></i>&nbsp;`
                        );
                        sHtmlRender = sHtmlRender.slice(0, -6);
                    }
                    sHtmlRender += '</button>';
                }
            }

            if(typeof(sColumna) != 'undefined' && sColumna == 'erroresEnvios'){

                if(rowData.errores > 0){
                    sHtmlRender = `<span id='errorEnvio_${rowData.id}' class='errorEnvio' style='cursor:pointer;'><i class="fas fa-exclamation-circle text-danger"></i></span>`;
                }else{
                    sHtmlRender = ``;
                };
            }
           
            

            // buttons group - sin construir
            if(typeof(sColumna) != 'undefined' && sColumna == 'buttons-group'){}
                        
            if(typeof(sColumna) != 'undefined' && sColumna == 'texto'){

                if(aDataRenderParams){
                    if(typeof(aDataRenderParams.tipoBusqueda)){

                        var tipoBusqueda = "Ejecucion Manual";

                        if(rowData.tipo == 1){
                            tipoBusqueda = "Ejecucion Programada";
                        }
                        sHtmlRender =`<span class='${aDataRenderParams.txtClass}'>${tipoBusqueda} </span>`;

                    }
                }
            }
        }

        // --- --- Acciones personalizadas
    }

    this.eGui.innerHTML = sHtmlRender;
};

AgCellRender.prototype.getGui = function(){
  return this.eGui;
};

export { AgCellRender };