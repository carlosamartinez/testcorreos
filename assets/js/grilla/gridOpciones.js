import localeText from "./aggrid-locale-es";
import { currencyFormatter, numberParser } from "./func-util";
import { alertaNotificar } from "../globales/alertas";

    function GridOpciones() { }
    GridOpciones.prototype.gridOptions = function (components = {}) {
        var gridOptions = {
            suppressDragLeaveHidesColumns: true,
            rowModelType: 'serverSide',

            rowHeight: 30,
            rowSelection: 'single',
            animateRows: true, // Animar filas insertadas
            components: components,
            context: { "currencyFormatter": currencyFormatter, "numberParser": numberParser },
            stopEditingWhenGridLosesFocus: true,
            // toolPanelSuppressSideButtons: true, // Oculta opción columnas
            suppressCopyRowsToClipboard: true, // Solamente permite copiar la celda seleccionada

            // Column
            defaultColDef: {
                flex: 1,
                sortable: true,
                resizable: true
            },

            // Pagination
            pagination: true,
            suppressPaginationPanel: true,

            // Menu Items
            getContextMenuItems: function(params){
                // https://www.ag-grid.com/javascript-grid-clipboard/
                // https://www.ag-grid.com/javascript-grid-context-menu/
                return ["copy", 
                "copyWithHeaders", 
                "separator", 
                {
                    name: "Copiar Fila",
                    action: function(){
                        params.api.copySelectedRowsToClipboard();
                    }
                },
                {
                    name: "Copiar Fila Con Cabecera ",
                    action: function(){
                        params.api.copySelectedRowsToClipboard(true);
                    }
                },
                "separator",
                "toolPanel", 
                "export"];
            },
            getMainMenuItems: function (params) {
                return ["pinSubMenu", "separator", "autoSizeThis", {
                    name: "Ajustar Columnas", 
                    action: function(){ 
                        params.api.sizeColumnsToFit(); 
                        alertaNotificar({title: `Columnas Ajustadas`, icon: 'info'});
                    }
                }, "separator"];
            },
            onCellEditingStopped: function (event) {

            },
            localeText: localeText
        };

        return gridOptions;
    };
export { GridOpciones };