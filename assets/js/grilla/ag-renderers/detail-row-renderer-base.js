export default class DetailRowRendererBase{
  constructor(){
    this.gridDefinition = {};
  }

  //#region Propiedades AgGrid
  /**
   * Metodo inicial renderizar
   * @param params parametros aggrid
   */
  init(params){
    this.params = params;
    this.masterGridApi = params.api;

    // Eventos antes de iniciar
    this.beforeInit();

    // Parámetros Renderer
    this.rowId = this.getRowId();
    this.haveGrid = !$.isEmptyObject(this.gridDefinition);
    this.eGui = this.getTemplate();

    // Eventos
    if(this.haveGrid)
      this.setupDetailGrid();
  }

  /**
   * Realiza eventos antes de iniciar el evento init
   */
  beforeInit(){
  }

  /**
   * Obtiene elemento dom de la fila renderizada
   * @returns Dom
   */
  getGui() {
    return this.eGui;
  };

  /**
   * Limpiar elementos creados
   */
  destroy(){

    if(this.haveGrid){
      const masterGridApi = this.params.api;
      masterGridApi.removeDetailGridInfo(this.rowId);
      this.detailGridApi.destroy();
      this.oGrid.removeGridSearch(); // Elimina el modal busqueda
    }

  }
  //#endregion

  //#region Public Event's
  /**
   * Define id de la grilla
   * Como es un ID debe ser único, si en una ventana hay más de 2 grillas(ej. tab's)
   * por cada hoja renderer debe tener su propio ID.
   * @returns Id Row
   */
  getRowId(){
    return `ag-masterdetail-grid-${this.params.node.id}`;
  }
  
  /**
   * Obtiene el contenedor 
   * Info: https://www.ag-grid.com/javascript-grid/master-detail-custom-detail/#custom-detail-with-grid
   * @returns template
   */
  getTemplate(){
    const data = this.params.data;
    let template = '<div class="ag-masterdetail-container">';
    if(this.haveGrid)
      template += `<div id="${this.rowId}" class="ag-masterdetail-grid ag-theme-fresh" style="height: ${this.gridDefinition.gridDetailHeight};"></div>`;
    else
      template += `<h1>Content Template, Row Id "${this.rowId}"</h1>`;
    template += "</div>";

    return $(template).get(0);
  }

  /**
   * Realiza configuración de la grilla
   */
  setupDetailGrid() {
    // Grilla
    this.oGrid = $(this.eGui).find(`#${this.rowId}`).SIP_AgGrid(this.gridDefinition);;
    const detailGridOptions = this.oGrid.getGridOptions();
    
    // Registramos Grilla al master detail
    this.detailGridApi = detailGridOptions.api;
    const detailGridInfo = {
      id: this.rowId,
      oGrid: this.oGrid,
      api: detailGridOptions.api,
      columnApi: detailGridOptions.columnApi,
    };
    
    const masterGridApi = this.params.api;
    masterGridApi.addDetailGridInfo(this.rowId, detailGridInfo);
  };
  //#endregion
}