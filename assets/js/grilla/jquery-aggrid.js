import $ from 'jquery'
import Datasource from "./datasource";
import AgPreferenciasGrilla from "./ag-plugins/agPreferenciasGrilla";
import {Grid} from 'ag-grid-community';
import {LicenseManager} from 'ag-grid-enterprise';
import { GridOpciones } from "./gridOpciones";
import { GridUtilidades } from "./gridUtilidades";
import { alertaNotificar } from "../globales/alertas";



/* Reutilizar código grilla */
/*
 * Anderson Barbosa
 * 2018-03-02
 * Implementación de JS GLOBAL de grilla
 */

// Creamos funciones globales
(function ($) {

    // Configuramos
    //LicenseManager.setLicenseKey("Waplicaciones_SAS_MultiApp_1Devs9_November_2018__MTU0MTcyMTYwMDAwMA==ac35d652df43d26a842363b7e0fbcd48");
    LicenseManager.setLicenseKey("Waplicaciones_SAS_MultiApp_1Devs9_November_2019__MTU3MzI1NzYwMDAwMA==a9d281f89a49eee9e211db0f5c23477d");


    /**
     * @typedef {object} SIPAgGridConfig - Configuracion de la grilla de SIP
     * @property {string} url - URL AJAX en donde se van a realizar peticiones
     * @property {any} gridOptions - Opciones de la grilla
     * @property {any} btn - Boton
     * @property {string[]} components - Nombres de los componentes de renderizado de celdas que se desean incluir
     */


    // Eventos Globales
    /**
     * @param {SIPAgGridConfig} options
     * @returns {jquery-aggridL#14.$.fn.SIP_AgGrid.jquery-aggridAnonym$3}
     */

    $.fn.SIP_AgGrid = function (options) {

        options = $.extend({}, {
            components: [],
            autoSize: true,
            noHeight: [],
            paginador: true
        }, options);

        // Parametros
        const datasource = new Datasource();
        const opcionesGrid = new GridOpciones();
        const funcionesUtileria = new GridUtilidades();

        // Variables
        let paginador=options.paginador;
        let autoSize = options.autoSize;
        let preferencias = options.preferencias;
        let initGridOption = opcionesGrid.gridOptions();
        let gridReady = (typeof options.gridReady === "function") ? [options.gridReady] : [function(params) { return this; }];


        /**********************************************************************/
        /* CONFIGURACIÓN GRILLA                                               */
        /**********************************************************************/
        // ------------------------------------------------------------------ //
        // ALTURA AUTOMATICA
        // ------------------------------------------------------------------ //
        if(options.autoHeight !== undefined && options.autoHeight !== false){

            let oContainerAutoHeight;
            if(options.autoHeight === "auto"){
                const gridHeight = $(this[0]).height()
                $(this[0]).height(gridHeight - 34);
            }else{
                let sType = typeof options.autoHeight;
                if(sType === "object" && options.autoHeight.container !== undefined && !$.isEmptyObject(options.autoHeight)){
                    oContainerAutoHeight = options.autoHeight.container;
                }else if(sType === "boolean" && options.autoHeight === true){
                    oContainerAutoHeight = $("body");
                }
    
                gridReady.push(function(params){
                    funcionesUtileria.adjustGridHeight(params.api.gridCore.eGridDiv, oContainerAutoHeight, paginador, options.noHeight);
                });
            }
        }

        // ------------------------------------------------------------------ //
        // PREFERENCIAS
        // ------------------------------------------------------------------ //
        if(preferencias !== undefined){
            let oPreferencias = new AgPreferenciasGrilla( preferencias.id );

            // Combinamos preferencias
            if(!$.isEmptyObject(preferencias.data) && preferencias.data.length > 0){
                let combinarPreferencias = funcionesUtileria.combinarPreferencias(options.gridOptions.columnDefs, preferencias.data);
                options.gridOptions.columnDefs = combinarPreferencias.columnDefs;

               // Guardamos preferencias cuando hay campo nuevo
                if(combinarPreferencias.guardarPreferencias){
                    gridReady.push(function(params){
                        oPreferencias.guardarPreferencias(params.columnApi.getColumnState());
                        alertaNotificar(combinarPreferencias.mensajes);
                    });
                }
            }

            // Asignamos Eventos Preferencia
            // https://www.ag-grid.com/javascript-grid-column-api/
            let keyColumna_bk; // Almacena llave de la última columna visible
            let aMenuItems = initGridOption.getMainMenuItems;
            options.gridOptions = $.extend(options.gridOptions, {
                getMainMenuItems: function (params) {
                    let aMenuGlobal = aMenuItems(params); // Items por defecto
                    // Agregamos Borrar Preferencia
                    aMenuGlobal.push({
                        name: "Borrar Preferencias",
                        action: (e) => { funcionesUtileria.borrarPreferencias(oPreferencias); },
                        icon: "<i class='fa fa-ban'></i>"
                    });
                    return aMenuGlobal;
                }.bind(this),
                onColumnResized: ((e) => oPreferencias.guardarPreferencias(e.columnApi.getColumnState())).bind(this),
                onColumnMoved: ((e) => oPreferencias.guardarPreferencias(e.columnApi.getColumnState())).bind(this),
                onColumnPinned: ((e) => oPreferencias.guardarPreferencias(e.columnApi.getColumnState())).bind(this),
                onColumnVisible: ((e) => {
                    let keyColumna = e.column.colId; // Llave Columna
                    let columnas = e.columnApi.getColumnState(); // Todas las columnas de la grilla
                    let columnasVisibles = columnas.filter(function(dataColumna) { // Filtramos únicamente las columnas visibles
                        return (dataColumna.hide === false);
                    });

                    if(columnasVisibles.length === 0){
                        // Aparece nuevamente el último campo
                        keyColumna_bk = keyColumna; // Guardamos Columna para no hacer guardado de preferencia nuevamente
                        e.columnApi.setColumnVisible(keyColumna, true);

                        // Mensaje
                        alertaNotificar({title: `No es posible ocultar todas las columnas.`, icon: 'warning'});
                    }else if( columnasVisibles.length >= 1 && columnasVisibles[0].colId !== keyColumna_bk){
                        // Guarda preferencias
                        oPreferencias.guardarPreferencias(e.columnApi.getColumnState());
                    }
                }).bind(this)
            });
        }

        // ------------------------------------------------------------------ //
        // LÓGICA
        // ------------------------------------------------------------------ //
        let gridOptions = $.extend(initGridOption, {
            onGridReady: (params) => {

                // Realizamos eventos de anteriores validaciones
                gridReady.forEach(function(funciones){
                    funciones(params);
                });

                // Si no existe preferencia realizar autoAjustado
                if(options.autoSize === true && (preferencias === undefined || ($.isEmptyObject(preferencias.data))) ) {
                    datasource.executeReadySizeColumns = true;
                }
            },
            onModelUpdated: function (event) {
                // Activamos Paginador Grilla
                this.onPaginationChanged = (e) => {
                    funcionesUtileria.activarEventosPaginador();
                };
            }
        }, options.gridOptions);



        // Configuramos DataSource - PETICIONES AJAX
        datasource.urlGetData = $.extend({}, datasource.urlGetData, options.url);
        datasource.gridOptions = gridOptions;

        /**********************************************************************/
        /* PROCEDIMIENTO GRILLA                                               */
        /**********************************************************************/
        // Variables propiedades
        let oSelector = $(this[0]);
        let sSelectorId = ( this.selector != "" && this.selector != null ) ? this.selector : `#${ oSelector.attr("id") }`;

        // Implementamos configuración para PAGINADOR
        funcionesUtileria.gridOptions = gridOptions;
        funcionesUtileria.datasource = datasource;
        funcionesUtileria.oSelector = oSelector;
        funcionesUtileria.selector = sSelectorId;
        funcionesUtileria.simpleSelector = sSelectorId.replace(".", "").replace("#", "");
        funcionesUtileria.footerButttons = options.btn;

        // Implementar Grilla
        const gridDiv = oSelector.get(0); // Seleccionamos elemento a interpretar tabla
        new Grid(gridDiv, gridOptions); // Implementamos Plugin y su configuración

        // Realizamos Llamado AJAX
        gridOptions.api.setServerSideDatasource( datasource );
        return {
            getGridOptions: function () {
                return gridOptions;
            },
            getDataSource: function () {
                return datasource;
            },
            refreshGrid: function () {
                funcionesUtileria.refreshGrid(false);
            },
            refreshGridClearFilter: function () {
                funcionesUtileria.refreshGrid(true);
            },
            removeGridSearch: function() {
                funcionesUtileria.removeSearchGrid();
            },
            getDivGrid: gridDiv
        };

        // Retornamos Vista
        // return {"gridOptions": gridOptions, "datasource": datasource};
    };
}(jQuery));
export default $;