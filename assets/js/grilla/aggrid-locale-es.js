
var localeText = {
    // for filter panel
    page: 'Página',
    more: 'más',
    to: ' a ',
    of: ' de ',
    next: 'Siguiente',
    last: 'Ultimo',
    first: 'Primer',
    previous: 'Anterior',
    loadingOoo: 'Cargando...',
    // for set filter
    selectAll: 'daSelect Allen',
    searchOoo: 'daSearch...',
    blanks: 'daBlanc',
    // for number filter and text filter
    filterOoo: 'daFilter...',
    applyFilter: 'daApplyFilter...',
    // for number filter
    equals: 'Igual', 
    notEqual: 'Difernte',
    lessThanOrEqual: 'daLessThanOrEqual',
    greaterThanOrEqual: 'daGreaterThanOrEqual',
    inRange: 'daInRange',
    lessThan: 'daLessThan',
    greaterThan: 'daGreaterThan',
    // for text filter
    contains: 'Contiene',
    startsWith: 'daStarts dawith',
    endsWith: 'daEnds dawith',
    // the header of the default group column
    group: 'Grupo',
    // tool panel
    columns: 'Columnas',
    rowGroupColumns: 'laPivot Cols',
    rowGroupColumnsEmptyMessage: 'la please drag cols to group',
    valueColumns: 'laValue Cols',
    pivotMode: 'laPivot-Mode',
    groups: 'Grupos',
    values: 'Valores',
    pivots: 'laPivots',
    valueColumnsEmptyMessage: 'la drag cols to aggregate',
    pivotColumnsEmptyMessage: 'la drag here to pivot',
    // other
    noRowsToShow: 'No se encontraron resultados',
    // enterprise menu
    pinColumn: 'Fijar Columna',
    valueAggregation: 'laValue Agg',
    autosizeThiscolumn: 'Auto ajustar Columna ',
    autosizeAllColumns: 'Auto ajustar Todas las Columnas',
    groupBy: 'Agrupar por',
    ungroupBy: 'Desagrupar por',
    resetColumns: 'Restablecer esas columnas',
    expandAll: 'Expandir todas',
    collapseAll: 'Contraer todas',
    toolPanel: 'laTool Panelo',
    export: 'Exportar',
    csvExport: 'Exportar',
    excelExport: 'Exportar a Excel',
    // enterprise menu pinning
    pinLeft: 'Fijar a la Izquierda',
    pinRight: 'Fijar a la Derecha',
    noPin: 'No Fijar',
    // enterprise menu aggregation and status panel
    sum: 'Suma',
    min: 'Min',
    max: 'Max',
    first: 'Primero',
    last: 'Último',
    none: 'Ninguno',
    count: 'Contar',
    average: 'Promerdio',
    // standard menu
    copy: 'Copiar',
    copyWithHeaders: 'Copiar con Cabecera',
    ctrlC: 'Ctrl + C',
    paste: 'Pegar',
    ctrlV: 'Ctrl + C'
}
 
export default localeText;