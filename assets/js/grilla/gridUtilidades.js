import $ from "jquery";
import { Ag_Search } from "./ag-plugins/agSearch";
import { alertaCompleta } from "../globales/alertas";

var pressedKeyCode = null;

function GridUtilidades() { }
GridUtilidades.prototype.gridOptions = {};
GridUtilidades.prototype.datasource = {};
GridUtilidades.prototype.oSelector = {};
GridUtilidades.prototype.selector = "";
GridUtilidades.prototype.simpleSelector = "";
GridUtilidades.prototype.footerButttons = "";
GridUtilidades.prototype.DOM_Pagination = {};
GridUtilidades.prototype.KEY_CODES = {
    KEY_UP: 38, KEY_DOWN: 40, KEY_ENTER: 13,
    contains: function (key) {
        var objProp = Object.keys(this);
        var exists = objProp.reduce((prev, curr) => {
            if (this[curr] == key || this[prev] == key || prev === true) {
                prev = true;
            } else {
                prev = false;
            }
            return prev;
        });
        return exists;
    }
};

GridUtilidades.prototype.activarEventosPaginador = function () {

    // Workaround for bug in events order
    if (this.gridOptions.pagination) {

        // Si no existe barra inferior, crearlo
        if($.isEmptyObject(this.DOM_Pagination)){
            this.addFooterGrid();
        }

        // VARIABLES PAGINADOR
        var oPagination = this.DOM_Pagination;            
        var currentPage = this.gridOptions.api.paginationGetCurrentPage();
        var maxRowFound = this.gridOptions.api.paginationIsLastPageFound();
        var rowCount = maxRowFound ? this.gridOptions.api.paginationGetRowCount() : null;
        var pageSize = this.gridOptions.api.paginationGetPageSize();
        var totalPages = this.gridOptions.api.paginationGetTotalPages();
        var startRow = (pageSize * this.gridOptions.api.paginationGetCurrentPage()) + 1;
        var endRow = startRow + pageSize - 1;
        if (maxRowFound && endRow > rowCount) {
            endRow = rowCount;
        }


        if (totalPages != 0) {


            if (currentPage == 0) {
                oPagination.btFirst.prop("disabled", true).removeClass('btn-muted');
                oPagination.btPrevious.prop("disabled", true).removeClass('btn-muted');
            } else {
                oPagination.btFirst.prop("disabled", false).addClass('btn-muted');
                oPagination.btPrevious.prop("disabled", false).addClass('btn-muted');
            }

            if (totalPages == 1) {
                oPagination.btNext.prop("disabled", true).removeClass('btn-muted');
                oPagination.btLast.prop("disabled", true).removeClass('btn-muted');
            } else {
                oPagination.btNext.prop("disabled", false).addClass('btn-muted');
                oPagination.btLast.prop("disabled", false).addClass('btn-muted');
            }

            // Texto Paginación
            oPagination.lbPagination.find(".lbCurrent").text(currentPage + 1);
            oPagination.lbPagination.find(".lbTotal").text(totalPages);

            // Texto Grilla
            oPagination.lbDescription.find(".lbFirstRowOnPage").text(startRow);
            oPagination.lbDescription.find(".lbLastRowOnPage").text(endRow);
            oPagination.lbDescription.find(".lbRecordCount").text(rowCount);

            // Eventos Botones
            let addEvent = oPagination.btFirst.data("addEvent");
            if (this.gridOptions.api.paginationProxy && (addEvent == undefined)) {
                oPagination.btFirst.click(e => this.gridOptions.api.paginationGoToFirstPage()).data("addEvent", "ag-event");
                oPagination.btPrevious.click(e => this.gridOptions.api.paginationGoToPreviousPage());
                oPagination.btNext.click(e => this.gridOptions.api.paginationGoToNextPage());
                oPagination.btLast.click(e => this.gridOptions.api.paginationGoToLastPage());
            }
        }

    }
}

GridUtilidades.prototype.adjustWidth = function (gridOptions) {
    let visibleColumns = gridOptions.columnApi.getAllDisplayedColumns()
            .filter(el => el.colId.indexOf("cantidad") == -1)
            .map(el => el.colId);
    gridOptions.columnApi.autoSizeColumns(visibleColumns)
}

GridUtilidades.prototype.adjustGridHeight = function (gridDiv, container, paginador=true, noHeight=[] ) {
    var totalWindowHeight = window.innerHeight;
    var heightOtherElements = 0;

    var oContainer = (( container.find(".container-fluid").not(".containerFooterGrid").length > 0 ) ? container.find(".container-fluid").eq(0) : $("body"));
    oContainer.children(":visible").each(function () {
        let $campo = $(this);
        let validateHeight = true;

        // Campos en objeto JQUERY que no cuenta altura
        if(noHeight.length > 0){
            noHeight.forEach(function(oCampo){
                if($campo.is(oCampo)){
                    validateHeight = false;
                    return;
                }
            });
        }

        // wsmenu :: Tiene altura del menu, no deja imprimir bien
        if (this != gridDiv && !($campo.hasClass("wsmenu")) && !($campo.hasClass('swal-overlay')) && validateHeight  ) {
            let heightCurrentElement = $campo.outerHeight(true);

            // si la altura del elemento es cero, sumar la altura maxima de sus hijos
            if (heightCurrentElement == 0) {
                $campo.children(":visible").each(function () {
                    if (heightCurrentElement < $(this).height()) {
                        heightCurrentElement = $(this).height();
                    }
                });
            }
            heightOtherElements += heightCurrentElement;
        }
    });

    var gridNewHeight = totalWindowHeight - heightOtherElements;
    if(paginador){
        gridNewHeight = gridNewHeight - 34;
    }else{
        gridNewHeight = gridNewHeight;
    }
    gridNewHeight = gridNewHeight -15;
    gridDiv.style.height = `${gridNewHeight}px`;
};

GridUtilidades.prototype.limpiarFiltroGrilla = function (datasource, gridOptions) {
    gridOptions.api.showLoadingOverlay();
    if (datasource != undefined && datasource.filters.length > 0) {
        datasource.filters = [];
        gridOptions.api.setServerSideDatasource(datasource);
    }
    gridOptions.api.hideOverlay();
};

GridUtilidades.prototype.addFooterGrid = function () {
    let instance = this;

    // Footer Result
    if (!$.isEmptyObject(this.gridOptions.optionGridFooter) && (this.gridOptions.alignedGrids != undefined)) {

        // Calculamos altura
        let nAltura = this.gridOptions.optionGridFooter.rowData.length * 38;

        // Creamos Elemento para integrar footer
        let divBottom = $(`<div style="height:${nAltura}px;" class="sipasociados-theme ag-blue"></div>`);

        let gridOptionBottom = $.extend({}, {
            rowModelType: 'inMemory',
            // hide the header on the bottom grid
            pagination: false,
            headerHeight: 0,
            alignedGrids: []
        }, this.gridOptions.optionGridFooter);

        this.gridOptions.alignedGrids.push(gridOptionBottom);
        gridOptionBottom.alignedGrids.push(this.gridOptions);

        // AGREGAMOS PIE RESULTADO
        divBottom.appendTo(this.oSelector);
        divBottom.SIP_AgGrid({url: {}, gridOptions: gridOptionBottom, btn: {}, components: ["productoCellRenderer"]});

    }

    if (this.gridOptions.pagination) {
        // VARIABLES
        const oFooter = this.footerButttons;
        const gridUtilidades = this;
        let hAdd = [];

        // FUNCIONES LOCALES
        const agGridFooterAddBtnPagination = (classBtn, classIcon, themePagination) => {
            let oButton = `<button type="button" class="${themePagination} ${classBtn}" id="ag-${this.simpleSelector}-${classBtn}"> <i class="${classIcon}"></i> </button>`;
            return $(oButton);
        };

        const agGridFooterAddLbPagination = (oData) => {
            let oSpan = $("<span></span>");
            $.each(oData, function (indice, valor) {
                let oSubSpan = $(`<span class="${valor.class}"></span>`);
                oSpan.append(valor.text).append(oSubSpan);
            });
            return oSpan;
        };

        // CONFIGURAR BOTONES
        if (!$.isEmptyObject(oFooter)) {

            // Temas Iconos
            let themeIcon = "btn btn-icon-only btn-default ";
            switch (oFooter.themeIcon) {
                case "circle":
                    themeIcon += "btn-circle";
                    break;
            }

            // Plugins
            if (!$.isEmptyObject(oFooter.plugins)) {
                $.each(oFooter.plugins, function (indice, valor) {
                    let oPlugin = gridUtilidades.agGridFooterAddPlugin(valor, themeIcon);
                    hAdd.push(oPlugin);
                });
            }

            // Add Buttons
            if (!$.isEmptyObject(oFooter.add)) {
                $.each(oFooter.add, function (indice, valor) {

                    let sClass = (!$.isEmptyObject(valor.class)) ? valor.class.join(' ') : "";
                    let initClass = (!$.isEmptyObject(valor.classTheme)) ? valor.classTheme.join(' ') : themeIcon;
                    let sIcon = (valor.icon != undefined) ? valor.icon : "";
                    let sId = (valor.id != undefined) ? valor.id : "";
                    let sTitle = (valor.title != undefined) ? valor.title : "";
                    let sText = (valor.text != undefined) ? valor.text : ""; 
                    let sIcons = (typeof sIcon == "object") ? sIcon.map((value) => `<i class="${value}"></i>&nbsp;`).join(" ") : `<i class="${sIcon}"></i>&nbsp;`;
                    let oBtn = $(`<button type="button" class="${initClass} ${sClass}" title="${sTitle}" id="ag-${gridUtilidades.simpleSelector}-${sId}"> ${sIcons} ${sText} </button>`);

                    // Eventos Botones
                    if(typeof valor.eventClick === "function"){
                        oBtn.click(() => {
                            valor.eventClick( instance.gridOptions, instance.datasource );
                        })
                    }

                    if (sTitle != "") {
                        oBtn.tooltip();
                    }

                    hAdd.push(oBtn);

                });
            }
        }

        // Creamos contenedor
        let oContend = $(`<div class="container-fluid containerFooterGrid"><div class="row rowGrid"><div class="col-sm-4 addBtnGrid"></div> <div class="col-sm-4 text-center addPaginatorGrid"></div> <div class="col-sm-4 text-right addTextGrid"></div> </div></div>`);

        // Agregamos Botones Grilla
        $.each(hAdd, function (indice, campo) {
            campo.appendTo(oContend.find(".addBtnGrid"));
        });

        // PAGINACIÓN
        // Temas Paginación
        let themePagination = "";
        switch (oFooter.themePagination) {
            case "circle":
                themePagination = "btn btn-circle btn-icon-only btn-default";
                break;
            case "square":
                themePagination = "btn btn-icon-only btn-default";
                break;
            default:
                themePagination = "btn btn-icon-only btn-default";
                break;
        }
        let oBtnPagination = {
            btFirst: agGridFooterAddBtnPagination("btFirst", "fas fa-fast-backward", themePagination),
            btPrevious: agGridFooterAddBtnPagination("btPrevious", "fas fa-backward", themePagination),
            lbPagination: agGridFooterAddLbPagination([{text: "Página ", class: "lbCurrent"}, {text: " de ", class: "lbTotal"}]),
            btNext: agGridFooterAddBtnPagination("btNext", "fas fa-forward", themePagination),
            btLast: agGridFooterAddBtnPagination("btLast", "fas fa-fast-forward", themePagination)
        };
        $.each(oBtnPagination, function (indice, campo) {
            campo.appendTo(oContend.find(".addPaginatorGrid"));
        });

        // TEXTO GRILLA
        let oLbDescription = {
            lbDescription: agGridFooterAddLbPagination([{text: "Mostrando fila ", class: "lbFirstRowOnPage"}, {text: " a ", class: "lbLastRowOnPage"}, {text: " de ", class: "lbRecordCount"}])
        };
        $.each(oLbDescription, function (indice, campo) {
            campo.appendTo(oContend.find(".addTextGrid"));
        });

        // RENDERIZAMOS EVENTO
        oContend.appendTo(this.oSelector);

        // EMPEZAMOS EVENTOS PAGINADOR
        let oTotPagination = $.extend({}, oBtnPagination, oLbDescription);
        this.DOM_Pagination = oTotPagination;

        this.activarEventosPaginador();
    }
};

/* ************************************************************************** */
/* EVENTOS CONSTRUIR PAGINADOR                                                             */
/* ************************************************************************** */
GridUtilidades.prototype.agGridFooterAddPlugin = function (sPlugin, sClass) {

    let oPlugin;
    let $oPlugin;
    switch (sPlugin) {
        case "refresh":
            oPlugin = `<button type="button" title="Refrescar" class="${sClass}" id="ag-${this.simpleSelector}-${sPlugin}"> <i class="fas fa-sync-alt"></i> </button>`;
            $oPlugin = $(oPlugin).click((e) => this.refreshGrid(true));
            break;
        case "search":
            oPlugin = `<button type="button" title="Buscar" class="${sClass}" id="ag-${this.simpleSelector}-${sPlugin}"> <i class="fas fa-search"></i> </button>`;
            $oPlugin = $(oPlugin).click((e) => this.searchGrid());
            break;
    }

    return $oPlugin.tooltip();
};




/* ************************************************************************** */
/* EVENTOS GRILLA                                                             */
/* ************************************************************************** */
// Refrescar
GridUtilidades.prototype.refreshGrid = function (bResetFilter) {
    let datasource = this.datasource;
    if (bResetFilter) {
        datasource.filters = [];
        delete datasource.urlGetData.ADD_DATA.filtroTipoBusqueda // Eliminamos filtro busqueda
    }
    this.gridOptions.api.setServerSideDatasource(datasource);
};

// Buscar
GridUtilidades.prototype.searchGrid = function () {
    let instance = this;
    let idModal = `ag-mdl-${this.simpleSelector}-search`;
    let idFrmSearch = `ag-mdl-frm-${this.simpleSelector}-search`;
    let idBtnSearch = `ag-mdl-btn-${this.simpleSelector}-search`;
    let idBtnSearchClear = `ag-mdl-btn-${this.simpleSelector}-search-clear`;
    let mdl = $(`#${idModal}`);

    if (mdl.length == 0) {
        // Create Modal
        mdl = this.modalContent(idModal, "Buscar");
        let mdlCuerpo = mdl.find(".modal-body"); // Selecciona contedor modal
        let mdlFooter = mdl.find(".modal-footer"); // Selecciona contedor modal

        // PLUGIN REFRESH
        const plugin_search = new Ag_Search(idFrmSearch, this.gridOptions);

        // TRAEMOS CONTENIDO
        let oContenedor = plugin_search.getContainerSearch(); // Información Contenedor
        mdlCuerpo.html(oContenedor); // Imprimimos información

        // Implementamos Boton de limpiar busqueda
        let btnClear = $("<button type='button'>").addClass("btn btn-info").attr("id", idBtnSearchClear).append("<i class='fa fa-repeat'></i> Limpiar").appendTo(mdlFooter);
        btnClear.click(() => {
            plugin_search.clearInModal();
            instance.refreshGrid(true);
        });

        // Implementamos Boton de busqueda
        let btnSearch = $("<button type='button'>").addClass("btn btn-primary").attr("id", idBtnSearch).append("<i class='fa fa-search'></i> Buscar").appendTo(mdlFooter);
        btnSearch.click(() => {

            let sendFilterItem = [], sTipoFiltro;
            oContenedor.find(".row").each(function (indice, campo) {
                let oCampos = $(campo).find(":input"); // 0 - Campo, 1 - operador, 2 - valor, 3 - button
                if (indice > 0) { // EVITAR BTNADICIONAR
                    oCampos = $(oCampos);

                    let field = $(oCampos[0]).val();
                    let operator = $(oCampos[1]).val();
                    let value = $(oCampos[2]).val();

                    if (field != "") {
                        sendFilterItem.push({"field": field, "value": value, "operator": operator});
                    }
                }else{
                    sTipoFiltro = $(oCampos[0]).val();
                }
            });
            this.refreshGridSearch(sendFilterItem, sTipoFiltro);

            mdl.modal("hide");
        });
    }

    mdl.modal("show");
};

GridUtilidades.prototype.removeSearchGrid = function () {
    const idModal = `ag-mdl-${this.simpleSelector}-search`;
    $(`#${idModal}`).remove();
};

GridUtilidades.prototype.refreshGridSearch = function (sendFilterItem, sTipoFiltro) {
    let datasource = this.datasource;
    datasource.filters = sendFilterItem;
    datasource.urlGetData.ADD_DATA.filtroTipoBusqueda = sTipoFiltro;
    this.gridOptions.api.setServerSideDatasource(datasource);
};

/**
 * 
 * @param {type} id : ID modal
 * @param {type} sTitleModal : Titulo Modal
 * @param {type} bActiveModal : Si desea activar modal
 * @returns {modalContent.oModal|window.$|$|Element|$.el|window.$.el}
 */
GridUtilidades.prototype.modalContent = (sId, sTitleModal, bActiveModal = true) => {

    let html = `<div id="${sId}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">`;
    html += `<div class="modal-dialog modal-lg">`;
    html += `<div class="modal-content">`;
    html += `<div class="modal-header">`;
    html += `<h5 class="modal-title">` + sTitleModal + `</h4>`;
    html += `<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>`;
    html += `</div>`;
    html += `<div class="modal-body">`;
    html += `</div>`;
    html += `<div class="modal-footer">`;
    html += `<span class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</span>`;
    html += `</div>`;  // content
    html += `</div>`;  // dialog
    html += `</div>`;  // footer
    html += `</div>`;  // modalWindow
    // Implementamos Modal
    let oModal = $(html);

    $('body').append(oModal);

    // Activamos Modal
    if (bActiveModal === true) {
        oModal.modal();
    }

    return oModal;
};

/* ************************************************************************** */
/* PREFERENCIAS GRILLA                                                        */
/* ************************************************************************** */
GridUtilidades.prototype.combinarPreferencias = function (aColumnasGrilla, aPreferencias) {
    
    // iterar sobre cada una de las definiciones de columna, y cuando detecte una preferencia personalizada
    // sobre dicha columna, entonces modificar las preferencias de columna a valores configurados en las preferencias
    let aColumnas = [];
    let aNuevasColumnas = [];
    let aMensaje = [];
    let aEnvio = [];
    let nColumnas = aColumnasGrilla.length;
    let bNotificar = false;
    
    aColumnasGrilla.forEach(function(aCampos, nIndice){
        let bIngreso = false;
        aPreferencias.forEach(function(aPreferencia, nIndicePreferencia){
            if(aCampos.field === aPreferencia.colId){
                aCampos.hide = (aPreferencia.hide === 'true');
                aCampos.pinned = aPreferencia.pinned;
                aCampos.width = parseInt(aPreferencia.width);
                
                bIngreso = true;
                aColumnas[ nIndicePreferencia ] = aCampos;
                return false;
            }
        });
        
        // Si el campo es nuevo se agrega al final
        if(bIngreso === false){
            delete aCampos.width;
            aColumnas[ nColumnas++ ] = aCampos;
            aNuevasColumnas.push(aCampos.headerName);
            bNotificar = true;
        }
    });
    
    // Informamos sobre los campos nuevos
    if(bNotificar){
        let sMensaje = "Los siguientes campos fueron agregados recientemente:";
        sMensaje += "<ol style='margin-left: 20px;'>";
        aNuevasColumnas.forEach(function(sCampo, nIndice){
            sMensaje += `<li><b>${nIndice + 1}. ${sCampo}</b></li>`;
        });
        sMensaje += "</ol>";
        
        oMensaje = {title: "CAMPOS NUEVOS", html: sMensaje, tipo: "info"};
    }
    
    // Reiniciamos los indices
    aEnvio.columnDefs = aColumnas.filter(aData => (aData !== undefined) );
    aEnvio.mensajes = oMensaje;
    aEnvio.guardarPreferencias = bNotificar;
    return aEnvio;
};

GridUtilidades.prototype.borrarPreferencias = function (oPreferencias) {
    alertaCompleta({
        titulo: "Borrar Preferencias", 
        mensaje: "¿Esta seguro de borrar las preferencias de columnas?", 
        error: true,
        tipo: "error", 
        textoBoton: "Si, Borrar!"
    }).then(function(result){
        if (result) {
            $("#loader-wrapper").show();
            oPreferencias.guardarPreferencias([]);
            
            setTimeout(() => {
                location.reload(true);
                $("#loader-wrapper").hide();
            }, 2500);
        }
    });
};

function currencyFormatter(params) {
    let result = "";
    if (!isNaN(params)) {
        result = "$" + formatNumber(Number(params));
    } else if (params != undefined) {
        result = "$" + formatNumber(params.value);
    }
    return result;
}


function formatNumber(number) {
    return Math.floor(number).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}

function implementarLicenciaAgGrid(){
    agGrid.LicenseManager.setLicenseKey("Waplicaciones_SAS_MultiApp_1Devs9_November_2018__MTU0MTcyMTYwMDAwMA==ac35d652df43d26a842363b7e0fbcd48");;
}

/**
 * Esqueleto Modal modales
 * @param {string} sId
 * @param {string} sTitleModal
 * @returns {modalContent.oModal|$|_$|Element|$.el|_$.el}
 */
function modalContent(sId, sTitleModal, sTheme = "default") {

    if($("#" + sId).length > 0){
        return $("#" + sId);
    }else{
        switch(sTheme){
            case "primary": sTheme = "bg-primary"; break;
            case "warning": sTheme = "bg-warning"; break;
            case "danger": sTheme = "bg-danger"; break;
            case "info": sTheme = "bg-info"; break;
            default: sTheme = "bg-default"; break;
        }
    
        let html = `<div id="${sId}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">`;
        html += `<div class="modal-dialog modal-lg">`;
        html += `<div class="modal-content">`;
        html += `<div class="modal-header ${sTheme}">`;
        html += `<h5 class="modal-title">` + sTitleModal + `</h4>`;
        html += `<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>`;
        html += `</div>`;
        html += `<div class="modal-body">`;
        html += `</div>`;
        html += `<div class="modal-footer">`;
        html += `<span class="btn btn-default" data-dismiss="modal">Cerrar</span>`;
        html += `</div>`; // content
        html += `</div>`; // dialog
        html += `</div>`; // footer
        html += `</div>`; // modalWindow
        // Implementamos Modal
        let oModal = $(html);
    
        $("body").append(oModal);
    
        return oModal;
    }
}

/**
 * Evento para reducir envió de peticiones AJAX 
 * @param {string} sUrl Dirección Url para realizar la petición
 * @param {string} oData Datos a enviar
 */
function enviarDatos(sUrl, oData = {}){
    let deferred = jQuery.Deferred();

    $.ajax({
        url: sUrl,
        data: oData,
        type: 'post',
        dataType: "json",
        beforeSend: () => $("#loader-wrapper").show(),
        success: function (response) {
            if(response.estado === undefined || response.estado === 3){
                deferred.reject(response);
            }else{
                deferred.resolve(response);
            }
        },
        error: err => { deferred.reject(err) }
    }).always( () => $("#loader-wrapper").hide() );

    return deferred.promise();
}

export { GridUtilidades, 
    currencyFormatter, 
    implementarLicenciaAgGrid,
    modalContent,
    enviarDatos
}; 