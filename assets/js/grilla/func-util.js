import $ from "jquery";
// import { GridApi } from "ag-grid/main";

function GrillaFuncUtil(grillaPedido = null) {
    if (grillaPedido != null) {
        this.grillaPedido = grillaPedido;
}
}
GrillaFuncUtil.prototype.grillaPedido = {};
GrillaFuncUtil.prototype.gridOptions = {};
GrillaFuncUtil.prototype.KEY_CODES = {
    KEY_UP: 38,
    KEY_DOWN: 40,
    KEY_ENTER: 13,
    contains: function (key) {
        var objProp = Object.keys(this);
        var exists = objProp.reduce((prev, curr) => {
            if (this[curr] == key || this[prev] == key || prev === true) {
                prev = true;
            } else {
                prev = false;
            }
            return prev;
        });
        return exists;
    }
};

GrillaFuncUtil.prototype.navegarUsandoTeclado = function (params) {
    console.log("%conNavigateToNextCell: %o", "color: green;", params);
    var previousCell = params.previousCellDef;
    var suggestedNextCell = params.nextCellDef;
    var KEY_UP = 38;
    var KEY_DOWN = 40;
    var KEY_LEFT = 37;
    var KEY_RIGHT = 39;
    switch (params.key) {
        case KEY_DOWN:
            previousCell = params.previousCellDef;
            // set selected cell on current cell + 1
            this.grillaPedido.gridOptions.api.forEachNode(node => {
                if (previousCell.rowIndex + 1 === node.rowIndex) {
                    node.setSelected(true);
                }
            });
            return suggestedNextCell;
        case KEY_UP:
            previousCell = params.previousCellDef;
            // set selected cell on current cell - 1
            this.grillaPedido.gridOptions.api.forEachNode(node => {
                if (previousCell.rowIndex - 1 === node.rowIndex) {
                    node.setSelected(true);
                }
            });
            return suggestedNextCell;
        case KEY_LEFT:
        case KEY_RIGHT:
            return suggestedNextCell;
        default:
            throw "this will never happen, navigation is always on of the 4 keys above";
    }
};

GrillaFuncUtil.prototype.activarNavegacionEntreCeldasCantidad = function (keyPressedCode) {
    pressedKeyCode = keyPressedCode;
    console.log("code", pressedKeyCode);
    console.log("Key Codes", this.KEY_CODES);
    if (this.KEY_CODES.contains(pressedKeyCode)) {
        var aumentarDisminuir = pressedKeyCode == this.KEY_CODES.KEY_UP ? -1 : 1;
        console.log("editingCell: %o", this.grillaPedido.gridOptions.api);

        if (pressedKeyCode === this.KEY_CODES.KEY_ENTER && app_role === "vendedor") {
            if( $("#capturatxt").is(":visible") ){
                $("#capturatxt").focus();
            }else{
                $("#valor-filtro-lista-desplegable").focus();
            }
        } else {
            var indexFilaAMostrar = this.grillaPedido.gridOptions.api.getFocusedCell().rowIndex + aumentarDisminuir;
            makeEditable(indexFilaAMostrar, "cantidad", this.grillaPedido.gridOptions );
        }
        pressedKeyCode = null;
    }
};

GrillaFuncUtil.prototype.navegarSiguienteTxtboxCantidad = function (event) {
    // verificar si la tecla presionada corresponde a un enter, tecla arriba o tecla abajo, para que se permita
    // el desplazamiento a la siguiente caja de texto para editar la cantidad
    if (
            typeof pressedKeyCode != "undefined" &&
            pressedKeyCode != null &&
            pressedKeyCode == [KEY_CODE_ENTER, KEY_CODE_UP, KEY_CODE_DOWN]
            ) {
        event.api.startEditingCell({
            rowIndex: event.rowIndex + 1,
            colKey: event.column.colId
        });
        pressedKeyCode = null;
    }
};

GrillaFuncUtil.prototype.activarEventosPaginador = function () {
    // Workaround for bug in events order
    if (this.grillaPedido.gridOptions.api) {
        var currentPage = this.grillaPedido.gridOptions.api.paginationGetCurrentPage();
        var maxRowFound = this.grillaPedido.gridOptions.api.paginationIsLastPageFound();
        var rowCount = maxRowFound
                ? this.grillaPedido.gridOptions.api.paginationGetRowCount()
                : null;
        var pageSize = this.grillaPedido.gridOptions.api.paginationGetPageSize();
        var totalPages = this.grillaPedido.gridOptions.api.paginationGetTotalPages();
        var startRow =
                pageSize * this.grillaPedido.gridOptions.api.paginationGetCurrentPage() +
                1;
        var endRow = startRow + pageSize - 1;
        if (maxRowFound && endRow > rowCount) {
            endRow = rowCount;
        }
        if (totalPages != 0) {
            if (currentPage == 0) {
                $("[ref='btFirst']")
                        .prop("disabled", true)
                        .removeClass("btn-danger");
                $("[ref='btPrevious']")
                        .prop("disabled", true)
                        .removeClass("btn-danger");
            } else {
                $("[ref='btFirst']")
                        .prop("disabled", false)
                        .addClass("btn-danger");
                $("[ref='btPrevious']")
                        .prop("disabled", false)
                        .addClass("btn-danger");
            }

            if (totalPages == 1) {
                $("[ref='btNext']")
                        .prop("disabled", true)
                        .removeClass("btn-danger");
                $("[ref='btLast']")
                        .prop("disabled", true)
                        .removeClass("btn-danger");
            } else {
                $("[ref='btNext']")
                        .prop("disabled", false)
                        .addClass("btn-danger");
                $("[ref='btLast']")
                        .prop("disabled", false)
                        .addClass("btn-danger");
            }

            $(".paginator.ag-paging-panel >:first-child").css("visibility", "show");
            $("[ref='lbCurrent']").text(currentPage + 1);
            $("[ref='lbTotal']").text(totalPages);
            $("[ref='lbFirstRowOnPage']").text(formatNumber(startRow));
            $("[ref='lbLastRowOnPage']").text(formatNumber(endRow));
            $("[ref='lbRecordCount']").text(formatNumber(rowCount));
            // add events to buttons
            if (
                    !$("[ref='btFirst']").hasClass("eventsAdded") &&
                    this.grillaPedido.gridOptions.api.paginationProxy
                    ) {
                $("[ref='btFirst']").click(e =>
                    this.grillaPedido.gridOptions.api.paginationGoToFirstPage()
                );
                $("[ref='btPrevious']").click(e =>
                    this.grillaPedido.gridOptions.api.paginationGoToPreviousPage()
                );
                $("[ref='btNext']").click(e =>
                    this.grillaPedido.gridOptions.api.paginationGoToNextPage()
                );
                $("[ref='btLast']").click(e =>
                    this.grillaPedido.gridOptions.api.paginationGoToLastPage()
                );
                $("[ref='btFirst']").addClass("eventsAdded");
            }
        } else {
            $(".paginator.ag-paging-panel >:first-child").css("visibility", "hidden");
        }
    }
};

/**
 *
 * @param {any} e Evento con descripcon de cambios aplicados sobre la celda
 * @param {string} urlActualizacionParcial URL REST API en donde se aplican los cambios parciales
 * @return {Promise} Promise apenas se cumpla la actualizacion
 */
GrillaFuncUtil.prototype.actualizacionParcialPedido = function (
        e,
        urlActualizacionParcial
        ) {
    var valorAnterior = e.oldValue;
    var valorNuevo = e.newValue;
    let seccion = getSeccionUrl();
    let deferred = jQuery.Deferred();
    let promiseRta = jQuery.Deferred().resolve(true);
    if ((valorAnterior != null && valorNuevo == null) || valorNuevo != null) {
        if (valorAnterior != valorNuevo) {
            // Validación obsequios
            if (seccion == 7) {
                console.log(valorNuevo);
                if (
                        valorNuevo < e.data.unidadesSolicitadas &&
                        valorNuevo != "" &&
                        valorNuevo != null
                        ) {
                    return deferred.reject({
                        estado: 3,
                        titulo: "",
                        descripcion: `El producto que intenta registrar requiere mínimo ${
                                e.data.unidadesSolicitadas
                                } Unidades.`
                    });
                }
            }
            // End Validación Obsequios

            var material = e.data.material;
            var lote = e.data.lote;
            let cantidad = valorNuevo;
            var idDescPedidoProv = e.data.idProductoPedido;
            var codigoTransferencia = e.data.consecutivoPedido;

            promiseRta = actualizacionParcialProducto(
                    urlActualizacionParcial,
                    material,
                    lote,
                    cantidad,
                    idDescPedidoProv,
                    codigoTransferencia
                    );

            /*$.ajax({
             url: urlActualizacionParcial,
             type: 'POST',
             dataType: "json",
             data: { material: material, lote: lote, cantidad: cantidad, idPedidoDescProv: idDescPedidoProv, codigoPedido: codigoTransferencia },
             beforeSend: () => $(".estadoAccion").html('Esperando Servidor de Coopidrogas... <i class="icon-spinner icon-spin"></i>'),
             success: function (response) {
             var estadoPeticion = (response.length == 3) ? response[1] : response[0];
             var infoPedido = (response.length == 3) ? response[2] : response[1];
             if (!infoPedido.hasOwnProperty("totalPedido")) {
             infoPedido = response[1];
             }
             if (!estadoPeticion.hasOwnProperty("descripcion")) {
             estadoPeticion = response[0];
             }
             actualizarTotales(infoPedido.totalPedido.totalProductos, infoPedido.totalPedido.productosTotales, infoPedido.totalPedido.total);
             actualizarCampoStatus(estadoPeticion);
             deferred.resolve(response);
             },
             error: err => {
             deferred.reject(err);
             }
             });*/
        }
    }
    return promiseRta;
    //return response;
};

GrillaFuncUtil.prototype.adjustGridHeight = function (gridDiv) {
    var totalWindowHeight = window.innerHeight;
    // ajustar inicialmente el alto al 76%
    //var gridNewHeight = totalWindowHeight * 0.75;
    //gridDiv.style.height = `${gridNewHeight}px`;

    var totalPageHeight = document.getElementsByTagName("body")[0].scrollHeight;
    var diffHeights = totalPageHeight - totalWindowHeight;
    var heightOtherElements = 0;
    $("body").children(":visible").each(function () {
        if (this != gridDiv) {
            let heightCurrentElement = $(this).outerHeight(true);
            // si la altura del elemento es cero, sumar la altura maxima de sus hijos
            if (heightCurrentElement == 0) {
                $(this).children(":visible").each(function () {
                    if (heightCurrentElement < $(this).height()) {
                        heightCurrentElement = $(this).height();
                    }
                });
            }
            heightOtherElements += heightCurrentElement;
        }
    });
    var gridNewHeight = totalWindowHeight - heightOtherElements;
    gridNewHeight = gridNewHeight * 1;
    gridDiv.style.height = `${gridNewHeight}px`;
};

/**
 * Oculta o muestra las columnas en estado "pinned" (fijas), de acuerdo a la resolucoin de la pantalla
 * @param {gridOptions} gridOptions Objeto gridOptions sobre el cual se obtiene la info de la grilla
 */
GrillaFuncUtil.prototype.adjustPinnedColumns = function (gridOptions) {
    var totalWindowWidth = window.screen.availWidth;
    const LIMITE_PERMITIDO = 680;
    // si el ancho de la pantalla ya es muy reducido, pierde todo el sentido dejar columnas fijas, por tanto se retiran y se deja unicamente la columna de cantidad
    if (
            totalWindowWidth < LIMITE_PERMITIDO &&
            gridOptions.columnApi.isPinningLeft()
            ) {
        gridOptions.previousLeftPinnedCols = gridOptions.columnApi
                .getDisplayedLeftColumns()
                .map(col => col.colId);
        gridOptions.previousRightPinnedCols = gridOptions.columnApi
                .getDisplayedRightColumns()
                .map(col => col.colId);
        // omitir la columna de cantidad, que siempre debe ser visible
        let pinnedColumnsDiffQuantity = gridOptions.previousRightPinnedCols.filter(
                colId => colId != "cantidad"
        );
        gridOptions.columnApi.setColumnsPinned(
                gridOptions.previousLeftPinnedCols,
                null
                );
        gridOptions.columnApi.setColumnsPinned(pinnedColumnsDiffQuantity, null);
    } else if (gridOptions.previousLeftPinnedCols.length > 0) {
        gridOptions.columnApi.setColumnsPinned(
                gridOptions.previousLeftPinnedCols,
                "left"
                );
        gridOptions.columnApi.setColumnsPinned(
                gridOptions.previousRightPinnedCols,
                "right"
                );
    }
};

/**
 * Ajusta el ancho de las columnas a su contenido
 */
GrillaFuncUtil.prototype.adjustWidth = function (gridOptions) {
    let visibleColumns = gridOptions.columnApi
            .getAllDisplayedColumns()
            .filter(el => el.colId.indexOf("cantidad") == -1)
            .map(el => el.colId);
    gridOptions.columnApi.autoSizeColumns(visibleColumns);
};

function makeEditable(indexFilaAMostrar, columnKey, gridOptions) {
    var indexFirstRow =
            gridOptions.api.paginationGetCurrentPage() *
            gridOptions.api.paginationGetPageSize();
    var indexLastRow =
            (gridOptions.api.paginationGetCurrentPage() + 1) *
            gridOptions.api.paginationGetPageSize() -
            1;
    if (indexFilaAMostrar < indexFirstRow) {
        indexFilaAMostrar = indexLastRow;
    } else if (indexFilaAMostrar > indexLastRow) {
        indexFilaAMostrar = indexFirstRow;
    }
    gridOptions.api.stopEditing();
    gridOptions.api.forEachNode(node => {
        if (node.data != undefined && indexFilaAMostrar === node.rowIndex) {
            node.setSelected(true);
        }
    });
    //gridOptions.api.ensureIndexVisible(indexFilaAMostrar);
    gridOptions.api.setFocusedCell(indexFilaAMostrar, columnKey);
    gridOptions.api.startEditingCell({
        rowIndex: indexFilaAMostrar,
        colKey: columnKey
    });
    if ([indexFirstRow, indexLastRow].indexOf(indexFilaAMostrar) != -1) {
        setTimeout(function () {
            gridOptions.api.startEditingCell({
                rowIndex: indexFilaAMostrar,
                colKey: columnKey
            });
        }, 100);
    }
}

var timeoutCuentaAtras = null;
var pressedKeyCode = null;

var actualizarCampoStatus = function (estadoPeticion) {
    var tiempoMaxEnSg = 3;
    var msgAMostrar = "¡Actualizado!";
    var iconoMsgMostrar = "fa-bookmark";
    var msgDesdeServer = estadoPeticion.descripcion;
    const ESTADO_OK = 1;
    var classTypeMsg = estadoPeticion.estado != ESTADO_OK ? "text-danger" : "text-success";
    if (msgDesdeServer.indexOf("Eliminado") != -1) {
        msgAMostrar = "¡Eliminado!";
        iconoMsgMostrar = "fa-trash";
    } else if (estadoPeticion.titulo.indexOf("Imposible guardar") != -1) {
        msgAMostrar = `¡Error! ${estadoPeticion.descripcion}`;
        iconoMsgMostrar = "fa-exclamation";
    }

    if (timeoutCuentaAtras != null){
        clearTimeout(timeoutCuentaAtras);
    }
    $(".estadoAccion").hide();
    $(".estadoAccion").html(
        `<a class='${classTypeMsg} server-info'><i class="fa ${iconoMsgMostrar}" title='${msgDesdeServer}' data-toggle="tooltip" data-placement="top"></i> ${msgAMostrar} <span class='timer'>${tiempoMaxEnSg}s</span></a>`
    );
    $('[data-toggle="tooltip"]').tooltip();
    $(".estadoAccion").show();
    var actualizarTimer = function (tiempo) {
        tiempo = tiempo - 1;
        $(".estadoAccion a .timer").text(`${tiempo}s`);
        if (tiempo > 0) {
            timeoutCuentaAtras = setTimeout(() => actualizarTimer(tiempo), 1000);
        } else {
            $(".estadoAccion a").addClass("hide-server-info");
            timeoutCuentaAtras = setTimeout(() => {
                $(".estadoAccion").hide();
                $(".estadoAccion").html("");
            }, 2000);
        }
    };
    timeoutCuentaAtras = setTimeout(() => actualizarTimer(tiempoMaxEnSg), 1000);
};

/**
 * Muestra el modal del estado de una accion en la grilla, que se encuentra configurado en la plantilla twig asociadoGlobales/modalesInformativas.html.twig
 * @param {string} title Titulo de la ventana modal
 * @param {string} body  Contenido de la ventana modal
 */
var mostrarModal = function (title, body) {
    $("#modal-estado-accion .modal-title").html(title);
    $("#modal-estado-accion .modal-body").html(body);
    $("#modal-estado-accion").modal("show");
};

var actualizarTotales = function (totalItemsDisp, totalItems, vrTotalPedido, vrTotalPie) {
    $(".totalItemsDisp").text(formatNumber(totalItemsDisp));
    $(".totalItems").text(formatNumber(totalItems));
    let cupo = $(".cupows").html();
    let cupoDisponible = cupo - vrTotalPedido;
    $(".cupodisponible").html(currencyFormatter(cupoDisponible));
    $(".totalPedidoPag").text(currencyFormatter(vrTotalPedido));

    if (vrTotalPie != "" && vrTotalPie != undefined) {
        $(".totalPedidoPag").eq(1).text(currencyFormatter(vrTotalPie));
    }

    // si el total de items disponible no es igual al total de items solicitados, entonces mostrar alerta tipo warning
    if (totalItemsDisp != totalItems) {
        $(".warn-prod-no-disp").removeClass("hidden").tooltip();
        $(".warn-prod-no-disp,.totalItemsDisp").addClass("text-warning");
    } else {
        $(".warn-prod-no-disp").addClass("hidden");
        $(".warn-prod-no-disp,.totalItemsDisp").removeClass("text-warning");
    }

    // EVENTOS BOTONES CABECERA
    console.log( formatNumber(totalItems) );
    if(formatNumber(totalItems) == 0){
        $("#cambioPedido").prop("disable", false).show();
    }else{
        $("#cambioPedido").prop("disable", true).hide();
    }

};

function currencyFormatter(params) {
    let result = "";
    if (!isNaN(params)) {
        result = "$" + formatNumber(Number(params));
    } else if (params != undefined) {
        result = "$" + formatNumber(params.value);
    }
    return result;
}

function formatNumber(number) {
    return Math.floor(number)
            .toString()
            .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}

// cualquier valor ingresado en la celda, lo convierte a numero
function numberParser(params) {
    if (params.data != undefined) {
        var valorAnterior = params.oldValue;
        var valorNuevo =
                isNaN(params.newValue) || params.newValue == null || params.newValue == ""
                ? null
                : Number(params.newValue);
        valorNuevo = valorNuevo == 0 ? null : valorNuevo;
        params.data.cantidad = valorNuevo;
        if (valorNuevo != null || (valorAnterior != null && valorNuevo == null)) {
            params.data.subtotal = params.data.cantidad * params.data.corriente;
        }
        // validar si la cantidad solicitada es mayor a la cantidad maxima permitida, entonces dejar el valor a la cantidad maxima
        let cantidadMaxPedido = params.data.maximoXPedido;
        if (cantidadMaxPedido > 0 && valorNuevo > cantidadMaxPedido) {
            params.data.cantidad = cantidadMaxPedido;
            let nombreProducto = params.data.producto;
            mostrarModal(
                    "Actualizado con novedades",
                    `El producto <i>${nombreProducto}</i>, tiene permitido un máximo de [${cantidadMaxPedido}] unidades por pedido, de esta manera configuramos el valor ingresado al máximo permitido y lo registramos en el pedido.`
                    );
        }
        return true;
    }
    return false;
}
// para las celdas que son de tipo fecha
function dateTimeFormatter(params) {
    let result = "";
    if (!isNaN(params)) {
        result = moment(params).format("YYYY-MM-DD");
    } else if (params != undefined) {
        result = moment(params.date).format("YYYY-MM-DD");
    }
    return result;
}

/**
 * Establece parametros para el encabezado de la grilla
 * @param {object} data
 * @param {GrillaPedido} grillaPedido
 * @returns {object}
 */
function setGridHeader(data, grillaPedido) {
    let oHeader = data.map(colDef => {
        colDef.headerClass = "sipasociados-theme";
        // hacer set del editor de columnas para la columna de "cantidad"
        if (typeof colDef.editable != "undefined") {
            colDef.editable = grillaPedido.detectarSiEditaCantidad;
            colDef.cellEditor = "numericCellEditor";
        }
        return colDef;
    });
    return oHeader;
}

/**
 * Retorna el tipo de seccion de acuerdo al que se encuentra por URL (ej: /catalogo/1 retorna 1 como el tipo de seccion)
 * @return {string|number} Tipo de seccion
 */
function getSeccionUrl() {
    let url = window.location.pathname.split("/");
    let tipoSeccion = url[url.length - 1];
    let sSeccion = tipoSeccion !== "catalogo" ? tipoSeccion : 1;
    return sSeccion;
}

/**
 * Agrega una nueva fila cuando  en un producto existe un obsequio asociado y se detecta que la cantidad
 * ingresada en la columna de cantidad es igual o superior a la parametrizada para el obsequio
 * @param {Event} event
 */
function agregarFilaSiHayObsequio(event, datasource) {
    let data = event.data;
    if (data.idBonificacion != null) {
        if (event.newValue < data.unidadesSolicitadas)
            event.api.purgeInfiniteCache();
        if (event.newValue >= data.unidadesSolicitadas) {
            agregarFilaObsequio(event.rowIndex, event.data, event.api);
        }
    }
}

/**
 * Agrega una fila de obsequio, despues de la fila indicada por rowIndex
 * @param {number} rowIndex
 * @param {object} data
 * @param {GridApi} gridApi
 */
function agregarFilaObsequio(rowIndex, data, gridApi, agregar = true) {
    // configurar en que # fila se va a insertar
    let idFilaInsertar = Number(rowIndex) + 1;
    let deferred = jQuery.Deferred();
    var rowNodeCurrPos = gridApi.getDisplayedRowAtIndex(idFilaInsertar);
    if ( rowNodeCurrPos.data == undefined || (rowNodeCurrPos != null && rowNodeCurrPos.data.producto.indexOf("OBS") == -1) ){
        // traer la informacion del obsequio desde AJAX
        $.ajax({
            url: ApiRestURLS.GET_DETALLE_OBSEQUIO + `/${data.idBonificacion}`,
            type: "GET",
            success: function (infoBonificacion) {
                let dataObsequio = $.extend({}, data);
                // crear fila de datos para el nuevo producto (es decir el obsequio)
                for (let prop in dataObsequio) {
                    dataObsequio[prop] = "";
                }

                // 2018-04-17 - Anderson Barbosa - Aplicar cantidad de regalos
                let nCantidad = parseInt(data.cantidad);
                let nCantidadObsequios = parseInt(infoBonificacion.cantidad1);
                let nUnidadSoilicitada = parseInt(infoBonificacion.uniSol);

                // calcular cuantos obsequios se va a adquirir
                let noObsequios = (nCantidad - nCantidad % nUnidadSoilicitada) / nUnidadSoilicitada * nCantidadObsequios;
                if (isNaN(noObsequios)) {
                    noObsequios = 0;
                }

                $.extend(dataObsequio, {
                    idBonificacion: null,
                    producto: infoBonificacion.obsequio,
                    material: infoBonificacion.codigoObsequio,
                    codigoBarras: infoBonificacion.codigoObsequio,
                    disp: infoBonificacion.disponibilidad,
                    proveedor: infoBonificacion.proveedor,
                    cantidad: noObsequios
                });

                // insertar la data
                if (agregar) {
                    gridApi.updateRowData({
                        add: [dataObsequio],
                        addIndex: this.idFilaInsertar
                    });
                } else {
                    dataObsequio.id = this.idFilaInsertar;
                    $(`.fila-obsequio[row-index='${idFilaInsertar - 1}'] div[col-id='cantidad']`).text(noObsequios);
                }
                // si la cantidad de obsequios es cero, entonces colocar la fila en gris, debido a que la grilla no deja retirar la fila
                if (noObsequios == 0) {
                    //$(`row-index='${idFilaInsertar - 1}'`).css("background-color", "");
                }
                deferred.resolve();
            }.bind({idFilaInsertar: idFilaInsertar})
        });
    } else {
        deferred.resolve();
    }
    return deferred.promise();
}

function getVariablesTwig() {
    // extraer las URL generadas desde TWIG (archivo routes.yaml)
    let attrData = document.querySelector(".twig-vars").attributes;
    let twigVars = {};
    for (let i = 0; i < attrData.length; i++) {
        const element = attrData[i];
        if (element.nodeName.startsWith("data")) {
            let twigVarName = element.nodeName.replace("data-", "");
            twigVarName = twigVarName.split("-").reduce((prev, curr) => {
                prev += curr[0].toUpperCase() + curr.substring(1);
                return prev;
            }, "");
            twigVars[twigVarName] = element.textContent;
        }
    }
    return twigVars;
}

function actualizacionParcialProducto(
        urlActualizacionParcial,
        material,
        lote,
        cantidad,
        idPedidoDescProv,
        codigoPedido
        ) {
    let deferred = jQuery.Deferred();
    let plazoFiltro = $("#Plazos").length > 0 ? $("#Plazos").val() : "";

    $.ajax({
        url: urlActualizacionParcial,
        type: "POST",
        dataType: "json",
        data: {
            material: material,
            lote: lote,
            cantidad: cantidad,
            idPedidoDescProv: idPedidoDescProv,
            codigoPedido: codigoPedido,
            plazoFiltro: plazoFiltro
        },
        beforeSend: () =>
            $(".estadoAccion").html(
                    'Esperando Servidor de Coopidrogas... <i class="icon-spinner icon-spin"></i>'
                    ),
        success: function (response) {
            var estadoPeticion = response.length == 3 ? response[1] : response[0];
            var infoPedido = response.length == 3 ? response[2] : response[1];
            if (infoPedido != undefined) {
                if (!infoPedido.hasOwnProperty("totalPedido")) {
                    infoPedido = response[1];
                }
                //                actualizarTotales(infoPedido.totalPedido.totalProductos, infoPedido.totalPedido.productosTotales, infoPedido.totalPedido.total, infoPedido.cupo);
                actualizarTotales(
                        infoPedido.totalPedido.productos,
                        infoPedido.totalPedido.productosTotales,
                        infoPedido.totalPedido.total,
                        infoPedido.totalPie
                        );
            }
            if (!estadoPeticion.hasOwnProperty("descripcion")) {
                estadoPeticion = response[0];
            }
            actualizarCampoStatus(estadoPeticion);
            if (estadoPeticion.estado == 3) {
                deferred.reject(estadoPeticion);
            } else {
                deferred.resolve(response);
            }
        },
        error: err => {
            deferred.reject(err);
        }
    });

    return deferred.promise();
}

/**
 * Este evento imprime los errores despues de solicitar producto
 * @param {object} response Respuesta con algun tipo de error que arrojo el servidor
 */
function imprimirErroresServidor(response, event = {}) {
    // ERRORES DEL SERVIDOR
    if (response.estado != undefined) {
        swal({
            title: response.titulo,
            html: response.descripcion,
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#E72B37",
            confirmButtonText: "ACEPTAR"
        }).then(() => {
            let nextRowIndex = event.rowIndex;
            event.api.startEditingCell({
                rowIndex: nextRowIndex,
                colKey: "cantidad"
            });
        });
    }
    // OTROS ERRORES
    if (event.newValue > 0) {
        event.data.cantidad = event.oldValue;
        event.node.setData(event.data);
}
}

/**
 * Esqueleto Modal modales
 * @param {string} sId
 * @param {string} sTitleModal
 * @returns {modalContent.oModal|$|_$|Element|$.el|_$.el}
 */
function modalContent(sId, sTitleModal) {
    let html = `<div id="${sId}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">`;
    html += `<div class="modal-dialog modal-lg">`;
    html += `<div class="modal-content">`;
    html += `<div class="modal-header">`;
    html += `<a class="close" data-dismiss="modal">×</a>`;
    html += `<h4>` + sTitleModal + `</h4>`;
    html += `</div>`;
    html += `<div class="modal-body">`;
    html += `</div>`;
    html += `<div class="modal-footer">`;
    html += `<span class="btn btn-default" data-dismiss="modal">Cerrar</span>`;
    html += `</div>`; // content
    html += `</div>`; // dialog
    html += `</div>`; // footer
    html += `</div>`; // modalWindow
    // Implementamos Modal
    let oModal = $(html);

    $("body").append(oModal);

    return oModal;
}

export {
GrillaFuncUtil,
        currencyFormatter,
        dateTimeFormatter,
        makeEditable,
        formatNumber,
        numberParser,
        actualizarTotales,
        getSeccionUrl,
        actualizarCampoStatus,
        agregarFilaSiHayObsequio,
        agregarFilaObsequio,
        actualizacionParcialProducto,
        getVariablesTwig,
        modalContent,
        imprimirErroresServidor,
        setGridHeader
};
