"use strict";

require("../../css/usuarios/index.css");

require("../grilla/jquery-aggrid");

var _agCellRender = require("../grilla/ag-plugins/agCellRender");

var _mdlCrud = _interopRequireDefault(require("./mdlCrud"));

var _mdlPermisos = _interopRequireDefault(require("./mdlPermisos"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// Importar - Css
// Importar - Javascrip

/* --- --- --- Variables Globales --- --- --- */
var oMdlCrud = new _mdlCrud["default"]();
var oMdlPermisos = new _mdlPermisos["default"](); // Configuracion Tabla

var _urlGetData = {
  GRILLA_LISTAR_JSON: ApiRestURLS.listUsuarios,
  ORDER_SORD: "DESC",
  ORDER_SIDX: "id"
};
var _gridOption = {
  columnDefs: aDfColumnas,
  rowSelection: 'single',
  headerHeight: 34,
  pagination: true,
  enableSorting: true,
  enableServerSideSorting: true,
  enableColResize: true,
  suppressPaginationPanel: true,
  components: {
    'agCellRender': _agCellRender.AgCellRender
  }
};
var _btn = {
  'themeIcon': 'square',
  'plugins': ['refresh', 'search'],
  'add': aGridButtons
}; // Iniciamos Tabla

oGrid = $("#myGrid").SIP_AgGrid({
  url: _urlGetData,
  gridOptions: _gridOption,
  btn: _btn,
  autoSize: false,
  autoHeight: true,
  paginador: true
});
$(function () {
  // Clear Cache
  $.ajaxSetup({
    cache: false
  }); // Crear Registro

  $('#myGrid').on('click', '#ag-myGrid-nuevo-registro', function () {
    $('#loading').show();
    $('.modal-dialog').addClass('modal-lg');
    $('#tituloModalGlobal').html('Nuevo Usuario <i class="fas fa-user"></i>');
    $('#contenidoModalGlobal').empty().load(ApiRestURLS.userNew, function () {
      $('#modalGlobal').modal({
        backdrop: true,
        keyboard: false
      });
      $('#modalGlobal').modal('show');
      oMdlCrud.init();
      $('#loading').hide();
    });
  }); // Editar Registro

  $('#myGrid').on('click', '#ag-myGrid-editar-registro', function () {
    var selectedRows = oGrid.getGridOptions().api.getSelectedRows();

    if (selectedRows.length > 0) {
      $('#loading').show();
      $('.modal-dialog').addClass('modal-lg'); // Titulo Modal

      $('#tituloModalGlobal').html('Editar Usuario <i class="fas fa-user"></i>&nbsp&nbsp' + selectedRows[0].nombre);
      $('#contenidoModalGlobal').empty().load(ApiRestURLS.userEdit, {
        idUser: selectedRows[0].id
      }, function () {
        $('#modalGlobal').modal({
          backdrop: true,
          keyboard: false
        });
        $('#modalGlobal').modal('show');
        oMdlCrud.init();
        $('#loading').hide();
      });
    } else {
      Swal.fire({
        icon: 'warning',
        title: 'Por favor seleccione una fila.',
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 6000,
        timerProgressBar: true,
        onOpen: function onOpen(toast) {
          toast.addEventListener('mouseenter', Swal.stopTimer);
          toast.addEventListener('mouseleave', Swal.resumeTimer);
        }
      });
    }
  }); // Eliminar Registro

  $('#myGrid').on('click', '#ag-myGrid-eliminar-registro', function () {
    var selectedRows = oGrid.getGridOptions().api.getSelectedRows();

    if (selectedRows.length > 0) {
      Swal.fire({
        title: '¿Realmente desea eliminar ?',
        html: "<b class=\"text-primary\">Al usuario:</b> <b class=\"text-info\">".concat(selectedRows[0], "</b><br>"),
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar'
      }).then(function (result) {
        if (result.isConfirmed) {
          $.ajax({
            url: ApiRestURLS.userDelete,
            data: {
              idUser: selectedRows[0].id
            },
            type: 'post',
            beforeSend: function beforeSend() {
              $('#loading').show();
            },
            success: function success(data) {
              $('#loading').hide();
              Swal.fire({
                icon: data.status == 1 ? 'success' : 'warning',
                title: data.message,
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: data.status == 1 ? 6000 : 8000,
                timerProgressBar: true,
                onOpen: function onOpen(toast) {
                  toast.addEventListener('mouseenter', Swal.stopTimer);
                  toast.addEventListener('mouseleave', Swal.resumeTimer);
                }
              });

              if (data.status == 1) {
                oGrid.refreshGrid();
              }
            },
            error: function error(data) {
              $('#loading').hide();
              alertaSimple({
                mensaje: data.responseJSON.msg,
                tipo: 'warning',
                toast: true,
                position: 'top-end',
                mostrarBoton: false,
                tiempo: 6000
              });
            }
          });
        }
      });
    } else {
      Swal.fire({
        icon: 'warning',
        title: 'Por favor seleccione una fila.',
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 6000,
        timerProgressBar: true,
        onOpen: function onOpen(toast) {
          toast.addEventListener('mouseenter', Swal.stopTimer);
          toast.addEventListener('mouseleave', Swal.resumeTimer);
        }
      });
    }

    return false;
  }); // Editar Permisos Registro

  $('#myGrid').on('click', '.btnGrillaPermisos', function () {
    var selectedRows = oGrid.getGridOptions().api.getSelectedRows();
    $('#loading').show();
    $('.modal-dialog').addClass('modal-lg'); // Titulo Modal

    $('#tituloModalGlobal').html('Editar permisos usuario&nbsp;' + selectedRows[0].nombre + '&nbsp;<i class="fas fa-user"></i>');
    $('#contenidoModalGlobal').empty().load(ApiRestURLS.userPermisos, {
      idUser: selectedRows[0].id
    }, function () {
      $('#modalGlobal').modal({
        backdrop: true,
        keyboard: false
      });
      $('#modalGlobal').modal('show');
      oMdlPermisos.init();
      $('#loading').hide();
    });
  }); // Loading Hide

  $("#loading").hide();
});