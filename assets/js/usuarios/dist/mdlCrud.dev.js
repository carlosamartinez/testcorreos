"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var MdlCrud =
/*#__PURE__*/
function () {
  function MdlCrud() {
    _classCallCheck(this, MdlCrud);
  }

  _createClass(MdlCrud, [{
    key: "init",
    value: function init() {
      $('#formUser').submit(function () {
        $.ajax({
          url: ApiRestURLS.mdlCrud,
          data: $('#formUser').serialize(),
          type: 'post',
          beforeSend: function beforeSend() {
            $('#loading').show();
          },
          success: function success(data) {
            $('#loading').hide();
            Swal.fire({
              icon: data.status == 1 ? 'success' : 'warning',
              title: data.message,
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 6000,
              timerProgressBar: true,
              onOpen: function onOpen(toast) {
                toast.addEventListener('mouseenter', Swal.stopTimer);
                toast.addEventListener('mouseleave', Swal.resumeTimer);
              }
            });

            if (data.status == 1) {
              $('#modalGlobal .close').click();
              oGrid.refreshGrid();
            }
          },
          error: function error(data) {
            $('#loading').hide();
          }
        });
        return false;
      });
    }
  }]);

  return MdlCrud;
}();

exports["default"] = MdlCrud;