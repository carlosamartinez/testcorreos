// Importar - Css
import '../../css/usuarios/index.css';
// Importar - Javascrip
import '../grilla/jquery-aggrid';
import { AgCellRender } from '../grilla/ag-plugins/agCellRender';
import MdlCrud from "./mdlCrud";
import MdlPermisos from "./mdlPermisos";

/* --- --- --- Variables Globales --- --- --- */

$(function () {
  // Clear Cache
  $.ajaxSetup({ cache: false });


  $('#formregister').submit(function (event) {
    event.preventDefault(event);


    if ($("#InputPassword1").val() !== $("#InputPassword2").val()) {

      $("#InputPassword2").addClass("border border-danger");
      //$("#InputPassword2").css("text-align","left");
      
      Swal.fire({
        icon: 'danger',
        title: "Las contraseñas deben coincidir",
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 2000,
        timerProgressBar: true
      });
      return;
    }

    let datos = {
      "Nombre": $("#InputNombre").val(),
      "Password": $("#InputPassword1").val(),
      "Email": $("#InputEmail").val(),
    };

    $.ajax({
      url: register,
      type: 'post',
      data: datos,
      success: function (data) {
        if (data == "") {
          Swal.fire({
            icon: 'success',
            title: "Te has registrado exitosamente, se te ha enviado un correo con tus credenciales",
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true
          });
        }
      }, 
      error: function (request, status, error) {
        
        
        if (status == "error") {
          Swal.fire({
            icon: 'warning',
            title: error,
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true
          });
        } 
      }
    });

  });

  // Loading Hide
  $("#loading").hide();
});