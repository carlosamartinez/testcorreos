// Importar - Css
import '../../css/usuarios/index.css';
// Importar - Javascrip
import '../grilla/jquery-aggrid';

$(function () {


    //Se te ha enviado un correo con un link con el cual podras reestablecer tu contraseña
  $.ajaxSetup({ cache: false });
  $("#btn-iniciarses").click(function(){
    window.location.href = "/";
  });
$("#sendMail").click(function(e){
    e.preventDefault();
    $('#loading').show();
    $.ajax({

        //url: saveNewPassUrl+"?u="+getUrlParameter('u'),
        url: forgotPassword,
        type: "get",
        data: {
            'email':$("#reset_password_request_form_email").val()
        },
        beforeSend: function(){ 

        },
        success: function(data){
            $("#loading").hide();

            if(data == ""){
                $("#loading").hide();
                Swal.fire({
                    icon: 'danger' ,
                    title: 'Ya se ha enviado un correo a esta dirección, revisa la carpeta spam o espera una hora para volver a enviar un correo',
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 9000 ,
                    timerProgressBar: true
                });
            }else if( data == 2){
                $("#loading").hide();
                Swal.fire({
                    icon: 'warning' ,
                    title: 'Usuario no existe.',
                    toast: true,
                    position: 'top-end',    
                    showConfirmButton: false,
                    timer: 5000 ,
                    timerProgressBar: true
                });

            }else if( data == 409){
                $("#loading").hide();
                Swal.fire({
                    icon: 'warning' ,
                    title: 'Ya se ha enviado un correo a esta dirección, revisa tu bandeja de entrada',
                    toast: true,
                    position: 'top-end',    
                    showConfirmButton: false,
                    timer: 5000 ,
                    timerProgressBar: true
                });

            }else{ 
                $("#loading").hide();
                Swal.fire({
                    icon: 'success' ,
                    title: 'Se te ha enviado un correo con un link con el cual podras reestablecer tu contraseña.',
                    toast: true,
                    position: 'top-end',    
                    showConfirmButton: false,
                    timer: 5000 ,
                    timerProgressBar: true
                });

            }

        }
      });
    });


  // Loading Hide
  //$("#loading").hide();

  $("#loading").hide();
});