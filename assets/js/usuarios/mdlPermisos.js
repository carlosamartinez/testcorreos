export default class MdlPermisos {

  constructor(){}
  init(){
     
    var previousId = "";
    
    var openProperties = {
        "position": "relative ",
        "width": "99% ",
        "transform": "translate3d(0px, 0px, 0px)",
        "will-change": "transform",
        "display": "block",
        "margin": "0.125rem 0 0",
     };

    $(".dropdown-custom-btn").on('click', function (e) {
        if( previousId != $(this).parent().find("div[id*='item-no']").attr("id") && previousId != "" ){
            $("#"+previousId).css({
                "display": "none"
             })
             $(this).parent().find("div[id*='item-no']").css(openProperties)
        }else if(previousId == "" ){
            $(this).parent().find("div[id*='item-no']").css(openProperties)
            
        }
        if(previousId == $(this).parent().find("div[id*='item-no']").attr("id") && previousId != ""){
            $("#"+previousId).css({
                "display": "none"
             })
             previousId = "";
        }else{
            previousId = $(this).parent().find("div[id*='item-no']").attr("id");

        }
    })
$(".dropdown-custom").on('click', 'input:checkbox', function (e) {

     if($("#"+previousId+" :input[id*='ver_']").length > 0) {
          if($("#"+previousId+" :input[id*='ver_']").is(':checked')){
  
              $("#"+previousId+" :input").not("#"+previousId+" :input[id*='ver']").each(function(){
                  $(this).prop( "disabled", false )
              });
          }else{
                  $("#"+previousId+" :input").not("#"+previousId+" :input[id*='ver']").each(function(){
                  $(this).prop( "disabled", true )
              });
          }
      }

      $("#"+previousId+" :input").not("#"+previousId+" input:checkbox[id*='ver']").parent().click(function() {
          if(!$("#"+previousId+" input:checkbox[id*='ver']").is(":checked") ){
              Swal.fire({
                  icon: 'warning',
                  title: 'Debes habilitar la opción "ver" para habilitar el resto de permisos',
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  timer: 3000,
                  timerProgressBar: true,
                  onOpen: (toast) => {
                  toast.addEventListener('mouseenter', Swal.stopTimer)
                  toast.addEventListener('mouseleave', Swal.resumeTimer)
                  }
              });  
          }
      });
    });
    
    $('#formPermisos').submit(function () {

      $.ajax({
        url: ApiRestURLS.userPermisos,
        data: $('#formPermisos').serialize(),
        type: 'post',
        beforeSend: function(){
          $('#loading').show();
        },
        success: function(data){

          $('#loading').hide();
          Swal.fire({
            icon: (data.status == 1) ? 'success' : 'warning',
            title: data.message,
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 6000,
            timerProgressBar: true,
            onOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer)
              toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
          });
          if( data.status == 1 ){
            $('#modalGlobal .close').click();
            oGrid.refreshGrid();
          }
        },
        error: function(data){
          $('#loading').hide();

        }
      });
      return false;
    });

  }
}