// Importar - Css
import '../../css/usuarios/index.css';
// Importar - Javascrip
import '../grilla/jquery-aggrid';
import { AgCellRender } from '../grilla/ag-plugins/agCellRender';


// Iniciamos Tabla
//oGrid = $("#myGrid").SIP_AgGrid({ url: _urlGetData, gridOptions: _gridOption, btn: _btn, autoSize: false, autoHeight: true, paginador: true });

$(function() {


    //Se te ha enviado un correo con un link con el cual podras reestablecer tu contraseña
  // Clear Cache
  $.ajaxSetup({ cache: false });
  
  $("#btn-iniciarses").click(function(){
    document.location.href = '/'
  });
    $("#cambiarContrasena").click(function(e){
        e.preventDefault();
        let first_field = $("#change_password_form_plainPassword_first").val();
        let second_field = $("#change_password_form_plainPassword_second").val();

        if ( first_field != second_field){  
            Swal.fire({
                icon: 'warning' ,
                title: 'Las contraseñas deben coincidir',
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 9000 ,
                timerProgressBar: true
            });
            return;
        }else if(first_field.length < 3  ){
            Swal.fire({
                icon: 'warning' ,
                title: 'Las contraseñas deben tener minimo 6 carácteres',
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 9000 ,
                timerProgressBar: true  
            });
            return;
        }

        $.ajax({
            url: forgotPassword,
            type: "get",
            data: {
                'plainPassword': first_field,
                'token': $("#token").val()
            },
            beforeSend: function(){ 
                $('#loading').show();
                $("#loading").hide();

            },
            success: function(data){
                $("#loading").hide();
                var res = JSON.parse(data);

                if(res.status == 200 ){

                    Swal.fire({
                      title: '',
                      html: `Contraseña actualizada correctamente`,
                      icon: 'success',
                      showCancelButton: false,
                      confirmButtonColor: '#3085d6',
                      confirmButtonText: 'Aceptar',
                    }).then((result) => {
                      if( result.isConfirmed ){
                        window.location.href = "/";
                      }
                    });

                }else{

                    Swal.fire({
                        icon: 'warning',
                        html: '<label class="font-weight-bold">'+res.message+'</label>',
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: (res.status == 200) ? 6000 : 8000,
                        timerProgressBar: true
                    });

                    return false;

                }

            }
        });
      });


  // Loading Hide
  //$("#loading").hide();

  $("#loading").hide();
});