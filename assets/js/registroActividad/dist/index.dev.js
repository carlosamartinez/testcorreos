"use strict";

require("../grilla/jquery-aggrid");

// Importar - Css
// import '../../css/usuarios/index.css';
// Importar - Javascrip

/* --- --- --- Variables Globales --- --- --- */
// Configuracion Tabla
var _urlGetData = {
  GRILLA_LISTAR_JSON: ApiRestURLS.listRegistroActividad,
  ORDER_SORD: "DESC",
  ORDER_SIDX: "id"
};
var _gridOption = {
  columnDefs: aDfColumnas,
  rowSelection: 'single',
  headerHeight: 34,
  pagination: true,
  enableSorting: true,
  enableServerSideSorting: true,
  enableColResize: true,
  suppressPaginationPanel: true
};
var _btn = {
  'themeIcon': 'square',
  'plugins': ['refresh', 'search'],
  'add': aGridButtons
}; // Iniciamos Tabla

oGrid = $("#myGrid").SIP_AgGrid({
  url: _urlGetData,
  gridOptions: _gridOption,
  btn: _btn,
  autoSize: false,
  autoHeight: true,
  paginador: true
});
$(function () {
  // Clear Cache
  $.ajaxSetup({
    cache: false
  });
  $("#myGrid").on("click", "#ag-myGrid-descargar-csv", function () {
    window.open(ApiRestURLS.downloadRegistroActividadCsv + oGrid.getDataSource().globalFilterQuery);
  }); // Loading Hide

  $("#loading").hide();
});