// Importar - Css
// import '../../css/usuarios/index.css';

// Importar - Javascrip
import '../grilla/jquery-aggrid';

/* --- --- --- Variables Globales --- --- --- */

// Configuracion Tabla
const _urlGetData = { GRILLA_LISTAR_JSON: ApiRestURLS.listRegistroActividad, ORDER_SORD: "DESC", ORDER_SIDX: "id" };
const _gridOption = {
  columnDefs: aDfColumnas,
  rowSelection: 'single',
  headerHeight: 34,
  pagination: true,
  enableSorting: true,
  enableServerSideSorting: true,
  enableColResize: true,
  suppressPaginationPanel: true
};

var _btn = {
  'themeIcon': 'square',
  'plugins': ['refresh', 'search'],
  'add': aGridButtons
};

// Iniciamos Tabla
oGrid = $("#myGrid").SIP_AgGrid({ url: _urlGetData, gridOptions: _gridOption, btn: _btn, autoSize: false, autoHeight: "auto", paginador: true });

$(function () {

  // Clear Cache
  $.ajaxSetup({ cache: false });

  $("#myGrid").on("click", "#ag-myGrid-descargar_csv", function(){
    window.open(ApiRestURLS.downloadRegistroActividadCsv + oGrid.getDataSource().globalFilterQuery);
  });

  // Loading Hide
  $("#loading").hide();

});