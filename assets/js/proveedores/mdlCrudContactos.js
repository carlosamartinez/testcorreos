export default class MdlCrudContactos {

  constructor(oGridContactos){
    this.oGridContactos = oGridContactos;
  }
  init(idProveedor){

    var validacion = false;
    if(accion == 'editar'){
      validacion = true;
    }
    $('#loading-email').hide();

    $('#formContactos').off().submit((event)=> {

      
      if(validacion == true){

        $.ajax({
          url: ApiRestURLS.mdlCrud,
          data: $('#formContactos').serialize(),
          type: 'post',
          beforeSend: function(){
            $('#loading').show();
          },
          success: (data)=>{

            $('#loading').hide();
            Swal.fire({
              icon: (data.status == 1) ? 'success' : 'warning',
              title: data.message,
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 6000,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            });
            if( data.status == 1 ){
              $('#modalGlobalContactos .close').click();
              this.oGridContactos.refreshGrid();
            }
          },
          error: function(data){
            $('#loading').hide();
          }
        });

      }
      
      return false;
    });


    $("#contactos_email").off().blur(function(){

      //alert('validacion');

      var email = $("#contactos_email").val();
      var idContacto = '';
      if(accion == 'editar'){
        idContacto = $("#idContacto").val();
      }

      if(email != ''){

        $.ajax({
          url: ApiRestURLS.verificacionEmail,
          data: {email:email, idContacto:idContacto},
          type: 'post',
          beforeSend: function(){
            $('#loading-email').show();
            validacion = false;
          },
          success: function(data){

            $('#loading-email').hide();

            if(data.status == 0){

              $("#contactos_email").val('');
              validacion = false;

              Swal.fire({
                icon: 'warning',
                title: data.message,
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 6000,
                timerProgressBar: true,
                onOpen: (toast) => {
                  toast.addEventListener('mouseenter', Swal.stopTimer)
                  toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
              });

            }else{
              validacion = true;
            }
            
          },
          error: function(data){
            $('#loading-email').hide();
          }
        });

      }

    });

  }
}