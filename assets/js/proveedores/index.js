// Importar - Css
//import '../../css/usuarios/index.css';
// Importar - Javascrip
import '../grilla/jquery-aggrid';
import { AgCellRender } from '../grilla/ag-plugins/agCellRender';
import ContactosDetailRenderer from './ag-components/contactos-detail-renderer';
import MdlCrud from "./mdlCrud";
import MdlContactos from "./mdlContactos";


/* --- --- --- Variables Globales --- --- --- */
let oMdlCrud = new MdlCrud();
let oMdlContactos = new MdlContactos();


// Configuracion Tabla
const _urlGetData = { GRILLA_LISTAR_JSON: ApiRestURLS.listProveedores, ORDER_SORD: "DESC", ORDER_SIDX: "id" };
const _gridOption = {
  columnDefs: aDfColumnas,
  rowSelection: 'single',
  headerHeight: 34,
  rowHeight: 34,
  pagination: true,
  enableSorting: true,
  enableServerSideSorting: true,
  enableColResize: true,
  suppressPaginationPanel: true,
  masterDetail: true,
  detailRowHeight: 300,
  detailCellRenderer: 'contactosDetailRenderer',
  components: {
    'agCellRender': AgCellRender,
    contactosDetailRenderer: ContactosDetailRenderer
  },
};

var _btn = {
  'themeIcon': 'square',
  'plugins': ['refresh', 'search'],
  'add': aGridButtons
};

// Iniciamos Tabla
oGrid = $("#myGrid").SIP_AgGrid({ url: _urlGetData, gridOptions: _gridOption, btn: _btn, autoSize: false, autoHeight: "auto", paginador: true });
    
$(function () {
  

  // Clear Cache
  $.ajaxSetup({ cache: false });

  // Crear Registro
  $('#myGrid').on('click', '#ag-myGrid-nuevo-registro', function(){
    $('#loading').show();
    $('.modal-dialog').addClass('modal-lg');
    $('#tituloModalGlobal').html('Nuevo Proveedor <i class="fas fa-user"></i>');
    $('#contenidoModalGlobal').empty().load(ApiRestURLS.proveedorNew, function (){
      $('#modalGlobal').modal({backdrop: true,keyboard: false});
      $('#modalGlobal').modal('show');
      oMdlCrud.init();
      $('#loading').hide();
    });
  });


  // Editar Registro
  $('#myGrid').on('click', '#ag-myGrid-editar-registro', function(){
    let selectedRows = oGrid.getGridOptions().api.getSelectedRows();
    if( selectedRows.length > 0 ){
      $('#loading').show();
      $('.modal-dialog').addClass('modal-lg');
      // Titulo Modal
      $('#tituloModalGlobal').html('Editar Proveedor <i class="fas fa-user"></i>&nbsp&nbsp' + selectedRows[0].nit );
      $('#contenidoModalGlobal').empty().load(ApiRestURLS.proveedorEdit, {idProveedor: selectedRows[0].id}, function (){
        $('#modalGlobal').modal({backdrop: true,keyboard: false});
        $('#modalGlobal').modal('show');
        oMdlCrud.init();
        $('#loading').hide();
      });
    }else{
      Swal.fire({
        icon: 'warning',
        title: 'Por favor seleccione una fila.',
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 6000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      });
    }
  });
  // Eliminar Registro
  $('#myGrid').on('click', '#ag-myGrid-eliminar-registro', function(){
    var selectedRows = oGrid.getGridOptions().api.getSelectedRows();
    if(selectedRows.length > 0){

      Swal.fire({
        title: '¿Realmente desea eliminar ?',
        html: `<b class="text-primary">Al proveedor con NIT:</b> <b class="text-info">${selectedRows[0].nit}</b><br>`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if( result.isConfirmed ){
          $.ajax({
            url: ApiRestURLS.proveedorDelete,
            data: { idProveedor: selectedRows[0].id},
            type: 'post',
            beforeSend: function(){
              $('#loading').show();
            },
            success: function(data){

              $('#loading').hide();
              Swal.fire({
                icon: (data.status == 1) ? 'success' : 'warning',
                title: data.message,
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: (data.status == 1) ? 6000 : 8000,
                timerProgressBar: true,
                onOpen: (toast) => {
                  toast.addEventListener('mouseenter', Swal.stopTimer)
                  toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
              });

              if( data.status == 1 ){
                oGrid.refreshGrid();
              }
            },
            error: function(data){
              $('#loading').hide();
              alertaSimple({
                mensaje: data.responseJSON.msg,
                tipo: 'warning',
                toast: true, 
                position: 'top-end',
                mostrarBoton:false,
                tiempo: 6000
              });
            }
          });
        }
      });
    }else{
      Swal.fire({
        icon: 'warning',
        title: 'Por favor seleccione una fila.',
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 6000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      });
    }
    return false;
  });
  

  $("#myGrid").on("click", "#ag-myGrid-descargar-csv", ()=>{
    window.open(ApiRestURLS.proveedorExportar + oGrid.getDataSource().globalFilterQuery);
  });


  $('#myGrid').on('click', '.btnContactos', function(){

    var idProveedor = $(this).data('idproveedor');

    $('#loading').show();
    $('.modal-dialog').addClass('modal-xl');
    $('#tituloModalGlobal').html('Contactos Proveedor <i class="fas fa-users"></i>');
    $('#contenidoModalGlobal').empty().load(ApiRestURLS.contactosProveedor, function (){
      $('#contenidoModalGlobal').height(450);
      $('#modalGlobal').modal({backdrop: true,keyboard: false});
      $('#modalGlobal').modal('show');
      oMdlContactos.init(idProveedor);
      $('#loading').hide();
    });


  });


  // Loading Hide
  $("#loading").hide();
});