import BaseDetailRowRenderer from '../../grilla/ag-renderers/detail-row-renderer-base'
import MdlCrudContactos from "../mdlCrudContactos";

export default class ContactosDetailRenderer extends BaseDetailRowRenderer{

  beforeInit(){
    // Variables
    //const contactos = infoProveedores.contactos;
    //const urlJson = this.params.data.urlContactos;
    const idProveedor = this.params.data.id
    const bottomButtons = this.getBottomButtons();

    

    // Detalle Grilla
    this.gridDefinition =  { 
      gridDetailHeight: '220px',
      url: { GRILLA_LISTAR_JSON: ApiRestURLS.contactosProveedorJson, ORDER_SORD: "DESC", ORDER_SIDX: "id", ADD_DATA:{idProveedor: idProveedor } },
      gridOptions: { 
        columnDefs: aDfColumnasContactos,
        components: {
        }
      }, 
      btn: {
        plugins: ['refresh', 'search'],
        'add': bottomButtons
      }
    };
  }


  getBottomButtons(){
    return [
      {
        id:"nuevo-registro",
        text:"",
        title:"Nuevo",
        icon:["fas fa-user-plus text-success"],
        class:["hover-success"],
        eventClick: this.btnNuevoContacto.bind(this)
      },
      {
        id:"editar-registro",
        text:"",
        title:"Editar",
        icon:["fas fa-user-edit text-warning"],
        class:["hover-success"],
        eventClick: this.btnEditarContacto.bind(this)
      },
      {
        id:"eliminar-registro",
        text:"",
        title:"Eliminar",
        icon:["fas fa-user-times text-danger"],
        class:["hover-success"],
        eventClick: this.btnEliminarContacto.bind(this)
      }
    ];
  }

  btnNuevoContacto(){

    console.log(this.oGrid);

    var oMdlCrudContactos = new MdlCrudContactos(this.oGrid);
    var idProveedor = this.params.data.id

    $('#loading').show();
    $('#tituloModalGlobalContactos').html('Nuevo Contacto <i class="fas fa-user"></i>');
    $('#contenidoModalGlobalContactos').empty().load(ApiRestURLS.contactosNew,{idProveedor:idProveedor}, function (){
      $('#modalGlobalContactos').modal({backdrop: true,keyboard: false});
      $('#modalGlobalContactos').modal('show');
      oMdlCrudContactos.init(idProveedor);
      $('#loading').hide();
    });

  }

  btnEditarContacto(){

    var oMdlCrudContactos = new MdlCrudContactos(this.oGrid);
    var idProveedor = this.params.data.id

    let selectedRows = this.oGrid.getGridOptions().api.getSelectedRows();
    if( selectedRows.length > 0 ){
      $('#loading').show();

      // Titulo Modal
      $('#tituloModalGlobalContactos').html('Editar Contacto <i class="fas fa-user"></i>&nbsp&nbsp' + selectedRows[0].nombre );
      $('#contenidoModalGlobalContactos').empty().load(ApiRestURLS.contactosEdit, {idContacto: selectedRows[0].id, idProveedor:idProveedor}, function (){
        $('#modalGlobalContactos').modal({backdrop: true,keyboard: false});
        $('#modalGlobalContactos').modal('show');
        oMdlCrudContactos.init(idProveedor);
        $('#loading').hide();
      });
    }else{
      Swal.fire({
        icon: 'warning',
        title: 'Por favor seleccione una fila.',
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 6000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      });
    }

  }

  btnEliminarContacto(){

    var selectedRows = this.oGrid.getGridOptions().api.getSelectedRows();
      if(selectedRows.length > 0){

        Swal.fire({
          title: '¿Realmente desea eliminar ?',
          html: `<b class="text-primary">Al contacto:</b> <b class="text-info">${selectedRows[0].nombre}</b><br>`,
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Aceptar',
          cancelButtonText: 'Cancelar'
        }).then((result) => {
          if( result.isConfirmed ){
            $.ajax({
              url: ApiRestURLS.contactosDelete,
              data: { idContacto: selectedRows[0].id},
              type: 'post',
              beforeSend: function(){
                $('#loading').show();
              },
              success: function(data){

                $('#loading').hide();
                Swal.fire({
                  icon: (data.status == 1) ? 'success' : 'warning',
                  title: data.message,
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  timer: (data.status == 1) ? 6000 : 8000,
                  timerProgressBar: true,
                  onOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                  }
                });

                if( data.status == 1 ){
                  this.oGrid.refreshGrid();
                }
              },
              error: function(data){
                $('#loading').hide();
                alertaSimple({
                  mensaje: data.responseJSON.msg,
                  tipo: 'warning',
                  toast: true, 
                  position: 'top-end',
                  mostrarBoton:false,
                  tiempo: 6000
                });
              }
            });
          }
        });
      }else{
        Swal.fire({
          icon: 'warning',
          title: 'Por favor seleccione una fila.',
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 6000,
          timerProgressBar: true,
          onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        });
      }

  }

}