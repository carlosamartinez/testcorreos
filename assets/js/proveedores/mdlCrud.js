export default class MdlCrud {

  constructor(){}
  init(){

    
    $('#formProveedor').submit(function () {

      $.ajax({
        url: ApiRestURLS.mdlCrud,
        data: $('#formProveedor').serialize(),
        type: 'post',
        beforeSend: function(){
          $('#loading').show();
        },
        success: function(data){

          $('#loading').hide();
          Swal.fire({
            icon: (data.status == 1) ? 'success' : 'warning',
            title: data.message,
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 6000,
            timerProgressBar: true,
            onOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer)
              toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
          });
          if( data.status == 1 ){
            $('#modalGlobal .close').click();
            oGrid.refreshGrid();
          }
        },
        error: function(data){
          $('#loading').hide();
        }
      });
      return false;
    });

  }
}