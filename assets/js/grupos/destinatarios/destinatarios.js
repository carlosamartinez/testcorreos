import { DestinatariosAsociados } from './asociados/destinatarios-asociados';
import { DestinatariosProveedores } from './proveedores/destinatarios-proveedores';

$(function(){
  // Variables
  const destinatariosAsociados = new DestinatariosAsociados(infoAsociados.index);
  const destinatariosProveedores = new DestinatariosProveedores(infoProveedores.index, idGrupo);

  // Clear Cache
  $.ajaxSetup({ cache: false });

  
  if(tipoGrilla == 2){
    // Iniciamos Proveedores
    destinatariosProveedores.initAgGrid();
  }else{
    destinatariosAsociados.initAgGrid();
  }

  // Iniciamos Asociados
  /*$("#nav-asociados-tab").one('click', function(){
    setTimeout(() => {
      destinatariosAsociados.initAgGrid();
    }, 250);
  });*/

  // Loading Hide
  $("#loading").hide();
});