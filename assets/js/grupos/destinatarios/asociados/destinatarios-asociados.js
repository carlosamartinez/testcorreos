import '../../../grilla/jquery-aggrid';
import { AgCellRender } from '../../../grilla/ag-plugins/agCellRender';
import MdlUpload from "../../mdlUpload.js";


export class DestinatariosAsociados{

	constructor(paramsAgGrid){
	    this.paramsAgGrid = paramsAgGrid;
	  }


  initAgGrid(){

  	

  	const settingAgGrid = this.getSettingsAgGrid();
    this.oGridAsociados = $("#myGridAsociados").SIP_AgGrid(settingAgGrid);

    var gridAsociados = this.oGridAsociados;

    var oMdlUpload = new MdlUpload(this.oGridAsociados);


    $("#myGridAsociados").on("click", "#ag-myGridAsociados-agregar-todos",  ()=>{

        Swal.fire({
            title: '',
            html: `¿Realmente desea agregar todos los asociados?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar'
          }).then((result) => {
            if( result.isConfirmed ){
              $.ajax({
                url: ApiRestURLS.agregarAsociados + gridAsociados.getDataSource().globalFilterQuery,
                data: { 
                  grupoId: idGrupo,
                  tipoGrupo: tipoGrupo
                },
                type: 'post',
                beforeSend: function(){
                  $('#loading').show();
                },
                success: function(data){
    
                  data = JSON.parse(data);
    			
                  $('#loading').hide();
                  Swal.fire({
                    icon: (data.status == 200) ? 'success' : 'warning',
                    title: data.message,
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: ( data.status == 200 ) ? 6000 : 8000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                      toast.addEventListener('mouseenter', Swal.stopTimer)
                      toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                  });
    
                  if( data.status == 200 ){
                    gridAsociados.refreshGrid();
                  }
                },
                error: function(data){
                  $('#loading').hide();
                  alertaSimple({
                    mensaje: data.responseJSON.msg,
                    tipo: 'warning',
                    toast: true, 
                    position: 'top-end',
                    mostrarBoton:false,
                    tiempo: 6000
                  });
                }
              });
            }
          });
    })



    // Eliminar todos asociados
    $("#myGridAsociados").on("click", "#ag-myGridAsociados-eliminar-todos",  ()=>{

        Swal.fire({
            title: '',
            html: `¿Realmente desea desasociar todos los asociados?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if( result.isConfirmed ){
                $.ajax({
                    url: ApiRestURLS.eliminarAsociados,
                    data: { grupoId: idGrupo},
                    type: 'post',
                    beforeSend: function(){
                    $('#loading').show();
                    },
                    success: function(data){

                    data = JSON.parse(data);
                    $('#loading').hide();
                    Swal.fire({
                        icon: ( data.status == 200 ) ? 'success' : 'warning',
                        title: data.message,
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: (data.status == 1) ? 6000 : 8000,
                        timerProgressBar: true,
                        onOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    });
        
                    if( data.status == 200 ){
                        gridAsociados.refreshGrid();
                    }
                    },
                    error: function(data){
                    $('#loading').hide();
                    alertaSimple({
                        mensaje: data.responseJSON.msg,
                        tipo: 'warning',
                        toast: true, 
                        position: 'top-end',
                        mostrarBoton:false,
                        tiempo: 6000
                    });
                    }
                });
            }
        });
    })


    $("#myGridAsociados").on("click","#ag-myGridAsociados-cargar-destinatarios", "click", ()=>{
        $('#loading').show();
        $('.modal-dialog').addClass('modal-xl');
        $('#tituloModalGlobal').html('Cargar Destinatarios <i class="far fa-building"></i><i class="fas fa-plus"></i>');
        $('#contenidoModalGlobal').empty().load(ApiRestURLS.cargarAsociados, {grupoId: idGrupo}, function (){
            $('#modalGlobal').modal({backdrop: true,keyboard: false});
            $('#modalGlobal').modal('show');
            $('#modalGlobal').height(700);
            oMdlUpload.init(idGrupo)
            
            $('#loading').hide();
        });
    })


    $('#myGridAsociados').on('click', '.agregar-cliente', function(){
	    let idCliente = $(this).data("id-cliente")

      if($(this).is(':checked')){
        var accion = "agregar";
      }else{
        var accion = "eliminar";
      }

	    $.ajax({
	      url: ApiRestURLS.agregarAsociado ,
	      data: { idCliente: idCliente,
	              grupoId: idGrupo,
                accion: accion },
	      type: 'post',
	      beforeSend: function(){
	        $('#loading').show();
	      },
	      success: function(data){
	        data = JSON.parse(data);
	        
	        $('#loading').hide();
	        Swal.fire({
	          icon: ( data.status == 200 ) ? 'success' : 'warning',
	          title:  ( data.status == 200 ) ? `<span style="color:#029a19">${data.message}</span>` : `<span style="color:#9a0228">${data.message}</span>`,
	          toast: true,
	          position: 'top-end',
	          showConfirmButton: false,
	          timer: (data.status == 1) ? 6000 : 8000,
	          timerProgressBar: true,
	          onOpen: (toast) => {
	            toast.addEventListener('mouseenter', Swal.stopTimer)
	            toast.addEventListener('mouseleave', Swal.resumeTimer)
	          }
	        });

	      },
	      error: function(data){
	        $('#loading').hide();
	        alertaSimple({
	          mensaje: data.responseJSON.msg,
	          tipo: 'warning',
	          toast: true, 
	          position: 'top-end',
	          mostrarBoton:false,
	          tiempo: 6000
	        });
	      }
	    });
	 });



  }


  /**
   * Obtiene configueación AgGrid para asociados
   * @returns settings AgGrid
   */
  getSettingsAgGrid(){
    const bottomButtons = this.getBottomButtons();
    return {
      url: { GRILLA_LISTAR_JSON: this.paramsAgGrid.urlJson, ORDER_SORD: "DESC", ORDER_SIDX: "id", ADD_DATA:{tipoGrupo:tipoGrupo } }, 
      gridOptions: {
        columnDefs: this.paramsAgGrid.columnas,
      
        // Detalle Fila
        detailRowHeight: 300,
        components: {
          'agCellRender': AgCellRender
        }
      }, 
      btn: {
        'plugins': ['refresh', 'search'],
        'add': bottomButtons
      }, 
      autoHeight: "auto"
    };
  }


  getBottomButtons(){
    return [
      {
        class: ["hover-success"],
        icon: ["fas fas fa-globe-americas text-primary", "fas fa-plus"],
        id: "agregar-todos",
        title: "Agregar Todos"
      },
      {
        class: ["hover-success"],
        icon: ["fas fas fa-globe-americas text-danger", "fas fa-minus"],
        id: "eliminar-todos",
        title: "Eliminar Todos" 
      },
      {
        class: ["hover-success"],
        icon: ["fas fas fa-file-upload text-success"],
        id: "cargar-destinatarios",
        title: "Cargar Destinatarios" 
      }
    ];
  }







}