import "../../../../../public/assets/fileinput/js/es.js";

export default class SubirArchivo {

    constructor(oGrid){
        this.oGrid = oGrid;
    }
    init(grupoId){

        $('#descargar_formato').click(function () {

            window.open( ApiRestURLS.descargarFormatoProveedores + "?tipo_registro=" + $(".uk-comment-meta input[name=tipoRegistro]:checked")[0].defaultValue );

        });

        var ComoVale='';
        var ComoBodega='';

        var resultadoCargue = {};
        var integrantesPrevios = {};
        $("#form_archivo").fileinput({
            //uploadUrl: ApiRestURLS.procesarArchivoProveedores, // server upload action
            uploadUrl: ApiRestURLS.previsualizarArchivoProveedores, // server upload action
            uploadAsync: true,
            showPreview: false,
            type:'post',
            language: "es",
            dataType:'json',
            allowedFileExtensions: ['xls','xlsx'],
            maxFileCount: 5,
            elErrorContainer: '#kv-error-1',
            uploadExtraData: function() {
                return {
                    tipoRegistro: $('input[name=tipoRegistro]:checked').val(),
                    grupoId: grupoId
                };
            }
        }).on('filebatchpreupload', function(event, data, id, index) {
            $('#kv-success-1').html('<h4>Estado del cargue</h4><ul></ul>').hide();
        }).on('fileuploaded', function(event, data, id, index) {

            console.log(data.response);

            let listadoIntegrantesPrevios = "";
            let listadoIntegrantesArchivo = "";
            let listadoNoEncontrados = "";

            integrantesPrevios = data.response.integrantesPrevios;
            resultadoCargue = data.response.integrantesArchivo;

            $.each( data.response.integrantesPrevios, function( key, value ) {
              listadoIntegrantesPrevios+= `<tr><td class='text-center'><input type='checkbox' checked class='form-control chk-destinatarios-confirmar' data-id='${value.idIG}'></td><td>${value.proveedor}</td><td>${value.nombre}</td><td>${value.email}</td></tr>`;
            });

            

            $.each( data.response.integrantesArchivo, function( key, value ) {
              listadoIntegrantesArchivo+= `<tr><td>${value.proveedor}</td><td>${value.nombre}</td><td>${value.email}</td></tr>`;
            });

            $.each( data.response.noEncontrados, function( key, value ) {
              listadoNoEncontrados+= `<tr><td>${value.fila}</td><td>${value.dato}</td><td>${value.motivo}</td></tr>`;
            });


            $(".listado-clientes").css({"display":"block"});
            $(".listado-noEncontrados").css({"display":"block"});
            $("#resultados").css({"display":"block"});
            if(listadoIntegrantesPrevios == "")listadoIntegrantesPrevios = `<tr><td>No se encontraron integrantes previos</td></tr>`;
            if(listadoIntegrantesArchivo == "")listadoIntegrantesArchivo = `<tr><td>No se insertaron nuevos integrantes</td></tr>`;
            $("#listado-clientes-actualizados-tbody").append(listadoIntegrantesPrevios);
            $("#listado-clientes-insertados-tbody").append(listadoIntegrantesArchivo);
            $("#listado-noencontrados-tbody").append(listadoNoEncontrados);
            if($(".modal-footer #confirmacionDatosArchivo").length < 1)$(".modal-footer .bd-highlight").prepend('<button class="btn btn-sm btn-primary " id="confirmacionDatosArchivo">Confirmar Integrantes</button>');

            $("#contentUpload").css({"display":"none"});

            $("#confirmacionDatosArchivo").click( () => {

                $('#loading').show();
                $.ajax({ 
                    url: ApiRestURLS.procesarArchivoProveedores,
                    type: 'post',
                    data: {
                        grupoId: grupoId,
                        tipoRegistro: $('input[name=tipoRegistro]:checked').val(),
                        resultadoCargue: JSON.stringify(resultadoCargue)
                    },
                    dataType: 'json',
                    success: (data)=>{
                        $('#loading').hide();
                        $("#mdlProveedorCargarDestinatarios").modal('hide');

                        Swal.fire({
                            icon: 'success',
                            title:  `Destinatarios asignados correctamente`,
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 6000 ,
                            timerProgressBar: true,
                            onOpen: (toast) => {
                              toast.addEventListener('mouseenter', Swal.stopTimer)
                              toast.addEventListener('mouseleave', Swal.resumeTimer)
                            }
                        });
                        
                        location.reload();
    
                    }
                }); 
    
            });
            $(".chk-destinatarios-confirmar").change((ev)=>{
                
                $.ajax({ 
                    url: ApiRestURLS.agregarProveedor,
                    type: 'post',
                    data: {
                        idProveedor: ev.target.dataset.id
                    },
                    dataType: 'json',
                    success: (data)=>{
                        $('#loading').hide();
                        
                        Swal.fire({
                            icon: ( ev.target.checked ) ? 'success' : 'success',
                            title:  ( ev.target.checked ) ? `<span style="color:#029a19">Proveedor asociado</span>` : `<span style="color:#9a0228">Proveedor desasociado</span>`,
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 6000 ,
                            timerProgressBar: true,
                            onOpen: (toast) => {
                              toast.addEventListener('mouseenter', Swal.stopTimer)
                              toast.addEventListener('mouseleave', Swal.resumeTimer)
                            }
                          });
    
    
                    }
                }); 

            });

        });
        $('#form1').submit(function(){
            var file = $('#form_archivo').val();
            return false; 
        });


       

    }
}