import '../../../grilla/jquery-aggrid';
import AsignadoCellRenderer from './ag-components/asignado-cell-renderer';
import ContactosDetailRenderer from './ag-components/contactos-detail-renderer';
import { enviarDatos, modalContent} from '../../../globales/utilidades';
import { alertaNotificar } from '../../../globales/alertas';
import Mensajes from '../../../globales/mensajes';
import SubirArchivo from './subirArchivo';

/**
 * Lógica de negocio para destinaratios proveedores
 */
export class DestinatariosProveedores{

  constructor(paramsAgGrid, idGrupo){
    
    this.paramsAgGrid = paramsAgGrid;
    this.idGrupo = idGrupo;
    this.oGrid;
    
    this.mensajes = new Mensajes();
    this.mdlSubirArchivo = new SubirArchivo(this.oGrid);
    this.mdlCargarDestinatarios = modalContent("mdlProveedorCargarDestinatarios", "Cargar Archivo", "primary", "full");
  }

  /**
   * Inicia logica de negocio de la grilla
   */
  initAgGrid(){
    const settingAgGrid = this.getSettingsAgGrid();
    this.oGrid = $("#myGridProveedores").SIP_AgGrid(settingAgGrid);
  }

  /**
   * Obtiene configueación AgGrid para proveedores
   * @returns settings AgGrid
   */
  getSettingsAgGrid(){
    const bottomButtons = this.getBottomButtons();
    console.log(this.paramsAgGrid.urlJson);
    return {
      url: { GRILLA_LISTAR_JSON: this.paramsAgGrid.urlJson, ORDER_SORD: "DESC", ORDER_SIDX: "id" }, 
      gridOptions: {
        columnDefs: this.paramsAgGrid.columnas,
      
        // Detalle Fila
        masterDetail: true,
        detailRowHeight: 300,
        detailCellRenderer: 'contactosDetailRenderer',
        components: {
          asignadoCellRenderer: AsignadoCellRenderer,
          contactosDetailRenderer: ContactosDetailRenderer
        }
      }, 
      btn: {
        'plugins': ['refresh', 'search'],
        'add': bottomButtons
      }, 
      autoHeight: "auto"
    };
  }

  /**
   * Obtiene botones en la parte inferior de la grilla
   * @returns array Listado botones 
   */
  getBottomButtons(){
    return [
      {
        class: ["hover-success"],
        icon: ["fas fas fa-globe-americas text-primary", "fas fa-plus"],
        id: "proveedores-agregar-todos",
        title: "Agregar Todos",
        eventClick: this.btnAgregarTodos.bind(this)
      },
      {
        class: ["hover-success"],
        icon: ["fas fas fa-globe-americas text-danger", "fas fa-minus"],
        id: "proveedores-eliminar-todos",
        title: "Eliminar Todos",
        eventClick: this.btnEliminarTodos.bind(this)
      },
      {
        class: ["hover-success"],
        icon: ["fas fas fa-file-upload text-success"],
        id: "cargar-destinatarios",
        title: "Cargar Destinatarios",
        eventClick: this.btnCargarDestinatarios.bind(this)
      },
      {
        class: ["hover-success"],
        icon: ["fas fas fa-file-download text-success"],
        id: "descargar-excel",
        title: "Descargar informe",
        eventClick: this.btnDescargarXls.bind(this)
      }
    ];
  }

  /**
   * Evento API para asignar todos los proveedores
   */
  btnAgregarTodos(){
    enviarDatos(this.paramsAgGrid.urlAsignarTodos).then(function(result){
      alertaNotificar({title: result.message});
      this.oGrid.refreshGridClearFilter();
    }.bind(this), function(err){
      // Alerta Errores
      const sMensaje = (err.status !== undefined && err.status === 0) ? err.message : this.mensajes.errorGlobal ;
      alertaNotificar({title: sMensaje, icon: "error"});
    }.bind(this));
  }

  /**
   * Evento API para eliminar todos los asignados
   */
  btnEliminarTodos(){
    enviarDatos(this.paramsAgGrid.urlBorrarTodos).then(function(result){
      alertaNotificar({title: result.message});
      this.oGrid.refreshGridClearFilter();
    }.bind(this), function(err){
      // Alerta Errores
      const sMensaje = (err.status !== undefined && err.status === 0) ? err.message : this.mensajes.errorGlobal ;
      alertaNotificar({title: sMensaje, icon: "error"});
    }.bind(this));
  }

  /**
   * Evento abrir modal para importar documento
   */
  btnCargarDestinatarios(){
    $('#loading').show();
    if( $(".modal-footer #confirmacionDatosArchivo").length > 0 )$(".modal-footer #confirmacionDatosArchivo").remove();

    this.mdlCargarDestinatarios.find(".modal-body").load(this.paramsAgGrid.urlCargarDestinatariosHtml, {}, function(response, status, xhr){
      $('#loading').hide();
      if (status == "error" ) {
        alertaNotificar({title: this.mensajes.errorGlobal, icon: "error"});
      }else{
        
        this.mdlCargarDestinatarios.find(".modal-dialog").addClass("modal-xl");
        
        this.mdlCargarDestinatarios.find(".modal-body").css({"max-height": "620px"});
        this.mdlCargarDestinatarios.modal("show");
        this.mdlCargarDestinatarios.modal();
        this.mdlSubirArchivo.init(this.idGrupo);
      }
    }.bind(this));
  }

    /**
   * Evento descargar .xls
   */
     btnDescargarXls(){
      $('#loading').show();
      window.open(ApiRestURLS.descargarInformeProveedores + this.oGrid.getDataSource().globalFilterQuery + "&idGrupo=" + this.idGrupo);
      $('#loading').hide();

    }
}