import { enviarDatos } from '../../../../globales/utilidades';
import { alertaNotificar } from '../../../../globales/alertas';
import Mensajes from '../../../../globales/mensajes';

export default class AsignadoCellRenderer{
  init(params){
    this.params = params;
    this.mensajes = new Mensajes();
    this.asignado = this.getValueToDisplay(params);
    this.eGui = this.getTemplate();
  }

  getGui() {
    return this.eGui;
  }

  getValueToDisplay(params) {
    return params.valueFormatted ? params.valueFormatted : params.value;
  }

  getTemplate(){

    this.button = $("<button />", {class: 'asignado-btn', html: this.getIconByStatus(this.asignado), click: this.actionChangeStatus.bind(this)});
    const template = $("<div />", {
      class: 'asignado-cell',
      html: this.button
    }); 

    return template.get(0);
  }

  getIconByStatus(status){
    let sClass = '';
    switch(status){
      case null: sClass = 'fas fa-circle-notch fa-spin text-secondary'; break;
      case true: sClass = 'fa fa-check text-success'; break;
      case false: sClass = 'fa fa-exclamation-circle text-warning'; break;
    }

    return $('<i />', {class: sClass});
  }

  actionChangeStatus(){
    // Variables
    const data = this.params.data;
    const rowDetailId = `ag-masterdetail-proveedor-${data.id}`;
    const params = {idProveedor: data.id, idContacto: null}; // parametro proveedor

    // Eventos iniciales
    this.button.html(this.getIconByStatus(null)).prop('disabled', true);

    // Parametro Contacto
    if(data.idProveedor){
      params.idContacto = data.id;
      params.idProveedor = data.idProveedor;
      if(this.asignado){
        params.accion = 'eliminar';
      }else{
        params.accion = 'insertar';
      }
    }

    // AJAX
    enviarDatos(infoProveedores.index.urlAsignar, params).then((result) => {
      this.asignado = result.newSatus;
      this.button.html(this.getIconByStatus(this.asignado)).prop('disabled', false);
      
      // Si el detalle esta expandido, refrescar grilla
      if(this.params.node.expanded){
        const masterDetail = this.params.api.getDetailGridInfo(rowDetailId);
        masterDetail.oGrid.refreshGridClearFilter();
      }
    }, (err) => {
      // Alerta Errores
      this.button.html(this.getIconByStatus(this.asignado)).prop('disabled', false);
      const sMensaje = (err.status !== undefined && err.status === 0) ? err.message : this.mensajes.errorGlobal ;
      alertaNotificar({title: sMensaje, icon: "error"});
    });
  }
}