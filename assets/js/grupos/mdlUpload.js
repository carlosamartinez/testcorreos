import "../../../public/assets/fileinput/js/es.js";

export default class MdlUpload {

    constructor(gridAsociados){
        this.gridAsociados = gridAsociados;
    }
    init(idGrupo){

        let listadoClientes = "";
        let listadoFallo = "";
        let listadoExiste = "";

        const instance = this; 
    
        $("#form_archivo").fileinput({
            uploadUrl: ApiRestURLS.cargarAsociadosxls, // server upload action
            uploadAsync: true,
            showPreview: false,
            language: "es",
            type:'post',
            dataType:'json',
            allowedFileExtensions: ['xls','xlsx'],
            maxFileCount: 1,
            uploadExtraData: function() {
                return { grupoId: idGrupo };
            }
        }).on('filebatchpreupload', function(event, data, id, index) {
            $("#loading").show();
            
        }).on('fileuploaded', function(event, data, id, index) {
            $("#loading").hide();
    
            if(data.response.status == 200 ){
                $("#contentUpload").remove();
    
                $("#resultados").css({"display":"block"})
                //$("#tabs-res").tabs();
    
                if(Object.values(data.response.registros_validos).length > 0){
                    let identificador = "";
                    
                    Object.values(data.response.registros_validos).forEach( element => {    
                        identificador = data.response.tipo_registros == "drogueria" ?  element.codigo : element.nit ;  
                        listadoClientes+= `<tr><td><input type="checkbox" class="clients-checked" name="idCliente[]" value="${element.id}" data-element-id="${element.id}" checked ></td><td>${identificador}</td><td>${element.nombre}</td><td>${element.email}</td></tr>`;
                    });
                    if( listadoClientes != "" ){

                        $(".listado-clientes").css({"display":"block"})
                        $("#listado-clientes-tbody").append(listadoClientes);
                    }
                    $("#confirm-all-clients").change((ev)=>{
                        if(ev.target.checked){
                            $("#listado-clientes-tbody .clients-checked").prop("checked",true)

                        }else{
                            $("#listado-clientes-tbody .clients-checked").prop("checked",false)

                        }
                    })

                    
                    $("#listado-clientes-tbody .clients-checked").change(()=>{
                        if($("#listado-clientes-tbody input:checkbox:not(:checked)").length > 0){
                            $("#listado-clientes-tbody #confirm-all-clients").prop("checked",false);
                        }
                    });

                    $("#confirmar-clientes").click((ev)=>{
                        ev.preventDefault();
                        let listadoFinal = new Array;
                        $("#listado-clientes-tbody input:checked").each(function(){
                            listadoFinal.push($(this)[0].defaultValue);
                        });
                        
                        
                        $.ajax({
                        url: ApiRestURLS.agregarAsociadosListado,
                        data: { 
                            grupoList: JSON.stringify(listadoFinal),
                            grupoId:   idGrupo 
                        },
                        type: 'post',
                        beforeSend: function(){
                        $('#loading').show();
                        },
                        success: function(data){
                        data = JSON.parse(data);
            
                        $('#loading').hide();
                        Swal.fire({
                            icon: ( data.status == 200 ) ? 'success' : 'warning',
                            title: data.message,
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: (data.status == 1) ? 6000 : 8000,
                            timerProgressBar: true,
                            onOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                            }
                        });
            
                        if( data.status == 200 ){
                            $('#modalGlobal').modal('hide');
                            instance.gridAsociados.refreshGrid();
                        }
                        },
                        error: function(data){
                        $('#loading').hide();
                        alertaSimple({
                            mensaje: data.responseJSON.msg,
                            tipo: 'warning',
                            toast: true, 
                            position: 'top-end',
                            mostrarBoton:false,
                            tiempo: 6000
                        });
                        }
                    });
                    })
                    
                }else{
                    $("#confirmar-clientes").remove();
                }
                if(Object.values(data.response.registros_invalidos).length > 0){
                
                    Object.values(data.response.registros_invalidos).forEach( element => {
                        if(element[1] == "No se encuentra nit" || element[1] == "No se encuentra codigo" ){

                            listadoFallo+= `<tr><td>${element[0]}</td><td>${element[1]}</td></tr>`;

                        }else{

                            listadoExiste+= `<tr><td><input type="checkbox" class="agregar-cliente-existente clients-checked" name="idCliente[]" value="${element[3]}" data-id-cliente="${element[3]}" checked ></td><td>${element[0]}</td><td>${element[5]}</td><td>${element[4]}</td><td>${element[1]}</td></tr>`;

                        }
                    });
                    if(listadoExiste != ""){
                        $(".listado-anterior").css({"display":"block"})
                        $("#listado-anterior-tbody").append(listadoExiste);

                    }
                    
                    if(listadoFallo != ""){
                        $(".listado-fallido").css({"display":"block"})
                        $("#listado-fallidos-tbody").append(listadoFallo);
                        
                    }
                }
                
            }else{
                
            }
        });

        $('#form1').submit(function(){
            var file = $('#form_archivo').val();
            return false; 
        });

        $('#descargar_formato').click(function () {

            window.open(ApiRestURLS.descargarFormato);

        });

        $('#listado-anterior-tbody').on('click', '#confirm-all-previous-clients', function(ev){

            $('#loading').show();
            $("#listado-anterior-tbody .clients-checked").each((index)=>{
                
                let idCliente = $("#listado-anterior-tbody .clients-checked").eq(index).val();
                $.ajax({
                    url: ApiRestURLS.agregarAsociado ,
                    data: { idCliente: idCliente,
                            grupoId: idGrupo
                        },
                    type: 'post',   
                    success: function(data){
                    }
                  });
            })
            $('#loading').hide();
            
            if($(this).prop("checked")){
                $("#listado-anterior-tbody .clients-checked").prop("checked",true)
            }else{
                $("#listado-anterior-tbody .clients-checked").prop("checked",false)
            }

            Swal.fire({
                icon: 'success',
                title: $(this).prop("checked") ? `<span style="color:#029a19">Todos los integrantes del archivo fueron asociados</span>` : `<span style="color:#029a19">Todos los integrantes del archivo fueron desasociados</span>`,
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 6000 ,
                timerProgressBar: true,
                onOpen: (toast) => {
                  toast.addEventListener('mouseenter', Swal.stopTimer)
                  toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
              });
      

        })

        $('.listado-anterior').on('click', '.agregar-cliente-existente', function(){

            let idCliente = $(this).data("id-cliente")
        
            $.ajax({
              url: ApiRestURLS.agregarAsociado ,
              data: { idCliente: idCliente,
                      grupoId: idGrupo },
              type: 'post',
              beforeSend: function(){
                $('#loading').show();
              },
              success: function(data){
                data = JSON.parse(data);
                console.log(data.status);
                $('#loading').hide();
                Swal.fire({
                  icon: ( data.status == 200 ) ? 'success' : 'success',
                  title:  ( data.status == 200 ) ? `<span style="color:#029a19">${data.message}</span>` : `<span style="color:#9a0228">${data.message}</span>`,
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  timer: (data.status == 1) ? 6000 : 8000,
                  timerProgressBar: true,
                  onOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                  }
                });
        
              },
              error: function(data){
                $('#loading').hide();
                alertaSimple({
                  mensaje: data.responseJSON.msg,
                  tipo: 'warning',
                  toast: true, 
                  position: 'top-end',
                  mostrarBoton:false,
                  tiempo: 6000
                });
              }
            });
          });
          setTimeout(() => {
            $(".file-input.file-input-new .input-group.file-caption-main .input-group-btn.input-group-append .btn.btn-primary.btn-file span").text("Examinar...")  
            $(".file-input.file-input-new .file-caption-name").attr("placeholder","Archivo seleccionado...");    
        }, 100);
    }

    makeAllRequest(){
        $("#listado-anterior-tbody .clients-checked").each(()=>{
            console.log($(this).val());
        console.log($(this).data("id-cliente"));
        return;
            let idCliente = $(this).data("id-cliente");
            $.ajax({
                url: ApiRestURLS.agregarAsociado ,
                data: { idCliente: idCliente,
                        grupoId: idGrupo },
                type: 'post',   
                success: function(data){
                }
              });
        })
    }
}