import '../grilla/jquery-aggrid';
import { AgCellRender } from '../grilla/ag-plugins/agCellRender';
import MdlCrud from "./mdlCrud.js";
import {alertaNotificar} from '../globales/alertas';

var oMdlCrud = new MdlCrud();
$(function () {
    if(errors.length > 0){
        const sError = errors.join('; ');
        alertaNotificar({title: sError, icon: 'error'});
    }
    const _urlGetData = { GRILLA_LISTAR_JSON: ApiRestURLS.listGrupos, ORDER_SORD: "DESC", ORDER_SIDX: "id" };
    const _gridOption = {
        columnDefs: aDfColumnas,
        rowSelection: 'single',
        headerHeight: 34,
        pagination: true,
        enableSorting: true,
        enableServerSideSorting: true,
        enableColResize: true,
        suppressPaginationPanel: true,
        components: {
            'agCellRender': AgCellRender
        },
    };
    var _btn = {
        'themeIcon': 'square',
        'plugins': ['refresh', 'search'],
        'add': aGridButtons
    };
    oGrid = $("#myGrid").SIP_AgGrid({ url: _urlGetData, gridOptions: _gridOption, btn: _btn, autoSize: false, autoHeight: "auto", paginador: true });
    $.ajaxSetup({ cache: false });
    $("#loading").hide();
    $("#myGrid").on("click", "#ag-myGrid-nuevo-registro",  ()=>{
        $('#loading').show();
        $('.modal-dialog').addClass('modal-xl');
        $('#tituloModalGlobal').html('Nuevo Grupo <i class="fas fa-users"></i>');
        $('#contenidoModalGlobal').empty().load(ApiRestURLS.grupoModif, function (){
          $('#modalGlobal').modal({backdrop: true,keyboard: false});
          $('#modalGlobal').modal('show');
          oMdlCrud.init();
          $('#loading').hide();
        });
    })
    $("#myGrid").on("click","#ag-myGrid-editar-registro", "click", ()=>{
        let selectedRows = oGrid.getGridOptions().api.getSelectedRows();
        if(selectedRows.length > 0){
            let selectedRows = oGrid.getGridOptions().api.getSelectedRows();
            $('#loading').show();
            $('.modal-dialog').addClass('modal-xl');
            $('#tituloModalGlobal').html('Nuevo Grupo <i class="fas fa-users"></i>');
            $('#contenidoModalGlobal').empty().load(ApiRestURLS.grupoModif, {id:selectedRows[0].id}, function (){
            $('#modalGlobal').modal({backdrop: true,keyboard: false});
            $('#modalGlobal').modal('show');
            oMdlCrud.init();
            $('#loading').hide();
            });
        }else{
            Swal.fire({
            icon: 'warning',
            title: 'Por favor seleccione una fila.',
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 6000,
            timerProgressBar: true,
            onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
            });
        }
    })
    $('#myGrid').on('click', '#ag-myGrid-eliminar-registro', function(){
        let selectedRows = oGrid.getGridOptions().api.getSelectedRows();
        if(selectedRows.length > 0){
            Swal.fire({
                title: '¿Confirma eliminar el registro seleccionado?',
                html: `<b class="text-primary">El grupo: </b> <b class="text-info">${selectedRows[0].nombre}</b><br>`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if( result.isConfirmed ){
                    $.ajax({
                        url: ApiRestURLS.grupoDelete,
                        data: { idGrupo: selectedRows[0].id},
                        type: 'post',
                        beforeSend: function(){
                            $('#loading').show();
                        },
                        success: function(data){
                            $('#loading').hide();
                            Swal.fire({
                                icon: (data.status == 1) ? 'success' : 'warning',
                                title: data.message,
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: (data.status == 1) ? 6000 : 8000,
                                timerProgressBar: true,
                                onOpen: (toast) => {
                                  toast.addEventListener('mouseenter', Swal.stopTimer)
                                  toast.addEventListener('mouseleave', Swal.resumeTimer)
                                }
                            });
                            if( data.status == 1 ){
                                oGrid.refreshGrid();
                            }
                        },
                        error: function(data){
                            $('#loading').hide();
                            alertaSimple({
                                mensaje: data.responseJSON.msg,
                                tipo: 'warning',
                                toast: true, 
                                position: 'top-end',
                                mostrarBoton:false,
                                tiempo: 6000
                            });
                        }
                    });
                }
            });
        }else{
            Swal.fire({
                icon: 'warning',
                title: 'Por favor seleccione una fila.',
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 6000,
                timerProgressBar: true,
                onOpen: (toast) => {
                  toast.addEventListener('mouseenter', Swal.stopTimer)
                  toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            });
        }
        return false;
    });
    $('#myGrid').on('click', '#ag-myGrid-descargar-csv', function(){
        let selectedRows = oGrid.getGridOptions().api.getSelectedRows();
        if(selectedRows.length > 0){
            window.open(ApiRestURLS.grupoDownload + "?grupoId=" + selectedRows[0].id);
        }else{
            Swal.fire({
            icon: 'warning',
            title: 'Por favor seleccione una fila.',
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 6000,
            timerProgressBar: true,
            onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
            });
        }
    })
    $('#myGrid').on('click', '#ag-myGrid-agregar-destinatarios', function(){
        let selectedRows = oGrid.getGridOptions().api.getSelectedRows();
        if(selectedRows.length > 0){
            window.location = ApiRestURLS.addDestinatarios.replace('_idGrupo_', selectedRows[0].id);
        }else{
            alertaNotificar({title: 'Por favor seleccione una fila', icon: 'warning'});
        }
    })  
});