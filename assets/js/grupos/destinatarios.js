import '../grilla/jquery-aggrid';
import { AgCellRender } from '../grilla/ag-plugins/agCellRender';
import MdlUpload from "./mdlUpload.js";

var oMdlUpload = new MdlUpload();

$(function () {

    $( "#tabs" ).tabs();

    // Configuracion Tabla asociados
    const _urlGetDataAsociados = { GRILLA_LISTAR_JSON: ApiRestURLS.AsociadosJson, ORDER_SORD: "DESC", ORDER_SIDX: "esAsociado" , ADD_DATA:{idGrupo: idGrupo }};
    
    const _gridOption = {
        columnDefs: aDfColumnas,
        rowSelection: 'single',
        headerHeight: 34,
        pagination: true,
        enableSorting: true,
        enableServerSideSorting: true,
        enableColResize: true,
        suppressPaginationPanel: true,
        components: {
            'agCellRender': AgCellRender
        },
    };

    // listado de botones que usarán
    var aGridButtons = { "agregar-todos": {
        "class": ["hover-success"],
        "icon": ["fas fas fa-globe-americas text-primary", "fas fa-plus"],
        "id": "agregar-todos",
        "text": "",
        "title": "Agregar Todos" 
    },"eliminar-todos": {
        "class": ["hover-success"],
        "icon": ["fas far fa-times-circle text-warning"],
        "id": "eliminar-todos",
        "text": "",
        "title": "Eliminar Todos" 
    },"cargar-destinatarios": {
        "class": ["hover-success"],
        "icon": ["fas fas fa-file-upload text-success"],
        "id": "cargar-destinatarios",
        "text": "",
        "title": "Cargar Destinatarios" 
    }}

    var _btn = {
        'themeIcon': 'square',
        'plugins': ['refresh', 'search'],
        'add': aGridButtons
    };

    // Iniciamos Tabla
    oGridAsociados = $("#myGridAsociados").SIP_AgGrid({ url: _urlGetDataAsociados, gridOptions: _gridOption, btn: _btn, autoSize: false, autoHeight: false, paginador: true });
        
    // Clear Cache
    $.ajaxSetup({ cache: false });

    // Loading Hide
    $("#loading").hide();
    // Agregar todos los clientes como asociados
    $("#myGridAsociados").on("click", "#ag-myGridAsociados-agregar-todos",  ()=>{

        Swal.fire({
            title: '¿Realmente desea agregar todos los asociados?',
            html: ``,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar'
          }).then((result) => {
            if( result.isConfirmed ){
              $.ajax({
                url: ApiRestURLS.agregarAsociados,
                data: { grupoId: idGrupo},
                type: 'post',
                beforeSend: function(){
                  $('#loading').show();
                },
                success: function(data){
    
                  data = JSON.parse(data);
    
                  $('#loading').hide();
                  Swal.fire({
                    icon: (data.status == 200) ? 'success' : 'warning',
                    title: data.message,
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: ( data.status == 200 ) ? 6000 : 8000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                      toast.addEventListener('mouseenter', Swal.stopTimer)
                      toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                  });
    
                  if( data.status == 200 ){
                    oGridAsociados.refreshGrid();
                  }
                },
                error: function(data){
                  $('#loading').hide();
                  alertaSimple({
                    mensaje: data.responseJSON.msg,
                    tipo: 'warning',
                    toast: true, 
                    position: 'top-end',
                    mostrarBoton:false,
                    tiempo: 6000
                  });
                }
              });
            }
          });
    })
    
    // Eliminar todos asociados
    $("#myGridAsociados").on("click", "#ag-myGridAsociados-eliminar-todos",  ()=>{

        Swal.fire({
            title: '¿Realmente desea desasociar todos los asociados?',
            html: ``,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if( result.isConfirmed ){
                $.ajax({
                    url: ApiRestURLS.eliminarAsociados,
                    data: { grupoId: idGrupo},
                    type: 'post',
                    beforeSend: function(){
                    $('#loading').show();
                    },
                    success: function(data){

                    data = JSON.parse(data);
                    $('#loading').hide();
                    Swal.fire({
                        icon: ( data.status == 200 ) ? 'success' : 'warning',
                        title: data.message,
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: (data.status == 1) ? 6000 : 8000,
                        timerProgressBar: true,
                        onOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    });
        
                    if( data.status == 200 ){
                        oGridAsociados.refreshGrid();
                    }
                    },
                    error: function(data){
                    $('#loading').hide();
                    alertaSimple({
                        mensaje: data.responseJSON.msg,
                        tipo: 'warning',
                        toast: true, 
                        position: 'top-end',
                        mostrarBoton:false,
                        tiempo: 6000
                    });
                    }
                });
            }
        });
    })

    $("#myGridAsociados").on("click","#ag-myGridAsociados-cargar-destinatarios", "click", ()=>{
        $('#loading').show();
        $('.modal-dialog').addClass('modal-xl');
        $('#tituloModalGlobal').html('Cargar Destinatarios <i class="far fa-building"></i><i class="fas fa-plus"></i>');
        $('#contenidoModalGlobal').empty().load(ApiRestURLS.cargarAsociados, {grupoId: idGrupo}, function (){
            $('#modalGlobal').modal({backdrop: true,keyboard: false});
            $('#modalGlobal').modal('show');
            $('#modalGlobal').height(700);
            oMdlUpload.init(idGrupo)
            
            $('#loading').hide();
        });
    })

  $('#myGridAsociados').on('click', '.agregar-cliente', function(){
    let idCliente = $(this).data("id-cliente")

    $.ajax({
      url: ApiRestURLS.agregarAsociado ,
      data: { idCliente: idCliente,
              grupoId: idGrupo },
      type: 'post',
      beforeSend: function(){
        $('#loading').show();
      },
      success: function(data){
        data = JSON.parse(data);
        console.log(data.status);
        $('#loading').hide();
        Swal.fire({
          icon: ( data.status == 200 ) ? 'success' : 'warning',
          title:  ( data.status == 200 ) ? `<span style="color:#029a19">${data.message}</span>` : `<span style="color:#9a0228">${data.message}</span>`,
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: (data.status == 1) ? 6000 : 8000,
          timerProgressBar: true,
          onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        });

      },
      error: function(data){
        $('#loading').hide();
        alertaSimple({
          mensaje: data.responseJSON.msg,
          tipo: 'warning',
          toast: true, 
          position: 'top-end',
          mostrarBoton:false,
          tiempo: 6000
        });
      }
    });
  });
});