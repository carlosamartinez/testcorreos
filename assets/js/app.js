/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
*/

// any CSS you import will output into a single css file (app.css in this case)
import '../css/app.scss';
import Swal from 'sweetalert2'

// create global $ and jQuery variables
import $ from 'jquery';
global.$ = global.jQuery = $;
global.Swal = Swal;
/* --- --- --- Bootstrap --- --- --- */
import 'bootstrap';

import MdlCambiarPassword from "./app/mdlCambiarPassword.js";  


import { alertaSimple, alertaPersonalizada, alertaCompleta } from './globales/alertas';
window.oAlertas = {alertaSimple: alertaSimple, alertaPersonalizada: alertaPersonalizada};

let omdlcambiarclave = new MdlCambiarPassword();

$('#btnCambiarClave').click( () => {
    $('#loading').show();
    $('.modal-dialog').addClass('modal-lg');
    $('#tituloModalGlobal').html('Cambiar Clave Usuario&nbsp;' + '&nbsp;<i class="fas fa-user"></i>');
    
    $('#contenidoModalGlobal').empty().load(_urlCambiarClaveIndex, {}, function (){
        $('#modalGlobal').modal({backdrop: true,keyboard: false});
        $('#modalGlobal').modal('show');
        omdlcambiarclave.init();
        $('#loading').hide();
    });

});


$('#btnCerrarSesion').click(()=>{
   alertaCompleta({mensaje: "¿ Confirma el cierre de Sesión ?", tipo: "question", textoBoton: "Aceptar", textoBotonCancelar: "Cancelar"}).then((willDelete) => {
       if (willDelete.value) {
           $('#loading').show();
           location.href = _urlCierreSession;
       }
   });
});