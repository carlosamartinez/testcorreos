
$(function () {

    $("#loading").hide();

    CKEDITOR.replace( 'configuracion_AceptarTerminosYcondiciones' );

    $("#formulario").bind("submit",function(e){

        e.preventDefault();

        $("#configuracion_AceptarTerminosYcondiciones").val(CKEDITOR.instances.configuracion_AceptarTerminosYcondiciones.getData());

        if(!$("#configuracion_tamanio").val() ){
            Swal.fire({
                icon: 'warning',
                title: "ingresa el tamaño",
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer:  6000 ,
                timerProgressBar: true
            });
            return;
        }
        if( !$("#configuracion_AceptarTerminosYcondiciones").val() ){
            Swal.fire({
                icon: 'warning',
                title: "ingresa los términos y condiciones",
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer:  6000 ,
                timerProgressBar: true
            });
            return;
        }
        if( !$("#configuracion_cantImgPaquete").val() ){
            Swal.fire({
                icon: 'warning',
                title: "ingresa cantidad maxima de fotos por paquete",
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer:  6000 ,
                timerProgressBar: true
            });
            return;
        }

        $.ajax({
            url: saveConfUrl,
            type: $(this).attr("method"),
            data: {
                'tamanio':$("#configuracion_tamanio").val(),
                'imgxpack':$("#configuracion_cantImgPaquete").val(),
                'tyc':$("#configuracion_AceptarTerminosYcondiciones").val(),
            },
            beforeSend: function(){
                $('#loading').show();
            },
            success: function(data){
                $("#loading").hide();
                Swal.fire({
                    icon: 'success' ,
                    title: "datos guardados correctamente",
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 6000 ,
                    timerProgressBar: true
                });
            }
        })
    })

});