// Importar - Css
//import '../../css/usuarios/index.css';
// Importar - Javascrip
import '../grilla/jquery-aggrid';
import { AgCellRender } from '../grilla/ag-plugins/agCellRender';



/* --- --- --- Variables Globales --- --- --- */


// Configuracion Tabla
const _urlGetDataAsociados = { GRILLA_LISTAR_JSON: ApiRestURLS.listDestinatariosAsociados, ORDER_SORD: "DESC", ORDER_SIDX: "id",ADD_DATA:{idEnvio: idEnvio, tipoGrupo:tipoGrupo } };
const _gridOptionAsociados = {
  columnDefs: aDfColumnasAsociado,
  rowSelection: 'single',
  headerHeight: 34,
  pagination: true,
  enableSorting: true,
  enableServerSideSorting: true,
  enableColResize: true,
  suppressPaginationPanel: true,
  components: {
    'agCellRender': AgCellRender
  },
};

var _btnAsociados = {
  'themeIcon': 'square',
  'plugins': ['refresh', 'search'],
  'add': [
    {
      "id":"asignar-todos",
      "text":"",
      "title":"Agregar Todo",
      "icon":["fas fa-plus text-success"],
      "class":["hover-success"]
    },
    {
      "id":"eliminar-todos",
      "text":"",
      "title":"Eliminar Todos",
      "icon":["far fa-trash-alt text-danger"],
      "class":["hover-success"]
    },
    {
      "id":"exportar-estadisticas",
      "text":"",
      "title":"Exportar Estadisticas",
      "icon":["far fa-file-excel text-success"],
      "class":["hover-success"]
    }
  ]
};

// Iniciamos Tabla
oGridAsociados = $("#myGridAsociados").SIP_AgGrid({ url: _urlGetDataAsociados, gridOptions: _gridOptionAsociados, btn: _btnAsociados, autoSize: false, autoHeight: "auto", paginador: true });
    
$(function () {
  

  // Clear Cache
  $.ajaxSetup({ cache: false });


  $('#myGridAsociados').on('click', '#ag-myGridAsociados-asignar-todos', function(){

    $('#loading').show();
    $.ajax({ 
        url: ApiRestURLS.agregarAsociado+oGridAsociados.getDataSource().globalFilterQuery,
        type: 'post',
        data: {envioId:idEnvio, multiple:true },
        dataType: 'json',
        success: function(data){
            $('#loading').hide();
            oGridAsociados.refreshGrid();
            
            Swal.fire({
              icon: 'success',
              title: 'Los Asociados se han agregado con exito a el para recibir copia del correo.',
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 6000,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            });

        }
    });  
    
  });


  $('#myGridAsociados').on('click', '#ag-myGridAsociados-eliminar-todos', function(){

    $('#loading').show();
    $.ajax({ 
        url: ApiRestURLS.eliminarAsociado+oGridAsociados.getDataSource().globalFilterQuery,
        type: 'post',
        data: {envioId:idEnvio, multiple:true },
        dataType: 'json',
        success: function(data){
            $('#loading').hide();
            
            oGridAsociados.refreshGrid();

            Swal.fire({
              icon: 'success',
              title: 'Ya no recibira copia de correo de ningun asociado.',
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 6000,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            });

        }
    }); 
    
  });


  $('#myGridAsociados').on('click', '#ag-myGridAsociados-exportar-estadisticas', function(){

    Swal.fire({
      icon: 'question',
      title: '',
      html: 'Que archivo quiere descargar?',
      showDenyButton: true,
      showCancelButton: false,
      confirmButtonText: `Detalle`,
      denyButtonText: `Consolidado`,
    }).then((result) => {

      if(result.isDismissed)
        return;
      
      if (result.isConfirmed) {
        window.open(ApiRestURLS.exportarReporteAsociado+'?detalle=1&envioId='+idEnvio+'&' + oGridAsociados.getDataSource().globalFilterQuery);
      } else if (result.isDenied) {
        window.open(ApiRestURLS.exportarReporteAsociado+'?envioId='+idEnvio+'&' + oGridAsociados.getDataSource().globalFilterQuery);
      }
    })

    
  });


  $('#myGridAsociados').on('click', '.selectAsociado', function(){
    
    var id = $(this).attr('id').split('_')[1];
    var tipo = $(this).attr('alt');

    var elemento = $(this);

    if(tipo=='agregar'){
       
       $('#loading').show();  
       $.ajax({ 
            url: ApiRestURLS.agregarAsociado ,
            type: 'post',
            data: {envioId:idEnvio, asociadoId: id},
            dataType: 'json',
            success: function(data){
                //$.noty.closeAll();
                $('#loading').hide();
                
                //$('#t_GrillaAsociados').html('El Asociado se ha agregado para recibir copia del Correo.');
                Swal.fire({
                  icon: 'success',
                  title: 'El Asociado se ha agregado para recibir copia del Correo.',
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  timer: 6000,
                  timerProgressBar: true,
                  onOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                  }
                });
                
                elemento.attr("alt", "quitar");
            }
        });
       
       
    }else{
       
       $('#loading').show();
       $.ajax({ 
            url: ApiRestURLS.eliminarAsociado,
            type: 'post',
            data: {envioId:idEnvio, asociadoId: id},
            dataType: 'json',
            success: function(data){
                $('#loading').hide();

                elemento.attr("alt", "agregar");

                Swal.fire({
                  icon: 'success',
                  title: 'Se cancela la recepcion de copia del correo del Asociado.',
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  timer: 6000,
                  timerProgressBar: true,
                  onOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                  }
                });

                
            }
        });
       
       
    }


  });


  $('#myGridAsociados').on('click', '.reenviarmsg', function(){
    
    var id = $(this).attr('id').split('_')[1];
    var envioIntegranteId = $(this).data('enviointegranteid');
    var integranteGrupoId = $(this).data('integrantegrupoid');

    var elemento = $(this);

    $('#loading').show();  
       $.ajax({ 
          url: ApiRestURLS.reenviarCorreo ,
          type: 'post',
          data: {
            envioId:idEnvio, 
            reenvioAsociadoId: id,
            envioIntegranteId: envioIntegranteId,
            integranteGrupoId: integranteGrupoId
          },
          dataType: 'json',
          success: function(data){
              $('#loading').hide();

              var mensaje = '';
              if(data.resultado=='1'){
                mensaje = 'El Asociado ha recibido copia del correo.';
              }else if(data.resultado=='6'){
                mensaje = 'Error: no se pudo validar las credenciales para el envio.';
              }else{
                mensaje = 'Error: el Asociado no ha recibido copia del correo.';
              }
              
              Swal.fire({
                icon: 'success',
                title: mensaje,
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 6000,
                timerProgressBar: true,
                onOpen: (toast) => {
                  toast.addEventListener('mouseenter', Swal.stopTimer)
                  toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
              });
              
          }
      });

  });


  // Loading Hide
  $("#loading").hide();
});