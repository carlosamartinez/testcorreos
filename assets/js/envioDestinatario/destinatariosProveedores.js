// Importar - Css
//import '../../css/usuarios/index.css';
// Importar - Javascrip
import '../grilla/jquery-aggrid';
import { AgCellRender } from '../grilla/ag-plugins/agCellRender';




/* --- --- --- Variables Globales --- --- --- */




// Configuracion Tabla
const _urlGetDataProveedores = { GRILLA_LISTAR_JSON: ApiRestURLS.listDestinatariosProveedor, ORDER_SORD: "DESC", ORDER_SIDX: "id",ADD_DATA:{idEnvio: idEnvio } };
const _gridOptionProveedores = {
  columnDefs: aDfColumnasProveedor,
  rowSelection: 'single',
  headerHeight: 34,
  pagination: true,
  enableSorting: true,
  enableServerSideSorting: true,
  enableColResize: true,
  suppressPaginationPanel: true,
  components: {
    'agCellRender': AgCellRender
  },
};

var _btnProveedores = {
  'themeIcon': 'square',
  'plugins': ['refresh', 'search'],
  'add': [
    {
      "id":"asignar-todos",
      "text":"",
      "title":"Agregar Todo",
      "icon":["fas fa-plus text-success"],
      "class":["hover-success"]
    },
    {
      "id":"eliminar-todos",
      "text":"",
      "title":"Eliminar Todos",
      "icon":["far fa-trash-alt text-danger"],
      "class":["hover-success"]
    },
    {
      "id":"exportar-estadisticas",
      "text":"",
      "title":"Exportar Estadisticas",
      "icon":["far fa-file-excel text-success"],
      "class":["hover-success"]
    }
  ]
};

// Iniciamos Tabla
oGridProveedores = $("#myGridProveedores").SIP_AgGrid({ url: _urlGetDataProveedores, gridOptions: _gridOptionProveedores, btn: _btnProveedores, autoSize: false, autoHeight: "auto", paginador: true });
    
$(function () {
  

  // Clear Cache
  $.ajaxSetup({ cache: false });


  $('#myGridProveedores').on('click', '#ag-myGridProveedores-asignar-todos', function(){

    $('#loading').show();
    $.ajax({ 
        url: ApiRestURLS.agregarProveedor+oGridProveedores.getDataSource().globalFilterQuery,
        type: 'post',
        data: {envioId:idEnvio, multiple:true },
        dataType: 'json',
        success: function(data){
            $('#loading').hide();
            oGridProveedores.refreshGrid();
            
            Swal.fire({
              icon: 'success',
              title: 'Los Proveedores se han agregado con exito a el grupo.',
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 6000,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            });

        }
    });  
    
  });


  $('#myGridProveedores').on('click', '#ag-myGridProveedores-eliminar-todos', function(){

    $('#loading').show();
    $.ajax({ 
        url: ApiRestURLS.eliminarProveedor+oGridProveedores.getDataSource().globalFilterQuery,
        type: 'post',
        data: {envioId:idEnvio, multiple:true },
        dataType: 'json',
        success: function(data){
            $('#loading').hide();
            
            oGridProveedores.refreshGrid();

            Swal.fire({
              icon: 'success',
              title: 'Los Proveedor se han eliminado con exito de el grupo.',
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 6000,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            });

        }
    }); 
    
  });



  $('#myGridProveedores').on('click', '.selectProveedor', function(){

    var id = $(this).attr('id').split('_')[1];
    var elemento = $(this);

    if(elemento.hasClass( "btn-warning" )){
      var ruta = ApiRestURLS.agregarProveedor;
      var texto ='El Proveedor se ha agregado con exito a el grupo.';
      var claseout ='btn-warning';
      var clasein ='btn-success';
    }else{
      var ruta = ApiRestURLS.eliminarProveedor;
      var texto ='El Proveedor se ha eliminado del grupo con exito.';
      var claseout ='btn-success';
      var clasein ='btn-warning';
    }

    $('#loading').show();  
       $.ajax({ 
          url: ruta ,
          type: 'post',
          data: {envioId:idEnvio, proveedorId: id},
          dataType: 'json',
          success: function(data){
              $('#loading').hide();

              elemento.removeClass(claseout);
              elemento.addClass(clasein);

              Swal.fire({
                icon: 'success',
                title: texto,
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 6000,
                timerProgressBar: true,
                onOpen: (toast) => {
                  toast.addEventListener('mouseenter', Swal.stopTimer)
                  toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
              });
              
          }
      });

  });


  $('#myGridProveedores').on('click', '.reenviarmsg', function(){
    
    var id = $(this).attr('id').split('_')[1];
    var envioIntegranteId = $(this).data('enviointegranteid');
    var integranteGrupoId = $(this).data('integrantegrupoid');

    var elemento = $(this);

    $('#loading').show();  
       $.ajax({ 
          url: ApiRestURLS.reenviarCorreo ,
          type: 'post',
          data: {
            envioId:idEnvio, 
            reenvioProveedorId: id,
            envioIntegranteId: envioIntegranteId,
            integranteGrupoId: integranteGrupoId
          },
          dataType: 'json',
          success: function(data){
              $('#loading').hide();

              var mensaje = '';
              var clase = '';
              if(data.resultado=='1'){
                mensaje = 'El Proveedor ha recibido copia del correo.';
                clase = 'success';
              }else if(data.resultado=='6'){
                mensaje = 'Error: no se pudo validar las credenciales para el envio.';
                clase = 'error';
              }else{
                mensaje = 'Error: el Proveedor/Contacto no ha recibido copia del correo.';
                clase = 'error';
              }
              
              Swal.fire({
                icon: clase,
                title: mensaje,
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 6000,
                timerProgressBar: true,
                onOpen: (toast) => {
                  toast.addEventListener('mouseenter', Swal.stopTimer)
                  toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
              });
              
          }
      });

  });


  $('#myGridProveedores').on('click', '#ag-myGridProveedores-exportar-estadisticas', function(){

    Swal.fire({
      icon: 'question',
      html: 'Que archivo quiere descargar?',
      showDenyButton: true,
      showCancelButton: false,
      confirmButtonText: `Detalle`,
      denyButtonText: `Consolidado`,
    }).then((result) => {
      
      if(result.isDismissed)
        return;

      let urlReporte = ApiRestURLS.exportarReporteProveedor+'?envioId='+idEnvio
      if(result.isConfirmed)
        urlReporte += "&detalle=1";
        
      window.open(urlReporte);

    })

  });

  // Loading Hide
  $("#loading").hide();
});