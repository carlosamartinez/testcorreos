import '../grilla/jquery-aggrid';

// Configuracion Tabla
const settingsAgGrid = {
    url: { GRILLA_LISTAR_JSON: ApiRestURLS.listClientes, ORDER_SORD: "DESC", ORDER_SIDX: "id" }, 
    gridOptions: {columnDefs: aDfColumnas}, 
    btn: {
      'plugins': ['refresh', 'search'],
      'add': aGridButtons
    }, 
    autoHeight: "auto"
};

// Iniciamos Tabla
oGrid = $("#myGrid").SIP_AgGrid(settingsAgGrid);

$(function () {
  // Clear Cache
  $.ajaxSetup({ cache: false });


  // Loading Hide
  $("#loading").hide();
});
