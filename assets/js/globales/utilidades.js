import $ from "jquery";
import "jquery-circle-progress"; // Grafica Circulos

/**
 * Anderson Barbosa
 * Evento para reducir envió de peticiones AJAX 
 * @param {string} sUrl Dirección Url para realizar la petición
 * @param {string} oData Datos a enviar
 * @param {string} oConfigure Parametros adicionales a implementar en el AJAX (ayuda especialmente para envío de archivos)
*/
function enviarDatos(sUrl, oData = {}, oConfigure = {}, bLoader = true){
    let deferred = jQuery.Deferred();

    let oDefaultAjax = {
        type: "post",
        dataType: "json",
        beforeSend: () => {
            if(bLoader)
                $('#loading').show();
        },
        success: function (response) {
            if(response.status === undefined || response.status === "" || response.status === 0){
                deferred.reject(response);
            }else{
                deferred.resolve(response);
            }
        },
        error: err => { deferred.reject(err.responseJSON) }
    };

    // Implementación configuración AJAX
    let oAjax = $.extend(oDefaultAjax, oConfigure, {
        url: sUrl,
        data: oData
    });

    //  AJAX
    $.ajax(oAjax).always( () => {
        if(bLoader)
            $('#loading').hide();
    } );

    return deferred.promise();
}

/**
 * Esqueleto Modal modales
 * @param {string} sId
 * @param {string} sTitleModal
 * @returns {modalContent.oModal|$|_$|Element|$.el|_$.el}
 */
function modalContent(sId, sTitleModal, sTheme = "default", sSize = "lg", infoFooter = [], buttons = [], sizeHeight = "") {

    if($("#" + sId).length > 0){
        return $("#" + sId);
    }else{
        switch(sTheme){
            case "primary": sTheme = "bg-primary text-white"; break;
            case "warning": sTheme = "bg-warning"; break;
            case "danger": sTheme = "bg-danger text-white"; break;
            case "info": sTheme = "bg-info text-white"; break;
            case "success": sTheme = "bg-success text-white"; break;
            case "secondary": sTheme = "bg-secondary text-white"; break;
            default: sTheme = "bg-default"; break;
        }
        
        switch(sSize){
            case "lg": sSize = "modal-lg"; break;
            case "sm": sSize = "modal-sm"; break;
            case "full": sSize = "modal-full"; break;
            case "full-sm": sSize = "modal-full-sm"; break;
            default: sSize = ""; break;
        }

        let sButtons = "";
        let sInfoFooter = "";

        buttons.forEach(function(btn){
            let iconos = "";
            btn.icon.forEach(function(icono){
                iconos += `<b><i class="${icono}"></i>&nbsp;</b>`;
            });
            sButtons += `<div class="p-1 bd-highlight" ${btn.visible}><button type="${btn.type}" id="${btn.id}" ${btn.evet} class="btn ${btn.class}" title="${btn.title}"><b>${btn.text} ${iconos}</b></button></div>`
        });

        infoFooter.forEach(function(info){
            sInfoFooter += `<div class="p-1 bd-highlight"><i class="${info.icon}"></i>&nbsp;<label>${info.text}</label>&nbsp;<b class="text-primary" id="${info.idEdit}">${info.textEdit}</b>&nbsp;</div>`
        });

        let html = `<div id="${sId}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">`;
        html += `<div class="modal-dialog ${sSize}" style="height:${sizeHeight};">`;
        html += `<div class="modal-content" style="height:${sizeHeight};">`;
        html += `<div class="modal-header ${sTheme}">`;
        html += `<h6 class="modal-title">` + sTitleModal + `</h6>`;
        html += `<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>`;
        html += `</div>`;
        html += `<div class="modal-body">`;
        html += `</div>`;
        html += `<div class="modal-footer py-1 d-flex flex-wrap bd-highlight">`;
        //html += `<div class="">`;
        html += `${sInfoFooter+sButtons}<div class="p-1 bd-highlight"><button type="button" data-dismiss="modal" class="btn btn-warning btn-sm" title="Cerrar"><b><i class="fa fa-window-close-o" aria-hidden="true"></i>&nbsp;Cerrar</b></button></div>`;
        //html += `</div>`;
        html += `</div>`; // content
        html += `</div>`; // dialog
        html += `</div>`; // footer
        html += `</div>`; // modalWindow
        // Implementamos Modal
        let oModal = $(html);
    
        $("body").append(oModal);
    
        return oModal;
    }
}

/**
 * Esqueleto Modal modales
 * @param {string} sId
 * @param {string} sTitleModal
 * @returns {modalContent.oModal|$|_$|Element|$.el|_$.el}
 */
function cardContent(oDataCard) {

    // sId, sTitleModal, sTheme = "default", sSize = "lg"

    if($("#" + sId).length > 0){
        return $("#" + sId);
    }else{

        let oDefault = {
            id: "card_" + Math.floor(Math.random() * 101),
            titulo: null,
            imagen: null,
            contenido: "",
            footer: true,
            tema: "ligth"
        }


        switch(sTheme){
            case "primary": sTheme = "bg-primary text-white"; break;
            case "warning": sTheme = "bg-warning"; break;
            case "danger": sTheme = "bg-danger text-white"; break;
            case "info": sTheme = "bg-info text-white"; break;
            case "success": sTheme = "bg-success text-white"; break;
            default: sTheme = "bg-default"; break;
        }
        
        switch(sSize){
            case "lg": sSize = "modal-lg"; break;
            case "sm": sSize = "modal-sm"; break;
            case "full": sSize = "modal-full"; break;
            case "full-sm": sSize = "modal-full-sm"; break;
            default: sSize = ""; break;
        }


        let html = `<div id="${sId}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">`;
        html += `<div class="modal-dialog ${sSize}">`;
        html += `<div class="modal-content">`;
        html += `<div class="modal-header ${sTheme}">`;
        html += `<h5 class="modal-title">` + sTitleModal + `</h4>`;
        html += `<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>`;
        html += `</div>`;
        html += `<div class="modal-body">`;
        html += `</div>`;
        html += `<div class="modal-footer py-1">`;
        html += `<button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>`;
        html += `</div>`; // content
        html += `</div>`; // dialog
        html += `</div>`; // footer
        html += `</div>`; // modalWindow
        // Implementamos Modal
        let oModal = $(html);
    
        $("body").append(oModal);
    
        return oModal;
    }
}


function createDataTotales(idDrogueria) {
    var totales = [];

    // console.log(oGrid.totales[idDrogueria].total.totalRotulos)
    let CjSelladasNoConfirmadas = oGrid.totales[idDrogueria].total.cajasSelladas - parseInt(oGrid.totales[idDrogueria].confirmado.cajasSelladas);
    var totalRotulos =  (oGrid.totales[idDrogueria].total.totalRotulos) - CjSelladasNoConfirmadas;
        // parseInt(oGrid.totales[idDrogueria].confirmado.cubetasPlasticas) + 
        // parseInt(oGrid.totales[idDrogueria].confirmado.cajasSelladas) + 
        // parseInt(oGrid.totales[idDrogueria].confirmado.cajasPlasticas) + 
        // parseInt(oGrid.totales[idDrogueria].confirmado.cajasBolsas) + 
        // parseInt(oGrid.totales[idDrogueria].confirmado.tarjetas) + 
        // parseInt(oGrid.totales[idDrogueria].confirmado.memorandos) + 
        // parseInt(oGrid.totales[idDrogueria].confirmado.neveras);
    // console.log("Prueba",totalRotulos)
    oGrid.totales[idDrogueria].confirmado.totalRotulos = totalRotulos;

    totales[0] = oGrid.totales[idDrogueria].total
    totales[1] = oGrid.totales[idDrogueria].confirmado;
    totales[2] = {id: 'T Nevera °C', cubetasPlasticas: oGrid.totales[idDrogueria].confirmado.tempNevera, seccion: 'footer' };
    // console.log("Prueba",totales)
    return totales;

}


function reservarDataTotales(idDrogueria, data) {

    if(oGrid.totales[idDrogueria] == undefined){
        oGrid.totales[idDrogueria] = { 
          'total' :{
              id: 'Totales',
              cubetasPlasticas: data.cubetasPlasticas,
              cajasSelladas: data.cajasSelladas,
              cajasPlasticas: data.cajasBolsas,
              cajasBolsas: data.cajasBolsas,
              neveras: data.neveras,
              tarjetas: data.tarjetas,
              memorandos: data.memorandos,
              tempNevera: data.tempNeveras,
              totalRotulos: data.totalOriginal,
              seccion: data.seccion
            }
          }


        oGrid.totales[idDrogueria].confirmado ={
            id: 'Totales Confirmado',
            cubetasPlasticas: data.totalCubetas,
            cajasSelladas: data.totalCajasSelladas,
            cajasPlasticas: data.totalCajas,
            cajasBolsas: data.totalBolsas,
            neveras: data.totalNeveras,
            tarjetas: data.totalTarjetas,
            memorandos: data.totalMemorandos,
            tempNevera: data.tempNeveras,
            totalRotulos: data.totales[0].totalRotulos,
            modificado : 0,
            seccion: data.seccion
        }
        
    }else{
        if(oGrid.totales[idDrogueria].confirmado.modificado == 0){

            oGrid.totales[idDrogueria].confirmado ={
                id: 'Totales Confirmado',
                cubetasPlasticas: data.totalCubetas,
                cajasSelladas: data.totalCajasSelladas,
                cajasPlasticas: data.totalCajas,
                cajasBolsas: data.totalBolsas,
                neveras: data.totalNeveras,
                tarjetas: data.totalTarjetas,
                memorandos: data.totalMemorandos,
                tempNevera: data.tempNeveras,
                totalRotulos: data.totales[0].totalRotulos,
                modificado : 0,
                seccion: data.seccion
            }

        }

    }
}


function generarGraficaCirculo(oSettings){

    let oDefault = {
        campo: null, // Campo Objeto JQUERY
        startAngle: -1.5, // Angulo
        color: "#000000", // Color Circulo
        value: 0, // Valor Porcentaje
        lineCap: "round",
        size: 50 // Tamaño 
    }

    // Validaciones Campo
    if(!oSettings.campo || oSettings.campo.length == 0){
        return;
    }

    // Implementar Configuración
    oSettings = $.extend(true, oDefault, oSettings);

    // Evento Circular
    oSettings.campo.circleProgress({
        value: oSettings.value,
        startAngle: oSettings.startAngle,
        lineCap: oSettings.lineCap,
        size: oSettings.size,
        fill: {color: oSettings.color}
    }).on('circle-animation-progress', function(event, progress, stepValue) {
        $(this).find('strong').html(Math.round(100 * stepValue) + '<i>%</i>');
    });
}


export { enviarDatos, modalContent, createDataTotales, reservarDataTotales, generarGraficaCirculo};