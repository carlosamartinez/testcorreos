export default class Mensajes{
   constructor(){
      this.soporte = "si el error persiste por favor contactar a soporte."
   }

   get errorGlobal(){
      return `Algo salió mal, ${this.soporte}`;
   }

   /**
    * Usualmente es utilizado para los iconos de la grilla transferencista e inventario
    * @param {string} sPopover 
   */
   errorMensajePopover(sMensaje){
      return `No fue posible <b>${sMensaje}</b> de <b>"${this.errorPopover}"</b> por favor intente nuevamente, ${this.soporte}`;
   }
   set setErrorPopover(sPopover){
      this.errorPopover = sPopover;
   }
   get getErrorPopoverObtener(){
      return this.errorMensajePopover("obtener información");
   }
   get getErrorPopoverGuardar(){
      return this.errorMensajePopover("guardar información");
   }
}