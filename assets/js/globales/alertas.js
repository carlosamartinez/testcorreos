import swal from 'sweetalert2';

// https://sweetalert.js.org/docs/#configuration

function alertaSencilla(titulo = 'Cargando') {
    const span = document.createElement("span");
    span.innerHTML = "<i class='fa fa-refresh fa-spin fa-3x fa-fw text-success'></i>";
    swal.fire({
        title: titulo,
        content: span,
        buttons: false
    });
}

function alertaPersonalizada(parametros) {
    return swal.fire(parametros);
}

function generarCarga() {
    let oDataMensaje = {
        title: "Cargando",
        content: $("<div />", {
            html: "<i class='fa fa-refresh fa-spin fa-3x fa-fw text-success'></i>"
        }).get(0),
        icon: "info",
        buttons: false
    }
    return swal.fire(oDataMensaje);
}

function alertaSimple(oOpciones) {
    let oSettings = {
        titulo: "",
        mensaje: "PENDIENTE MENSAJE",
        tipo: "success",
        textoBoton: "Aceptar",
        toast: false,
        position: '',
        background: '#EFF2FB',
        mostrarBoton: true,
    }

    let oMensaje = $.extend(oSettings, oOpciones);

    var fondo = '';
    var customClass = '';
    if (oMensaje.toast == true) {
        customClass = 'switalertError';
        if (oMensaje.tipo === "success") fondo = '#5DCF82';
        else if (oMensaje.tipo === "error") fondo = '#AF4B4B';
        else if (oMensaje.tipo === "warning") fondo = '#FEB303';
        else if (oMensaje.tipo === "info") fondo = '#17A2B8';
        else fondo = '#C2C8F6';
    } else {
        fondo = '#EFF2FB';
    }

    let oDataMensaje = {
        title: oMensaje.titulo,
        html: $("<div />", {
            html: oMensaje.mensaje
        }).get(0),
        icon: (oMensaje.tipo) ? oMensaje.tipo : 'error',
        button: {
            text: (oMensaje.textoBoton) ? oMensaje.textoBoton : 'ACEPTAR',
            className: (oMensaje.tipo === "error") ? "btn-danger" : "btn-primary"
        },
        toast: (oMensaje.toast) ? oMensaje.toast : 'true',
        position: (oMensaje.position) ? oMensaje.position : 'top-end',
        background: fondo,
        showConfirmButton: (oMensaje.mostrarBoton) ? oMensaje.mostrarBoton : false,
        timer: (oMensaje.tiempo) ? oMensaje.tiempo : 2000,
        customClass: customClass
    }
    return swal.fire(oDataMensaje);
}

function alertaCompleta(oOpciones) {
    let oSettings = {
        title: '',
        icon: 'info',
        html: '',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        showCloseButton: false,
        focusConfirm: false
    }

    let oMensaje = $.extend(oSettings, oOpciones);
    let oDataMensaje = {
        title: oMensaje.titulo,
        html: $("<div />", {
            html: oMensaje.mensaje
        }).get(0),
        icon: (oMensaje.tipo) ? oMensaje.tipo : 'info',
        allowOutsideClick: (oMensaje.allowOutsideClick) ? oMensaje.allowOutsideClick : false,
        showCancelButton: (oMensaje.showCancelButton == false) ? oMensaje.showCancelButton : true,
        confirmButtonColor: (oMensaje.confirmButtonColor) ? oMensaje.confirmButtonColor : '#3FC3EE',
        cancelButtonColor: (oMensaje.cancelButtonColor) ? oMensaje.cancelButtonColor : '#DC3545',
        confirmButtonText: oMensaje.textoBoton,
        cancelButtonText: oMensaje.textoBotonCancelar,
        showCloseButton: (oMensaje.showCloseButton) ? oMensaje.showCloseButton : false,
        focusConfirm: (oMensaje.focusConfirm) ? oMensaje.focusConfirm : false,
        allowEnterKey: (oMensaje.allowEnterKey) ? oMensaje.allowEnterKey : false,
        allowEscapeKey: (oMensaje.allowEscapeKey) ? oMensaje.allowEscapeKey : false
    }
    return swal.fire(oDataMensaje);
}

function alertaNotificar(oOpciones) {


    let oSettings = $.extend({
        icon: 'success',
        title: '',
        text: '',
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 7500,
        timerProgressBar: true,
    }, oOpciones);

    switch(oSettings.icon){
        case 'error': oSettings.background = '#FECACA'; break;
        case 'warning': oSettings.background = '#FED7AA'; break;
    }

    return swal.fire(oSettings);
}

export {
   generarCarga,
   alertaSencilla,
   alertaPersonalizada,
   alertaNotificar,
   alertaSimple,
   alertaCompleta
};