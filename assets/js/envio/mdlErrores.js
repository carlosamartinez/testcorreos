
export default class MdlErrores {

  constructor(){}

  init(idEnvio){



    // Configuracion Tabla
    const _urlGetDataError = { GRILLA_LISTAR_JSON: ApiRestURLS.listErroesJson, ORDER_SORD: "DESC", ORDER_SIDX: "id", ADD_DATA:{idEnvio: idEnvio } };
    const _gridOptionError = {
      columnDefs: aDfColumnasError,
      onRowDoubleClicked: function (params) {
      },
      rowSelection: 'single',
      headerHeight: 34,
      pagination: true,
      enableSorting: true,
      enableServerSideSorting: true,
      enableColResize: true,
      suppressPaginationPanel: true
    };


    var _btn = {
      'themeIcon': 'square',
      'plugins': ['refresh', 'search'],
      'add': aGridButtonsError
    };

    // Iniciamos Tabla
    oGridError = $("#myGridError").SIP_AgGrid({ url: _urlGetDataError, gridOptions: _gridOptionError, btn: _btn, autoSize: false, paginador: true });


  }
}