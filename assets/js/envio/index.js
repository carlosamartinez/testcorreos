import 'select2';
import 'select2/dist/css/select2.min.css';
import '@ttskch/select2-bootstrap4-theme/dist/select2-bootstrap4.min.css'
import '../grilla/jquery-aggrid';
import { AgCellRender } from '../grilla/ag-plugins/agCellRender';
import MdlCrud from "./mdlCrud";
import MdlErrores from "./mdlErrores";
import { alertaNotificar } from '../globales/alertas';
import { enviarDatos } from '../globales/utilidades';
import Mensajes from '../globales/mensajes';
 

/* --- --- --- Variables Globales --- --- --- */
let oMdlCrud = new MdlCrud();
let oMdlErrores = new MdlErrores();
const mensajes = new Mensajes();
// Configuracion Tabla
const _urlGetData = { GRILLA_LISTAR_JSON: ApiRestURLS.listEnvios, ORDER_SORD: "DESC", ORDER_SIDX: "id" };
const _gridOption = {
  columnDefs: aDfColumnas,
  rowSelection: 'single',
  headerHeight: 34,
  rowHeight: 34,
  pagination: true,
  enableSorting: true,
  enableServerSideSorting: true,
  enableColResize: true,
  suppressPaginationPanel: true,
  components: {
    'agCellRender': AgCellRender
  },
};
aGridButtons["confirma-programacion"] = {
  class: ["hover-success"],
  icon: ["fas fa-calendar-check text-warning"],
  id: "confirma-programacion",
  text: "",
  title: "Programar envío"
};

aGridButtons["pausar-envio"] = {
  class: ["hover-success"],
  icon: ["far fa-hand-paper text-danger"],
  id: "pausar-envio",
  text: "",
  title: "Pausar envío"
};

var _btn = {
  'themeIcon': 'square',
  'plugins': ['refresh', 'search'],
  'add': aGridButtons
};



console.log("aGridButtons", aGridButtons);

// Iniciamos Tabla
oGrid = $("#myGrid").SIP_AgGrid({ url: _urlGetData, gridOptions: _gridOption, btn: _btn, autoSize: false, autoHeight: "auto", paginador: true });
    
// script para corregir el bloquedo de la modal generada por ckeditor
$.fn.modal.Constructor.prototype._enforceFocus = function() {
  $(document).off('focusin.bs.modal').on('focusin.bs.modal', $.proxy((function(e) {
    if (typeof this.$element !== 'undefined' &&
      this.$element[0] !== e.target &&
      !this.$element.has(e.target).length &&
      !$(e.target).closest('.cke_dialog, .cke').length) {
      this.$element.trigger('focus');
    }
  }), this));
};
// fin script


$(function () {
  // Clear Cache
  $.ajaxSetup({ cache: false });
  // Crear Registro
  $('#myGrid').on('click', '#ag-myGrid-nuevo-registro', function(){
    $('#loading').show();
    $('.modal-dialog').addClass('modal-xl');
    $('#tituloModalGlobal').html('Nuevo envío');
    $('#contenidoModalGlobal').empty().load(ApiRestURLS.listEnviosNew, function (){
      $('#modalGlobal').modal({backdrop: true,keyboard: false});
      $('#modalGlobal').modal('show');
      oMdlCrud.init();
      $('#loading').hide();
    });
  });
  // Editar Registro
  $('#myGrid').on('click', '#ag-myGrid-editar-registro', function(){
    let selectedRows = oGrid.getGridOptions().api.getSelectedRows();
    if( selectedRows.length > 0 ){

      var estado = selectedRows[0].codEstado;

      if(estado == 0){

        $('#loading').show();
        $('.modal-dialog').addClass('modal-xl');
        // Titulo Modal
        $('#tituloModalGlobal').html('Editar envío ');
        $('#contenidoModalGlobal').empty().load(ApiRestURLS.listEnviosEdit, {idEnvio: selectedRows[0].id}, function (){
          $('#modalGlobal').modal({backdrop: true,keyboard: false});
          $('#modalGlobal').modal('show');
          oMdlCrud.init();
          $('#loading').hide();
        });

      }else{
        Swal.fire({
          icon: 'warning',
          title: 'No es posible editar un envio que ya fue programado.',
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 6000,
          timerProgressBar: true,
          onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        });
      }

      
    }else{
      Swal.fire({
        icon: 'warning',
        title: 'Por favor seleccione una fila.',
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 6000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      });
    }
  });
  // Eliminar Registro
  $('#myGrid').on('click', '#ag-myGrid-eliminar-registro', function(){
    var selectedRows = oGrid.getGridOptions().api.getSelectedRows();
    if(selectedRows.length > 0){

      var estado = selectedRows[0].codEstado;
      if(estado == 0){

        Swal.fire({
          title: '¿Confirma eliminar el registro seleccionado?',
          html: `<b class="text-primary">El envío:</b> <b class="text-info">${selectedRows[0].asunto}</b><br>`,
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Confirmo',
          cancelButtonText: 'Cancelar'
        }).then((result) => {
          if( result.isConfirmed ){
            $.ajax({
              url: ApiRestURLS.listEnviosDelete,
              data: { idEnvio: selectedRows[0].id},
              type: 'post',
              beforeSend: function(){
                $('#loading').show();
              },
              success: function(data){

                $('#loading').hide();
                Swal.fire({
                  icon: (data.status == 1) ? 'success' : 'warning',
                  title: data.message,
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  timer: (data.status == 1) ? 6000 : 8000,
                  timerProgressBar: true,
                  onOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                  }
                });

                if( data.status == 1 ){
                  oGrid.refreshGrid();
                }
              },
              error: function(data){
                $('#loading').hide();
                alertaSimple({
                  mensaje: data.responseJSON.msg,
                  tipo: 'warning',
                  toast: true, 
                  position: 'top-end',
                  mostrarBoton:false,
                  tiempo: 6000
                });
              }
            });
          }
        });

      }else{

        Swal.fire({
          icon: 'warning',
          title: 'No es posible eliminar un envio que ya fue programado.',
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 6000,
          timerProgressBar: true,
          onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        });

      }

      
    }else{
      Swal.fire({
        icon: 'warning',
        title: 'Por favor seleccione una fila.',
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 6000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      });
    }
    return false;
  });
  $('#myGrid').on('click', '.btnEnvios', function(){
    var idEnvio = $(this).data('idenvio');
    window.location = ApiRestURLS.listDestinatarios + "?idEnvio=" + idEnvio;
  });
  $('#myGrid').on('click', '.errorEnvio', function(){
    var idEnvio = $(this).attr('id').split('_')[1];
    $('#loading').show();
    $('.modal-dialog').addClass('modal-xl');
    $('#tituloModalGlobal').html('Errores en el envío');
    $('#contenidoModalGlobal').empty().load(ApiRestURLS.listErrores, function (){
      $('#contenidoModalGlobal').height(450);
      $('#modalGlobal').modal({backdrop: true,keyboard: false});
      $('#modalGlobal').modal('show');
      oMdlErrores.init(idEnvio);
      $('#loading').hide();
    });
  });
  $('#myGrid').on('click', '#ag-myGrid-vista_previa', function(){
        let selectedRows = oGrid.getGridOptions().api.getSelectedRows();
        if(selectedRows.length > 0){
          var tipo = (selectedRows[0].clasificacion == 'Asociados' || selectedRows[0].clasificacion == 'Droguerías' )?'asociado':'proveedor';
          $('#loading').show();
          $('.modal-dialog').addClass('modal-xl');
          $('#tituloModalGlobal').html('Previsualización <i class="far fa-eye"></i>');


          const data = { Tipo:tipo, envioId: selectedRows[0].id };
          if($("#modalGlobal").hasClass("show")){
            data.destinatarioId = $("#detalleDestinatario").val();
          }
          $('#contenidoModalGlobal').empty().load( ApiRestURLS.previsualizar , data, function (){
              $('#modalGlobal').modal({backdrop: true,keyboard: false});
              $('#modalGlobal').modal('show');
              $('#loading').hide();

              // JS para buscar en la tabla
              setTimeout(() => {
                $('#detalleDestinatario').off().on("change", () => {
                  setTimeout(() => {
                    $("#ag-myGrid-vista_previa").trigger("click");
                  }, 500);
                }).select2({theme: 'bootstrap4', dropdownParent: "#modalGlobal" });
              }, 500);
          });
        }else{
            Swal.fire({
            icon: 'warning',
            title: 'Por favor seleccione una fila.',
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 6000,
            timerProgressBar: true,
            onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
            });
        }
      });
  /**
   * Confirma programación
   */
  $('#myGrid').on('click', '#ag-myGrid-confirma-programacion', () => {

    const selectedRows = oGrid.getGridOptions().api.getSelectedRows();
    if( selectedRows.length === 0 ){
      alertaNotificar({title: "Por favor seleccionar un envio.", icon:"info"});
      return;
    }
    const rowData = selectedRows[0];
    if(rowData.codEstado != 0){
      alertaNotificar({title: "El envío ya fue programado o enviado, solo es posible programar envío una vez.", icon:"warning"});
      return;
    }
    Swal.fire({
      title: 'Programar envío',
      text: `¿Confirma la programación del envío "${rowData.asunto}" ?`,
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if(!result.isConfirmed) return;
      const urlConfirmarProgramacion = ApiRestURLS.confirmarProgramacion.replace("0", rowData.id);
      enviarDatos(urlConfirmarProgramacion).then((result) => {
        alertaNotificar({title: result.message});
        oGrid.refreshGrid();
      }, (err) => {
        // Alerta Errores
        const sMensaje = (err.status !== undefined && err.status === 0) ? err.message : mensajes.errorGlobal ;
        alertaNotificar({title: sMensaje, icon: "error"});
      });
    });
  });




  $('#myGrid').on('click', '#ag-myGrid-pausar-envio', function(){
    var selectedRows = oGrid.getGridOptions().api.getSelectedRows();
    if(selectedRows.length > 0){

      var estado = selectedRows[0].codEstado;
      if(estado == 1){

        var accion ='';

        Swal.fire({
          title: '',
          html: `¿Que acción desea realizar?`,
          icon: 'warning',
          showDenyButton: true,
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          denyButtonColor: '#3085d6',
          confirmButtonText: 'Pausar Envío',
          cancelButtonText: 'Cancelar',
          denyButtonText: `Pausar y Reiniciar Envío`,
        }).then((result) => {
          if( result.isConfirmed ){
            
            var accion = 'pausar';


            $.ajax({
              url: ApiRestURLS.cancelarEnvio,
              data: { 
                idEnvio: selectedRows[0].id,
                accion: accion
              },
              type: 'post',
              beforeSend: function(){
                $('#loading').show();
              },
              success: function(data){

                $('#loading').hide();
                Swal.fire({
                  icon: (data.status == 1) ? 'success' : 'warning',
                  title: data.message,
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  timer: (data.status == 1) ? 6000 : 8000,
                  timerProgressBar: true,
                  onOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                  }
                });

                if( data.status == 1 ){
                  oGrid.refreshGrid();
                }
              },
              error: function(data){
                $('#loading').hide();
                alertaSimple({
                  mensaje: data.responseJSON.msg,
                  tipo: 'warning',
                  toast: true, 
                  position: 'top-end',
                  mostrarBoton:false,
                  tiempo: 6000
                });
              }
            });


          }else if (result.isDenied) {
            
            var accion = 'reiniciar';

            $.ajax({
              url: ApiRestURLS.cancelarEnvio,
              data: { 
                idEnvio: selectedRows[0].id,
                accion: accion
              },
              type: 'post',
              beforeSend: function(){
                $('#loading').show();
              },
              success: function(data){

                $('#loading').hide();
                Swal.fire({
                  icon: (data.status == 1) ? 'success' : 'warning',
                  title: data.message,
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  timer: (data.status == 1) ? 6000 : 8000,
                  timerProgressBar: true,
                  onOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                  }
                });

                if( data.status == 1 ){
                  oGrid.refreshGrid();
                }
              },
              error: function(data){
                $('#loading').hide();
                Swal.fire({
                  icon: 'warning',
                  title: 'No se ha podido pausar el envío. Intente nuevamente',
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  timer: 8000,
                  timerProgressBar: true,
                  onOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                  }
                });
              }
            });


          }
        });

      }else{

        Swal.fire({
          icon: 'warning',
          title: 'No es posible pausar un envio que no ha sido programado.',
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 6000,
          timerProgressBar: true,
          onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        });

      }

      
    }else{
      Swal.fire({
        icon: 'warning',
        title: 'Por favor seleccione una fila.',
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 6000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      });
    }
    return false;
  });




  $("#loading").hide();
});