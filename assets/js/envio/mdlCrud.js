export default class MdlCrud {

  constructor(){}
  init(){

    var oObservaciones = CKEDITOR.replace( 'envio_contenido', {
      height: '250px',
      language: 'es',
      skin: 'office2013'
    });

    CKEDITOR.config.extraPlugins = 'xml';
    CKEDITOR.config.extraPlugins = 'ajax';
    CKEDITOR.config.extraPlugins = 'pastetools';
    CKEDITOR.config.extraPlugins = 'pastefromword';

    CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;

    
    $('#formEnvio').submit(function () {

      $('#envio_contenido').val( oObservaciones.getData() );

      $.ajax({
        url: ApiRestURLS.mdlCrud,
        data: $('#formEnvio').serialize(),
        type: 'post',
        beforeSend: function(){
          $('#loading').show();
        },
        success: function(data){

          $('#loading').hide();
          Swal.fire({
            icon: (data.status == 1) ? 'success' : 'warning',
            title: data.message,
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 6000,
            timerProgressBar: true,
            onOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer)
              toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
          });
          if( data.status == 1 ){
            $('#modalGlobal .close').click();
            oGrid.refreshGrid();
          }
        },
        error: function(data){
          $('#loading').hide();
        }
      });
      return false;
    });

    $('#verArchivos-tab').click(function(){

      $('#verArchivos').empty().html('<i class="fa fa-spinner fa-pulse fa-2x fa-fw margin-bottom"></i>').load(ApiRestURLS.listArchivos);

    });

  } 
}