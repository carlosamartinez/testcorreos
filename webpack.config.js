var Encore = require('@symfony/webpack-encore');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
// if (!Encore.isRuntimeEnvironmentConfigured()) {
//   Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
// }

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')
    .cleanupOutputBeforeBuild()
    .enableVersioning(Encore.isProduction())
    .enableSourceMaps(!Encore.isProduction())

    .enableSingleRuntimeChunk()
    .configureBabel(function (babelConfig) {
      // babelConfig.presets.push("env");
    })
    // .configureBabelPresetEnv((config) => {
    //   config.useBuiltIns = 'usage';
    //   config.corejs = 3;
    // })

    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')

    /*
     * ENTRY CONFIG
     *
     * Add 1 entry for each "page" of your app
     * (including one that's included on every page - e.g. "app")
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if your JavaScript imports CSS.
    */

    .addEntry('app', './assets/js/app.js')
    // login
    .addEntry('js/login', './assets/js/app/signin.js')
    // Admin
    //.addEntry('js/rutas', './assets/js/rutas/index.js')
    //.addEntry('js/puntosRuta', './assets/js/rutas/puntosRuta.js')
    .addEntry('js/usuarios', './assets/js/usuarios/index.js')
    .addEntry('js/proveedores', './assets/js/proveedores/index.js')
    .addEntry('js/registrarUsuarios', './assets/js/usuarios/register.js')
    .addEntry('js/usuarios/passwordRecovery', './assets/js/usuarios/passwordRecovery.js')
    .addEntry('js/usuarios/changepassword', './assets/js/usuarios/changePassword.js')
    .addEntry('js/registroActividad', './assets/js/registroActividad/index.js')
    .addEntry('js/timepicker', './assets/timePicker/jquery.timepicker.min.js')
    .addEntry('js/timepickerEs', './public/assets/timePicker/jquery.ui.timepicker-es.js')
    .addEntry('js/clientes', './assets/js/clientes/index.js')
    .addEntry('js/envios', './assets/js/envio/index.js')


    
    .addEntry('js/grupos/index', './assets/js/grupos/index.js')
    .addEntry('js/grupos/destinatarios', './assets/js/grupos/destinatarios/destinatarios.js')


    .addEntry('js/destinatariosAsociados', './assets/js/envioDestinatario/destinatariosAsociado.js')
    .addEntry('js/destinatariosProveedores', './assets/js/envioDestinatario/destinatariosProveedores.js')
    .addEntry('js/grupos', './assets/js/grupos/index.js')


    .addEntry('js/gruposDestinatarios', './assets/js/grupos/destinatarios.js')

    //.addEntry('page1', './assets/js/page1.js')
    //.addEntry('page2', './assets/js/page2.js')

    // --- --- ---  Css --- --- --- \\
    .addStyleEntry('css/grilla', './assets/css/utilidades/grilla.scss')
    .addStyleEntry('css/loader', './assets/css/loader/loader.scss') 
    .addStyleEntry('css/timepicker', './assets/timePicker/jquery.timepicker.min.css')
    .addStyleEntry('css/timepicker/custom', './public/assets/timePicker/jquery-ui-1.10.0.custom.min.css')
    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    // .splitEntryChunks()

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .enableBuildNotifications()
    // enables hashed filenames (e.g. app.abc123.css)

    // enables @babel/preset-env polyfills


    // uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    // uncomment to get integrity="..." attributes on your script & link tags
    // requires WebpackEncoreBundle 1.4 or higher
    //.enableIntegrityHashes(Encore.isProduction())

    // uncomment if you're having problems with a jQuery plugin
    // enables Sass/SCSS support
    .enableSassLoader()
    .autoProvidejQuery()
    .autoProvideVariables({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
        'Swal': 'sweetalert2'
    });

    // uncomment if you use API Platform Admin (composer req api-admin)
    //.enableReactPreset()
    //.addEntry('admin', './assets/js/admin.js')
;

module.exports = Encore.getWebpackConfig();
