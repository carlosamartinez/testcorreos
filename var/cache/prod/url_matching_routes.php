<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/reset-password' => [
            [['_route' => 'app_forgot_password_request', '_controller' => 'App\\Controller\\ResetPasswordController::request'], null, null, null, false, false, null],
            [['_route' => 'forgot_password', '_controller' => 'App\\Controller\\ResetPasswordController::request'], null, null, null, false, false, null],
        ],
        '/reset-password/check-email' => [[['_route' => 'app_check_email', '_controller' => 'App\\Controller\\ResetPasswordController::checkEmail'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'admin_login', '_controller' => 'App\\Controller\\AppController::login'], null, null, null, false, false, null]],
        '/admin/forgotpassword' => [[['_route' => 'password_forgot', '_controller' => 'App\\Controller\\UsuariosController::forgotPassword'], null, null, null, false, false, null]],
        '/admin/changepassword' => [[['_route' => 'password_reset_in_app', '_controller' => 'App\\Controller\\ResetPasswordController::resetPasswordInApp'], null, ['POST' => 0], null, false, false, null]],
        '/admin/validate_duplicate_password' => [[['_route' => 'password_validate_duplicate_in_app', '_controller' => 'App\\Controller\\ResetPasswordController::validarContrasenasPrevias'], null, ['POST' => 0], null, false, false, null]],
        '/admin/changepasswordindex' => [[['_route' => 'password_reset_in_app_index', '_controller' => 'App\\Controller\\ResetPasswordController::resetPasswordInAppIndex'], null, ['POST' => 0], null, false, false, null]],
        '/admin/logout' => [[['_route' => 'admin_logout', '_controller' => 'App\\Controller\\AppController::logout'], null, null, null, false, false, null]],
        '/sendresetpassword' => [[['_route' => 'forgot_password_request', '_controller' => 'App\\Controller\\ResetPasswordController::requestSending'], null, null, null, false, false, null]],
        '/resetpassword' => [[['_route' => 'reset_password_request', '_controller' => 'App\\Controller\\ResetPasswordController::resetPassword'], null, null, null, true, false, null]],
        '/admin/grupos' => [[['_route' => 'admin_grupo', 'accion' => 'ver', '_controller' => 'App\\Controller\\GruposController::index'], null, null, null, false, false, null]],
        '/admin/grupo_json' => [[['_route' => 'admin_grupo_json', '_controller' => 'App\\Controller\\GruposController::indexJson'], null, null, null, false, false, null]],
        '/admin/grupo_modif' => [[['_route' => 'admin_grupo_modif', '_controller' => 'App\\Controller\\GruposController::grupoModif'], null, null, null, false, false, null]],
        '/admin/grupo_delete' => [[['_route' => 'admin_grupo_delete', '_controller' => 'App\\Controller\\GruposController::grupoDelete'], null, null, null, false, false, null]],
        '/admin/grupo_download_report' => [[['_route' => 'admin_download_report', '_controller' => 'App\\Controller\\GruposController::downloadReport'], null, null, null, false, false, null]],
        '/admin/grupo/destinatariosGrupo' => [[['_route' => 'admin_destinatarios_grupo', '_controller' => 'App\\Controller\\GruposController::asociadosIndex'], null, null, null, false, false, null]],
        '/admin/grupo/destinatariosGrupo/asociadosIndex' => [[['_route' => 'admin_destinatarios_grupo_index_asociados', '_controller' => 'App\\Controller\\GruposController::asociadosIndexJson'], null, null, null, false, false, null]],
        '/admin/grupo/destinatariosGrupo/cargarasociados' => [[['_route' => 'admin_destinatarios_grupo_cargar_asociados', '_controller' => 'App\\Controller\\GruposController::cargarDestinatarios'], null, null, null, false, false, null]],
        '/admin/grupo/destinatariosGrupo/descargarasociados' => [[['_route' => 'admin_destinatarios_grupo_descargar_asociados', '_controller' => 'App\\Controller\\GruposController::descargarFormatoAsociados'], null, null, null, false, false, null]],
        '/admin/grupo/destinatariosGrupo/agregarasociados' => [[['_route' => 'admin_destinatarios_grupo_agregar_asociados', '_controller' => 'App\\Controller\\GruposController::agregarTodosAsociados'], null, null, null, false, false, null]],
        '/admin/grupo/destinatariosGrupo/eliminarasociados' => [[['_route' => 'admin_destinatarios_grupo_eliminar_asociados', '_controller' => 'App\\Controller\\GruposController::eliminarTodosAsociados'], null, null, null, false, false, null]],
        '/admin/grupo/destinatariosGrupo/agregarListadoAsociado' => [[['_route' => 'admin_destinatarios_grupo_agregar_asociados_listado', '_controller' => 'App\\Controller\\GruposController::agregarListadoClientes'], null, null, null, false, false, null]],
        '/admin/grupo/destinatariosGrupo/agregarAsociado' => [[['_route' => 'admin_destinatarios_grupo_agregar_asociado', '_controller' => 'App\\Controller\\GruposController::agregarEliminarAsociados'], null, null, null, false, false, null]],
        '/admin/grupo/destinatariosGrupo/cargarproveedores' => [[['_route' => 'admin_destinatarios_grupo_cargar_proveedores', '_controller' => 'App\\Controller\\Grupos\\Destinatarios\\DestinatariosController::cargarDestinatariosProveedores'], null, null, null, false, false, null]],
        '/admin/grupo/destinatariosGrupo/cargarproveedoresupload' => [[['_route' => 'admin_destinatarios_grupo_cargar_proveedores_upload', '_controller' => 'App\\Controller\\Grupos\\Destinatarios\\DestinatariosController::procesarArchivoActionProveedor'], null, null, null, false, false, null]],
        '/admin/grupo/destinatariosGrupo/previsualizar_archivo_proveedores' => [[['_route' => 'admin_destinatarios_previsualizar_archivo_proveedores', '_controller' => 'App\\Controller\\Grupos\\Destinatarios\\DestinatariosController::previsualizarArchivoProveedores'], null, null, null, false, false, null]],
        '/admin/grupo/destinatariosGrupo/descargarproveedores' => [[['_route' => 'admin_destinatarios_grupo_descargar_proveedores_formato', '_controller' => 'App\\Controller\\GruposController::descargarFormatoProveedores'], null, null, null, false, false, null]],
        '/admin/grupo/destinatariosGrupo/proveedoresInforme' => [[['_route' => 'admin_destinatarios_grupo_descargar_proveedores', '_controller' => 'App\\Controller\\Grupos\\Destinatarios\\DestinatariosProveedoresController::destinatariosCsv'], null, null, null, false, false, null]],
        '/admin/usuarios' => [[['_route' => 'admin_usuarios', 'accion' => 'ver', '_controller' => 'App\\Controller\\UsuariosController::index'], null, null, null, false, false, null]],
        '/admin/usuarios_json' => [[['_route' => 'admin_usuarios_json', '_controller' => 'App\\Controller\\UsuariosController::indexJson'], null, null, null, false, false, null]],
        '/admin/usuarios_new' => [[['_route' => 'admin_usuarios_new', '_controller' => 'App\\Controller\\UsuariosController::userNew'], null, null, null, false, false, null]],
        '/admin/usuarios_edit' => [[['_route' => 'admin_usuarios_edit', '_controller' => 'App\\Controller\\UsuariosController::userEdit'], null, null, null, false, false, null]],
        '/admin/usuarios_delete' => [[['_route' => 'admin_usuarios_delete', '_controller' => 'App\\Controller\\UsuariosController::userDelete'], null, null, null, false, false, null]],
        '/admin/usuarios_permisos' => [[['_route' => 'admin_usuarios_permisos', '_controller' => 'App\\Controller\\UsuariosController::userPermisos'], null, null, null, false, false, null]],
        '/admin/clientes' => [[['_route' => 'admin_clientes', 'accion' => 'ver', '_controller' => 'App\\Controller\\ClientesController::index'], null, null, null, false, false, null]],
        '/admin/clientes_json' => [[['_route' => 'admin_clientes_json', '_controller' => 'App\\Controller\\ClientesController::indexJson'], null, null, null, false, false, null]],
        '/admin/proveedores' => [[['_route' => 'admin_proveedores', '_controller' => 'App\\Controller\\ProveedoresController::index'], null, null, null, false, false, null]],
        '/admin/proveedores_json' => [[['_route' => 'admin_proveedores_json', '_controller' => 'App\\Controller\\ProveedoresController::indexJson'], null, null, null, false, false, null]],
        '/admin/proveedores_new' => [[['_route' => 'admin_proveedores_new', '_controller' => 'App\\Controller\\ProveedoresController::proveedorNew'], null, null, null, false, false, null]],
        '/admin/proveedores_edit' => [[['_route' => 'admin_proveedores_edit', '_controller' => 'App\\Controller\\ProveedoresController::proveedorEdit'], null, null, null, false, false, null]],
        '/admin/proveedores_delete' => [[['_route' => 'admin_proveedores_delete', '_controller' => 'App\\Controller\\ProveedoresController::proveedorDelete'], null, null, null, false, false, null]],
        '/admin/proveedores_csv' => [[['_route' => 'admin_proveedores_csv', '_controller' => 'App\\Controller\\ProveedoresController::proveedorCsv'], null, null, null, false, false, null]],
        '/admin/contactos_proveedor' => [[['_route' => 'admin_contactos_proveedores', '_controller' => 'App\\Controller\\ContactosController::index'], null, null, null, false, false, null]],
        '/admin/contacots_proveedores_json' => [[['_route' => 'admin_contactos_proveedores_json', '_controller' => 'App\\Controller\\ContactosController::indexJson'], null, null, null, false, false, null]],
        '/admin/contactos_proveedores_new' => [[['_route' => 'admin_contactos_proveedores_new', '_controller' => 'App\\Controller\\ContactosController::contactosNew'], null, null, null, false, false, null]],
        '/admin/contactos_proveedores_edit' => [[['_route' => 'admin_contactos_proveedores_edit', '_controller' => 'App\\Controller\\ContactosController::contactosEdit'], null, null, null, false, false, null]],
        '/admin/contactos_proveedores_delete' => [[['_route' => 'admin_contactos_proveedores_delete', '_controller' => 'App\\Controller\\ContactosController::contactosDelete'], null, null, null, false, false, null]],
        '/admin/contactos_proveedores_verificacion_email' => [[['_route' => 'admin_contactos_proveedores_verificacion_email', '_controller' => 'App\\Controller\\ContactosController::verificacionEmail'], null, null, null, false, false, null]],
        '/admin/envios' => [[['_route' => 'admin_envio', '_controller' => 'App\\Controller\\EnvioController::index'], null, null, null, false, false, null]],
        '/admin/envios_json' => [[['_route' => 'admin_envio_json', '_controller' => 'App\\Controller\\EnvioController::indexJson'], null, null, null, false, false, null]],
        '/admin/envios_new' => [[['_route' => 'admin_envio_new', '_controller' => 'App\\Controller\\EnvioController::enviosNew'], null, null, null, false, false, null]],
        '/admin/envios_edit' => [[['_route' => 'admin_envio_edit', '_controller' => 'App\\Controller\\EnvioController::enviosEdit'], null, null, null, false, false, null]],
        '/admin/envios_delete' => [[['_route' => 'admin_envio_delete', '_controller' => 'App\\Controller\\EnvioController::enviosDelete'], null, null, null, false, false, null]],
        '/admin/envios_vista_previa' => [[['_route' => 'admin_envio_vista_previa', '_controller' => 'App\\Controller\\EnvioController::enviosVistaPrevia'], null, null, null, false, false, null]],
        '/admin/envios/cancelar_envio' => [[['_route' => 'admin_envio_cancelar_envio', '_controller' => 'App\\Controller\\EnvioController::cancelarEnvio'], null, null, null, false, false, null]],
        '/admin/archivo_adjunto/csv' => [[['_route' => 'admin_archivo_adjunto_csv', '_controller' => 'App\\Controller\\ArchivosAdjuntosController::csv'], null, null, null, false, false, null]],
        '/admin/archivo_adjunto/otros' => [[['_route' => 'admin_archivo_adjunto_otros', '_controller' => 'App\\Controller\\ArchivosAdjuntosController::otros'], null, null, null, false, false, null]],
        '/admin/archivo_adjunto/listar' => [[['_route' => 'admin_archivo_adjunto_listar', '_controller' => 'App\\Controller\\ArchivosAdjuntosController::listar'], null, null, null, false, false, null]],
        '/admin/archivo_adjunto/eliminar' => [[['_route' => 'admin_archivo_adjunto_eliminar', '_controller' => 'App\\Controller\\ArchivosAdjuntosController::eliminar'], null, null, null, false, false, null]],
        '/admin/envios_destinatarios' => [[['_route' => 'admin_envio_destinatario_index', '_controller' => 'App\\Controller\\EnvioDestinatarioController::index'], null, null, null, false, false, null]],
        '/admin/envios_destinatarios_proveedor_json' => [[['_route' => 'admin_envio_destinatario_proveedor_json', '_controller' => 'App\\Controller\\EnvioDestinatarioController::proveedoresDestinatariosJson'], null, null, null, false, false, null]],
        '/admin/envios_destinatarios_asociado_json' => [[['_route' => 'admin_envio_destinatario_asociado_json', '_controller' => 'App\\Controller\\EnvioDestinatarioController::asociadosDestinatariosJson'], null, null, null, false, false, null]],
        '/admin/envios_destinatarios_asociado_agregar' => [[['_route' => 'admin_envio_destinatario_asociado_agregar', '_controller' => 'App\\Controller\\EnvioDestinatarioController::asociadosDestinatariosAgregar'], null, null, null, false, false, null]],
        '/admin/envios_destinatarios_asociado_eliminar' => [[['_route' => 'admin_envio_destinatario_asociado_eliminar', '_controller' => 'App\\Controller\\EnvioDestinatarioController::asociadosDestinatariosEliminar'], null, null, null, false, false, null]],
        '/admin/envios_destinatarios_asociado_xls_leidos' => [[['_route' => 'admin_envio_destinatario_asociado_xls_leidos', '_controller' => 'App\\Controller\\EnvioDestinatarioController::asociadosDestinatariosXls'], null, null, null, false, false, null]],
        '/admin/envios_destinatarios_proveedor_agregar' => [[['_route' => 'admin_envio_destinatario_proveedor_agregar', '_controller' => 'App\\Controller\\EnvioDestinatarioController::proveedorDestinatariosAgregar'], null, null, null, false, false, null]],
        '/admin/envios_destinatarios_proveedor_eliminar' => [[['_route' => 'admin_envio_destinatario_proveedor_eliminar', '_controller' => 'App\\Controller\\EnvioDestinatarioController::proveedorDestinatariosEliminar'], null, null, null, false, false, null]],
        '/admin/envios_destinatarios_proveedor_xls_leidos' => [[['_route' => 'admin_envio_destinatario_proveedor_xls_leidos', '_controller' => 'App\\Controller\\EnvioDestinatarioController::proveedorDestinatariosXls'], null, null, null, false, false, null]],
        '/admin/envios_destinatarios_reenviar' => [[['_route' => 'admin_envio_destinatario_reenviar', '_controller' => 'App\\Controller\\EnvioDestinatarioController::reenviarMensaje'], null, null, null, false, false, null]],
        '/administrador/envio/xls' => [[['_route' => 'admin_envio_destinatario_xls', '_controller' => 'App\\Controller\\EnvioDestinatarioController::envioDestinatarioXls'], null, null, null, false, false, null]],
        '/administrador/envio/envio_lectura' => [[['_route' => 'admin_envio_destinatario_lectura', '_controller' => 'App\\Controller\\EnvioDestinatarioController::envioDestinatarioLectura'], null, null, null, false, false, null]],
        '/admin/envio_errores_index' => [[['_route' => 'admin_envio_errores_index', '_controller' => 'App\\Controller\\ErroresController::index'], null, null, null, false, false, null]],
        '/admin/envio_errores_index_json' => [[['_route' => 'admin_envio_erroes_index_json', '_controller' => 'App\\Controller\\ErroresController::indexJson'], null, null, null, false, false, null]],
        '/api/login_check' => [[['_route' => 'api_login_check'], null, null, null, false, false, null]],
        '/api/data_user' => [[['_route' => 'api_user_data', '_controller' => 'App\\Controller\\Api\\UsuariosApiController::getDataUser'], null, ['GET' => 0], null, false, false, null]],
        '/api/rutas' => [[['_route' => 'api_rutas_list', '_controller' => 'App\\Controller\\Api\\RutasApiController::rutasList'], null, ['GET' => 0], null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/reset(?'
                    .'|\\-password/reset/([^/]++)(*:41)'
                    .'|password/reset/([^/]++)(*:71)'
                .')'
                .'|/admin/(?'
                    .'|grupos/([^/]++)/destinatarios(?'
                        .'|(*:121)'
                        .'|/(?'
                            .'|proveedores/(?'
                                .'|json(*:152)'
                                .'|asignar(?'
                                    .'|(*:170)'
                                    .'|\\-todos(*:185)'
                                .')'
                                .'|eliminar\\-todos(*:209)'
                                .'|cargar\\-destinatarios(*:238)'
                                .'|([^/]++)/contactos/json(*:269)'
                            .')'
                            .'|asociados/json(*:292)'
                        .')'
                    .')'
                    .'|registro_actividad_admin(?'
                        .'|(?:/([^/]++))?(*:343)'
                        .'|_(?'
                            .'|json(?:/([^/]++))?(*:373)'
                            .'|download(?:/([^/]++))?(*:403)'
                        .')'
                    .')'
                    .'|envios/([^/]++)/confirmar_programacion;(*:452)'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        41 => [[['_route' => 'app_reset_password', '_controller' => 'App\\Controller\\ResetPasswordController::resetPasswordInApp'], ['token'], null, null, false, true, null]],
        71 => [[['_route' => 'reset_password', '_controller' => 'App\\Controller\\ResetPasswordController::reset'], ['token'], null, null, false, true, null]],
        121 => [[['_route' => 'admin_grupo_destinatarios_index', '_controller' => 'App\\Controller\\Grupos\\Destinatarios\\DestinatariosController::index'], ['idGrupo'], null, null, false, false, null]],
        152 => [[['_route' => 'admin_grupo_destinatarios_proveedores_json', '_controller' => 'App\\Controller\\Grupos\\Destinatarios\\DestinatariosProveedoresController::proveedoresJson'], ['idGrupo'], null, null, false, false, null]],
        170 => [[['_route' => 'admin_grupo_destinatarios_proveedores_asignarProveedor', '_controller' => 'App\\Controller\\Grupos\\Destinatarios\\DestinatariosProveedoresController::asignarProveedor'], ['idGrupo'], null, null, false, false, null]],
        185 => [[['_route' => 'admin_grupo_destinatarios_proveedores_asignarTodos', '_controller' => 'App\\Controller\\Grupos\\Destinatarios\\DestinatariosProveedoresController::asignarTodos'], ['idGrupo'], null, null, false, false, null]],
        209 => [[['_route' => 'admin_grupo_destinatarios_proveedores_eliminarTodos', '_controller' => 'App\\Controller\\Grupos\\Destinatarios\\DestinatariosProveedoresController::eliminarTodos'], ['idGrupo'], null, null, false, false, null]],
        238 => [[['_route' => 'admin_grupo_destinatarios_proveedores_cargarDestinatarios', '_controller' => 'App\\Controller\\Grupos\\Destinatarios\\DestinatariosProveedoresController::cargarDestinatariosHtml'], ['idGrupo'], null, null, false, false, null]],
        269 => [[['_route' => 'admin_grupo_destinatarios_proveedores_contactos_json', '_controller' => 'App\\Controller\\Grupos\\Destinatarios\\DestinatariosProveedoresController::proveedoresContactosJson'], ['idGrupo', 'idProveedor'], null, null, false, false, null]],
        292 => [[['_route' => 'admin_grupo_destinatarios_asociados_json', '_controller' => 'App\\Controller\\Grupos\\Destinatarios\\DestinatariosAsociadosController::asociadosJson'], ['idGrupo'], null, null, false, false, null]],
        343 => [[['_route' => 'admin_registro_actividad_admin', 'listado' => 'admin', '_controller' => 'App\\Controller\\RegistroActividadController::registroActividad'], ['listado'], null, null, false, true, null]],
        373 => [[['_route' => 'admin_registro_actividad_admin_json', 'listado' => 'admin', '_controller' => 'App\\Controller\\RegistroActividadController::registroActividadJson'], ['listado'], null, null, false, true, null]],
        403 => [[['_route' => 'admin_registro_actividad_admin_download', 'listado' => 'admin', '_controller' => 'App\\Controller\\RegistroActividadController::registroActividadDescargaLogsCsv'], ['listado'], null, null, false, true, null]],
        452 => [
            [['_route' => 'admin_envio_confirmarProgramacion', '_controller' => 'App\\Controller\\EnvioController::confirmarProgramacion'], ['idEnvio'], null, null, false, false, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
