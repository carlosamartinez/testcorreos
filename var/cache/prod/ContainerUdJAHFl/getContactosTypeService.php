<?php

namespace ContainerUdJAHFl;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/*
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getContactosTypeService extends App_KernelProdContainer
{
    /*
     * Gets the private 'App\Form\ContactosType' shared autowired service.
     *
     * @return \App\Form\ContactosType
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['App\\Form\\ContactosType'] = new \App\Form\ContactosType();
    }
}
