<?php

namespace ContainerUdJAHFl;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/*
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getAppControllerService extends App_KernelProdContainer
{
    /*
     * Gets the public 'App\Controller\AppController' shared autowired service.
     *
     * @return \App\Controller\AppController
     */
    public static function do($container, $lazyLoad = true)
    {
        $container->services['App\\Controller\\AppController'] = $instance = new \App\Controller\AppController(($container->services['twig'] ?? $container->load('getTwigService')), ($container->privates['App\\Security\\PasswordEncoder'] ?? $container->load('getPasswordEncoderService')), ($container->privates['App\\Services\\Mail'] ?? $container->load('getMailService')));

        $instance->setContainer(($container->privates['.service_locator.g9CqTPp'] ?? $container->load('get_ServiceLocator_G9CqTPpService'))->withContext('App\\Controller\\AppController', $container));

        return $instance;
    }
}
