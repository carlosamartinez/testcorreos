<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* envio/email_envio_terminado.html.twig */
class __TwigTemplate_b8c27e7341eca73d7fba16e9a3f6c109bc606b93fd1cc5935f81511fdfe84ca7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<style>
    .card{
        width:450px;
        border:1px silver solid;
        -moz-border-radius: 9px;
\t\t -webkit-border-radius:9px;
\t\t padding: 10px;
    }
    .textSmall{
        font-size: 10px;
        color:#8B1602;
        text-align: center;
    }
    .detalle{
        color:#0070BA;
        text-align: justify;
        texto-justify: inter-palabra;
    }
    .link{
        color:#E56605;
    }
</style>
<div class=\"card\"  style=\"justify-content:center;text-align:center;margin: 0 auto;padding:40px\">
    <h1>Aplicación de envío de correos.</h1>
  <div class=\"card-header\" style=\"width:70%;margin: 0 auto;justify-content;center;text-align:center\">
  ";
        // line 27
        echo "    <h3>¡Hola ";
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, ($context["nombre"] ?? null)), "html", null, true);
        echo "!</h3>
  </div>
  <div class=\"card-body\" style=\"width:80%;margin: 0 auto;justify-content;center;text-align:center\">
   <p class=\"detalle\">El Envío con asunto [<strong>";
        // line 30
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, ($context["nombreEnvio"] ?? null)), "html", null, true);
        echo "</strong>] ha finalizado su ejecución para [<strong>";
        echo twig_escape_filter($this->env, ($context["nDestinatarios"] ?? null), "html", null, true);
        echo "</strong>] destinatario(s).<br><br></p>
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "envio/email_envio_terminado.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 30,  64 => 27,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "envio/email_envio_terminado.html.twig", "/srv/www/correosProduccion/templates/envio/email_envio_terminado.html.twig");
    }
}
