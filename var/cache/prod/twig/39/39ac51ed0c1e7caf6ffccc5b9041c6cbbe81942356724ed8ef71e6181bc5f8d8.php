<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* grupos/modals/proveedor-cargar-destinatarios.html.twig */
class __TwigTemplate_87baeb8728a26a9431e5bac97dd5149c261d36f824271bc25fc0f01174990ff3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "
";
        // line 2
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 26
        echo "
<div class=\"container-fluid\" id=\"contentUpload\">
    <div class=\"row\">
        <div class=\"col-xs-12 col-md-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    <h4 class=\"panel-title\">Agregar integrantes al grupo.</h4>
                </div>
                <div class=\"panel-body\">
                    <button id=\"descargar_formato\" target=\"_blank\" class=\"btn btn-primary btn-sm\" type=\"submit\"><i class=\"fa fa-download\" aria-hidden=\"true\"></i>
                        Descargar Formatos
                    </button>
                    <small>La información debe estar bien digitada. <br/>
                    La carga del archivo puede tardar varios minutos, tenga paciencia...</small>
                </div>
            </div>
        </div>  
        <form id=\"form1\" action=\"#\" method=\"post\" class=\"col-xs-12 col-md-12\">
            <div class=\"col-xs-6 col-md-12\">
                <div class=\"uk-comment-meta\">Solo para proveedores, registrar todos los que coincidan con:<br>
                    <input type=\"radio\" checked=\"\" value=\"None\" name=\"tipoRegistro\"> No Aplica (si los datos corresponden a Proveedores se cargaran por Nit)<br>
                    <input type=\"radio\" value=\"Nit\" name=\"tipoRegistro\"> Nit<br>
                    <input type=\"radio\" value=\"Email\" name=\"tipoRegistro\"> Email
                </div>
            </div>
            <div class=\"col-xs-12 col-md-12\">
                <div id=\"btnExamin\">
                    <input type=\"file\" required=\"required\" name=\"form[archivo]\" id=\"form_archivo\">
                </div>
            </div>
           
        </form>
    </div>
</div>
<div id=\"resultados\" style=\"display: none\" class=\"container\">

    <ul class=\"nav nav-tabs\" id=\"myTab\" role=\"tablist\">
      <li class=\"nav-item\" role=\"presentation\">
        <a class=\"nav-link active\" id=\"home-tab\" data-toggle=\"tab\" href=\"#home\" role=\"tab\" aria-controls=\"home\" aria-selected=\"true\">Integrantes Previos</a>
      </li>
      <li class=\"nav-item\" role=\"presentation\">
        <a class=\"nav-link\" id=\"profile-tab\" data-toggle=\"tab\" href=\"#profile\" role=\"tab\" aria-controls=\"profile\" aria-selected=\"false\">Integrantes Archivo</a>
      </li>
      <li class=\"nav-item\" role=\"presentation\">
        <a class=\"nav-link\" id=\"contact-tab\" data-toggle=\"tab\" href=\"#contact\" role=\"tab\" aria-controls=\"contact\" aria-selected=\"false\">Integrantes No Encontrados</a>
      </li>
    </ul>
    <div class=\"tab-content\" id=\"myTabContent\" style=\"overflow-y: scroll;height: 400px;\">
      <div class=\"tab-pane fade show active\" id=\"home\" role=\"tabpanel\" aria-labelledby=\"home-tab\">

        <table class=\"listado-clientes table table-sm table-stripped \" style=\"display:none\" >
            <tbody id=\"listado-clientes-actualizados-tbody\" style=\"width:100%;display: inline-table;\">
                <tr>
                    <th>Deshabilitar</th>
                    <th>Proveedor</th>
                    <th>Nombre</th>
                    <th>Email</th>
                </tr>
            </tbody>
        </table>
          
      </div>
      <div class=\"tab-pane fade\" id=\"profile\" role=\"tabpanel\" aria-labelledby=\"profile-tab\">

        <table class=\"listado-clientes table table-sm table-stripped\" style=\"display:none\">
            <tbody id=\"listado-clientes-insertados-tbody\" style=\"width:100%;display: inline-table;\">
                <tr>
                    <th>Proveedor</th>
                    <th>Nombre</th>
                    <th>Email</th>
                </tr>
            </tbody>
        </table>
          
      </div>
      <div class=\"tab-pane fade\" id=\"contact\" role=\"tabpanel\" aria-labelledby=\"contact-tab\">

        <table class=\"listado-noEncontrados table table-sm table-stripped\" style=\"display:none\">
            <tbody id=\"listado-noencontrados-tbody\" style=\"width:100%;display: inline-table;\">
                <tr>
                    <th>Fila</th>
                    <th>Dato</th>
                    <th>Motivo</th>
                </tr>
            </tbody>
        </table>
          
      </div>
    </div>


    ";
        // line 135
        echo "</div>
";
    }

    // line 2
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        echo "  <style>
    .panel-heading {
        padding: 10px;
        background-color: #ececec;
        border-radius: 4px;
        border-bottom: solid 1px #c8c8c8;
        border-right: solid 1px #c8c8c8;
    }
    .panel-body{
        margin-bottom: 10px;
        padding: 20px;
        border-bottom: solid 1px #c8c8c8;
        border-left: solid 1px #c8c8c8;
        border-right: solid 1px #c8c8c8;
    }
    .btn-primary{
        background-color:#02387F !important
    }
    .chk-destinatarios-confirmar{
        width: 12px;
    }
  </style>
";
    }

    public function getTemplateName()
    {
        return "grupos/modals/proveedor-cargar-destinatarios.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  145 => 3,  141 => 2,  136 => 135,  43 => 26,  41 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "grupos/modals/proveedor-cargar-destinatarios.html.twig", "/srv/www/correosProduccion/templates/grupos/modals/proveedor-cargar-destinatarios.html.twig");
    }
}
