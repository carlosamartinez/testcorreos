<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* usuarios/index.html.twig */
class __TwigTemplate_e62ec149dc7fdbeacb7831e780d973766968bc9c4dd7e1d0b837a6ba3661a7c3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'header' => [$this, 'block_header'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "usuarios/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Correos - Usuarios";
    }

    // line 5
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "  ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
  ";
        // line 7
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("js/usuarios");
        echo "
  ";
        // line 8
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("css/grilla");
        echo "
  ";
        // line 10
        echo "  <style>
    input[type=number]::-webkit-inner-spin-button, 
    input[type=number]::-webkit-outer-spin-button { 
        -webkit-appearance: none; 
        margin: 0; 
    }

    input[type=number] { 
        -moz-appearance:textfield; 
    }
    a.active{
      background-color: #f8f9fa!important;
    }
  </style>
  ";
    }

    // line 27
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 28
        echo "  ";
        $this->loadTemplate("app/adminNavbar.html.twig", "usuarios/index.html.twig", 28)->display($context);
        // line 29
        echo "  ";
        $this->loadTemplate("app/menu.html.twig", "usuarios/index.html.twig", 29)->display($context);
        // line 30
        echo "  
";
    }

    // line 33
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 34
        echo "  <div id=\"myGrid\" style=\"height: 100%;\" class=\"ag-theme-fresh\"></div>
";
    }

    // line 37
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 38
        echo "  ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
  <script>
    var oGrid;
    var aDfColumnas = ";
        // line 41
        echo ($context["dfColumnas"] ?? null);
        echo ";
    var aGridButtons = ";
        // line 42
        echo ($context["aGridButtons"] ?? null);
        echo ";

    ApiRestURLS.listUsuarios = `";
        // line 44
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_usuarios_json");
        echo "`;
    ApiRestURLS.userNew = `";
        // line 45
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_usuarios_new");
        echo "`;
    ApiRestURLS.userEdit = `";
        // line 46
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_usuarios_edit");
        echo "`;
    ApiRestURLS.userDelete = `";
        // line 47
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_usuarios_delete");
        echo "`;
    ApiRestURLS.userPermisos = `";
        // line 48
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_usuarios_permisos");
        echo "`;
  </script>
  ";
        // line 50
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("js/usuarios");
        echo "
  ";
    }

    public function getTemplateName()
    {
        return "usuarios/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  157 => 50,  152 => 48,  148 => 47,  144 => 46,  140 => 45,  136 => 44,  131 => 42,  127 => 41,  120 => 38,  116 => 37,  111 => 34,  107 => 33,  102 => 30,  99 => 29,  96 => 28,  92 => 27,  74 => 10,  70 => 8,  66 => 7,  61 => 6,  57 => 5,  50 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "usuarios/index.html.twig", "/srv/www/correosProduccion/templates/usuarios/index.html.twig");
    }
}
