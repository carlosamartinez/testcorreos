<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html.twig */
class __TwigTemplate_5a96f16a6bd00ca159e21c718b7a710102fba22c5daac5471b1be71c5fcba821 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'header' => [$this, 'block_header'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html>

  <head>
    <meta charset=\"UTF-8\">
    <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    ";
        // line 8
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 17
        echo "  </head>

  <body>

 <!-- LOADER -->
    <div id=\"loading\">
        <div class=\"loader\">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </div>
    </div>

    <div class=\"p-0 m-0 container-fluid\" style=\"height: 100%; display:flex; flex-direction: column;\">
      <!-- Contenido - Logueado -->
      ";
        // line 34
        $this->displayBlock('header', $context, $blocks);
        // line 35
        echo "      ";
        $this->displayBlock('body', $context, $blocks);
        // line 36
        echo "    </div>

    <!-- Modal Global -->
    <div class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" id=\"modalGlobal\">
      <div class=\"modal-dialog\">
        <div class=\"modal-content\">
          <div class=\"text-center modal-header text-info\">
            <h6 class=\"modal-title font-weight-bold\" id=\"tituloModalGlobal\">...</h6>
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
              <span aria-hidden=\"true\">&times;</span>
            </button>
          </div>
          <div class=\"modal-body\" id=\"contenidoModalGlobal\"></div>
        </div>
      </div>
    </div>

    ";
        // line 53
        $this->displayBlock('javascripts', $context, $blocks);
        // line 58
        echo "  </body>
</html>";
    }

    // line 7
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Correos";
    }

    // line 8
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "      ";
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("app");
        echo "
      ";
        // line 10
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("css/loader");
        echo "

      <!-- MENU -->
        <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/menuSlider/css/color-theme.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/menuSlider/css/webslidemenu.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/menuSlider/css/demo.css"), "html", null, true);
        echo "\" />
    ";
    }

    // line 34
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 35
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 53
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 54
        echo "      ";
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("app");
        echo "
      <script type=\"text/javascript\"> var ApiRestURLS = {}; </script>
      <script type=\"text/javascript\" src=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/menuSlider/js/webslidemenu.js"), "html", null, true);
        echo "\"></script>
    ";
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  167 => 56,  161 => 54,  157 => 53,  151 => 35,  145 => 34,  139 => 15,  135 => 14,  131 => 13,  125 => 10,  120 => 9,  116 => 8,  109 => 7,  104 => 58,  102 => 53,  83 => 36,  80 => 35,  78 => 34,  59 => 17,  57 => 8,  53 => 7,  49 => 6,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "base.html.twig", "/srv/www/correosProduccion/templates/base.html.twig");
    }
}
