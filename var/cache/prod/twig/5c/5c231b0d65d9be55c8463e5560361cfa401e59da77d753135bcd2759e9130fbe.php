<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* security/login.html.twig */
class __TwigTemplate_23622733e0255e2bab45a37d9d2cfe23c15e7678987fe53a0f688d8e6752043a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'bodylogin' => [$this, 'block_bodylogin'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "baseLogin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("baseLogin.html.twig", "security/login.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Correos Masivos";
    }

    // line 3
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "  ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
  ";
        // line 5
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("js/login");
        echo "
";
    }

    // line 7
    public function block_bodylogin($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "  <form class='form-signin' method='post'>
    ";
        // line 9
        if (($context["error"] ?? null)) {
            // line 10
            echo "      ";
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, ($context["error"] ?? null), "messageKey", [], "any", false, false, false, 10), "Invalid credentials."))) {
                // line 11
                echo "        <div class=\"alert alert-danger\">Credenciales no válidas.</div>
      ";
            } else {
                // line 13
                echo "        <div class=\"alert alert-danger\">";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, ($context["error"] ?? null), "messageKey", [], "any", false, false, false, 13), twig_get_attribute($this->env, $this->source, ($context["error"] ?? null), "messageData", [], "any", false, false, false, 13), "security"), "html", null, true);
                echo "</div>
      ";
            }
            // line 15
            echo "    ";
        }
        // line 16
        echo "    ";
        if (twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "token", [], "any", false, false, false, 16)) {
            // line 17
            echo "      <div class=\"mb-3\">
        You are logged in as ";
            // line 18
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "token", [], "any", false, false, false, 18), "user", [], "any", false, false, false, 18), "email", [], "any", false, false, false, 18), "html", null, true);
            echo ", <a href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_logout");
            echo "\">Logout</a>
      </div>
    ";
        }
        // line 21
        echo "    <img class='mb-4' src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/logo-correos.png"), "html", null, true);
        echo "\" alt=\"logo \" width=\"300\">
    <h1 class=\"h3 mb-3 font-weight-normal\">Ingrese los datos para iniciar sesión</h1>
    <label class=\"sr-only\" for=\"inputEmail\">Email</label>
    <input type=\"text\" class='form-control mt-1' id=\"inputUsuario\" name=\"usuario\" value=\"";
        // line 24
        echo twig_escape_filter($this->env, ($context["last_username"] ?? null), "html", null, true);
        echo "\" placeholder=\"Usuario\" required autofocus>
    <label for=\"inputPassword\" class=\"sr-only\">Contraseña</label>
    <input type=\"password\" class='form-control mt-1' id=\"inputPassword\" name=\"password\" placeholder=\"Contraseña\" required>
    <button type='button' id='btn-olvidoContrasenia' class='text-primary bg-transparent p-2 rounded border-0 w-100 text-center'>
      ¿Olvidaste la contraseña?
    </button>
    <button class='btn btn-lg btn-primary btn-block mt-2' type='submit'>Iniciar Sesión</button>
    <input type='hidden' name='_csrf_token' value=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken("authenticate"), "html", null, true);
        echo "\" >
    <p class='mt-5 mb-3 text-muted text-center'>Copyright &copy; COOPIDROGAS 2021 - Por: <a href=\"http://www.waplicaciones.co\" target=\"_blank\"><img class=\"img-responsive mg-top logo-wapli\" src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/waplicaciones.png"), "html", null, true);
        echo "\" width=\"80px\" /></a></p>
  </form>

";
    }

    // line 37
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 38
        echo "<script>
    var forgotPassword = `";
        // line 39
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("password_forgot");
        echo "`;
</script>
  ";
        // line 41
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
  ";
        // line 42
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("js/login");
        echo "
";
    }

    public function getTemplateName()
    {
        return "security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 42,  151 => 41,  146 => 39,  143 => 38,  139 => 37,  131 => 32,  127 => 31,  117 => 24,  110 => 21,  102 => 18,  99 => 17,  96 => 16,  93 => 15,  87 => 13,  83 => 11,  80 => 10,  78 => 9,  75 => 8,  71 => 7,  65 => 5,  60 => 4,  56 => 3,  49 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "security/login.html.twig", "/srv/www/correosProduccion/templates/security/login.html.twig");
    }
}
