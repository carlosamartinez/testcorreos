<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* grupos/index.html.twig */
class __TwigTemplate_58e905a8b2030a118b83b365fb082b818829edabc8030cbcd2d5221efc03fb63 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'header' => [$this, 'block_header'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "grupos/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Correos - Grupos";
    }

    // line 5
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "  ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
  ";
        // line 7
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("css/grilla");
        echo "
  ";
        // line 9
        echo "  <style>
    input[type=number]::-webkit-inner-spin-button, 
    input[type=number]::-webkit-outer-spin-button { 
        -webkit-appearance: none; 
        margin: 0; 
    }

    input[type=number] { 
        -moz-appearance:textfield; 
    }
    a.active{
      background-color: #f8f9fa!important;
    }
  </style>
";
    }

    // line 25
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 26
        echo "  ";
        $this->loadTemplate("app/adminNavbar.html.twig", "grupos/index.html.twig", 26)->display($context);
        // line 27
        echo "  ";
        $this->loadTemplate("app/menu.html.twig", "grupos/index.html.twig", 27)->display($context);
    }

    // line 30
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 31
        echo "  <div id=\"myGrid\" style=\"height: 100%; width: 100%;\" class=\"ag-theme-fresh\"></div>
";
    }

    // line 34
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 35
        echo "  ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>

    var oGrid;
    var aDfColumnas = ";
        // line 39
        echo ($context["dfColumnas"] ?? null);
        echo ";
    var aGridButtons = ";
        // line 40
        echo ($context["aGridButtons"] ?? null);
        echo ";
    const errors = ";
        // line 41
        echo json_encode(twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "flashes", [0 => "error"], "method", false, false, false, 41));
        echo ";


    ApiRestURLS.listGrupos = `";
        // line 44
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_grupo_json");
        echo "`;
    ApiRestURLS.grupoModif = `";
        // line 45
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_grupo_modif");
        echo "`;
    ApiRestURLS.grupoDelete = `";
        // line 46
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_grupo_delete");
        echo "`;
    ApiRestURLS.grupoDownload = `";
        // line 47
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_download_report");
        echo "`;
    ApiRestURLS.grupoDownload = `";
        // line 48
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_download_report");
        echo "`;

    ApiRestURLS.addDestinatarios = `";
        // line 50
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_grupo_destinatarios_index", ["idGrupo" => "_idGrupo_"]);
        echo "`;

    ApiRestURLS.AsociadosJson = `";
        // line 52
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_destinatarios_grupo_index_asociados");
        echo "`;

    
  </script>
    ";
        // line 56
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("js/grupos/index");
        echo "
";
    }

    public function getTemplateName()
    {
        return "grupos/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 56,  161 => 52,  156 => 50,  151 => 48,  147 => 47,  143 => 46,  139 => 45,  135 => 44,  129 => 41,  125 => 40,  121 => 39,  113 => 35,  109 => 34,  104 => 31,  100 => 30,  95 => 27,  92 => 26,  88 => 25,  70 => 9,  66 => 7,  61 => 6,  57 => 5,  50 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "grupos/index.html.twig", "/srv/www/correosProduccion/templates/grupos/index.html.twig");
    }
}
