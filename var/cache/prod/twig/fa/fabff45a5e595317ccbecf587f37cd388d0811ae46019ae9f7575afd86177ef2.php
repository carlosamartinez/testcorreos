<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* envio/mdlCrud.html.twig */
class __TwigTemplate_72de112feddb2967ab1e9fa044fe58bb314a51066b41b9ca4a1d3dccf1439290 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((array_key_exists("frm_msj", $context) && (0 !== twig_compare(($context["frm_msj"] ?? null), null)))) {
            // line 2
            echo "    ";
            echo twig_escape_filter($this->env, ($context["frm_msj"] ?? null), "html", null, true);
            echo "
";
        }
        // line 4
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
        echo "
<div class=\"container\">

  <form id=\"formEnvio\" autocomplete=\"off\" class=\"row\">

    <div class=\"col-md-4\">

      <div class=\"row\">

        <div class=\"col-md-12\">
          <div class=\"control-group\">
            <label class=\"control-label\" style=\"margin-top: 0.5em;\">";
        // line 15
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "asunto", [], "any", false, false, false, 15), 'label');
        echo "</label>
            <div class=\"controls\">
              ";
        // line 17
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "asunto", [], "any", false, false, false, 17), 'widget', ["attr" => ["class" => "form-control  input-sm"]]);
        echo "
            </div>
          </div>
        </div>

        <div class=\"col-md-12\">
          <div class=\"control-group\">
            <label class=\"control-label\" style=\"margin-top: 0.5em;\">";
        // line 24
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "columnaCombinacion", [], "any", false, false, false, 24), 'label');
        echo "</label>
            <div class=\"controls\">
              ";
        // line 26
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "columnaCombinacion", [], "any", false, false, false, 26), 'widget', ["attr" => ["class" => "form-control  input-sm"]]);
        echo "
            </div>
          </div>
        </div>

      </div>

      <div class=\"row\">

        <div class=\"col-md-12\">
          <div class=\"control-group\">
            <label class=\"control-label\" style=\"margin-top: 0.5em;\">";
        // line 37
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "grupo", [], "any", false, false, false, 37), 'label');
        echo "</label>
            <div class=\"controls\">
              ";
        // line 39
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "grupo", [], "any", false, false, false, 39), 'widget', ["attr" => ["class" => "form-control  input-sm"]]);
        echo "
            </div>
          </div>
        </div>

        ";
        // line 52
        echo "        
      </div>

      <div class=\"row\">
        <div class=\"col-md-12\" id=\"agrupacionAsociado\">
          <div class=\"mt-3\">
            <input type=\"checkbox\" class=\"\" name=\"agruparCorrespondencia\" id=\"agruparCorrespondencia\" value=\"1\">
            <label class=\"control-label\" style=\"margin-top: 0.5em;\">Agrupar Correspondencia</label>
          </div>
        </div>

        <div class=\"col-md-12\" id=\"agrupacionProveedor\">
          <div class=\"mt-3\">
            <input type=\"checkbox\" name=\"agruparProveedor\" id=\"agruparProveedor\" value=\"1\">
            <label class=\"control-label\" style=\"margin-top: 0.5em;\">Agrupar Proveedor</label>
          </div>
        </div>

        <div class=\"col-md-12\" id=\"envioCopia\">
          <div class=\"mt-3\">
            <input type=\"checkbox\" name=\"enviarCopia\" id=\"enviarCopia\" value=\"1\">
            <label class=\"control-label\" style=\"margin-top: 0.5em;\">Enviar Copia</label>
          </div>
        </div>

      </div>
      
    </div>

    <div class=\"col-md-8\">
      <div class=\"row\">
        <div class=\"col-12\">
          <div class=\"control-group\">
            <label class=\"control-label\" style=\"margin-top: 0.5em;\">";
        // line 85
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "contenido", [], "any", false, false, false, 85), 'label');
        echo "</label>
            <div class=\"controls\">
              ";
        // line 87
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "contenido", [], "any", false, false, false, 87), 'widget', ["attr" => ["class" => "form-control  input-sm"]]);
        echo "
            </div>
          </div>
        </div>

      </div>
      
    </div>
     <div class=\"col-md-12\">
       <div class=\"row\">
        <div class=\"col-12 text-center\">
           <div class=\"form-group font-weight-bold m-2 border-top pt-3\">
              ";
        // line 99
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "save", [], "any", false, false, false, 99), 'widget', ["label" => "Guardar"]);
        echo "
              ";
        // line 100
        if ((0 === twig_compare(($context["accion"] ?? null), "editar"))) {
            // line 101
            echo "                <input type='hidden' name='idEnvio' id='idEnvio' value='";
            echo twig_escape_filter($this->env, ($context["idEnvio"] ?? null), "html", null, true);
            echo "' />
              ";
        }
        // line 103
        echo "           </div>
        </div>
      </div>
     </div>
  </form>
</div>


<script type=\"text/javascript\">

  var accion = \"";
        // line 113
        echo twig_escape_filter($this->env, ($context["accion"] ?? null), "html", null, true);
        echo "\";
  ApiRestURLS.mdlCrud = ApiRestURLS.listEnviosNew;

  if( accion == 'editar' ){
    ApiRestURLS.mdlCrud = ApiRestURLS.listEnviosEdit;
  }


  \$(document).ready(function(){

    //alert(\"entra 1 \");
    \$(\"#agrupacionProveedor\").hide();
    \$(\"#agrupacionAsociado\").hide();

    verificacionGrupo();


    \$(\"#envio_grupo\").change(function(){

      verificacionGrupo();


    });

    
    \$(\"#agruparCorrespondencia\").change(function(){
      if(\$(this).is(':checked')){
        \$(\"#agruparProveedor\").prop( \"checked\", false );
        \$(\"#envio_columnaCombinacion\").attr(\"disabled\", true);
      }else{
        \$(\"#envio_columnaCombinacion\").removeAttr(\"disabled\");
      }
      
    });


    \$(\"#agruparProveedor\").change(function(){
      if(\$(this).is(':checked')){
        \$(\"#agruparCorrespondencia\").prop( \"checked\", false );
        \$(\"#envio_columnaCombinacion\").removeAttr(\"disabled\");
      }
      
    });


  });

  function verificacionGrupo(){

    var tipoGrupo = \$(\"#envio_grupo\").find(':selected').data('tipo');

    if(tipoGrupo == 'No Aplica'){

      \$(\"#envio_columnaCombinacion\").removeAttr(\"disabled\");
      \$(\"#agruparCorrespondencia\").prop( \"checked\", false );

      \$(\"#agruparProveedor\").removeAttr(\"disabled\");
      \$(\"#agrupacionProveedor\").show();

      \$(\"#agruparCorrespondencia\").attr(\"disabled\", true);
      \$(\"#agrupacionAsociado\").hide();

    }else{
      \$(\"#agruparProveedor\").prop( \"checked\", false );

      \$(\"#agruparCorrespondencia\").removeAttr(\"disabled\");
      \$(\"#agrupacionAsociado\").show();

      \$(\"#agruparProveedor\").attr(\"disabled\", true);
      \$(\"#agrupacionProveedor\").hide();
    }

  }

  
</script>";
    }

    public function getTemplateName()
    {
        return "envio/mdlCrud.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  185 => 113,  173 => 103,  167 => 101,  165 => 100,  161 => 99,  146 => 87,  141 => 85,  106 => 52,  98 => 39,  93 => 37,  79 => 26,  74 => 24,  64 => 17,  59 => 15,  45 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "envio/mdlCrud.html.twig", "/srv/www/correosProduccion/templates/envio/mdlCrud.html.twig");
    }
}
