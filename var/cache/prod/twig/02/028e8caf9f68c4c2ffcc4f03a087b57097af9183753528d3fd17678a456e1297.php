<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* envio/adminArchivos.html.twig */
class __TwigTemplate_3d00966eb7d2a65baff8aaab13097506f8ac33890a73b4875627d06a0f6817f1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((0 === twig_compare(($context["hayExcepcion"] ?? null), false))) {
            // line 2
            echo "  <div class=\" mt-2\">  
    <div class=\"col-md-2\">
      <input type=\"checkbox\"  value=\"1\" id=\"archivosAdjuntos\">
    </div>
    <div class=\"col-md-10\">
      <div class=\" ocultarCSV\"  style=\"border:solid 2px #00C231;padding:2px;margin-top:10px;\"> <b>Recuerde:</b> Sólo se permite cargar un archivo en formato csv.</div>
      <div class=\" ocultarOtros\" style=\"border:solid 2px #E66711;padding:2px;margin-top:10px;\" ><b>Recuerde:</b> Puede cargar multiples archivos con un peso total de 2MB.</div>
    </div>
  </div>

  <div class=\" mt-2\">  
    <div class=\"col-md-12\">
      <form enctype=\"multipart/form-data\" id=\"formCSV\" class=\"ocultarCSV\">
        <div class=\"form-group text-center\">
            <label class=\"control-label\">Seleccione El Archivo</label>
            <input id=\"fileCSV\"  type=\"file\"  data-preview-file-type=\"any\"  name=\"csv\"  class=\"file-loading\">
        </div>
      </form>
      <form enctype=\"multipart/form-data\" id=\"formOtro\" class=\"ocultarOtros\">
        <div class=\"form-group text-center\">
            <label class=\"control-label\">Seleccione El Archivo</label>
            <input id=\"fileOtros\"  type=\"file\"  data-preview-file-type=\"any\"  name=\"adjuntos[]\" multiple class=\"file-loading\" ";
            // line 23
            if ((0 <= twig_compare(($context["peso"] ?? null), 2))) {
                echo "disabled";
            }
            echo ">
        </div>
      </form>
    </div>
  </div>

<script type=\"text/javascript\">
  \$().ready(function(){

    
    \$(\"#archivosAdjuntos\").bootstrapSwitch({
      size:'small',
      labelText:'Adjuntos',
      onText:'CSV',
      offText:'Otros',
      offColor:'warning',
      onColor: 'success'
    });

    
    if(\$(\"#agruparCorrespondencia\").is(':checked')){
      \$('#archivosAdjuntos').bootstrapSwitch('state', false);
      \$(\"#archivosAdjuntos\").bootstrapSwitch('disabled',true);
      
      \$('.ocultarCSV').hide();
      \$('.ocultarOtros').show();

    }else{
      \$('#archivosAdjuntos').bootstrapSwitch('state', true);
      \$('.ocultarCSV').show();
      \$('.ocultarOtros').hide();
    }


    \$(\"#agruparCorrespondencia\").change(function(){
      if(\$(this).is(':checked')){
        \$(\"#archivosAdjuntos\").bootstrapSwitch('disabled',true);
      }else{
        \$(\"#archivosAdjuntos\").bootstrapSwitch('disabled',false);
      }
    });

    /*\$(\"#envio_grupo\").change(function(){

      var tipoGrup = \$(\"#envio_grupo\").find(':selected').data('tipo');
      if(tipoGrupo == 'No Aplica'){


      }else{

      }

    });*/

    

    \$('#archivosAdjuntos').on('switchChange.bootstrapSwitch', function (event, state) {
      if(state){
        \$('.ocultarCSV').show();
        \$('.ocultarOtros').hide();

      }else{
        \$('.ocultarCSV').hide();
        \$('.ocultarOtros').show();
      }
    });



    \$(\"#fileCSV\").fileinput({
        uploadUrl: \"";
            // line 93
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_archivo_adjunto_csv");
            echo "?idEnvio=";
            echo twig_escape_filter($this->env, ($context["idEnvio"] ?? null), "html", null, true);
            echo "\", // server upload action
        uploadAsync: false,
        showPreview: false,
        type:'post',
        dataType:'json',
        allowedFileExtensions: ['csv','CSV'],
        language: \"es\",
        maxFileCount: 1
    }).on('filebatchpreupload', function(event, data, id, index) {
        \$(\"#loading\").show();
        
    }).on('filebatchuploadsuccess', function(event, data) {
        \$(\"#loading\").hide();
        
        if(data.response.status == 1){
          Swal.fire({
            icon: 'success',
            title:'',
            html: 'Archivo de combinacion cargado correctamente.',
            showConfirmButton: true
          });
        }else{
          console.log(data.response.descripcion);

          Swal.fire({
            icon: 'warning',
            title:'',
            html: 'Ha ocurrido un error en la carga del archivo, intente nuevamente.',
            showConfirmButton: true,
          });
        }

    });

    \$(\"#fileOtros\").fileinput({
        uploadUrl: \"";
            // line 128
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_archivo_adjunto_otros");
            echo "\", // server upload action
        uploadAsync: false,
        showPreview: true,
        allowedFileExtensions: [\"xls\",\"pdf\",\"png\",\"jpg\",\"doc\",\"docx\",\"xlsx\",\"txt\"],
        language: \"es\",
        maxFileCount: 20,
        maxFileSize: 2000
    }).on('filebatchpreupload', function(event, data, id, index) {
        \$(\"#loading\").show();
        
    }).on('filebatchuploadsuccess', function(event, data, id, index) {
        \$(\"#loading\").hide();

        if(data.response.status == 1 ){
            Swal.fire({
              icon: 'success',
              title:'',
              html: 'Archivos adjuntos cargados correctamente.',
              showConfirmButton: true
            });
            \$('#fileOtros').fileinput('reset'); 
        }else{
            console.log(data.response.descripcion);

            Swal.fire({
              icon: 'warning',
              title:'',
              html: 'Ha ocurrido un error en la carga del archivo, intente nuevamente.',
              showConfirmButton: true,
            });
        }
    });


  });
</script>
";
        } else {
            // line 165
            echo "  <div  class=\"row\">
    <div class=\"col-md-12 text-center text-danger\" >
      <p>
        Al parecer el sistema no ha podido acceder a las carpetas del administrador. Por favor pongase en contacto con el Administrador del sistema.
      </p>
    </div>
  </div>
";
        }
    }

    public function getTemplateName()
    {
        return "envio/adminArchivos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  217 => 165,  177 => 128,  137 => 93,  62 => 23,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "envio/adminArchivos.html.twig", "/srv/www/correosProduccion/templates/envio/adminArchivos.html.twig");
    }
}
