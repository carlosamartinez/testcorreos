<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* envio/vistaPreviaMensaje.html.twig */
class __TwigTemplate_bcc3a3bd5703f7b9f8b2edbe19f36463be3f334f44b869a2a83978dc38cb698b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<style>
    #modalGlobal.modal.fade.show{display: flex !important;} 
    #modalGlobal.modal.fade.show .modal-body{overflow: auto;}
    #modalGlobal.modal.fade.show .modal-content{height: 100%;}
    #modalGlobal.modal.fade.show .modal-content .form-group{margin: 0}
    .tablaDatos {margin:0px;padding:0px;width:100%;border:1px solid #7f7f7f;-moz-border-radius-bottomleft:0px;-webkit-border-bottom-left-radius:0px;border-bottom-left-radius:0px;-moz-border-radius-bottomright:0px;-webkit-border-bottom-right-radius:0px;border-bottom-right-radius:0px;-moz-border-radius-topright:0px;-webkit-border-top-right-radius:0px;border-top-right-radius:0px;-moz-border-radius-topleft:0px;-webkit-border-top-left-radius:0px;border-top-left-radius:0px;}.tablaDatos table{border-collapse:collapse;border-spacing:0;width:100%;height:100%;margin:0px;padding:0px;}.tablaDatos tr:last-child td:last-child {-moz-border-radius-bottomright:0px;-webkit-border-bottom-right-radius:0px;border-bottom-right-radius:0px;}.tablaDatos table tr:first-child td:first-child {-moz-border-radius-topleft:0px;-webkit-border-top-left-radius:0px;border-top-left-radius:0px;.tablaDatos table tr:first-child td:last-child {-moz-border-radius-topright:0px;-webkit-border-top-right-radius:0px;border-top-right-radius:0px;}.tablaDatos tr:last-child td:first-child{-moz-border-radius-bottomleft:0px;-webkit-border-bottom-left-radius:0px;border-bottom-left-radius:0px;}.tablaDatos tr:hover td{}}.tablaDatos tr:nth-child(odd){background-color:#aad4ff;}.tablaDatos tr:nth-child(even){background-color:#ffffff;}.tablaDatos td{vertical-align:middle;border:1px solid #7f7f7f;border-width:0px 1px 1px 0px;text-align:left;padding:7px;font-size:10px;font-family:Arial;font-weight:normal;color:#000000;}.tablaDatos tr:last-child td{border-width:0px 1px 0px 0px;}.tablaDatos tr td:last-child{border-width:0px 0px 1px 0px;}.tablaDatos tr:last-child td:last-child{border-width:0px 0px 0px 0px;}.tablaDatos tr:first-child td{background:-o-linear-gradient(bottom,#005fbf 5%,#005fbf 100%);background:-webkit-gradient(linear,left top,left bottom,color-stop(0.05, #005fbf),color-stop(1,#005fbf));background:-moz-linear-gradient( center top, #005fbf 5%, #005fbf 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=\"#005fbf\",endColorstr=\"#005fbf\");background:-o-linear-gradient(top,#005fbf,005fbf);background-color:#005fbf;border:0px solid #7f7f7f;text-align:center;border-width:0px 0px 1px 1px;font-size:14px;font-family:Arial;font-weight:bold;color:#ffffff;}.tablaDatos tr:first-child:hover td{background:-o-linear-gradient(bottom,#005fbf 5%,#005fbf 100%);background:-webkit-gradient(linear,left top,left bottom,color-stop(0.05,#005fbf),color-stop(1,#005fbf));background:-moz-linear-gradient(center top,#005fbf 5%,#005fbf 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=\"#005fbf\",endColorstr=\"#005fbf\");background:-o-linear-gradient(top,#005fbf,005fbf);background-color:#005fbf;}.tablaDatos tr:first-child td:first-child{border-width:0px 0px 1px 0px;}.tablaDatos tr:first-child td:last-child{border-width:0px 0px 1px 1px;}
    hr.solid {
        border-top: 2px solid #023A78;
    }
    hr.hr-text {
  position: relative;
    border: none;
    height: 1px;
    background: #999;
}

hr.hr-text::before {
    content: attr(data-content);
    display: inline-block;
    background: #fff;
    font-weight: bold;
    font-size: 0.85rem;
    color: #999;
    border-radius: 30rem;
    padding: 0.2rem 2rem;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
}
</style>
<div class=\"container\" >
    ";
        // line 33
        if (($context["infoDetalle"] ?? null)) {
            // line 34
            echo "        <div class=\"row\">
            <div class=\"col-md-3\">
                <div class=\"form-group\">
                    Seleccione el destinatario a visualizar
                </div>
            </div>
            <div class=\"col-md\">
                <div class=\"form-group\">
                    <select class=\"form-control\" id=\"detalleDestinatario\">
                        <option value=\"\">.:: Seleccionar ::.</option>
                        ";
            // line 44
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["infoDetalle"] ?? null), "destinatarios", [], "any", false, false, false, 44));
            foreach ($context['_seq'] as $context["_key"] => $context["destinatario"]) {
                // line 45
                echo "                            <option value=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["destinatario"], "id", [], "any", false, false, false, 45), "html", null, true);
                echo "\" ";
                echo (((0 === twig_compare(($context["destinatarioId"] ?? null), twig_get_attribute($this->env, $this->source, $context["destinatario"], "id", [], "any", false, false, false, 45)))) ? ("selected") : (""));
                echo ">";
                echo twig_escape_filter($this->env, twig_join_filter(twig_array_filter($this->env, $context["destinatario"], function ($__b__, $__index__) use ($context, $macros) { $context["b"] = $__b__; $context["index"] = $__index__; return ((0 !== twig_compare(($context["b"] ?? null), "")) &&  !twig_in_filter(($context["index"] ?? null), [0 => "id", 1 => "tipo"])); }), ", "), "html", null, true);
                echo "</option>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['destinatario'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 47
            echo "                    </select>
                </div>
            </div>
            <div class=\"col-md-3\">
                <div class=\"form-group\">
                    <div class=\"alert alert-primary\" role=\"alert\" style=\"margin: 0; padding: 8px 16px;\">
                        <i class=\"mr-2 fas fa-info-circle\"></i>
                        ";
            // line 54
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, ($context["entity"] ?? null), "agruparProveedor", [], "any", false, false, false, 54), 1))) {
                // line 55
                echo "                            Agrupado Por Proveedor
                        ";
            } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 56
($context["entity"] ?? null), "grupo", [], "any", false, false, false, 56), "clasificacion", [], "any", false, false, false, 56), "Droguerías"))) {
                // line 57
                echo "                            No Agrupado - Droguerías.
                        ";
            } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 58
($context["entity"] ?? null), "grupo", [], "any", false, false, false, 58), "clasificacion", [], "any", false, false, false, 58), "Asociados"))) {
                // line 59
                echo "                            No Agrupado - Asociados.
                        ";
            } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 60
($context["entity"] ?? null), "grupo", [], "any", false, false, false, 60), "clasificacion", [], "any", false, false, false, 60), "No Aplica"))) {
                // line 61
                echo "                            No Agrupado - Proveedores.
                        ";
            }
            // line 63
            echo "                    </div>
                </div>
            </div>
        </div>
        <hr class=\"solid\">
    ";
        }
        // line 69
        echo "    <div class=\"row\" >
        ";
        // line 71
        echo "        <div class=\"col-md\">
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <span class=\"font-weight-bold text-uppercase\">";
        // line 74
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["entity"] ?? null), "asunto", [], "any", false, false, false, 74), "html", null, true);
        echo "</span>
                </div>
                <div class=\"col-md-12\">
                    <span class=\"font-weight-bold text-secondary\">Para: </span>

                    ";
        // line 79
        if ((($context["infoDetalle"] ?? null) && twig_get_attribute($this->env, $this->source, ($context["infoDetalle"] ?? null), "para", [], "any", false, false, false, 79))) {
            // line 80
            echo "                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["infoDetalle"] ?? null), "para", [], "any", false, false, false, 80));
            foreach ($context['_seq'] as $context["_key"] => $context["destinatario"]) {
                // line 81
                echo "                            <span class=\"mr-2 font-weight-light Small\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["destinatario"], "nombre", [], "any", false, false, false, 81), "html", null, true);
                echo " &lt;";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["destinatario"], "email1", [], "any", false, false, false, 81), "html", null, true);
                echo "&gt;,</span>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['destinatario'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 83
            echo "                    ";
        } elseif (($context["destinatario"] ?? null)) {
            // line 84
            echo "                        ";
            if ((0 === twig_compare(($context["usuarioTipo"] ?? null), "proveedor"))) {
                // line 85
                echo "                            <span class=\"font-weight-light Small\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["destinatario"] ?? null), "empresa", [], "any", false, false, false, 85), "html", null, true);
                echo " - ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["destinatario"] ?? null), "nombre", [], "any", false, false, false, 85), "html", null, true);
                echo " &lt;";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["destinatario"] ?? null), "email1", [], "any", false, false, false, 85), "html", null, true);
                echo "&gt;</span>
                        ";
            } else {
                // line 87
                echo "                            ";
                if ((0 === twig_compare(($context["tipoGrupo"] ?? null), 1))) {
                    // line 88
                    echo "                                ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["destinatario"] ?? null), "asociado", [], "any", false, false, false, 88), "html", null, true);
                    echo " - ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["destinatario"] ?? null), "emailAsociado", [], "any", false, false, false, 88), "html", null, true);
                    echo "
                            ";
                } else {
                    // line 90
                    echo "                                ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["destinatario"] ?? null), "drogueria", [], "any", false, false, false, 90), "html", null, true);
                    echo " - ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["destinatario"] ?? null), "email", [], "any", false, false, false, 90), "html", null, true);
                    echo "
                            ";
                }
                // line 92
                echo "                        ";
            }
            // line 93
            echo "                    ";
        } else {
            // line 94
            echo "                        Coopidrogas => coopidrogas@coopidrogas.com.co
                    ";
        }
        // line 96
        echo "                    <hr data-content=\"MENSAJE\" class=\"hr-text\">
                </div>
                <div class=\"col-md-12\">
                    ";
        // line 99
        echo ($context["contenido"] ?? null);
        echo "
                </div>
                ";
        // line 101
        if (($context["adjuntos"] ?? null)) {
            // line 102
            echo "                    <hr>
                    <div class=\"col-md-12\">
                        <span class=\"text-warning\">Adjuntos:</span><br>
                        ";
            // line 105
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["adjuntos"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["adjunto"]) {
                echo "<i class=\"fas fa-paperclip\"></i><a href=\"../../adjuntos/";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 105), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, ($context["carpetaAdmin"] ?? null), "html", null, true);
                echo "/otros/";
                echo twig_escape_filter($this->env, $context["adjunto"], "html", null, true);
                echo "\"> ";
                echo twig_escape_filter($this->env, $context["adjunto"], "html", null, true);
                echo "</a><br>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['adjunto'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 106
            echo "                    </div>
                ";
        }
        // line 108
        echo "            </div>
        </div>
    </div>
</div>






";
    }

    public function getTemplateName()
    {
        return "envio/vistaPreviaMensaje.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  257 => 108,  253 => 106,  236 => 105,  231 => 102,  229 => 101,  224 => 99,  219 => 96,  215 => 94,  212 => 93,  209 => 92,  201 => 90,  193 => 88,  190 => 87,  180 => 85,  177 => 84,  174 => 83,  163 => 81,  158 => 80,  156 => 79,  148 => 74,  143 => 71,  140 => 69,  132 => 63,  128 => 61,  126 => 60,  123 => 59,  121 => 58,  118 => 57,  116 => 56,  113 => 55,  111 => 54,  102 => 47,  89 => 45,  85 => 44,  73 => 34,  71 => 33,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "envio/vistaPreviaMensaje.html.twig", "/srv/www/correosProduccion/templates/envio/vistaPreviaMensaje.html.twig");
    }
}
