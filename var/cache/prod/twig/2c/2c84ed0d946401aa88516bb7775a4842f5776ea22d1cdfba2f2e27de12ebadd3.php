<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* reset_password/request.html.twig */
class __TwigTemplate_e53adbbbb2b809d46ffbd8f1d3dbdf19c90817fa39e3f4fbb1c65816ff4984e5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'title' => [$this, 'block_title'],
            'bodylogin' => [$this, 'block_bodylogin'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "baseLogin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("baseLogin.html.twig", "reset_password/request.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        echo "  ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
    }

    // line 6
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Correos - Recuperación De Contraseña";
    }

    // line 8
    public function block_bodylogin($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "
    ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "flashes", [0 => "reset_password_error"], "method", false, false, false, 10));
        foreach ($context['_seq'] as $context["_key"] => $context["flashError"]) {
            // line 11
            echo "        <div class=\"alert alert-danger\" role=\"alert\">";
            echo twig_escape_filter($this->env, $context["flashError"], "html", null, true);
            echo "</div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashError'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "<div id=\"form-recuperar\" style=\"justify-content;center;text-align:center\">
<img class='mb-4' src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/logo-correos.png"), "html", null, true);
        echo "\" width=\"300\">
<p class=\"h3\">Recuperación De Contraseña</p><hr><br>
    <div class=\"row\">

        <div class=\"col-4\"></div>
        <div class=\"col-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    ";
        // line 22
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["requestForm"] ?? null), 'form_start');
        echo "
                    ";
        // line 23
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["requestForm"] ?? null), "email", [], "any", false, false, false, 23), 'row');
        echo "
                    <div>
                        <small>
                            Ingrese su email y le enviaremos un link con el que podra reestablecer su contraseña.   
                        </small>
                    </div>
                    <br>
                    <button id=\"sendMail\" class=\"btn btn-primary\">Enviar Correo</button>
                    <button type='button' id=\"btn-iniciarses\" class=\"btn btn-secondary\">Iniciar Sesion</button>
                    ";
        // line 32
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["requestForm"] ?? null), 'form_end');
        echo "
                </div>
            </div>
            
        </div>
        <div class=\"col-4\"></div>
        
    </div>
    
    <p class='mt-5 mb-3 text-muted text-center'>Copyright &copy; COOPIDROGAS 2021 - Por: <a href=\"http://www.waplicaciones.co\" target=\"_blank\"><img class=\"img-responsive mg-top logo-wapli\" src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/waplicaciones.png"), "html", null, true);
        echo "\" width=\"80px\" /></a></p>

</div>
";
    }

    // line 45
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 46
        echo "<script>
    var forgotPassword = `";
        // line 47
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("forgot_password_request");
        echo "`;
</script>
  ";
        // line 49
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
  ";
        // line 50
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("js/usuarios/passwordRecovery");
        echo "
";
    }

    public function getTemplateName()
    {
        return "reset_password/request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  153 => 50,  149 => 49,  144 => 47,  141 => 46,  137 => 45,  129 => 41,  117 => 32,  105 => 23,  101 => 22,  90 => 14,  87 => 13,  78 => 11,  74 => 10,  71 => 9,  67 => 8,  60 => 6,  53 => 3,  49 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "reset_password/request.html.twig", "/srv/www/correosProduccion/templates/reset_password/request.html.twig");
    }
}
