<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* clientes/index.html.twig */
class __TwigTemplate_a08973097b3ad2b78d72deadabbe820ef7d724e94da3188b8a5d6ff0bc5b8618 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'header' => [$this, 'block_header'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "clientes/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Correos - Usuarios";
    }

    // line 5
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "  ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
  ";
        // line 7
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("css/grilla");
        echo "
";
    }

    // line 10
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 11
        echo "  ";
        $this->loadTemplate("app/adminNavbar.html.twig", "clientes/index.html.twig", 11)->display($context);
        // line 12
        echo "  ";
        $this->loadTemplate("app/menu.html.twig", "clientes/index.html.twig", 12)->display($context);
    }

    // line 15
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 16
        echo "  <div id=\"myGrid\" style=\"height: 100%;\" class=\"ag-theme-fresh\"></div>
";
    }

    // line 19
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 20
        echo "  ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
  <script>
    var oGrid;
    var aDfColumnas = ";
        // line 23
        echo ($context["dfColumnas"] ?? null);
        echo ";
    var aGridButtons = ";
        // line 24
        echo ($context["aGridButtons"] ?? null);
        echo ";

    ApiRestURLS.listClientes = `";
        // line 26
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_clientes_json");
        echo "`;
  </script>
  ";
        // line 28
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("js/clientes");
        echo "
";
    }

    public function getTemplateName()
    {
        return "clientes/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 28,  113 => 26,  108 => 24,  104 => 23,  97 => 20,  93 => 19,  88 => 16,  84 => 15,  79 => 12,  76 => 11,  72 => 10,  66 => 7,  61 => 6,  57 => 5,  50 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "clientes/index.html.twig", "/srv/www/correosProduccion/templates/clientes/index.html.twig");
    }
}
