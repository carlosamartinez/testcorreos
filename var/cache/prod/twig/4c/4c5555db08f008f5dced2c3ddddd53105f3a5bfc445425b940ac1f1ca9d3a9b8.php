<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* envio/mdlCrudEditar.html.twig */
class __TwigTemplate_a2cd21d56e179dc8eaee8eba8145aa06db4e17801f52fb5c0027923aaafbbf44 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((array_key_exists("frm_msj", $context) && (0 !== twig_compare(($context["frm_msj"] ?? null), null)))) {
            // line 2
            echo "    ";
            echo twig_escape_filter($this->env, ($context["frm_msj"] ?? null), "html", null, true);
            echo "
";
        }
        // line 4
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
        echo "

<ul class=\"nav nav-tabs\" id=\"myTab\" role=\"tablist\">
  <li class=\"nav-item\" role=\"presentation\">
    <a class=\"nav-link active\" id=\"datosbasicos-tab\" data-toggle=\"tab\" href=\"#datosbasicos\" role=\"tab\" aria-controls=\"datosbasicos\" aria-selected=\"true\">Datos Basicos</a>
  </li>
  <li class=\"nav-item\" role=\"presentation\">
    <a class=\"nav-link\" id=\"cargaArchivo-tab\" data-toggle=\"tab\" href=\"#cargaArchivo\" role=\"tab\" aria-controls=\"cargaArchivo\" aria-selected=\"false\">Cargar Archivos</a>
  </li>
  <li class=\"nav-item\" role=\"presentation\">
    <a class=\"nav-link\" id=\"verArchivos-tab\" data-toggle=\"tab\" href=\"#verArchivosContent\" role=\"tab\" aria-controls=\"verArchivosContent\" aria-selected=\"false\">Ver Archivos</a>
  </li>
</ul>

<div class=\"tab-content\" id=\"myTabContent\">
  <div class=\"tab-pane fade show active\" id=\"datosbasicos\" role=\"tabpanel\" aria-labelledby=\"datosbasicos-tab\">

    <div class=\"container\" style=\"height: 480px;\">

      <form id=\"formEnvio\" autocomplete=\"off\" class=\"row\">

        <div class=\"col-md-4\">

          <div class=\"row\">

            <div class=\"col-md-12\">
              <div class=\"control-group\">
                <label class=\"control-label\" style=\"margin-top: 0.5em;\">";
        // line 31
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "asunto", [], "any", false, false, false, 31), 'label');
        echo "</label>
                <div class=\"controls\">
                  ";
        // line 33
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "asunto", [], "any", false, false, false, 33), 'widget', ["attr" => ["class" => "form-control  input-sm"]]);
        echo "
                </div>
              </div>
            </div>

            <div class=\"col-md-12\">
              <div class=\"control-group\">
                <label class=\"control-label\" style=\"margin-top: 0.5em;\">";
        // line 40
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "columnaCombinacion", [], "any", false, false, false, 40), 'label');
        echo "</label>
                <div class=\"controls\">
                  ";
        // line 42
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "columnaCombinacion", [], "any", false, false, false, 42), 'widget', ["attr" => ["class" => "form-control  input-sm"]]);
        echo "
                </div>
              </div>
            </div>

          </div>

          <div class=\"row\">

            <div class=\"col-md-12\">
              <div class=\"control-group\">
                <label class=\"control-label\" style=\"margin-top: 0.5em;\">";
        // line 53
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "grupo", [], "any", false, false, false, 53), 'label');
        echo "</label>
                <div class=\"controls\">
                  ";
        // line 55
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "grupo", [], "any", false, false, false, 55), 'widget', ["attr" => ["class" => "form-control  input-sm"]]);
        echo "
                </div>
              </div>
            </div>

            ";
        // line 68
        echo "            
          </div>

          <div class=\"row\">
            <div class=\"col-md-12\" id=\"agrupacionAsociado\">
              <div class=\"mt-3\">
                <input type=\"checkbox\" class=\"\" name=\"agruparCorrespondencia\" id=\"agruparCorrespondencia\" ";
        // line 74
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, ($context["entity"] ?? null), "agruparCorrespondencia", [], "any", false, false, false, 74), 1))) {
            echo "checked";
        }
        echo " value=\"1\">
                <label class=\"control-label\" style=\"margin-top: 0.5em;\">Agrupar Correspondencia</label>
              </div>
            </div>

            <div class=\"col-md-12\" id=\"agrupacionProveedor\">
              <div class=\"mt-3\">
                <input type=\"checkbox\" name=\"agruparProveedor\" id=\"agruparProveedor\" ";
        // line 81
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, ($context["entity"] ?? null), "agruparProveedor", [], "any", false, false, false, 81), 1))) {
            echo "checked";
        }
        echo " value=\"1\">
                <label class=\"control-label\" style=\"margin-top: 0.5em;\">Agrupar Proveedor</label>
              </div>
            </div>

            <div class=\"col-md-12\" id=\"envioCopia\">
              <div class=\"mt-3\">
                <input type=\"checkbox\" name=\"enviarCopia\" id=\"enviarCopia\" ";
        // line 88
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, ($context["entity"] ?? null), "enviarCopia", [], "any", false, false, false, 88), 1))) {
            echo "checked";
        }
        echo " value=\"1\">
                <label class=\"control-label\" style=\"margin-top: 0.5em;\">Enviar Copia</label>
              </div>
            </div>

          </div>

          
        </div>
        <div class=\"col-md-8\">

          <div class=\"row\">

            <div class=\"col-12\">
              <div class=\"control-group\">
                <label class=\"control-label\" style=\"margin-top: 0.5em;\">";
        // line 103
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "contenido", [], "any", false, false, false, 103), 'label');
        echo "</label>
                <div class=\"controls\">
                  ";
        // line 105
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "contenido", [], "any", false, false, false, 105), 'widget', ["attr" => ["class" => "form-control  input-sm"]]);
        echo "
                </div>
              </div>
            </div>

          </div>
          
        </div>

        <div class=\"col-md-12\">
          <div class=\"row\">
            <div class=\"col-12 text-center\">
               <div class=\"form-group font-weight-bold m-2 border-top pt-3\">
                  ";
        // line 118
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "save", [], "any", false, false, false, 118), 'widget', ["label" => "Guardar"]);
        echo "
                  <input type='hidden' name='idEnvio' id='idEnvio' value='";
        // line 119
        echo twig_escape_filter($this->env, ($context["idEnvio"] ?? null), "html", null, true);
        echo "' />
               </div>
            </div>
         </div>
        </div>
      </form>
      
    </div>

  </div>
  <div class=\"tab-pane fade\" id=\"cargaArchivo\" role=\"tabpanel\" aria-labelledby=\"cargaArchivo-tab\" style=\"height: 480px;max-height: 500px;overflow: auto;\">
    <div class=\"col-md-12 text-right mt-2\">
      <a href=\"http://correos.coopidrogas.com.co/uploads/archivoCombinacion.csv\" target=\"_blank\" class=\"btn btn-sm btn-primary\" type=\"submit\">
        <i class=\"fa fa-download\" aria-hidden=\"true\"></i>
        Descargar Formato Proveedor
      </a> 
      <a href=\"http://correos.coopidrogas.com.co/uploads/archivoCombinacionAsociados.csv\" target=\"_blank\" class=\"btn btn-sm btn-primary\" type=\"submit\">
        <i class=\"fa fa-download\" aria-hidden=\"true\"></i>
        Descargar Formato Asociado
      </a>
    </div>
    

    ";
        // line 142
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment(Symfony\Bridge\Twig\Extension\HttpKernelExtension::controller("App\\Controller\\ArchivosAdjuntosController::formSubirArchivos", ["idEnvio" => ($context["idEnvio"] ?? null)]));
        echo "

  </div>
  <div class=\"tab-pane fade\" id=\"verArchivosContent\" role=\"tabpanel\" aria-labelledby=\"verArchivos-tab\">
    <div class=\"row\">
        <div class=\"col-md-12 text-center mt-3\" id=\"verArchivos\" style=\"height: 480px;max-height: 500px;overflow: auto;\">
        </div>
      </div>
  </div>
</div>



<script type=\"text/javascript\">

  var accion = \"";
        // line 157
        echo twig_escape_filter($this->env, ($context["accion"] ?? null), "html", null, true);
        echo "\";
  ApiRestURLS.mdlCrud = ApiRestURLS.listEnviosEdit;

  \$(document).ready(function(){

    //alert(\"entra 1 \");
    \$(\"#agrupacionProveedor\").hide();
    \$(\"#agrupacionAsociado\").hide();

    verificacionGrupo();


    \$(\"#envio_grupo\").change(function(){

      verificacionGrupo();


    });

    if(\$(\"#agruparCorrespondencia\").is(':checked')){
      \$(\"#envio_columnaCombinacion\").attr(\"disabled\", true);
    }

    \$(\"#agruparCorrespondencia\").change(function(){
      if(\$(this).is(':checked')){
        \$(\"#agruparProveedor\").prop( \"checked\", false );
        \$(\"#envio_columnaCombinacion\").attr(\"disabled\", true);

        \$('.ocultarCSV').hide();
        \$('.ocultarOtros').show();
        \$('#archivosAdjuntos').bootstrapSwitch('state', false);
      }else{
        \$(\"#envio_columnaCombinacion\").removeAttr(\"disabled\");
      }
      
    });


    \$(\"#agruparProveedor\").change(function(){
      if(\$(this).is(':checked')){
        \$(\"#agruparCorrespondencia\").prop( \"checked\", false );
        \$(\"#envio_columnaCombinacion\").removeAttr(\"disabled\");
      }
      
    });


  });

  function verificacionGrupo(){

    var tipoGrupo = \$(\"#envio_grupo\").find(':selected').data('tipo');

    if(tipoGrupo == 'No Aplica'){

      \$(\"#envio_columnaCombinacion\").removeAttr(\"disabled\");
      \$(\"#agruparCorrespondencia\").prop( \"checked\", false );

      \$(\"#agruparProveedor\").removeAttr(\"disabled\");
      \$(\"#agrupacionProveedor\").show();

      \$(\"#agruparCorrespondencia\").attr(\"disabled\", true);
      \$(\"#agrupacionAsociado\").hide();

      \$(\"#archivosAdjuntos\").bootstrapSwitch('disabled',false);

    }else{
      \$(\"#agruparProveedor\").prop( \"checked\", false );

      \$(\"#agruparCorrespondencia\").removeAttr(\"disabled\");
      \$(\"#agrupacionAsociado\").show();

      \$(\"#agruparProveedor\").attr(\"disabled\", true);
      \$(\"#agrupacionProveedor\").hide();
    }

  }

</script>";
    }

    public function getTemplateName()
    {
        return "envio/mdlCrudEditar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  243 => 157,  225 => 142,  199 => 119,  195 => 118,  179 => 105,  174 => 103,  154 => 88,  142 => 81,  130 => 74,  122 => 68,  114 => 55,  109 => 53,  95 => 42,  90 => 40,  80 => 33,  75 => 31,  45 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "envio/mdlCrudEditar.html.twig", "/srv/www/correosProduccion/templates/envio/mdlCrudEditar.html.twig");
    }
}
