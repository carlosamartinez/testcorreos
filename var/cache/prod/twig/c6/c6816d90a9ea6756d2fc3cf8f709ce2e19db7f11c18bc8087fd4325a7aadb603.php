<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* proveedores/index.html.twig */
class __TwigTemplate_08a465200e6c53f4dbedb51d5b212fbfc943df92981bbd88c9eb8fcb0370ca7b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'header' => [$this, 'block_header'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "proveedores/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Correos - Proveedores";
    }

    // line 5
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "  ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
  ";
        // line 7
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("js/usuarios");
        echo "
  ";
        // line 8
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("css/grilla");
        echo "
  ";
        // line 10
        echo "  <style>
    input[type=number]::-webkit-inner-spin-button, 
    input[type=number]::-webkit-outer-spin-button { 
        -webkit-appearance: none; 
        margin: 0; 
    }

    input[type=number] { 
        -moz-appearance:textfield; 
    }
    a.active{
      background-color: #f8f9fa!important;
    }
  </style>

";
    }

    // line 27
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 28
        echo "  ";
        $this->loadTemplate("app/adminNavbar.html.twig", "proveedores/index.html.twig", 28)->display($context);
        // line 29
        echo "  ";
        $this->loadTemplate("app/menu.html.twig", "proveedores/index.html.twig", 29)->display($context);
    }

    // line 32
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 33
        echo "  <div id=\"myGrid\" style=\"height: 100%;\" class=\"ag-theme-fresh\"></div>

  <div class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" id=\"modalGlobalContactos\" style=\"z-index: 1060!important;\">
    <div class=\"modal-dialog modal-dialog-scrollable\">
      <div class=\"modal-content\">
        <div class=\"text-center modal-header text-info\">
          <h6 class=\"modal-title font-weight-bold\" id=\"tituloModalGlobalContactos\">...</h6>
          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
            <span aria-hidden=\"true\">&times;</span>
          </button>
        </div>
        <div class=\"modal-body\" id=\"contenidoModalGlobalContactos\"></div>
      </div>
    </div>
  </div>
";
    }

    // line 50
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 51
        echo "  ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
  <script>
    var oGrid;
    var oGridContactos;
    var aDfColumnas = ";
        // line 55
        echo ($context["dfColumnas"] ?? null);
        echo ";
    var aDfColumnasContactos = ";
        // line 56
        echo ($context["dfColumnasContactos"] ?? null);
        echo ";
    var aGridButtons = ";
        // line 57
        echo ($context["aGridButtons"] ?? null);
        echo ";

    ApiRestURLS.listProveedores = `";
        // line 59
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_proveedores_json");
        echo "`;
    ApiRestURLS.proveedorNew = `";
        // line 60
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_proveedores_new");
        echo "`;
    ApiRestURLS.proveedorEdit = `";
        // line 61
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_proveedores_edit");
        echo "`;
    ApiRestURLS.proveedorDelete = `";
        // line 62
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_proveedores_delete");
        echo "`;
    ApiRestURLS.proveedorExportar = `";
        // line 63
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_proveedores_csv");
        echo "`;
    ApiRestURLS.contactosProveedor = `";
        // line 64
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_contactos_proveedores");
        echo "`;
    ApiRestURLS.contactosProveedorJson = `";
        // line 65
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_contactos_proveedores_json");
        echo "`;
    ApiRestURLS.contactosNew = `";
        // line 66
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_contactos_proveedores_new");
        echo "`;
    ApiRestURLS.contactosEdit = `";
        // line 67
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_contactos_proveedores_edit");
        echo "`;
    ApiRestURLS.contactosDelete = `";
        // line 68
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_contactos_proveedores_delete");
        echo "`;

  </script>
  ";
        // line 71
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("js/proveedores");
        echo "
  
";
    }

    public function getTemplateName()
    {
        return "proveedores/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  195 => 71,  189 => 68,  185 => 67,  181 => 66,  177 => 65,  173 => 64,  169 => 63,  165 => 62,  161 => 61,  157 => 60,  153 => 59,  148 => 57,  144 => 56,  140 => 55,  132 => 51,  128 => 50,  109 => 33,  105 => 32,  100 => 29,  97 => 28,  93 => 27,  74 => 10,  70 => 8,  66 => 7,  61 => 6,  57 => 5,  50 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "proveedores/index.html.twig", "/srv/www/correosProduccion/templates/proveedores/index.html.twig");
    }
}
