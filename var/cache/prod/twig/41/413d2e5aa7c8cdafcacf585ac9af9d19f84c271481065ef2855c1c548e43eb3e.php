<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* envio/index.html.twig */
class __TwigTemplate_6485a3d99b7acf49467ec19a1fe17d7f541dcdbd501cf80df469c91e478d6626 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'header' => [$this, 'block_header'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "envio/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Correos - Envio";
    }

    // line 5
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "  ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
  ";
        // line 7
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("js/usuarios");
        echo "
  ";
        // line 8
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("css/grilla");
        echo "
  ";
        // line 9
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("js/envios");
        echo "
  <link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/bootstrap-switch/css/bootstrap-switch.min.css"), "html", null, true);
        echo "\" />
  <link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/fileinput/css/fileinput.min.css"), "html", null, true);
        echo "\"/>
  ";
        // line 13
        echo "  <style>
    input[type=number]::-webkit-inner-spin-button, 
    input[type=number]::-webkit-outer-spin-button { 
        -webkit-appearance: none; 
        margin: 0; 
    }

    input[type=number] { 
        -moz-appearance:textfield; 
    }
    a.active{
      background-color: #f8f9fa!important;
    }
  </style>

";
    }

    // line 30
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 31
        echo "  ";
        $this->loadTemplate("app/adminNavbar.html.twig", "envio/index.html.twig", 31)->display($context);
        // line 32
        echo "  ";
        $this->loadTemplate("app/menu.html.twig", "envio/index.html.twig", 32)->display($context);
    }

    // line 35
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 36
        echo "  <div id=\"myGrid\" style=\"height: 100%;\" class=\"ag-theme-fresh\"></div>


";
    }

    // line 41
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 42
        echo "  ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
  <script>
    var oGrid;
    var oGridError;
    var aDfColumnas = ";
        // line 46
        echo ($context["dfColumnas"] ?? null);
        echo ";
    var aGridButtons = ";
        // line 47
        echo ($context["aGridButtons"] ?? null);
        echo ";

    ApiRestURLS.listEnvios = `";
        // line 49
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_envio_json");
        echo "`;
    ApiRestURLS.listEnviosNew = `";
        // line 50
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_envio_new");
        echo "`;
    ApiRestURLS.listEnviosEdit = `";
        // line 51
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_envio_edit");
        echo "`;
    ApiRestURLS.listEnviosDelete = `";
        // line 52
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_envio_delete");
        echo "`;
    ApiRestURLS.listArchivos = `";
        // line 53
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_archivo_adjunto_listar");
        echo "`;
    ApiRestURLS.listDestinatarios = `";
        // line 54
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_envio_destinatario_index");
        echo "`;
    ApiRestURLS.previsualizar = `";
        // line 55
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_envio_vista_previa");
        echo "`;

    ApiRestURLS.listErrores = `";
        // line 57
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_envio_errores_index");
        echo "`;
    ApiRestURLS.confirmarProgramacion = `";
        // line 58
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_envio_confirmarProgramacion", ["idEnvio" => 0]);
        echo "`;
    ApiRestURLS.cancelarEnvio = `";
        // line 59
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_envio_cancelar_envio");
        echo "`;
    

  </script>
  <script type=\"text/javascript\" src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/ckeditor/ckeditor.js"), "html", null, true);
        echo "\"></script>
  <script type=\"text/javascript\" src=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/bootstrap-switch/js/bootstrap-switch.min.js"), "html", null, true);
        echo "\"></script>
  <script type=\"text/javascript\" src=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/fileinput/js/fileinput.min.js"), "html", null, true);
        echo "\"></script>
  <script type=\"text/javascript\" src=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/fileinput/js/es.js"), "html", null, true);
        echo "\"></script>
  ";
        // line 67
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("js/envios");
        echo "
  
";
    }

    public function getTemplateName()
    {
        return "envio/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  209 => 67,  205 => 66,  201 => 65,  197 => 64,  193 => 63,  186 => 59,  182 => 58,  178 => 57,  173 => 55,  169 => 54,  165 => 53,  161 => 52,  157 => 51,  153 => 50,  149 => 49,  144 => 47,  140 => 46,  132 => 42,  128 => 41,  121 => 36,  117 => 35,  112 => 32,  109 => 31,  105 => 30,  86 => 13,  82 => 11,  78 => 10,  74 => 9,  70 => 8,  66 => 7,  61 => 6,  57 => 5,  50 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "envio/index.html.twig", "/srv/www/correosProduccion/templates/envio/index.html.twig");
    }
}
