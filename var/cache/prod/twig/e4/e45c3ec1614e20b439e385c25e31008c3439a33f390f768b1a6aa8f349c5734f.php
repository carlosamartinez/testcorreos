<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* envio/listaArchivos.html.twig */
class __TwigTemplate_7f85554f6c70d0106d90e78adcd2c67e9f99fc8599906c9e4cf0dc7a925dc385 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"row\">
\t<div class=\"col-md-12\">
\t\t<h3 class=\"text-center\">Archivo de combinación CSV</h3>
\t  <table class=\"table table-striped table-sm\" style=\"font-size: 13px;\">
\t    <thead>
\t      <tr>
\t        <th>ARCHIVO</th>
\t        <th>TAMAÑO</th>
\t        <th>ACCIÓN</th>
\t      </tr>
\t    </thead>
\t    <tbody>
\t    \t<tr>
\t\t    \t";
        // line 14
        if (twig_get_attribute($this->env, $this->source, ($context["csv"] ?? null), "peso", [], "array", true, true, false, 14)) {
            // line 15
            echo "\t\t        <td>";
            echo twig_escape_filter($this->env, (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["csv"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["archivo"] ?? null) : null), "html", null, true);
            echo "</td>
\t\t        <td>";
            // line 16
            echo twig_escape_filter($this->env, (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = ($context["csv"] ?? null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["peso"] ?? null) : null), "html", null, true);
            echo " bytes </td>
\t\t        <td>
\t\t        \t<i class=\"fa fa-spinner fa-spin fa-fw margin-bottom load\"  id=\"csvload\"></i>
\t\t        \t<button type=\"button\" class=\"btn btn-sm btn-primary eliminar\" id=\"csv\" title=\"Descartar\" value=\"";
            // line 19
            echo twig_escape_filter($this->env, (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = ($context["csv"] ?? null)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["archivo"] ?? null) : null), "html", null, true);
            echo "\">X</button>
\t\t        </td>
\t\t\t\t\t";
        } else {
            // line 22
            echo "\t\t\t\t\t\t<td rowspan=\"3\">
\t\t\t\t\t\t\t";
            // line 23
            if (twig_get_attribute($this->env, $this->source, ($context["csv"] ?? null), "error", [], "array", true, true, false, 23)) {
                // line 24
                echo "\t\t\t\t\t\t\t\t";
                echo twig_escape_filter($this->env, (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = ($context["csv"] ?? null)) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["error"] ?? null) : null), "html", null, true);
                echo "
\t\t\t\t\t\t\t";
            } else {
                // line 26
                echo "\t\t\t\t\t\t\t\tNo hay CSV cargado actualmente.
\t\t\t\t\t\t\t";
            }
            // line 28
            echo "\t\t\t\t\t\t</td>
\t\t\t\t\t";
        }
        // line 30
        echo "\t\t\t  </tr>
\t\t    
\t    </tbody>
\t  </table>
\t </div>
</div>


<div class=\"row\">
\t<div class=\"col-md-12\">
\t\t<h3 class=\"text-center\">Archivos adjuntos</h3>
\t  <table class=\"table table-striped table-sm\" style=\"font-size: 13px;\">
\t    <thead>
\t      <tr>
\t        <th>ARCHIVO</th>
\t        <th>TAMAÑO</th>
\t        <th>ACCIÓN</th>
\t      </tr>
\t    </thead>
\t    <tbody>
\t    \t";
        // line 50
        $context["contador"] = 0;
        // line 51
        echo "\t    \t";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["otros"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["archivo"]) {
            // line 52
            echo "\t\t      <tr id=\"";
            echo twig_escape_filter($this->env, ($context["contador"] ?? null), "html", null, true);
            echo "tr\">
\t\t        <td>";
            // line 53
            echo twig_escape_filter($this->env, twig_slice($this->env, (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = $context["archivo"]) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4["nombre"] ?? null) : null), 0, 50), "html", null, true);
            echo "</td>
\t\t        <td>";
            // line 54
            echo twig_escape_filter($this->env, (($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = $context["archivo"]) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666["peso"] ?? null) : null), "html", null, true);
            echo " bytes </td>
\t\t        <td>
\t\t        \t<span class=\"load\" id=\"";
            // line 56
            echo twig_escape_filter($this->env, ($context["contador"] ?? null), "html", null, true);
            echo "load\"> <i class=\"fa fa-spinner fa-spin fa-fw margin-bottom\" ></i> </span>
\t\t        \t<button type=\"button\" class=\"btn btn-sm btn-primary eliminar\" id=\"";
            // line 57
            echo twig_escape_filter($this->env, ($context["contador"] ?? null), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, (($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = $context["archivo"]) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e["nombre"] ?? null) : null), "html", null, true);
            echo "\" title=\"Descartar\">X</button>
\t\t        </td>
\t\t      </tr>
\t\t      ";
            // line 60
            $context["contador"] = (($context["contador"] ?? null) + 1);
            // line 61
            echo "\t\t    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['archivo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 62
        echo "\t    </tbody>
\t    <tfooter>
\t    \t<tr>
\t    \t\t<td rowspan=\"3\">
\t    \t\t\t<b>Peso total : &nbsp;</b> ";
        // line 66
        if ((1 === twig_compare(($context["peso"] ?? null), 0))) {
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (($context["peso"] ?? null) / 1000000), 2, "."), "html", null, true);
            echo " MB ";
        } else {
            echo "0";
        }
        // line 67
        echo "\t    \t\t</td>
\t    \t</tr>
\t    </tfooter>
\t  </table>
\t </div>
</div>
<script type=\"text/javascript\">
\t\$().ready(function(){

\t\t\$('.load').hide();

\t\t\$('.eliminar').click( function(){

\t\t\t\$(this).attr('disabled',true);
\t\t\t
\t\t\tvar archivo=\$(this).val();
\t\t\tvar id=\$(this).attr('id');
\t\t\tvar opcion=( id == 'csv' )?1:2;
\t\t\t
\t\t\t\$.ajax({
\t\t\t\turl:'";
        // line 87
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_archivo_adjunto_eliminar");
        echo "',
\t\t\t\tmethod:'post',
\t\t\t\tdata:{'archivo':archivo,'opcion':opcion},
\t\t\t\tbeforeSend:function(){\t
\t\t\t\t\t\$('#'+id+'load').show();\t\t\t
\t\t\t\t},
\t\t\t\tstatusCode:{
\t\t\t\t\t200:function(){
\t\t\t\t\t\t//AlertaPersonalizable('Archivo eliminado.',1000,'information');
\t\t\t\t\t\t\$('#verArchivos').empty().html('<i class=\"fa fa-spinner fa-spin fa-3x fa-fw margin-bottom\"></i>').load('";
        // line 96
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_archivo_adjunto_listar");
        echo "');
\t\t\t\t\t},
\t\t\t\t\t500:function(){
\t\t\t\t\t\t\$(this).attr('disabled',false);
\t\t\t\t\t}
\t\t\t\t}

\t\t\t});

\t\t} );

\t});
</script>";
    }

    public function getTemplateName()
    {
        return "envio/listaArchivos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  200 => 96,  188 => 87,  166 => 67,  159 => 66,  153 => 62,  147 => 61,  145 => 60,  137 => 57,  133 => 56,  128 => 54,  124 => 53,  119 => 52,  114 => 51,  112 => 50,  90 => 30,  86 => 28,  82 => 26,  76 => 24,  74 => 23,  71 => 22,  65 => 19,  59 => 16,  54 => 15,  52 => 14,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "envio/listaArchivos.html.twig", "/srv/www/correosProduccion/templates/envio/listaArchivos.html.twig");
    }
}
