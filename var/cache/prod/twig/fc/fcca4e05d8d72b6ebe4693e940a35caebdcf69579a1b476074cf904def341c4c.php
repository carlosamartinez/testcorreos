<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* app/menu.html.twig */
class __TwigTemplate_a8f1960aa11273265633395cb16dda7203a0938733b44d8ae23787b159629d13 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "
  <!-- Main Menu HTML Code -->
  

  ";
        // line 6
        echo "  <div class='wsmenucontent overlapblackbg pm_buttoncolor wa-primary'></div>
  <div class='wsmenuexpandermain slideRight'>
    <div style='background-color:red;'>
      <a id='navToggle' class='animated-arrow slideLeft'>
      <!--<img src=\"public/images/x-icon.png\">-->
        <span></span>
      </a>
    </div>
    <span class='callusicon text-center'></span>
  </div>

  ";
        // line 18
        echo "  <nav class='wsmenu slideLeft clearfix'>
    <ul class='mobile-sub wsmenu-list clearfix'>

      <!-- Logo -->
      <li class='border-white border-bottom'>
        <div class='logoforwhite'>
          <img src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/logo-correos.png"), "html", null, true);
        echo "\" style=\"width:200px;height:auto\" alt='Logo'>
        </div>
      </li>

      ";
        // line 28
        $context["menuPermisos"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "session", [], "any", false, false, false, 28), "get", [0 => "menuPermisos"], "method", false, false, false, 28);
        // line 29
        echo "
      ";
        // line 30
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["menuPermisos"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["menu"]) {
            // line 31
            echo "        ";
            if (twig_get_attribute($this->env, $this->source, $context["menu"], 0, [], "array", true, true, false, 31)) {
                // line 32
                echo "
          <!-- Elementos Sub Menu -->
          <li class='border-bottom bg-primary'>
            <a class='list-group-item' href='#'>
              <i class='";
                // line 36
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = $context["menu"]) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[0] ?? null) : null), "icono", [], "any", false, false, false, 36), "html", null, true);
                echo "'></i>&nbsp;<b class='text-white'>";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = $context["menu"]) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144[0] ?? null) : null), "label", [], "any", false, false, false, 36), "html", null, true);
                echo "</b>
            </a>
            <ul class='wsmenu-submenu'>
              <!-- Items Sub Menu -->
              ";
                // line 40
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["menu"]);
                foreach ($context['_seq'] as $context["keyItemMenu"] => $context["itemSubMenu"]) {
                    // line 41
                    echo "                ";
                    if ((0 !== twig_compare($context["keyItemMenu"], 0))) {
                        // line 42
                        echo "                  ";
                        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["itemSubMenu"], "permisos", [], "any", false, true, false, 42), "ver", [], "array", true, true, false, 42)) {
                            // line 43
                            echo "                    <li>
                      <a class='list-group-item font-weight-bold text-dark' href=\"";
                            // line 44
                            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, $context["itemSubMenu"], "pathName", [], "any", false, false, false, 44));
                            echo "\">
                        <i class=\"";
                            // line 45
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["itemSubMenu"], "icono", [], "any", false, false, false, 45), "html", null, true);
                            echo "\"></i>&nbsp;";
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["itemSubMenu"], "label", [], "any", false, false, false, 45), "html", null, true);
                            echo "
                      </a>
                    </li>
                  ";
                        }
                        // line 49
                        echo "                ";
                    }
                    // line 50
                    echo "              ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['keyItemMenu'], $context['itemSubMenu'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 51
                echo "            </ul>
          </li>

        ";
            } else {
                // line 55
                echo "          ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["menu"]);
                foreach ($context['_seq'] as $context["_key"] => $context["menuItem"]) {
                    // line 56
                    echo "            ";
                    if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["menuItem"], "permisos", [], "any", false, true, false, 56), "ver", [], "any", true, true, false, 56) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["menuItem"], "permisos", [], "any", false, false, false, 56), "ver", [], "any", false, false, false, 56), "permiso", [], "any", false, false, false, 56), 1)))) {
                        // line 57
                        echo "              <li class='border-bottom bg-primary'>
              ";
                        // line 59
                        echo "                <a class='list-group-item' href=\"";
                        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, $context["menuItem"], "pathName", [], "any", false, false, false, 59));
                        echo "\">
                  <i class=\"";
                        // line 60
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["menuItem"], "icono", [], "any", false, false, false, 60), "html", null, true);
                        echo "\"></i> <b style=\"color:#4F4F4F\">";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["menuItem"], "label", [], "any", false, false, false, 60), "html", null, true);
                        echo "</b>
                </a>
              </li>
            ";
                    }
                    // line 64
                    echo "
          ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menuItem'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 66
                echo "        ";
            }
            // line 67
            echo "      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 68
        echo "

    </ul>
        <div class='logoforwhite'>
          <img src=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/coopi.png"), "html", null, true);
        echo "\" alt='Logo' style=\"position: absolute; bottom: 0; left: 0\">
        </div>
      
  </nav>
";
    }

    public function getTemplateName()
    {
        return "app/menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  186 => 72,  180 => 68,  174 => 67,  171 => 66,  164 => 64,  155 => 60,  150 => 59,  147 => 57,  144 => 56,  139 => 55,  133 => 51,  127 => 50,  124 => 49,  115 => 45,  111 => 44,  108 => 43,  105 => 42,  102 => 41,  98 => 40,  89 => 36,  83 => 32,  80 => 31,  76 => 30,  73 => 29,  71 => 28,  64 => 24,  56 => 18,  43 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "app/menu.html.twig", "/srv/www/correosProduccion/templates/app/menu.html.twig");
    }
}
