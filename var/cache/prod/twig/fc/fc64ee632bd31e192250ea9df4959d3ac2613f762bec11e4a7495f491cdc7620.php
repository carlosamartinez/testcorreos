<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* app/adminNavbar.html.twig */
class __TwigTemplate_7b1199628957768076cff730e595fd7e1b1ffb834581571aa757b86d274d5292 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "
  <nav class=\"navbar navbar-light bg-light\" style=\"padding: 0rem 0rem\">
    <div class=\"d-flex justify-content-between align-items-baseline\" style=\"width:100vw\">
        <div class=\"p-2 bd-highlight\" ></div>
        <div class=\"p-2 bd-highlight\" ></div>
        <div class=\"p-2 bd-highlight\" ></div>
        <div class=\"p-2 bd-highlight\" >
                <img  src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/User_icon.png"), "html", null, true);
        echo "\" style=\"width:25px;heigth:25px;\">

            <span class=\"text-success\" style=\"\" >
            
                
                <b>En línea:</b>
                <span class=\"text-primary\" style=\"color:#02387F !important;\">
                    <b>";
        // line 15
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "session", [], "any", false, false, false, 15), "get", [0 => "nombre"], "method", false, false, false, 15), "html", null, true);
        echo "</b>
                </span>
            </span> 
            <span class=\"text-danger text-center ml-auto mr-auto\">
        </div>

        
        <div class=\"ml-auto p-2 bd-highlight\">

            <b>
                ";
        // line 25
        if ((array_key_exists("moduloCabecera", $context) && (1 === twig_compare(twig_length_filter($this->env, ($context["moduloCabecera"] ?? null)), 0)))) {
            echo twig_escape_filter($this->env, ($context["moduloCabecera"] ?? null), "html", null, true);
        }
        // line 26
        echo "                ";
        if ((array_key_exists("tituloCabecera", $context) && (1 === twig_compare(twig_length_filter($this->env, ($context["tituloCabecera"] ?? null)), 0)))) {
            echo "<span class='text-primary'>: ";
            echo twig_escape_filter($this->env, ($context["tituloCabecera"] ?? null), "html", null, true);
        }
        echo "</span>
            </b>
            </span>

            ";
        // line 31
        echo "            <button id=\"btnCerrarSesion\" class=\"btn btn-outline-danger my-1 btn-sm mr-2\" type=\"button\">
            <img src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/salir.png"), "html", null, true);
        echo "\" style=\"width:30px;heigth:auto\" >
            
            </button>
            <button id=\"btnCambiarClave\" class=\"btn btn-outline-warning my-1 btn-sm mr-2\" type=\"button\">
            <img src=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/pass.png"), "html", null, true);
        echo "\" style=\"width:30px;heigth:auto\" >     
            </button>
        </div>

    </div>

  </nav>




<!-- Cambiar Clave modal -->
";
        // line 75
        echo "  ";
        // line 84
        echo "
  <script>
    var _urlCierreSession          = `";
        // line 86
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_logout");
        echo "`; 
    var _urlCambiarClave           = `";
        // line 87
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("password_reset_in_app");
        echo "`;
    var _urlVerificarClaveAntigua  = `";
        // line 88
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("password_validate_duplicate_in_app");
        echo "`;
    var _urlCambiarClaveIndex = `";
        // line 89
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("password_reset_in_app_index");
        echo "`; 
    var user = `";
        // line 90
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "session", [], "any", false, false, false, 90), "get", [0 => "id"], "method", false, false, false, 90), "html", null, true);
        echo "`;
  </script>
";
    }

    public function getTemplateName()
    {
        return "app/adminNavbar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  131 => 90,  127 => 89,  123 => 88,  119 => 87,  115 => 86,  111 => 84,  109 => 75,  94 => 36,  87 => 32,  84 => 31,  73 => 26,  69 => 25,  56 => 15,  46 => 8,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "app/adminNavbar.html.twig", "/srv/www/correosProduccion/templates/app/adminNavbar.html.twig");
    }
}
