<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* emails/confirmAccount.html.twig */
class __TwigTemplate_2b3fe96e9a026d76643cdff7f6ff739efc5d3414dc0e1066c790b50ada5fa466 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<style>
    .card{
        width:450px;
        border:1px silver solid;
    }
    .textSmall{
        font-size: 12px;
        color:#8B1602;
        text-align: center;
        padding:20px;
    }
    .detalle{
        color:#0070BA;
        text-align: justify;
        texto-justify: inter-palabra;
    }
    .user{
        color:#023A78;
        text-decoration: underline wavy red;
        min-width:200px;
    }
    .userDato{
        color:#023A78;
        font-weight:bold;
    }
    .link{
        color:#E56605;
    }
</style>
<div class=\"card\" style=\"justify-content:center;text-align:center;margin: 0 auto;padding:40px\">
  <h1>Aplicación de envío de correos.</h1>
  <div class=\"card-header\" style=\"width:70%;margin: 0 auto;justify-content;center;text-align:center\">
  <img class='mb-4' src=\"";
        // line 33
        echo twig_escape_filter($this->env, ($context["headerImg"] ?? null), "html", null, true);
        echo "\" alt=\"logo Correos\" width=\"300\">
    <h3>¡Hola ";
        // line 34
        echo twig_escape_filter($this->env, ($context["nombre"] ?? null), "html", null, true);
        echo "!</h3>
  </div>
  <div class=\"card-body\" style=\"width:80%;margin: 0 auto;justify-content;center;text-align:center\">
  <p class=\"detalle\">Se ha registrado en la aplicación ENVÍO DE CORREOS COOPIDROGAS, a continuación los datos de acceso.
    <div><span class=\"user\">Usuario: </span><span class=\"userDato\">";
        // line 38
        echo twig_escape_filter($this->env, ($context["usuario"] ?? null), "html", null, true);
        echo "</span></div>
    <div><span class=\"user\">Contraseña: </span><span class=\"userDato\">";
        // line 39
        echo twig_escape_filter($this->env, ($context["pass"] ?? null), "html", null, true);
        echo "</span></div>
    <div><a href=\"";
        // line 40
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("admin_login");
        echo "\" class=\"link\">Haga clic aquí para ingresar a la aplicación.</a></div>
    <div class=\"textSmall\">Si usted desconoce está información haga caso omiso a este correo.</div>
  </p>
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "emails/confirmAccount.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 40,  86 => 39,  82 => 38,  75 => 34,  71 => 33,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "emails/confirmAccount.html.twig", "/srv/www/correosProduccion/templates/emails/confirmAccount.html.twig");
    }
}
