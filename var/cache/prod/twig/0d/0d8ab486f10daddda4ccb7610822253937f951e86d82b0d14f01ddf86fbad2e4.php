<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* reset_password/email.html.twig */
class __TwigTemplate_e1e205da82b9d4a4b540b9c7896e8fed08e25eb432d7bd72d67f9fd97e5251bc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<style>
    .card{
        width:450px;
        border:1px silver solid;
        -moz-border-radius: 9px;
\t\t -webkit-border-radius:9px;
\t\t padding: 10px;
    }
    .textSmall{
        font-size: 10px;
        color:#8B1602;
        text-align: center;
    }
    .detalle{
        color:#0070BA;
        text-align: justify;
        texto-justify: inter-palabra;
    }
    .link{
        color:#E56605;
    }
</style>
<div class=\"card\"  style=\"justify-content:center;text-align:center;margin: 0 auto;padding:40px\">
    <h1>Aplicación de envío de correos.</h1>
  <div class=\"card-header\" style=\"width:70%;margin: 0 auto;justify-content;center;text-align:center\">
  <img class='mb-4' src=\"";
        // line 26
        echo twig_escape_filter($this->env, ($context["headerImg"] ?? null), "html", null, true);
        echo "\" alt=\"logo Correos\" width=\"300\">
    <h3>¡Hola ";
        // line 27
        echo twig_escape_filter($this->env, ($context["nombre"] ?? null), "html", null, true);
        echo "!</h3>
  </div>
  <div class=\"card-body\" style=\"width:80%;margin: 0 auto;justify-content;center;text-align:center\">
   <p class=\"detalle\">Se ha solicitado recuperar su clave para la aplicación ENVÍO DE CORREOS, por favor haga clic <a style=\"width:80%;justify-content;margin: 0 auto;center;text-align:center\" href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("reset_password", ["token" => twig_get_attribute($this->env, $this->source, ($context["resetToken"] ?? null), "token", [], "any", false, false, false, 30)]), "html", null, true);
        echo "\" class=\"link\">AQUÍ</a> y siga las instrucciones.<br><br><div class=\"textSmall\">Este link expirará en ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, ($context["tokenLifetime"] ?? null), "g"), "html", null, true);
        echo " horas.</div></p>
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "reset_password/email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 30,  68 => 27,  64 => 26,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "reset_password/email.html.twig", "/srv/www/correosProduccion/templates/reset_password/email.html.twig");
    }
}
