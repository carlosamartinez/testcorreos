<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* envioDestinatario/email_masivo.html.twig */
class __TwigTemplate_fb01b6281772d2bf1287a683adb6ec171b643ed9ae83cf8c65c5546d20e6f157 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<style>
\t\t.info{color:#2D7091;}.data{color:#DD0055;font-size:12px;font-family:Consolas,monospace,serif;font-weight:bold;}.uk-container{-moz-box-sizing:border-box;max-width:980px;padding:0 25px;}.uk-container-center{margin-left: auto;margin-right: auto;}hr{background-color: #FF0000;color: #FF0000;height: 3px;margin-top: 4px;position: relative;}.uk-table{width:100%;}.tdLogo{width: 100px;}.textoCentrado{text-align:center;}.uk-article-title {font-size: 36px;font-weight: normal;line-height: 42px;text-transform: none;}.uk-article-meta {color: #999999;font-size: 12px;line-height: 18px;}
\t\t.tableDes {background-color:transparent;border-collapse:collapse;border-spacing:0;max-width:100%;}
\t\t        .tableDes thead th {font-weight:bold; background-color:#DFDFDF;border:1px #9e9e9e solid;}
\t\t        .tableDes tbody td {border:1px #9e9e9e solid;}
\t\t.tablaDatos {margin:0px;padding:0px;width:100%;border:1px solid #7f7f7f;-moz-border-radius-bottomleft:0px;-webkit-border-bottom-left-radius:0px;border-bottom-left-radius:0px;-moz-border-radius-bottomright:0px;-webkit-border-bottom-right-radius:0px;border-bottom-right-radius:0px;-moz-border-radius-topright:0px;-webkit-border-top-right-radius:0px;border-top-right-radius:0px;-moz-border-radius-topleft:0px;-webkit-border-top-left-radius:0px;border-top-left-radius:0px;}.tablaDatos table{border-collapse:collapse;border-spacing:0;width:100%;height:100%;margin:0px;padding:0px;}.tablaDatos tr:last-child td:last-child {-moz-border-radius-bottomright:0px;-webkit-border-bottom-right-radius:0px;border-bottom-right-radius:0px;}.tablaDatos table tr:first-child td:first-child {-moz-border-radius-topleft:0px;-webkit-border-top-left-radius:0px;border-top-left-radius:0px;.tablaDatos table tr:first-child td:last-child {-moz-border-radius-topright:0px;-webkit-border-top-right-radius:0px;border-top-right-radius:0px;}.tablaDatos tr:last-child td:first-child{-moz-border-radius-bottomleft:0px;-webkit-border-bottom-left-radius:0px;border-bottom-left-radius:0px;}.tablaDatos tr:hover td{}}.tablaDatos tr:nth-child(odd){background-color:#aad4ff;}.tablaDatos tr:nth-child(even){background-color:#ffffff;}.tablaDatos td{vertical-align:middle;border:1px solid #7f7f7f;border-width:0px 1px 1px 0px;text-align:left;padding:7px;font-size:10px;font-family:Arial;font-weight:normal;color:#000000;}.tablaDatos tr:last-child td{border-width:0px 1px 0px 0px;}.tablaDatos tr td:last-child{border-width:0px 0px 1px 0px;}.tablaDatos tr:last-child td:last-child{min-width:100px;border-width:0px 0px 0px 0px;}.tablaDatos tr:first-child td{min-width:100px;background:-o-linear-gradient(bottom,#005fbf 5%,#005fbf 100%);background:-webkit-gradient(linear,left top,left bottom,color-stop(0.05, #005fbf),color-stop(1,#005fbf));background:-moz-linear-gradient( center top, #005fbf 5%, #005fbf 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=\"#005fbf\",endColorstr=\"#005fbf\");background:-o-linear-gradient(top,#005fbf,005fbf);background-color:#005fbf;border:0px solid #7f7f7f;text-align:center;border-width:0px 0px 1px 1px;font-size:14px;font-family:Arial;font-weight:bold;color:#ffffff;}.tablaDatos tr:first-child:hover td{min-width:100px;background:-o-linear-gradient(bottom,#005fbf 5%,#005fbf 100%);background:-webkit-gradient(linear,left top,left bottom,color-stop(0.05,#005fbf),color-stop(1,#005fbf));background:-moz-linear-gradient(center top,#005fbf 5%,#005fbf 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=\"#005fbf\",endColorstr=\"#005fbf\");background:-o-linear-gradient(top,#005fbf,005fbf);background-color:#005fbf;}.tablaDatos tr:first-child td:first-child{min-width:100px;border-width:0px 0px 1px 0px;}.tablaDatos tr:first-child td:last-child{border-width:0px 0px 1px 1px;}
</style>
<div class=\"uk-container uk-container-center\">
    <div>";
        // line 9
        echo ($context["mensaje"] ?? null);
        echo "</div>
</div>
";
        // line 11
        if ((0 !== twig_compare(($context["urlLectura"] ?? null), ""))) {
            // line 12
            echo "<img width=\"1\" height=\"1\" src=\"";
            echo twig_escape_filter($this->env, ($context["urlLectura"] ?? null), "html", null, true);
            echo "\">
";
        }
        // line 13
        echo " ";
    }

    public function getTemplateName()
    {
        return "envioDestinatario/email_masivo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 13,  54 => 12,  52 => 11,  47 => 9,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "envioDestinatario/email_masivo.html.twig", "/srv/www/correosProduccion/templates/envioDestinatario/email_masivo.html.twig");
    }
}
