<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* proveedores/mdlCrud.html.twig */
class __TwigTemplate_6d3676855651328c4b8ec723a4b81cef7522ce1a87aac56409187ba521041ee3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((array_key_exists("frm_msj", $context) && (0 !== twig_compare(($context["frm_msj"] ?? null), null)))) {
            // line 2
            echo "    ";
            echo twig_escape_filter($this->env, ($context["frm_msj"] ?? null), "html", null, true);
            echo "
";
        }
        // line 4
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
        echo "

<form id=\"formProveedor\" autocomplete=\"off\">
   
   <div class=\"row\">

      <div class=\"col-12 col-md-6\">
         <div class=\"form-group font-weight-bold\">
            ";
        // line 12
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "nit", [], "any", false, false, false, 12), 'label');
        echo "&nbsp;<i class=\"fa fa-asterisk text-danger\" aria-hidden=\"true\"></i> :
            ";
        // line 13
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "nit", [], "any", false, false, false, 13), 'widget');
        echo "
         </div>
      </div>
      <div class=\"col-12 col-md-6\">
         <div class=\"form-group font-weight-bold\">
            ";
        // line 18
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "codigo", [], "any", false, false, false, 18), 'label');
        echo "&nbsp;<i class=\"fa fa-asterisk text-danger\" aria-hidden=\"true\"></i> :
            ";
        // line 19
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "codigo", [], "any", false, false, false, 19), 'widget');
        echo "
         </div>
      </div>

      <div class=\"col-12 col-md-6\">
         <div class=\"form-group font-weight-bold\">
            ";
        // line 25
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "nombre", [], "any", false, false, false, 25), 'label');
        echo "&nbsp;<i class=\"fa fa-asterisk text-danger\" aria-hidden=\"true\"></i> :
            ";
        // line 26
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "nombre", [], "any", false, false, false, 26), 'widget');
        echo "
         </div>
      </div>
      <div class=\"col-12 col-md-6\">
         <div class=\"form-group font-weight-bold\">
            ";
        // line 31
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "representanteLegal", [], "any", false, false, false, 31), 'label');
        echo "&nbsp;<i class=\"fa fa-asterisk text-danger\" aria-hidden=\"true\"></i> :
            ";
        // line 32
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "representanteLegal", [], "any", false, false, false, 32), 'widget');
        echo "
         </div>
      </div>
      <div class=\"col-12 col-md-6\">
         <div class=\"form-group font-weight-bold\">
            ";
        // line 37
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "emailRepresentanteLegal", [], "any", false, false, false, 37), 'label');
        echo "&nbsp;<i class=\"fa fa-asterisk text-danger\" aria-hidden=\"true\"></i> :
            ";
        // line 38
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "emailRepresentanteLegal", [], "any", false, false, false, 38), 'widget');
        echo "
         </div>
      </div>
      <div class=\"col-12 col-md-6\">
         <div class=\"form-group font-weight-bold\">
            ";
        // line 43
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "telefonoRepresentanteLegal", [], "any", false, false, false, 43), 'label');
        echo "&nbsp;<i class=\"fa fa-asterisk text-danger\" aria-hidden=\"true\"></i> :
            ";
        // line 44
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "telefonoRepresentanteLegal", [], "any", false, false, false, 44), 'widget');
        echo "
         </div>
      </div>

      <div class=\"col-12 text-center\">
         <div class=\"form-group font-weight-bold m-2 border-top pt-3\">
            ";
        // line 50
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "save", [], "any", false, false, false, 50), 'widget', ["label" => "Guardar"]);
        echo "
            ";
        // line 51
        if ((0 === twig_compare(($context["accion"] ?? null), "editar"))) {
            // line 52
            echo "              <input type='hidden' name='id_proveedor' value='";
            echo twig_escape_filter($this->env, ($context["idProveedor"] ?? null), "html", null, true);
            echo "' />
            ";
        }
        // line 54
        echo "         </div>
      </div>
   </div>

</form>

<script type=\"text/javascript\">

  var accion = \"";
        // line 62
        echo twig_escape_filter($this->env, ($context["accion"] ?? null), "html", null, true);
        echo "\";
  ApiRestURLS.mdlCrud = ApiRestURLS.proveedorNew;

  if( accion == 'editar' ){
    ApiRestURLS.mdlCrud = ApiRestURLS.proveedorEdit;
  }
</script>";
    }

    public function getTemplateName()
    {
        return "proveedores/mdlCrud.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  152 => 62,  142 => 54,  136 => 52,  134 => 51,  130 => 50,  121 => 44,  117 => 43,  109 => 38,  105 => 37,  97 => 32,  93 => 31,  85 => 26,  81 => 25,  72 => 19,  68 => 18,  60 => 13,  56 => 12,  45 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "proveedores/mdlCrud.html.twig", "/srv/www/correosProduccion/templates/proveedores/mdlCrud.html.twig");
    }
}
