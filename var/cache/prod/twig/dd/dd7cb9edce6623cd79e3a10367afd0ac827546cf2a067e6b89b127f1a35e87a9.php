<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* app/resetPassword.html.twig */
class __TwigTemplate_22f37b1a7da6269351837ace28e0c1fe6f397742dd27a0ae33cc9bb0459eda45 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "

 <div class=\"d-flex justify-content-center\"> 
    <div class=\"p-2 bd-highlight\">
        <i class=\"fas fa-exclamation-circle fa-3x btn-outline-danger\"></i>
    </div>
    <div class=\"p-2 bd-highlight\">
    <p>Al cambiar tu contraseña seras redirigido al ingreso de usuario</p>
    </div>
</div>

 <div class=\"input-group\">
    <input type=\"password\" class=\"form-control\" id=\"PreviousPassword\" placeholder=\"Ingresa tu contraseña Actual\" required>
    <div class=\"input-group-append passoriginal\">
        <button id=\"seeoldpass\" class=\"input-group-text passoriginal\"><i class=\"fas fa-eye\"></i></button>
    </div>
</div><br>
<div class=\"input-group\">
    <input type=\"password\" class=\"form-control\" id=\"InputPassword1\" placeholder=\"Ingresa tu contraseña\" required>
    <div class=\"input-group-append pass1\">
        <button id=\"seepass1\" class=\"input-group-text\"><i class=\"fas fa-eye\"></i></button>
    </div>
</div><br> 
<div class=\"input-group\">
    <input type=\"password\" class=\"form-control \" id=\"InputPassword2\" placeholder=\"Repite tu contraseña\" required>
    <div class=\"input-group-append\">
        <button id=\"seepass2\" class=\"input-group-text\"><i class=\"fas fa-eye\"></i></button>
    </div>
</div>

<div class=\"d-flex justify-content-between\">
    <div class=\"p-2 bd-highlight\">
        <button class=\"btn btn-primary \" id=\"CambiarClave\">Cambiar Contraseña</button>
    </div> 
    <div class=\"p-2 bd-highlight\">
        <button class=\"btn btn-danger\" data-dismiss=\"modal\" aria-label=\"Close\">Cancelar</button>
    </div> 
</div>
";
    }

    public function getTemplateName()
    {
        return "app/resetPassword.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "app/resetPassword.html.twig", "/srv/www/correosProduccion/templates/app/resetPassword.html.twig");
    }
}
