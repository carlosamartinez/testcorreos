<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* usuarios/mdlCrud.html.twig */
class __TwigTemplate_01f44c81cd1262372b4a7d7dcec76f5358a4705041a6e8a90b98742bac329cc7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((array_key_exists("frm_msj", $context) && (0 !== twig_compare(($context["frm_msj"] ?? null), null)))) {
            // line 2
            echo "    ";
            echo twig_escape_filter($this->env, ($context["frm_msj"] ?? null), "html", null, true);
            echo "
";
        }
        // line 4
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
        echo "

<form id=\"formUser\" autocomplete=\"off\">
   
   <div class=\"row\">

      <div class=\"col-12 col-md-6\">
         <div class=\"form-group font-weight-bold\">
            ";
        // line 12
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "usuario", [], "any", false, false, false, 12), 'label');
        echo "&nbsp;<i class=\"fa fa-asterisk text-danger\" aria-hidden=\"true\"></i> :
            ";
        // line 13
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "usuario", [], "any", false, false, false, 13), 'widget');
        echo "
         </div>
      </div>
      <div class=\"col-12 col-md-6\">
         <div class=\"form-group font-weight-bold\">
            ";
        // line 18
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "nombre", [], "any", false, false, false, 18), 'label');
        echo "&nbsp;<i class=\"fa fa-asterisk text-danger\" aria-hidden=\"true\"></i> :
            ";
        // line 19
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "nombre", [], "any", false, false, false, 19), 'widget');
        echo "
         </div>
      </div>


      <div class=\"col-12 col-md-6\">
         <div class=\"form-group font-weight-bold\">
            ";
        // line 26
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "email", [], "any", false, false, false, 26), 'label');
        echo "&nbsp;<i class=\"fa fa-asterisk text-danger\" aria-hidden=\"true\"></i> :
            ";
        // line 27
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "email", [], "any", false, false, false, 27), 'widget');
        echo "
         </div>
      </div>

      <input type=\"hidden\" name=\"user_claveEmail\" id=\"user_claveEmail\" value=''>

      <input type=\"hidden\" id=\"user_rol\" name=\"user_rol\" value=\"admin\">

      <div class=\"col-12 col-md-6\">       
        <div class=\"form-group font-weight-bold\">
          ";
        // line 37
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "estado", [], "any", false, false, false, 37), 'label');
        echo "&nbsp;<i class=\"fa fa-asterisk text-danger\" aria-hidden=\"true\"></i> :
          ";
        // line 38
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "estado", [], "any", false, false, false, 38), 'widget');
        echo "
        </div>
      </div>

      <div class=\"col-12 col-md-6\">
         <div class=\"form-group font-weight-bold\">
            ";
        // line 44
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "tipoUsuario", [], "any", false, false, false, 44), 'label');
        echo "&nbsp;<i class=\"fa fa-asterisk text-danger\" aria-hidden=\"true\"></i> :
            ";
        // line 45
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "tipoUsuario", [], "any", false, false, false, 45), 'widget');
        echo "
         </div>
      </div>
      
      <div class=\"col-12 text-center\">
         <div class=\"form-group font-weight-bold m-2 border-top pt-3\">
            ";
        // line 51
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "save", [], "any", false, false, false, 51), 'widget', ["label" => "Guardar"]);
        echo "
            ";
        // line 52
        if ((0 === twig_compare(($context["accion"] ?? null), "editar"))) {
            // line 53
            echo "              <input type='hidden' name='id_user' value='";
            echo twig_escape_filter($this->env, ($context["idUser"] ?? null), "html", null, true);
            echo "' />
              <input type='hidden' name='usuario' value='";
            // line 54
            echo twig_escape_filter($this->env, ($context["usuario"] ?? null), "html", null, true);
            echo "' />
            ";
        }
        // line 56
        echo "         </div>
      </div>
   </div>

</form>

<script type=\"text/javascript\">

  var accion = \"";
        // line 64
        echo twig_escape_filter($this->env, ($context["accion"] ?? null), "html", null, true);
        echo "\";
  ApiRestURLS.mdlCrud = ApiRestURLS.userNew;

  if( accion == 'editar' ){
    ApiRestURLS.mdlCrud = ApiRestURLS.userEdit;
  }
</script>";
    }

    public function getTemplateName()
    {
        return "usuarios/mdlCrud.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 64,  141 => 56,  136 => 54,  131 => 53,  129 => 52,  125 => 51,  116 => 45,  112 => 44,  103 => 38,  99 => 37,  86 => 27,  82 => 26,  72 => 19,  68 => 18,  60 => 13,  56 => 12,  45 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "usuarios/mdlCrud.html.twig", "/srv/www/correosProduccion/templates/usuarios/mdlCrud.html.twig");
    }
}
