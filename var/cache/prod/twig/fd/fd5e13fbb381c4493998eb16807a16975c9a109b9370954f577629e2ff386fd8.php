<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* reset_password/reset.html.twig */
class __TwigTemplate_dd5d1351402c6b569e9a629a760f3b6bf2ae94c4ec7a04967b5d54b125d649a8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'bodylogin' => [$this, 'block_bodylogin'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "baseLogin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("baseLogin.html.twig", "reset_password/reset.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Correos - Cambia Tu Contraseña";
    }

    // line 5
    public function block_bodylogin($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "<div style=\"justify-content;center;text-align:center\">
<img class='mb-4' src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/logo-correos.png"), "html", null, true);
        echo "\" alt=\"logo Correos\" width=\"300\">
<p class=\"h3\">Cambio De Contraseña</p><hr><br>
\t<div class=\"row\">
\t\t<div class=\"col-4\"></div>
        <div class=\"col-4\">

        \t<div class=\"card\">
\t\t\t\t <div class=\"card-body\">
\t\t\t\t    ";
        // line 15
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["resetForm"] ?? null), 'form_start');
        echo "
\t\t        \t";
        // line 16
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["resetForm"] ?? null), "plainPassword", [], "any", false, false, false, 16), 'row');
        echo "
\t\t            <input type=\"hidden\" id=\"token\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, ($context["token"] ?? null), "html", null, true);
        echo "\">
\t\t\t        <button class=\"btn btn-primary\" id=\"cambiarContrasena\">Cambiar Contraseña</button>
\t\t\t        <button type=\"button\" id=\"btn-iniciarses\" class=\"btn btn-secondary\">Iniciar Sesión</button>
\t\t\t    \t";
        // line 20
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["resetForm"] ?? null), 'form_end');
        echo "
\t\t\t\t </div>
\t\t\t</div>

        </div>
        <div class=\"col-4\"></div>
\t</div>

</div>
";
    }

    // line 30
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 31
        echo "<script>
 var forgotPassword = `";
        // line 32
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("reset_password_request");
        echo "`;
</script>
  ";
        // line 34
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
  ";
        // line 35
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("js/usuarios/changepassword");
        echo "
";
    }

    public function getTemplateName()
    {
        return "reset_password/reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 35,  113 => 34,  108 => 32,  105 => 31,  101 => 30,  87 => 20,  81 => 17,  77 => 16,  73 => 15,  62 => 7,  59 => 6,  55 => 5,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "reset_password/reset.html.twig", "/srv/www/correosProduccion/templates/reset_password/reset.html.twig");
    }
}
