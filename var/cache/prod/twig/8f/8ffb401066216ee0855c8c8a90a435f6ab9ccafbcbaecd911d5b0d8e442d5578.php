<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* grupos/destinatarios.html.twig */
class __TwigTemplate_fe8bf513b0dcd2a83fb67e70a6926cbe2d2f9b955090aa608855219a99242600 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'header' => [$this, 'block_header'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "grupos/destinatarios.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Grupos Destinatarios";
    }

    // line 5
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "  ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
  ";
        // line 7
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("css/grilla");
        echo "

  <link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/fileinput/css/fileinput.min.css"), "html", null, true);
        echo "\"/>

  <style type=\"text/css\">
    .asignado-cell{
      display: flex;
      align-items: center;
      flex-direction: column;
    }
    .asignado-btn{
      color: blue;
      background-color: transparent;
      border: none;
      height: 25px;
      width: 25px;
      display: flex;
      align-items: center;
      border-radius: 5px;
    }
    .asignado-btn:hover{
      background-color: #F3F4F6;
    }

    html,body{
      width: 100%;
      height: 100%;
    }
  </style>
";
    }

    // line 39
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 40
        echo "  ";
        $this->loadTemplate("app/adminNavbar.html.twig", "grupos/destinatarios.html.twig", 40)->display($context);
        // line 41
        echo "  ";
        $this->loadTemplate("app/menu.html.twig", "grupos/destinatarios.html.twig", 41)->display($context);
        // line 42
        echo "    
  <nav aria-label=\"breadcrumb\" style =\"margin-bottom: -1rem; !important; font-size: 13px;\">
  <ol class=\"breadcrumb\">
    <li class=\"breadcrumb-item\"><a href=\"/admin/grupos\">Grupos</a></li>
    <li class=\"active\"> / Tipo [";
        // line 46
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["infoGrupo"] ?? null), "clasificacion", [], "any", false, false, false, 46), "html", null, true);
        echo "] / ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["infoGrupo"] ?? null), "nombre", [], "any", false, false, false, 46), "html", null, true);
        echo "</li>
  </ol>
</nav>
";
    }

    // line 51
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 52
        echo "
";
        // line 53
        if ((0 === twig_compare(($context["tipoGrilla"] ?? null), 2))) {
            // line 54
            echo "  <div id=\"myGridProveedores\" style=\"height: 100%;\" class=\"ag-theme-fresh\"></div>
";
        } else {
            // line 56
            echo "  <div id=\"myGridAsociados\" style=\"height: 100%;\" class=\"ag-theme-fresh\"></div>
";
        }
        // line 57
        echo " 

  <div class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" id=\"modalGlobalContactos\" style=\"z-index: 1060!important;\">
    <div class=\"modal-dialog modal-lg\">
      <div class=\"modal-content\">
        <div class=\"text-center modal-header text-info\">
          <h6 class=\"modal-title font-weight-bold\" id=\"tituloModalGlobalContactos\">...</h6>
          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
            <span aria-hidden=\"true\">&times;</span>
          </button>
        </div>
        <div class=\"modal-body\" id=\"contenidoModalGlobalContactos\"></div>
      </div>
    </div>
  </div>
";
    }

    // line 74
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 75
        echo "  ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "

  ";
        // line 77
        $context["packages"] = [0 => 1, 1 => 2, 2 => 3, 3 => 4];
        // line 78
        echo "    <script type=\"text/javascript\">
      const idGrupo = ";
        // line 79
        echo twig_escape_filter($this->env, ($context["idGrupo"] ?? null), "html", null, true);
        echo ";
      const tipoGrupo = ";
        // line 80
        echo twig_escape_filter($this->env, ($context["tipoGrupo"] ?? null), "html", null, true);
        echo ";
      const tipoGrilla = ";
        // line 81
        echo twig_escape_filter($this->env, ($context["tipoGrilla"] ?? null), "html", null, true);
        echo ";
      const infoAsociados = ";
        // line 82
        echo json_encode(($context["infoAsociados"] ?? null));
        echo ";
      const infoProveedores = ";
        // line 83
        echo json_encode(($context["infoProveedores"] ?? null));
        echo ";

      ApiRestURLS.agregarAsociados = `";
        // line 85
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_destinatarios_grupo_agregar_asociados");
        echo "`;
      ApiRestURLS.eliminarAsociados = `";
        // line 86
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_destinatarios_grupo_eliminar_asociados");
        echo "`;
      ApiRestURLS.cargarAsociados = `";
        // line 87
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_destinatarios_grupo_cargar_asociados");
        echo "`;
      
      ApiRestURLS.agregarAsociado = `";
        // line 89
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_destinatarios_grupo_agregar_asociado");
        echo "`;
      ApiRestURLS.cargarAsociadosxls = `";
        // line 90
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_destinatarios_grupo_cargar_asociados");
        echo "`;
      ApiRestURLS.agregarAsociadosListado = `";
        // line 91
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_destinatarios_grupo_agregar_asociados_listado");
        echo "`;
      
      ApiRestURLS.descargarFormato = `";
        // line 93
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_destinatarios_grupo_descargar_asociados");
        echo "`;
      ApiRestURLS.descargarFormatoProveedores = `";
        // line 94
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_destinatarios_grupo_descargar_proveedores_formato");
        echo "`;
      ApiRestURLS.descargarInformeProveedores = `";
        // line 95
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_destinatarios_grupo_descargar_proveedores");
        echo "`;
      
      ApiRestURLS.procesarArchivoProveedores = `";
        // line 97
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_destinatarios_grupo_cargar_proveedores_upload");
        echo "`;
      ApiRestURLS.previsualizarArchivoProveedores = `";
        // line 98
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_destinatarios_previsualizar_archivo_proveedores");
        echo "`;
      ApiRestURLS.agregarProveedor = `";
        // line 99
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_grupo_destinatarios_proveedores_asignarProveedor", ["idGrupo" => ($context["idGrupo"] ?? null)]), "html", null, true);
        echo "`;

      ApiRestURLS.contactosNew = `";
        // line 101
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_contactos_proveedores_new");
        echo "`;
      ApiRestURLS.contactosEdit = `";
        // line 102
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_contactos_proveedores_edit");
        echo "`;
      ApiRestURLS.contactosDelete = `";
        // line 103
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_contactos_proveedores_delete");
        echo "`;


    </script>
    <script type=\"text/javascript\" src=\"";
        // line 107
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/fileinput/js/fileinput.min.js"), "html", null, true);
        echo "\"></script>
    ";
        // line 108
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("js/grupos/destinatarios");
        echo "
";
    }

    public function getTemplateName()
    {
        return "grupos/destinatarios.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  272 => 108,  268 => 107,  261 => 103,  257 => 102,  253 => 101,  248 => 99,  244 => 98,  240 => 97,  235 => 95,  231 => 94,  227 => 93,  222 => 91,  218 => 90,  214 => 89,  209 => 87,  205 => 86,  201 => 85,  196 => 83,  192 => 82,  188 => 81,  184 => 80,  180 => 79,  177 => 78,  175 => 77,  169 => 75,  165 => 74,  146 => 57,  142 => 56,  138 => 54,  136 => 53,  133 => 52,  129 => 51,  119 => 46,  113 => 42,  110 => 41,  107 => 40,  103 => 39,  71 => 9,  66 => 7,  61 => 6,  57 => 5,  50 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "grupos/destinatarios.html.twig", "/srv/www/correosProduccion/templates/grupos/destinatarios.html.twig");
    }
}
