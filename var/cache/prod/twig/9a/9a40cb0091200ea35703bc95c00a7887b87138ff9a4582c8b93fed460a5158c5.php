<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* envioDestinatario/index.html.twig */
class __TwigTemplate_20e9c5806e776b17743010ebe241c8319ff70ccbaee69fd774e3d49468f19ab6 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'header' => [$this, 'block_header'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "envioDestinatario/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Correos - Destinatarios Envio";
    }

    // line 5
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "  ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
  ";
        // line 7
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("css/grilla");
        echo "
  ";
        // line 9
        echo "
  <style>
    input[type=number]::-webkit-inner-spin-button, 
    input[type=number]::-webkit-outer-spin-button { 
        -webkit-appearance: none; 
        margin: 0;  
    }

    input[type=number] { 
        -moz-appearance:textfield; 
    }
    a.active{
      background-color: #f8f9fa!important;
    }

    .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active, a.ui-button:active, .ui-button:active, .ui-button.ui-state-active:hover{
        background:#02387F !important;
    }
    .ui-tabs .ui-tabs-panel {
        padding: .1em 0em !important;
        
    }
    #tabs{
      height: 100%;
      position: absolute;
      width: 100%;   
    }

  </style>

";
    }

    // line 41
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 42
        echo "  ";
        $this->loadTemplate("app/adminNavbar.html.twig", "envioDestinatario/index.html.twig", 42)->display($context);
        // line 43
        echo "  ";
        $this->loadTemplate("app/menu.html.twig", "envioDestinatario/index.html.twig", 43)->display($context);
        // line 44
        echo "  
<nav aria-label=\"breadcrumb\" style =\"margin-bottom: -1rem; !important; font-size: 13px;\">
  <ol class=\"breadcrumb\">
    <li class=\"breadcrumb-item\"><a href=\"/admin/envios\">Envios </a></li>
    <li class=\"breadcrumb-item\"> ";
        // line 48
        if (($context["proveedor"] ?? null)) {
            echo "Proveedores - Contactos ";
        } else {
            echo "Asociados - Droguerias ";
        }
        echo " </li>
    <li class=\"active\"> / Destinatarios del Envio [";
        // line 49
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["entity"] ?? null), "asunto", [], "any", false, false, false, 49)), "html", null, true);
        echo "]</li>
  </ol>
</nav>

<div class=\"modal fade\" id=\"modalLg\" role=\"dialog\">
  <div class=\"modal-dialog modal-lg modalProveedores\">
    <div class=\"modal-content\">
      <div class=\"text-center modal-header\">
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
        <h4 class=\"modal-title\" id=\"idTituloModalProveedores\"></h4>
      </div>
      <div class=\"modal-body\" id=\"idBodyModalProveedores\">

      </div>
    </div>
  </div>
</div>


<div class=\"modal fade\" id=\"modalLgAsociados\" role=\"dialog\">
  <div class=\"modal-dialog modal-lg modalAsociados\">
    <div class=\"modal-content\">
      <div class=\"text-center modal-header\">
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
        <h4 class=\"modal-title\" id=\"idTituloModalAsociados\"></h4>
      </div>
      <div class=\"modal-body\" id=\"idBodyModalAsociados\">

      </div>
    </div>
  </div>
</div>

";
    }

    // line 84
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 85
        echo "
";
        // line 86
        if (($context["proveedor"] ?? null)) {
            // line 87
            echo "  <div id=\"myGridProveedores\" style=\"height: 100%;\" class=\"ag-theme-fresh\"></div>
";
        } else {
            // line 89
            echo "  <div id=\"myGridAsociados\" style=\"height: 100%;\" class=\"ag-theme-fresh\"></div>
";
        }
        // line 91
        echo "


";
    }

    // line 96
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 97
        echo "  ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>

        var oGridAsociados;
        var oGrid;
        var oGridProveedores;
        var aDfColumnasAsociado = ";
        // line 103
        echo ($context["dfColumnasAsociado"] ?? null);
        echo ";
        var aDfColumnasProveedor = ";
        // line 104
        echo ($context["dfColumnasProveedor"] ?? null);
        echo ";
        var idEnvio = ";
        // line 105
        echo twig_escape_filter($this->env, ($context["envioId"] ?? null), "html", null, true);
        echo ";
        var tipoGrupo = ";
        // line 106
        echo twig_escape_filter($this->env, ($context["tipoGrupo"] ?? null), "html", null, true);
        echo ";

        ";
        // line 108
        if (($context["proveedor"] ?? null)) {
            // line 109
            echo "          ";
            // line 110
            echo "          ApiRestURLS.listDestinatariosProveedor = `";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_envio_destinatario_proveedor_json");
            echo "`;
          ApiRestURLS.agregarProveedor = `";
            // line 111
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_envio_destinatario_proveedor_agregar");
            echo "`;
          ApiRestURLS.eliminarProveedor = `";
            // line 112
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_envio_destinatario_proveedor_eliminar");
            echo "`;
          ApiRestURLS.exportarReporteProveedor = `";
            // line 113
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_envio_destinatario_proveedor_xls_leidos");
            echo "`;
        ";
        } else {
            // line 115
            echo "          ";
            // line 116
            echo "          ApiRestURLS.listDestinatariosAsociados = `";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_envio_destinatario_asociado_json");
            echo "`;
          ApiRestURLS.agregarAsociado = `";
            // line 117
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_envio_destinatario_asociado_agregar");
            echo "`;
          ApiRestURLS.eliminarAsociado = `";
            // line 118
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_envio_destinatario_asociado_eliminar");
            echo "`;
          ApiRestURLS.exportarReporteAsociado = `";
            // line 119
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_envio_destinatario_proveedor_xls_leidos");
            echo "`;
        ";
        }
        // line 121
        echo "

        ApiRestURLS.reenviarCorreo = `";
        // line 123
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_envio_destinatario_reenviar");
        echo "`;


    </script>

    ";
        // line 128
        if (($context["proveedor"] ?? null)) {
            // line 129
            echo "      ";
            echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("js/destinatariosProveedores");
            echo "
    ";
        } else {
            // line 131
            echo "      ";
            echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("js/destinatariosAsociados");
            echo "
    ";
        }
        // line 133
        echo "  
";
    }

    public function getTemplateName()
    {
        return "envioDestinatario/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  289 => 133,  283 => 131,  277 => 129,  275 => 128,  267 => 123,  263 => 121,  258 => 119,  254 => 118,  250 => 117,  245 => 116,  243 => 115,  238 => 113,  234 => 112,  230 => 111,  225 => 110,  223 => 109,  221 => 108,  216 => 106,  212 => 105,  208 => 104,  204 => 103,  194 => 97,  190 => 96,  183 => 91,  179 => 89,  175 => 87,  173 => 86,  170 => 85,  166 => 84,  128 => 49,  120 => 48,  114 => 44,  111 => 43,  108 => 42,  104 => 41,  70 => 9,  66 => 7,  61 => 6,  57 => 5,  50 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "envioDestinatario/index.html.twig", "/srv/www/correosProduccion/templates/envioDestinatario/index.html.twig");
    }
}
