<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* grupos/mdlCrud.html.twig */
class __TwigTemplate_84b96b0848daffb4052e9491c749e32afd9317f6c5813f700b2956cc414a6b48 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((array_key_exists("frm_msj", $context) && (0 !== twig_compare(($context["frm_msj"] ?? null), null)))) {
            // line 2
            echo "    ";
            echo twig_escape_filter($this->env, ($context["frm_msj"] ?? null), "html", null, true);
            echo "
";
        }
        // line 4
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
        echo "

<form id=\"formGrupo\" autocomplete=\"off\">
   
   <div class=\"row\">

        ";
        // line 10
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "id", [], "any", false, false, false, 10), 'widget');
        echo "
      
      <div class=\"col-12 col-md-6\">
         <div class=\"form-group font-weight-bold\">
            ";
        // line 14
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "nombre", [], "any", false, false, false, 14), 'label');
        echo "&nbsp;<i class=\"fa fa-asterisk text-danger\" aria-hidden=\"true\"></i> :
            ";
        // line 15
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "nombre", [], "any", false, false, false, 15), 'widget');
        echo "
         </div>
      </div>
      
      <div class=\"col-12 col-md-6\">
         <div class=\"form-group font-weight-bold\">
            ";
        // line 21
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "clasificacion", [], "any", false, false, false, 21), 'label');
        echo "&nbsp;<i class=\"fa fa-asterisk text-danger\" aria-hidden=\"true\"></i> :
            ";
        // line 22
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "clasificacion", [], "any", false, false, false, 22), 'widget');
        echo "
         </div>
      </div>

      <div class=\"col-12 col-md-6\">
         <div class=\"form-group font-weight-bold\">
            ";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "descripcion", [], "any", false, false, false, 28), 'label');
        echo "&nbsp;<i class=\"fa fa-asterisk text-danger\" aria-hidden=\"true\"></i> :
            ";
        // line 29
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "descripcion", [], "any", false, false, false, 29), 'widget');
        echo "
         </div>
      </div>
     
      <div class=\"col-12 text-center\">
         <div class=\"form-group font-weight-bold m-2 border-top pt-3\">
            ";
        // line 35
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "save", [], "any", false, false, false, 35), 'widget', ["label" => "Guardar"]);
        echo "
         </div>
      </div>
   </div>

</form>

<script type=\"text/javascript\">

</script>";
    }

    public function getTemplateName()
    {
        return "grupos/mdlCrud.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 35,  91 => 29,  87 => 28,  78 => 22,  74 => 21,  65 => 15,  61 => 14,  54 => 10,  45 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "grupos/mdlCrud.html.twig", "/srv/www/correosProduccion/templates/grupos/mdlCrud.html.twig");
    }
}
